<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information
	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	 
	if (true)  { // Change this to a check for bidi languages 
	%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>
<HEAD>
<title><%= msgs.get("KEY_OVERVIEW") %></title>
<META name="GENERATOR" content="IBM Rational Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
<STYLE TYPE="text/css">
TEXTAREA {
	border-style: none;
	scrollbar-face-color: #CCCCCC;
	scrollbar-shadow-color: #FFFFFF;
	scrollbar-highlight-color: #FFFFFF;
	scrollbar-3dlight-color: #6B7A92;
	scrollbar-darkshadow-color: #6B7A92;
	scrollbar-track-color: #E2E2E2;
	scrollbar-arrow-color: #6B7A92
}
</STYLE>
</HEAD>
<body>
<admin:UserMessage />
<admin:Scope />
<table border="0" cellpadding="20" cellspacing="0" width="95%">
	<tr>
		<td width="50%" valign="top">
		<table border="0" cellpadding="7" cellspacing="0" width="100%">
			<tr>
				<td class="nolines"><img src="images/ibm_com_link.gif"
					alt="" align="left"> <a
					href="http://www.ibm.com/software/awdtools/hats/" target="_blank">
				<span class="bluebold"><%= msgs.get("KEY_HATS_ON_IBM_COM") %></span> </a></td>
			</tr>
		</table>

		<table border="0" cellpadding="5" cellspacing="0" width="100%">
			<tr>
				<td class="linesmost"><span class="desctext"><%= msgs.get("KEY_LINK_TO_HATS_ON_IBMCOM") %></span>
				</td>
			</tr>
		</table>
		</td>
		<td width="50%" valign="top">
		<table border="0" cellpadding="7" cellspacing="0" width="100%">
			<tr>
				<td class="nolines"><img src="images/about.gif" alt=""
					align="left"><label for="input1"> <span class="purplebold"><%= msgs.get("KEY_ABOUT")%>
				</span><span class="graytext"><%= msgs.get("KEY_YOUR_HATS") %> </span></label>
				</td>
			</tr>
		</table>

		<table border="0" cellpadding="5" cellspacing="0" width="100%">
			<tr>
				<td class="linesmost">				
				<form><TEXTAREA id="input1" name="abouttext" rows="6" cols="40" readonly>
<%  if (bean.getScopeManager().isManagingAllApplications() 
		|| bean.getScopeManager().isManagingClusteredApplication()) {
		out.println(msgs.get("KEY_BUILD_INFO_REQUIRES_JVM_SCOPE"));
	} else {   
	String[] buildInfo = bean.getBuildInfo();
    if (buildInfo!=null) {
   		out.println(buildInfo[1]);
   		out.println(msgs.get("KEY_BUILD_LEVEL") + "  " + buildInfo[0]);
    } else
    	out.println(msgs.get("KEY_NOTAVAIL")); 
    }%> 
</TEXTAREA></form>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td width="50%" valign="top">
		<table border="0" cellpadding="7" cellspacing="0" width="100%">
			<tr>
				<td class="nolines"><img src="images/dev_domain.gif"
					alt="" align="left"><a href="news://news.software.ibm.com/ibm.software.websphere.hats" > <span class="bluebold"><%= msgs.get("KEY_HATS_FORUMS") %>
				</span> </a> </td>
			</tr>
		</table>

		<table border="0" cellpadding="5" cellspacing="0" width="100%">
			<tr>
				<td class="linesmost"><span class="desctext"><%= msgs.get("KEY_HATS_FORUMS_DESC") %></span>
				</td>
			</tr>
		</table>
		</td>
		<td width="50%" valign="top">
		<table border="0" cellpadding="7" cellspacing="0" width="100%">
			<tr>
				<td class="nolines"><img src="images/infocenter.gif"
					alt="" align="left"> <a
					href="http://publib.boulder.ibm.com/infocenter/hatshelp/v71/index.jsp"
					target="_blank"> <span class="bluebold"><%= msgs.get("KEY_INFOCENTER")%></span>
				</a></td>
			</tr>
		</table>

		<table border="0" cellpadding="5" cellspacing="0" width="100%">
			<tr>
				<td class="linesmost"><span class="desctext"><%=msgs.get("KEY_INFOCENTER_DESC")%></span>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</body>
</html>

