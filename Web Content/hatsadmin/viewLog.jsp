<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminTroubleManager manager = bean.getTroubleManager(); // ScopeManager contains all the logic for scope management
	
	String url = response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION
	                                   + "=" + HATSAdminConstants.FUNCTION_VIEW_LOG
                                       + "&" + HATSAdminConstants.PARAM_OPERATION
                                       + "=" + HATSAdminConstants.OPERATION_DOWNLOAD_LOG);

	String downloadLink = "<a href=\"" + url + "\">" + msgs.get("KEY_DOWNLOAD") + "</a>";
	
	if (true)  { // Change this to a check for bidi languages 
	%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>
<HEAD>
<title><%= msgs.get("KEY_VIEW_LOG") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>
<script language="javascript1.2">
<!--Insert the JavaScript here if any that is required for form validation/processing-->
</script>


<admin:UserMessage />

<% if (!manager.isLogInfoNull()) { %>

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4 >
	<tr class="header">
		<td><%= msgs.get("KEY_VIEW_LOG") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_VIEW_LOG") %>  <p>
		<%= msgs.get("KEY_FILEDOWNLOAD",downloadLink) %> <admin:HelpLink topicID="VIEW_LOG" /> </td>
	</tr>
</table>
<!--End of the instructions table-->



<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>
		<admin:Scope />
	<% if (bean.isOperator() || bean.isAdministrator() ) { %>
	<table width=100% cellpadding=7>
			<!-- start of top-buttons/refresh link table -->
			<tr>				
				<td>
                                    <form method="post" action="<%= response.encodeURL("admin")%>"><input type="hidden"
                                            name="<%=HATSAdminConstants.PARAM_FUNCTION%>"
                                            value="<%=HATSAdminConstants.FUNCTION_VIEW_LOG%>"> 

                                        <input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.OPERATION_CLEAR_LOG_FILE%>" value="<%=msgs.get("KEY_CLEAR_LOG_FILE")%>"> 
                                    </form>
                                </td>
			</tr>
		</table>
	<% } %>

<form method="post" action="<%= response.encodeURL("admin")%>">
                   <input type="hidden"
                           name="<%=HATSAdminConstants.PARAM_FUNCTION%>"
                          value="<%=HATSAdminConstants.FUNCTION_VIEW_LOG%>"> 
		<table width=100% cellpadding=7>
        	<!-- start of top-buttons/refresh link table -->

			<tr>
       				    <td>
					<%= msgs.get("KEY_TOTAL") + ": " + manager.getNumTotalLines() %><br>
					<% if (manager.getNumTotalLines() > 0) 
							out.println(msgs.get("KEY_DISPLAYED") + ": " + (manager.getStartingLineNumber()+1) + " - " + (manager.getEndingLineNumber()+1)); %>
                                    </td>
			<tr>
				<td> <label for="input1" ><%=msgs.get("KEY_RETRIEVE_LINES") %> </label>
					 <input id="input1" type="text" name="<%=HATSAdminConstants.PARAM_RANGE%>">
					 <input class="button" type="submit" value="<%=msgs.get("KEY_REFRESH_2")%>">
					 </td>
			

			</tr>
                   

		</table>
</form>		
		<!-- end of top-buttons/refresh link table -->
		
		
		<table class="data-table" width="100%" cellspacing="1" cellpadding="4" border="1"  summary="<%= msgs.get("KEY_INSTRUCTIONS_VIEW_LOG") %> ">
			<caption><%= msgs.get("KEY_VIEW_LOG") %></caption>
			<!-- start of data table -->
			<tr class="columnheader">
				<!-- Row for all the column headers -->
<td>
	<%= manager.getLogFileName(true) %>
	</td>
			

			</tr>
			<!-- end of the column-header row -->

			<!-- Start of the data in the data table -->
			<tr class="data-table-row">
		<td>
		<pre>
		<% String[] logLines=manager.getLogFileAsStringArray();
			for (int i=0;i<logLines.length;i++) {
			 out.println(logLines[i]);
			 }
		
		%>
		</pre>
		</td>
	</tr>


			<!-- end of data in the data table -->
		</table>
		<!-- end of the data table --> <!-- No need for the footer table on this page  so comment it out-->
	
		</td>
	</tr>
</table> <!-- End of the layout manager table-->
<% } %>

</body>
</html>



