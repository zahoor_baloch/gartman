<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*, java.text.DateFormat, com.ibm.hats.runtime.connmgr.HodTraceInfo"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
        // Get the msgs object for generating messages and get the bean from the session
        // for obtaining all the required information
        
        HATSAdminBean bean=null;
        HatsMsgs msgs = HATSAdminServlet.getMessages(request);
        bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
        HATSAdminTroubleManager manager = bean.getTroubleManager();
        HodTraceInfo hti = manager.getHodTraceInfo();
        
        String url = response.encodeURL( "admin?" + HATSAdminConstants.PARAM_FUNCTION
                                            + "=" + HATSAdminConstants.FUNCTION_SET_TRACE_OPTIONS
                                                                + "&" + HATSAdminConstants.PARAM_OPERATION
                                                                + "=" + HATSAdminConstants.OPERATION_DOWNLOAD_TRACE );
                                                                
        String downloadLink = "<a href=\"" + url + "\">" + msgs.get("KEY_DOWNLOAD") + "</a>";
        
        String downloadString = bean.startWithUpperCase(msgs.get("KEY_DOWNLOAD"));
        boolean enabledFlag = (bean.isMonitor() ? false: true );
        boolean displayTerminalAllowed = (bean.isAdministrator() ? true: false );
        boolean enableSimulationRecordingAllowed = (bean.isAdministrator() ? true: false );                     
        if (true)  { // Change this to a check for bidi languages 
        %>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>
<HEAD>
<title><%= msgs.get("KEY_SET_TRACE_OPTIONS") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
<script language="javascript1.2">
<!-- Hiding JavaScript    

var showServiceTraceOptions=<%= manager.getServiceTracingFlag()%>;

// This function toggles the display state
function toggleServiceOptionsVisibility()
{
        if (showServiceTraceOptions == true)
         {
                hideServiceOptions();
        } 
        else 
        {
        showServiceOptions();
        }
}

// This function sets the display state
function setServiceOptionsVisibility()
{
        if ( showServiceTraceOptions == false )
    {
        hideServiceOptions();
        } 
        else 
        {
                showServiceOptions();
        }
}

// This function sets two service option checkboxes based upon the server tracing flag setting.
function setRuntimeServiceOptions()
{
    if ( document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_SERVER_TRACING%>" ).checked == true )
        {
        document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_ENABLE_SERVICE_TRACES %>" ).checked = true;
        document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_PSEVENT_TRACING%>" ).checked = true;
                document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_OIAEVENT_TRACING%>" ).checked = true;
        showServiceOptions();
        }
        else
        {
        document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_PSEVENT_TRACING%>" ).checked = false;
        document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_OIAEVENT_TRACING%>" ).checked = false;
        
            if ( ( document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_PS_TRACING%>" ).value == "0" )
                 &&
                 ( document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_DS_TRACING%>" ).value == "0" )
             &&
             ( document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_TRANSPORT_TRACING%>" ).value == "0" )
             &&
             ( document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_MACRO_TRACING%>" ).value == "0" )
             &&
             ( document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_SESSION_TRACING%>" ).value == "0" )
             &&      
             ( document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_HOD_COMMEVENT_TRACING%>" ).checked == false )
           )
        {
            document.getElementById( "<%=HATSAdminConstants.PARAM_TRACE_ENABLE_SERVICE_TRACES %>" ).checked = false;
            hideServiceOptions();
        }
        }
}

// Enables the display of the service trace options
function showServiceOptions() {
        document.getElementById("serviceTracingOptions").style.display="block";
        showServiceTraceOptions = true;
}

// Disables the display of the service trace options
function hideServiceOptions() {
    document.getElementById("serviceTracingOptions").style.display="none";
    showServiceTraceOptions = false;
}

function showIndividualServerTracingOptions() {
        document.getElementById('individualServerTracingOptions').style.display="block";
}

function hideIndividualServerTracingOptions() {
        document.getElementById('individualServerTracingOptions').style.display="none";
}

function showTracePattern() {
        document.forms[0].<%=HATSAdminConstants.PARAM_TRACE_IO_CLASSNAME%>.disabled=false;
}

function hideTracePattern() {
        document.forms[0].<%=HATSAdminConstants.PARAM_TRACE_IO_CLASSNAME%>.disabled=true;
}

function toggleTracePatternVisibility() {
        if (document.forms[0].<%=HATSAdminConstants.PARAM_TRACE_IO_TRACING%>.checked) {
                showTracePattern();
        } else {
                hideTracePattern();
        }
}

var showSimulatorOptions=<%= manager.isRecordSimulationTrace()%> ;

function toggleSimulatorOptionsVisibility() {
        if (showSimulatorOptions == true) {
                document.getElementById('simulatorOptions').style.display="none";
                showSimulatorOptions=false;
        } else {
                document.getElementById('simulatorOptions').style.display="block";
                showSimulatorOptions=true;
        }
}

function hideSimulatorOptions() {
        document.getElementById('simulatorOptions').style.display="none";
}

// Done hiding --> 

</script>
</HEAD>

<body>

<form method="post" action="<%= response.encodeURL("admin")%>"><input type="hidden"
        name="<%=HATSAdminConstants.PARAM_FUNCTION%>"
        value="<%=HATSAdminConstants.FUNCTION_SET_TRACE_OPTIONS%>"> 
<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4 >
        <tr class="header">
                <td><%= msgs.get("KEY_SET_TRACE_OPTIONS") %></td>
        </tr>
        <tr>
                <td class="instructions">
                <%= msgs.get("KEY_INSTRUCTIONS_SET_TRACE_OPTIONS") %>  
                <%= bean.isOperator() || bean.isAdministrator() ? msgs.get("KEY_PRESS_OK") : "" %>
                <% if (!Util.isZosPlatform()) {
                      out.println(bean.isAdministrator() ? msgs.get("KEY_TRACE_FILEDOWNLOAD",downloadLink) : "") ; 
                 }%> 
                </td>
        </tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
        width=95%>
        <tr>
                <td>
                  <admin:Scope />
        <% if (bean.isOperator() || bean.isAdministrator() ) { %>
        <table width=100% cellpadding=7>
                        <!-- start of top-buttons/refresh link table -->
                        <tr>                            
                                <td><input class="button" type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CLEAR_TRACE_FILE%>" value="<%=msgs.get("KEY_CLEAR_TRACE_FILE")%>">
                                <% if (Util.isZosPlatform() && bean.isAdministrator()) { %>
                                        <input class="button" type="SUBMIT"     name="<%=HATSAdminConstants.OPERATION_DOWNLOAD_TRACE%>" value="<%=downloadString%>"> </td>
                                <% } %>
                        
                        </tr>
                </table>
        <% } %>
        <!-- end of top-buttons/refresh link table -->
        <!------------------------------------------------------------>

        <table class="input-table" border="1" cellpadding="2" cellspacing="1"
                width="100%">
                 <caption><%= msgs.get("KEY_TRACE_SETTINGS") %></caption>
                <tr class="header">
                        <td colspan="3"></td>
                </tr>
                <tr class="input-table-text" >
                        <td width="30%"><%=msgs.get("KEY_SERVER_TRACING")%></td>
                        <td nowrap>
                                <div id="individualServerTracingOptions">
                                <fieldset>
                        		<legend><b><%=msgs.get("KEY_SERVER_OPTIONS")%></b></legend>                        
            					<table border=0 cellpadding=2 cellspacing=5>
              					<tr>
              					<td>
                                &nbsp;&nbsp;<input type="checkbox" id="<%=HATSAdminConstants.PARAM_TRACE_SERVER_TRACING%>" name="<%=HATSAdminConstants.PARAM_TRACE_SERVER_TRACING%>" <%= manager.getServerTracingFlag() ? "checked" : "" %> onclick='return setRuntimeServiceOptions();' <%= enabledFlag ? "" : "disabled" %>><label for="<%=HATSAdminConstants.PARAM_TRACE_SERVER_TRACING%>"><%= msgs.get("KEY_ENABLE_SERVER_TRACING") %></label><BR>
                                &nbsp;&nbsp;<input type="checkbox" id="<%=HATSAdminConstants.PARAM_TRACE_SERVER_WIDGET_TRACING%>" name="<%=HATSAdminConstants.PARAM_TRACE_SERVER_WIDGET_TRACING%>" <%= manager.getWidgetTracingFlag() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>><label for="<%=HATSAdminConstants.PARAM_TRACE_SERVER_WIDGET_TRACING%>"><%= msgs.get("KEY_ENABLE_WIDGET_TRACING") %></label><BR>
                                &nbsp;&nbsp;<input type="checkbox" id="<%=HATSAdminConstants.PARAM_TRACE_SERVER_ACTION_TRACING%>" name="<%=HATSAdminConstants.PARAM_TRACE_SERVER_ACTION_TRACING%>" <%= manager.getActionTracingFlag() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>><label for="<%=HATSAdminConstants.PARAM_TRACE_SERVER_ACTION_TRACING%>"><%= msgs.get("KEY_ENABLE_ACTION_TRACING") %></label><BR>
                                &nbsp;&nbsp;<input type="checkbox" id="<%=HATSAdminConstants.PARAM_TRACE_SERVER_COMPONENT_TRACING%>" name="<%=HATSAdminConstants.PARAM_TRACE_SERVER_COMPONENT_TRACING%>" <%= manager.getComponentTracingFlag() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>><label for="<%=HATSAdminConstants.PARAM_TRACE_SERVER_COMPONENT_TRACING%>"><%= msgs.get("KEY_ENABLE_COMPONENT_TRACING") %></label><BR>
                                &nbsp;&nbsp;<input type="checkbox" id="<%=HATSAdminConstants.PARAM_TRACE_SERVER_UTIL_TRACING%>" name="<%=HATSAdminConstants.PARAM_TRACE_SERVER_UTIL_TRACING%>" <%= manager.getUtilTracingFlag() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>><label for="<%=HATSAdminConstants.PARAM_TRACE_SERVER_UTIL_TRACING%>"><%= msgs.get("KEY_ENABLE_UTIL_TRACING") %></label><BR>
                                </td>
                                </tr>
                                </table>
                                </fieldset>
                                </div>
                        </td>
                        
                        <td> <admin:HelpLink topicID="SERVER_TRACING" /> <%= msgs.get("KEY_ENABLE_SERVER_TRACING_DESC") %></td>
                </tr>
                <tr class="input-table-text">
                        <td><%=msgs.get("KEY_IO_TRACING")%></td>
                        <td nowrap> <input id="<%=HATSAdminConstants.PARAM_TRACE_IO_TRACING%>" type="checkbox" name="<%=HATSAdminConstants.PARAM_TRACE_IO_TRACING%>" onclick='return toggleTracePatternVisibility();' <%= manager.getIOTracingFlag() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>> <label for="<%=HATSAdminConstants.PARAM_TRACE_IO_TRACING%>" ><%= msgs.get("KEY_ENABLE_IO_TRACING")%> </label>
                        <div id="tracePattern">
                        <p> <label for="<%=HATSAdminConstants.PARAM_TRACE_IO_CLASSNAME%>" ><%=msgs.get("KEY_CLASSNAME")%></label>&nbsp;<input id="<%=HATSAdminConstants.PARAM_TRACE_IO_CLASSNAME%>" type="text" name="<%=HATSAdminConstants.PARAM_TRACE_IO_CLASSNAME%>" value="<%=manager.getIOTracePattern()%>" <%= enabledFlag ? "" : "disabled" %>> </div></td>
                        
                        <td> <admin:HelpLink topicID="IO_TRACING" /> <%= msgs.get("KEY_ENABLE_IO_TRACING_DESC") %> </td>
                </tr>
                <% if (!manager.getIOTracingFlag()) { %>
                                <SCRIPT language="JavaScript"> hideTracePattern(); </SCRIPT>
                <% } %>
                <tr class="input-table-text">
                        <td><%=msgs.get("KEY_APPLET_TRACING")%></td>
                        <td> <SELECT id="<%=HATSAdminConstants.PARAM_TRACE_APPLET_TRACING%>"
                                        name="<%=HATSAdminConstants.PARAM_TRACE_APPLET_TRACING%>" <%= enabledFlag ? "" : "disabled" %>>
                                        <OPTION value="0" <%= manager.getAppletTraceLevel() == 0 ? "selected" : "" %> > <%=msgs.get("KEY_NONE")%></OPTION>
                                        <OPTION value="3" <%= manager.getAppletTraceLevel() == 3 ? "selected" : "" %>> <%=msgs.get("KEY_MINIMUM")%></OPTION>
                                        <OPTION value="5" <%= manager.getAppletTraceLevel() == 5 ? "selected" : "" %>> <%=msgs.get("KEY_NORMAL")%></OPTION>

                                </SELECT>
                                <label for="<%=HATSAdminConstants.PARAM_TRACE_APPLET_TRACING%>" ><%= msgs.get("KEY_ENABLE_APPLET_TRACING")%> </label>
                        </td>
                        <td> <admin:HelpLink topicID="APPLET_TRACING" /> <%= msgs.get("KEY_ENABLE_APPLET_TRACING_DESC") %> </td>
                </tr>
                <% if (!manager.getIOTracingFlag()) { %>
                                <SCRIPT language="JavaScript"> hideTracePattern(); </SCRIPT>
                <% } %>
                
                <tr class="input-table-text">
                        <td><%=msgs.get("KEY_USER_MACRO_TRACING")%></td>
                        <td><SELECT id="<%=HATSAdminConstants.PARAM_TRACE_HOD_USER_MACRO_TRACING%>"
                                        name="<%=HATSAdminConstants.PARAM_TRACE_HOD_USER_MACRO_TRACING%>" <%= enabledFlag ? "" : "disabled" %>>
                                        <OPTION value="0" <%= hti.getUserMacroTracingLevel() == 0 ? "selected" : "" %> > <%=msgs.get("KEY_NONE")%></OPTION>
                                        <OPTION value="1" <%= hti.getUserMacroTracingLevel() == 1 ? "selected" : "" %>> <%=msgs.get("KEY_MINIMUM")%></OPTION>
                                        <OPTION value="2" <%= hti.getUserMacroTracingLevel() == 2 ? "selected" : "" %>> <%=msgs.get("KEY_NORMAL")%></OPTION>
                                        <OPTION value="3" <%= hti.getUserMacroTracingLevel() == 3 ? "selected" : "" %>> <%=msgs.get("KEY_MAXIMUM")%></OPTION>
                                </SELECT> <label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_USER_MACRO_TRACING%>"> <%=msgs.get("KEY_USER_MACRO_TRACING")%></label></td>
                                
                        <td> <admin:HelpLink topicID="USER_MACRO_TRACING" /> <%= msgs.get("KEY_ENABLE_HOD_USER_MACRO_TRACING_DESC") %> </td>
                </tr>
                <tr class="input-table-text">
                        <td> <%=msgs.get("KEY_DISPLAY_TERMINAL")%></td>
                        <td><input id="<%=HATSAdminConstants.PARAM_TRACE_DISPLAY_TERMINAL%>" type="checkbox" name="<%=HATSAdminConstants.PARAM_TRACE_DISPLAY_TERMINAL%>"<%= hti.isDisplayTerminal() ? "checked" : "" %> <%= displayTerminalAllowed ? "" : "disabled" %>> <label for="<%=HATSAdminConstants.PARAM_TRACE_DISPLAY_TERMINAL%>"><%= msgs.get("KEY_ENABLE_DISPLAY_TERMINAL")%></label> </td>
                        <td> <admin:HelpLink topicID="DISPLAY_TERMINAL_TRACING" /> <%= msgs.get("KEY_ENABLE_DISPLAY_TERMINAL_DESC") %></td>
                </tr> 
                <tr class="input-table-text">
                        <td><%=msgs.get("KEY_SIMULATION_RECORDING")%></td>
                        <td><input id="<%=HATSAdminConstants.PARAM_TRACE_RECORD_SIMULATION_TRACE%>" type="checkbox" name="<%=HATSAdminConstants.PARAM_TRACE_RECORD_SIMULATION_TRACE%>"<%= manager.isRecordSimulationTrace() ? "checked" : "" %> onclick="return toggleSimulatorOptionsVisibility();" <%= enableSimulationRecordingAllowed ? "" : "disabled" %>> <label for="<%=HATSAdminConstants.PARAM_TRACE_RECORD_SIMULATION_TRACE%>"><%= msgs.get("KEY_ENABLE_SIMULATION_RECORDING")%></label>
                           <p><div id="simulatorOptions">                       
                                <label> <%= msgs.get("KEY_ENABLE_SIMULATION_RECORDING_PORT_RANGE") %></label>
                                <br> &nbsp;<input id="<%=HATSAdminConstants.PARAM_TRACE_START_PORT%>" type="text" name="<%=HATSAdminConstants.PARAM_TRACE_START_PORT%>" value="<%= manager.getSimulatorStartPort() %>" <%= enabledFlag ? "" : "disabled" %>> <label> ~ </label>
                                <br> &nbsp;<input id="<%=HATSAdminConstants.PARAM_TRACE_END_PORT%>" type="text" name="<%=HATSAdminConstants.PARAM_TRACE_END_PORT%>" value="<%= manager.getSimulatorEndPort() %>" <%= enabledFlag ? "" : "disabled" %>> 
                              </div>
                           </td>
                        <td> <admin:HelpLink topicID="ENABLE_SIMULATION_RECORDING" /> <%= msgs.get("KEY_ENABLE_SIMULATION_RECORDING_DESC") %></td>
                        <% if (! manager.isRecordSimulationTrace()) { %>
                           <SCRIPT language="JavaScript"> hideSimulatorOptions(); </SCRIPT>
                        <% } %>                 
                </tr>                           
                <tr class="input-table-text">
                        <td><%=msgs.get("KEY_IBM_SERVICE_TRACE_OPTIONS")%></td>
                        <td><input id="<%=HATSAdminConstants.PARAM_TRACE_ENABLE_SERVICE_TRACES %>" type="checkbox" name="<%=HATSAdminConstants.PARAM_TRACE_ENABLE_SERVICE_TRACES %>" <%= manager.getServiceTracingFlag() ? "checked" : "" %> onclick="return toggleServiceOptionsVisibility();" <%= enabledFlag ? "" : "disabled" %>> <label for="<%=HATSAdminConstants.PARAM_TRACE_ENABLE_SERVICE_TRACES %>">  <%=msgs.get("KEY_ENABLE_SERVICE_TRACING")%> </label>
                           <p><div id="serviceTracingOptions" style="display:none" >
                           <SELECT id="<%=HATSAdminConstants.PARAM_TRACE_HOD_PS_TRACING%>"
                                name="<%=HATSAdminConstants.PARAM_TRACE_HOD_PS_TRACING%>" <%= enabledFlag ? "" : "disabled" %>>
                                        <OPTION value="0" <%= hti.getPsTracingLevel() == 0 ? "selected" : "" %>> <%=msgs.get("KEY_NONE")%> </OPTION>
                                        <OPTION value="1" <%= hti.getPsTracingLevel() == 1 ? "selected" : "" %>> <%=msgs.get("KEY_MINIMUM")%></OPTION>
                                        <OPTION value="2" <%= hti.getPsTracingLevel() == 2 ? "selected" : "" %>> <%=msgs.get("KEY_NORMAL")%></OPTION>
                                        <OPTION value="3" <%= hti.getPsTracingLevel() == 3 ? "selected" : "" %>> <%=msgs.get("KEY_MAXIMUM")%></OPTION>
                                </SELECT> <label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_PS_TRACING%>"><%=msgs.get("KEY_HOD_PS_TRACING")%></label>
                                <p> <SELECT id="<%=HATSAdminConstants.PARAM_TRACE_HOD_DS_TRACING%>" 
                                        name="<%=HATSAdminConstants.PARAM_TRACE_HOD_DS_TRACING%>" <%= enabledFlag ? "" : "disabled" %>>
                                        <OPTION value="0" <%= hti.getDsTracingLevel() == 0 ? "selected" : "" %>> <%=msgs.get("KEY_NONE")%></OPTION>
                                        <OPTION value="1" <%= hti.getDsTracingLevel() == 1 ? "selected" : "" %>> <%=msgs.get("KEY_MINIMUM")%></OPTION>
                                        <OPTION value="2" <%= hti.getDsTracingLevel() == 2 ? "selected" : "" %>> <%=msgs.get("KEY_NORMAL")%></OPTION>
                                        <OPTION value="3" <%= hti.getDsTracingLevel() == 3 ? "selected" : "" %>> <%=msgs.get("KEY_MAXIMUM")%></OPTION>
                                </SELECT> <label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_DS_TRACING%>"> <%=msgs.get("KEY_HOD_DS_TRACING")%></label>
                                <p><SELECT id="<%=HATSAdminConstants.PARAM_TRACE_HOD_TRANSPORT_TRACING%>"
                                        name="<%=HATSAdminConstants.PARAM_TRACE_HOD_TRANSPORT_TRACING%>" <%= enabledFlag ? "" : "disabled" %>>
                                        <OPTION value="0" <%= hti.getTransportTracingLevel() == 0 ? "selected" : "" %>> <%=msgs.get("KEY_NONE")%></OPTION>
                                        <OPTION value="1" <%= hti.getTransportTracingLevel() == 1 ? "selected" : "" %>> <%=msgs.get("KEY_MINIMUM")%></OPTION>
                                        <OPTION value="2" <%= hti.getTransportTracingLevel() == 2 ? "selected" : "" %>> <%=msgs.get("KEY_NORMAL")%></OPTION>
                                        <OPTION value="3" <%= hti.getTransportTracingLevel() == 3 ? "selected" : "" %>> <%=msgs.get("KEY_MAXIMUM")%></OPTION>
                                </SELECT> <label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_TRANSPORT_TRACING%>"> <%=msgs.get("KEY_HOD_TRANSPORT_TRACING")%></label>
                                <p><SELECT id="<%=HATSAdminConstants.PARAM_TRACE_HOD_MACRO_TRACING%>"
                                        name="<%=HATSAdminConstants.PARAM_TRACE_HOD_MACRO_TRACING%>" <%= enabledFlag ? "" : "disabled" %>>
                                        <OPTION value="0" <%= hti.getMacroTracingLevel() == 0 ? "selected" : "" %>> <%=msgs.get("KEY_NONE")%></OPTION>
                                        <OPTION value="1" <%= hti.getMacroTracingLevel() == 1 ? "selected" : "" %>> <%=msgs.get("KEY_EVENT")%></OPTION>
                                        <OPTION value="2" <%= hti.getMacroTracingLevel() == 2 ? "selected" : "" %>> <%=msgs.get("KEY_SUPPORT")%></OPTION>
                                </SELECT><label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_MACRO_TRACING%>"> <%=msgs.get("KEY_HOD_MACRO_TRACING")%></label>
                                <p><SELECT id="<%=HATSAdminConstants.PARAM_TRACE_HOD_SESSION_TRACING%>"
                                        name="<%=HATSAdminConstants.PARAM_TRACE_HOD_SESSION_TRACING%>" <%= enabledFlag ? "" : "disabled" %>>
                                        <OPTION value="0" <%= hti.getSessionTracingLevel() == 0 ? "selected" : "" %>> <%=msgs.get("KEY_NONE")%></OPTION>
                                        <OPTION value="1" <%= hti.getSessionTracingLevel() == 1 ? "selected" : "" %>> <%=msgs.get("KEY_MINIMUM")%></OPTION>
                                        <OPTION value="2" <%= hti.getSessionTracingLevel() == 2 ? "selected" : "" %>> <%=msgs.get("KEY_NORMAL")%></OPTION>
                                        <OPTION value="3" <%= hti.getSessionTracingLevel() == 3 ? "selected" : "" %>> <%=msgs.get("KEY_MAXIMUM")%></OPTION>
                                </SELECT><label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_SESSION_TRACING%>"> <%=msgs.get("KEY_HOD_SESSION_TRACING")%></label>
                                <p><input id="<%=HATSAdminConstants.PARAM_TRACE_HOD_PSEVENT_TRACING%>" type="checkbox" name="<%=HATSAdminConstants.PARAM_TRACE_HOD_PSEVENT_TRACING%>" <%=hti.isPsEventTracing() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>> <label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_PSEVENT_TRACING%>"><%=msgs.get("KEY_ENABLE_HOD_PSEVENT_TRACING")%></label>
                                <p><input id="<%=HATSAdminConstants.PARAM_TRACE_HOD_OIAEVENT_TRACING%>" type="checkbox" name="<%=HATSAdminConstants.PARAM_TRACE_HOD_OIAEVENT_TRACING%>" <%=hti.isOiaEventTracing() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>> <label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_OIAEVENT_TRACING%>"><%=msgs.get("KEY_ENABLE_HOD_OIAEVENT_TRACING")%></label>
                                <p><input id="<%=HATSAdminConstants.PARAM_TRACE_HOD_COMMEVENT_TRACING%>" type="checkbox" name="<%=HATSAdminConstants.PARAM_TRACE_HOD_COMMEVENT_TRACING%>" <%=hti.isCommEventTracing() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>><label for="<%=HATSAdminConstants.PARAM_TRACE_HOD_COMMEVENT_TRACING%>"> <%=msgs.get("KEY_ENABLE_HOD_COMMEVENT_TRACING")%></label>
                                </div>
                                </td>
                        <td> <admin:HelpLink topicID="IBM_SERVICE_TRACING_OPTIONS" /> <%= msgs.get("KEY_ENABLE_IBM_TRACE_OPTIONS_DESC") %></td>                 
                </tr> 

                <tr class="input-table-text">
                        <td><%=msgs.get("KEY_TRACE_OUTPUT")%></td>
                        <td><label for="<%=HATSAdminConstants.PARAM_TRACE_FILESIZE%>"> <%= msgs.get("KEY_TRACE_FILESIZE")  %></label> &nbsp;<input id="<%=HATSAdminConstants.PARAM_TRACE_FILESIZE%>" type="text" name="<%=HATSAdminConstants.PARAM_TRACE_FILESIZE%>" value="<%= manager.getTraceFileSize() %>" <%= enabledFlag ? "" : "disabled" %>>  KB 
                                <br> <label for="<%=HATSAdminConstants.PARAM_NUM_TRACE_FILES%>"> <%= msgs.get("KEY_NUM_TRACE_FILES") %></label> &nbsp;<input id="<%=HATSAdminConstants.PARAM_NUM_TRACE_FILES%>" type="text" name="<%=HATSAdminConstants.PARAM_NUM_TRACE_FILES%>" value="<%= manager.getNumTraceFiles() %>" <%= enabledFlag ? "" : "disabled" %>>
                                <br> <label for="<%=HATSAdminConstants.PARAM_TRACE_FILENAME%>"> <%=msgs.get("KEY_TRACE_FILENAME")%></label> &nbsp;<input id="<%=HATSAdminConstants.PARAM_TRACE_FILENAME%>" type="text" name="<%=HATSAdminConstants.PARAM_TRACE_FILENAME%>" value="<%= manager.getTraceFileName() %>" <%= enabledFlag ? "" : "disabled" %>> 
                           </td>
                        <td> <admin:HelpLink topicID="TRACE_OUTPUT" /> <%= msgs.get("KEY_TRACE_OUTPUT_DESC") %></td>
                </tr>
        </table>
        
<% if (bean.isOperator() || bean.isAdministrator() ) { %>
        <table class="data-footer" width=100% cellspacing=1 cellpadding=7>
                <tr>
                        <td><input class="button" type="SUBMIT"
                                name="<%=HATSAdminConstants.OPERATION_OK%>"
                                value="<%=msgs.get("KEY_OK")%>"> <input class="button" type="SUBMIT"
                                name="<%=HATSAdminConstants.OPERATION_CANCEL%>"
                                value="<%=msgs.get("KEY_CANCEL")%>"></td>
                </tr>
        </table>
        <!-- end of data-footer table -->
<% } %>
        </td>
        </tr>
</table>

    <!-- Don't mess with this placement.  It needs to be near the end and it needs to have a slight delay to work correctly within firefox (timing problem). -->
        <SCRIPT language="JavaScript"> setTimeout( 'setServiceOptionsVisibility()', 2 ); // delay 2 milliseconds </SCRIPT>

<!-- End of the layout manager table--></form>
</body>
</html>
