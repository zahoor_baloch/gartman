<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*" %>


<% 
	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminServlet.BEAN_ID);
	HATSAdminConnectionManager manager = bean.getConnectionManager();
	String indexString = request.getParameter(HATSAdminConstants.PARAM_INDEX);
	int index = Integer.parseInt(indexString);
	String[] details=null;
	if (index != -1){
		details = manager.getPoolDefinitionDetails(index);
	}
	String[] headers = HATSAdminConstants.POOLDEF_DETAILS_HEADERS;
	
	
%>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >

<HEAD>
<TITLE><%=msgs.get("KEY_POOLDEF_DETAILS")%></TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META name="GENERATOR" content="IBM WebSphere Studio">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>
<BODY>
<form>
<% if (index == -1) { %> 
<%= msgs.get("KEY_SESSION_NO_LONGER_EXISTS") %>
<%} else {  %>

<TABLE width=100% border=1 cellpadding=4 cellspacing=1 bgcolor="#cccccc">
	<% for (int row=0;row<details.length;row++) { 
	if (row%2==0) { %>
	<TR class="evenrow">
		<% } else { %>
	<TR class="oddrow">
		<% } %>

		<TH nowrap><STRONG><%= msgs.get(headers[row]) %></STRONG></TH>
		<TD><%= details[row]%></TD>
	</TR>

	<% } %>
</table>

<p>
<TABLE width=100% border=0 cellpadding=4 cellspacing=1 bgcolor="black">
	<TR class="footer">
		<TD><input class="button" type="button"
			value="<%=msgs.get("KEY_CLOSE")%>" onclick="window.close()"></TD>
	</TR>
</TABLE>
<%}%></form>
</BODY>
</HTML>
