<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*, java.text.DateFormat"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminConnectionManager manager = bean.getConnectionManager();
	String function = HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS;
	int numDataItems = manager.getNumDBConnections();
	int start = manager.getStartDBConn();
	int finish = manager.getFinishDBConn();
	int total = numDataItems;
	String[] headers = HATSAdminConstants.DB_CONNECTIONS_HEADERS;
	/* Calculate the sorting preference and order */
	String unsortedImage = "<img src=\"images/sort_none.gif\"  width=\"9\"  height=\"13\"  alt = \"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
	int sortOrder = manager.getSortDBConnPreference();
  	int descending = sortOrder % 2;
  	String modifier = null;
  	String alt = " alt=\"";
  	if (descending == 1) {
  		modifier = "descend";
  		alt += msgs.get("KEY_DESCENDING") + "\"";
  	} else {
  		modifier = "ascend";
  		alt += msgs.get("KEY_ASCENDING") + "\"";
  	}
  	sortOrder = sortOrder - descending;

  	/* end of sorting calculations */
 
	if (true)  { // Change this to a check for bidi languages 
	%>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>

<HEAD>
<title><%= msgs.get("KEY_MANAGE_DB_CONNECTIONS") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>
<script language="javascript1.2">
<!-- Hiding JavaScript    
	function setChecked(checked) {
 		form = document.forms[0];
		for (i=0; i < form.elements.length; i++) {
			if (form.elements[i].type=='checkbox'){
				form.elements[i].checked = checked;
			}
		}
	}
	
	function checkBigCheck(checked) {
		if (checked == false) {
			form.BIG_CHECK.checked = false;
			return;
		}
		form = document.forms[0];
		for (i = 0; i < form.elements.length; i++) {
			if (form.elements[i] != form.BIG_CHECK &&
					form.elements[i].type == 'checkbox') {
				if (form.elements[i].checked == false) {
					form.BIG_CHECK.checked = false;
					return;
				}
			}
		}
		form.BIG_CHECK.checked = true;
	}
	
	function confirmDisconnect() {
		form = document.forms[0];

		boxeschecked = 0;
		for (i=0; i< form.elements.length; i++) {
   			if (form.elements[i] != form.BIG_CHECK &&
   					form.elements[i].type=='checkbox'){
      			if (form.elements[i].checked == true){
					boxeschecked++;
           		}
           	}
        }
        
        if (boxeschecked == 0) {
        	alert("<%=msgs.get("KEY_DISCONNECT_PROMPT")%>");
        	return false;
        }
		return confirm("<%=msgs.get("KEY_DISCONNECT_CONFIRM")%>");
	}
	
	function openWindow(id) {
        var temp = window.open('<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_DB_CONNECTION_DETAILS + "&" + HATSAdminConstants.PARAM_ID + "=")%>' + id, "ChangeMeToSomethingElse", 'width=500,height=550,menubar=0,scrollbars=0,status=0,toolbar=0,resizable=yes');
	
		temp.focus();
		return false;
	}	

// Done hiding --> 
</script>

<form method="post" action="<%= response.encodeURL("admin")%>">
<input type="hidden" name="<%=HATSAdminConstants.PARAM_FUNCTION%>" value="<%=HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS%>">

<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4>
	<tr class="header">
		<td><%= msgs.get("KEY_MANAGE_DB_CONNECTIONS") %></td>
	</tr>
	<tr>
		<td class="instructions">
		   <%= msgs.get("KEY_INSTRUCTIONS_MANAGE_DB_CONNECTIONS") %> 
		   <%= bean.isOperator() || bean.isAdministrator() ? msgs.get("KEY_INSTRUCTIONS_DISCONNECT_CONNECTIONS") : "" %>
		   <admin:HelpLink topicID="DB_CONNECTIONS" /></td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>

		<admin:Scope />

		<table width=100% cellpadding=7>
			<!-- start of top-buttons/refresh link table -->
			<tr>
			<td width=25 ><a href="javascript:document.forms[0].submit();"><img border="0" src="images/refresh.gif" width="16"  height="16" alt="<%=msgs.get("KEY_REFRESH")%>" onclick="submit();"
					style="cursor: hand"></a></td> 
		<% if (bean.isOperator() || bean.isAdministrator()) { %>
				<td><input class="button" type="SUBMIT"
					name="<%=HATSAdminConstants.PARAM_SHUTDOWN%>"
					value="<%=msgs.get("KEY_DISCONNECT")%>"
					onClick="return confirmDisconnect(this.form)"> </td>
				<% } %>
				

			</tr>
		</table>
		<!-- end of top-buttons/refresh link table -->

		<admin:BrowseButtons function="<%=function%>" start="<%=start%>" finish="<%=finish%>" total="<%=total%>" />

		<table class="data-table" width="100%" cellspacing="1" cellpadding="4" border="1" summary="<%= msgs.get("KEY_INSTRUCTIONS_MANAGE_DB_CONNECTIONS") %> ">
			<caption><%= msgs.get("KEY_MANAGE_DB_CONNECTIONS") %></caption>
	
			<!-- start of data table -->
			<tr class="columnheader">
				<!-- Row for all the column headers -->

			
				<% if(numDataItems > 0 && (bean.isOperator() || bean.isAdministrator())) { // Extra td needed for checkbox, radio etc %>
				<td width=1%><input type="checkbox" name="BIG_CHECK"
					onClick="setChecked(BIG_CHECK.checked)"
					title="<%=msgs.get("KEY_SELECT_ALL")%>"></td>
				<% } %>

				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_DBCONN_CONN_ID)%>"
					title="<%=msgs.get("KEY_CONN_ID_DESC")%>"> <%=msgs.get("KEY_CONN_ID")%></a><%=(sortOrder == manager.SORT_DBCONN_CONN_ID ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_DBCONN_CONN_SPEC)%>"
					title="<%=msgs.get("KEY_CONNSPEC_DESC")%>"> <%=msgs.get("KEY_CONN_SPEC")%></a><%=(sortOrder == manager.SORT_DBCONN_CONN_SPEC? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""   + alt + ">": unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_DBCONN_BEAN)%>"
					title="<%=msgs.get("KEY_BEAN_DESC")%>"> <%=msgs.get("KEY_BEAN")%></a><%=(sortOrder == manager.SORT_DBCONN_BEAN ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">": unsortedImage) %></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_DBCONN_DBPRODUCT)%>"
					title="<%=msgs.get("KEY_DBPRODUCT_DESC")%>"><%= msgs.get("KEY_DBPRODUCT") %></a><%=(sortOrder == manager.SORT_DBCONN_DBPRODUCT ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_DBCONN_DBVERSION)%>"
					title="<%=msgs.get("KEY_DBVERSION_DESC")%>"><%= msgs.get("KEY_DBVERSION") %></a><%=(sortOrder == manager.SORT_DBCONN_DBVERSION ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_DBCONN_JDBC)%>"
					title="<%=msgs.get("KEY_JDBC_DESC")%>"><%= msgs.get("KEY_JDBC") %></a><%=(sortOrder == manager.SORT_DBCONN_JDBC ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_DBCONN_URL)%>"
					title="<%=msgs.get("KEY_URL_DESC")%>"><%= msgs.get("KEY_URL") %></a><%=(sortOrder == manager.SORT_DBCONN_URL? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_DB_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_DBCONN_USERNAME)%>"
					title="<%=msgs.get("KEY_USERNAME_DESC")%>"><%= msgs.get("KEY_USERNAME") %></a><%=(sortOrder == manager.SORT_DBCONN_USERNAME ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>


			</tr>
			<!-- end of the column-header row -->

			<!-- Start of the data in the data table -->
			<% 

if (numDataItems==0)  
{ %>
			<tr class="evenrow">
				<td colspan=<%= headers.length %> ><%= msgs.get("KEY_NONE") %></td>
			</tr>
			<%
}
//int start = manager.getStartConnection(), finish = manager.getEndConnection();
else for (int i = start; i <= finish; i++) {
    String encodedID = manager.getEncodedDBConnectionID(i);
     String[] rowData=null;
    
	if (i % 2 == 0) { %>
			<tr class="evenrow" nowrap>
				<%} else {%>
			<tr class="oddrow" nowrap >
				<%} %>
				<td class="listItem"><input name="SELECTED_ROW" value="<%= encodedID %>" type="checkbox" onclick="checkBigCheck(this.checked)"></td>
				<% rowData = manager.getDBConnectionBasics(i); 
				   int column = 0; %>
				<td class="listItem"><a href="#" onclick="openWindow('<%=encodedID%>');"
					title="<%=msgs.get("KEY_SHOW_DETAILS")%>"> <%=rowData[column++]%></a></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
			</tr>
			<%  }  %>

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table --> <!-- No need for the footer table on this page  so comment it out-->
		
	</tr>
</table>
<!-- End of the layout manager table--></form>
</body>
</html>


