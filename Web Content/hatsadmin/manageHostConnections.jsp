<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*, java.text.DateFormat"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminConnectionManager manager = bean.getConnectionManager();
	String function = HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS;
	int numDataItems = manager.getNumHostConnections();
	int start = manager.getStartHostConn();
	int finish = manager.getFinishHostConn();
	String lastSearchKey = manager.getSearchKey();
	String lastSearchValue = manager.getSearchValue();
	int total = numDataItems;
	// System.out.println("start = " + start  + " , finish = " + finish +  " ,  total = "  +total);
	String unsortedImage = "<img src=\"images/sort_none.gif\" width=\"9\"  height=\"13\" alt = \"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
	/* Calculate the sorting preference and order */
	int sortOrder = manager.getSortHostConnPreference();
  	int descending = sortOrder % 2;
  	String modifier = null;
  	String alt = " alt=\"";
  	if (descending == 1) {
  		modifier = "descend";
  		alt += msgs.get("KEY_DESCENDING") + "\"";
  	} else {
  		modifier = "ascend";
  		alt += msgs.get("KEY_ASCENDING") + "\"";
  	}
  	sortOrder = sortOrder - descending;


  	/* end of sorting calculations */
 
	if (true)  { // Change this to a check for bidi languages 
	%>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>

<HEAD>
<title><%= msgs.get("KEY_MANAGE_HOST_CONNECTIONS") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>
<script language="javascript1.2">
<!-- Hiding JavaScript    
	function setChecked(checked) {
 		form = document.forms[0];
		for (i=0; i < form.elements.length; i++) {
			if (form.elements[i].type=='checkbox'){
				form.elements[i].checked = checked;
			}
		}
	}
	
	function checkBigCheck(checked) {
		if (checked == false) {
			form.BIG_CHECK.checked = false;
			return;
		}
		form = document.forms[0];
		for (i = 0; i < form.elements.length; i++) {
			if (form.elements[i] != form.BIG_CHECK &&
					form.elements[i].type == 'checkbox') {
				if (form.elements[i].checked == false) {
					form.BIG_CHECK.checked = false;
					return;
				}
			}
		}
		form.BIG_CHECK.checked = true;
	}
	
	function confirmDisconnect() {
		form = document.forms[0];

		boxeschecked = 0;
		for (i=0; i< form.elements.length; i++) {
   			if (form.elements[i] != form.BIG_CHECK &&
   					form.elements[i].type=='checkbox'){
      			if (form.elements[i].checked == true){
					boxeschecked++;
           		}
           	}
        }
        
        if (boxeschecked == 0) {
        	alert("<%=msgs.get("KEY_DISCONNECT_PROMPT")%>");
        	return false;
        }
		return confirm("<%=msgs.get("KEY_DISCONNECT_CONFIRM")%>");
	}
	
	function validateToggle() {
		form = document.forms[0];

		boxeschecked = 0;
		for (i=0; i< form.elements.length; i++) {
   			if (form.elements[i] != form.BIG_CHECK &&
   					form.elements[i].type=='checkbox'){
      			if (form.elements[i].checked == true){
					boxeschecked++;
           		}
           	}
        }
        
        if (boxeschecked == 0) {
        	alert("<%=msgs.get("KEY_TOGGLE_PROMPT")%>");
        	return false;
        }
		return true;
	}
	
	function openWindow(id) {
		var temp = window.open('<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_HOST_CONNECTION_DETAILS + "&" + HATSAdminConstants.PARAM_ID + "=")%>' + id, "ChangeMeToSomethingElse", 'width=700,height=600,menubar=0,scrollbars=0,status=0,toolbar=0,resizable=yes');
	
		temp.focus();
		return false;
	}	

	function pulldownEntrySelected() {
		form = document.forms[0];

		if (form.<%=HATSAdminConstants.PARAM_CONN_SEARCH_KEY%>.value == "KEY") {
		      form.<%=HATSAdminConstants.PARAM_CONN_SEARCH_VALUE%>.disabled=true;
		      form.<%=HATSAdminConstants.PARAM_CONN_SEARCH_VALUE%>.value="<%=msgs.get("KEY_VALUE")%>";
		}
		else {
		      form.<%=HATSAdminConstants.PARAM_CONN_SEARCH_VALUE%>.disabled=false;
		}
	}


// Done hiding --> 

</script>

<form method="post" action="<%= response.encodeURL("admin")%>"><input type="hidden"
	name="<%=HATSAdminConstants.PARAM_FUNCTION%>"
	value="<%=HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS%>"> <admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4>
	<tr class="header">
		<td><%= msgs.get("KEY_MANAGE_HOST_CONNECTIONS") %></td>
	</tr>
	<tr>
		<td class="instructions">
		     <%= msgs.get("KEY_INSTRUCTIONS_MANAGE_HOST_CONNECTIONS") %> 
		     <%= bean.isOperator() || bean.isAdministrator() ? msgs.get("KEY_INSTRUCTIONS_DISCONNECT_CONNECTIONS")  : "" %>
		     <%= bean.isAdministrator() ? msgs.get("KEY_INSTRUCTIONS_TOGGLE_CONNECTION")  : "" %>
		     <admin:HelpLink topicID="HOST_CONNECTIONS" /> 
		</td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>

		<admin:Scope />




		<table width=100% cellpadding=7>
			<!-- start of top-buttons/refresh link table -->
			<tr> <td width=25 ><a href="javascript:document.forms[0].submit();"><img border="0" src="images/refresh.gif"  width="14"  height="14" alt="<%=msgs.get("KEY_REFRESH")%>" onclick="submit();"
					style="cursor: hand"></a></td> 
			<% if( bean.isOperator() || bean.isAdministrator() ) { %>
           		<td><input class="button" type="SUBMIT"
					name="<%=HATSAdminConstants.PARAM_SHUTDOWN%>"
					value="<%=msgs.get("KEY_DISCONNECT")%>"
					onClick="return confirmDisconnect(this.form)"> 
				<% } 
				   if (bean.isAdministrator()) { %>					
					<input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.PARAM_DISPLAY_TERMINAL%>"
					value="<%=msgs.get("KEY_TOGGLE_DISPLAY_STATUS")%>"
					onClick="return validateToggle(this.form)">
				<% } %>
                                   <%=msgs.get("KEY_SEARCH_CRITERIA")%>:
				<%   if ( bean.isOperator() || bean.isAdministrator()) { %>					
					<select	name="<%=HATSAdminConstants.PARAM_CONN_SEARCH_KEY%>"
					onChange="pulldownEntrySelected()">
					    <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_SHOW_ALL%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_SHOW_ALL)) ?  "SELECTED" : "")%>><%=msgs.get("KEY_SHOW_ALL")%>
					    <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_CONNID%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_CONNID)) ? "SELECTED" : "")%>><%=msgs.get("KEY_CONN_ID")%>
                                            <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_CONNDEF_NAME%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_CONNDEF_NAME)) ? "SELECTED" : "")%>><%=msgs.get("KEY_CONN_SPEC")%>
                                            <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_IPADDRESS%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_IPADDRESS)) ? "SELECTED" :  "")%>><%=msgs.get("KEY_IP_ADDRESS")%>
                                            <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_TNSERVERPORT%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_TNSERVERPORT)) ? "SELECTED" : "")%>><%= msgs.get("KEY_HOSTPORT") %>
                                            <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_LUNAME%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_LUNAME)) ? "SELECTED" : "")%>><%= msgs.get("KEY_LUNAME") %>
                                            <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_SESSIONID%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_SESSIONID)) ? "SELECTED" : "")%>><%= msgs.get("KEY_SESSION_ID") %>
                                            <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_APPNAME%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_APPNAME)) ? "SELECTED" : "")%>><%= msgs.get("KEY_APPNAME") %>
                                            <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_COMMSTATUS%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_COMMSTATUS)) ? "SELECTED" : "")%>><%= msgs.get("KEY_COMM_STATUS") %>
                                            <option value="<%=HATSAdminConstants.PARAM_CONN_SEARCH_DISPTERM%>" <%=((lastSearchKey.equals(HATSAdminConstants.PARAM_CONN_SEARCH_DISPTERM)) ? "SELECTED" : "")%>><%= msgs.get("KEY_DISPLAY_STATUS") %>
                                        </select>

					<input type="text"	name="<%=HATSAdminConstants.PARAM_CONN_SEARCH_VALUE%>"
					value="<%=lastSearchValue%>" size="30">

                                        <input class="button"
					type="submit"
					name="submitSearch"
					value="<%=msgs.get("KEY_SUBMIT_SEARCH")%>">
					</td>
				<% } %>
				
			</tr>
		</table>
		<!-- end of top-buttons/refresh link table -->

		<admin:BrowseButtons function="<%=function%>" start="<%=start%>" finish="<%=finish%>" total="<%=total%>" />

		<table class="data-table" width=100% cellspacing=1 cellpadding=4 border=1 summary=<%= msgs.get("KEY_MANAGE_HOST_CONNECTIONS_SUMMARY") %>>
		<caption><%= msgs.get("KEY_MANAGE_HOST_CONNECTIONS") %></caption>
			<!-- start of data table -->
			<tr class="columnheader">
				<!-- Row for all the column headers -->

			
				<% if(numDataItems > 0) { // Extra td needed for checkbox, radio etc %>
				<td width=1%><input type="checkbox" name="BIG_CHECK"
					onClick="setChecked(BIG_CHECK.checked)"
					title="<%=msgs.get("KEY_SELECT_ALL")%>"></td>
				<% } %>

				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_CONN_ID)%>"
					title="<%=msgs.get("KEY_CONN_ID_DESC")%>"> <%=msgs.get("KEY_CONN_ID")%></a><%=(sortOrder == manager.SORT_HOSTCONN_CONN_ID ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\" " + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_CONN_SPEC)%>"
					title="<%=msgs.get("KEY_CONNSPEC_DESC")%>"> <%=msgs.get("KEY_CONN_SPEC")%></a><%=(sortOrder == manager.SORT_HOSTCONN_CONN_SPEC? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\" "  + alt + ">": unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_IP_ADDRESS)%>"
					title="<%=msgs.get("KEY_IP_ADDRESS_DESC")%>"> <%=msgs.get("KEY_IP_ADDRESS")%></a><%=(sortOrder == manager.SORT_HOSTCONN_IP_ADDRESS? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  +  alt + ">": unsortedImage) %></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_HOSTPORT)%>"
					title="<%=msgs.get("KEY_HOSTPORT_DESC")%>"><%= msgs.get("KEY_HOSTPORT") %>
				</a><%=(sortOrder == manager.SORT_HOSTCONN_HOSTPORT ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\" height=\"13\" " + alt + ">" : unsortedImage)%></th>
                <th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_LUNAME_ID)%>"
					title="<%=msgs.get("KEY_LUNAME_ID_DESC")%>"><%= msgs.get("KEY_LUNAME") %>
				</a><%=(sortOrder == manager.SORT_HOSTCONN_LUNAME_ID? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\" height=\"13\" " + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_SESSION_ID)%>"
					title="<%=msgs.get("KEY_SESSION_ID_DESC")%>"><%= msgs.get("KEY_SESSION_ID") %>
				</a><%=(sortOrder == manager.SORT_HOSTCONN_SESSION_ID? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\" "  + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_APPNAME)%>"
					title="<%=msgs.get("KEY_APPNAME_DESC")%>"><%= msgs.get("KEY_APPNAME") %>
				</a><%=(sortOrder == manager.SORT_HOSTCONN_APPNAME ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\" " + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_COMM_STATUS)%>"
					title="<%=msgs.get("KEY_COMM_STATUS_DESC")%>"><%= msgs.get("KEY_COMM_STATUS") %>
				</a><%=(sortOrder == manager.SORT_HOSTCONN_COMM_STATUS ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_HOST_CONNECTIONS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_HOSTCONN_DISPLAY_STATUS)%>"
					title="<%=msgs.get("KEY_DISPLAY_STATUS_DESC")%>"><%= msgs.get("KEY_DISPLAY_STATUS") %>
				</a><%=(sortOrder == manager.SORT_HOSTCONN_DISPLAY_STATUS ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">" : unsortedImage)%></th>



			</tr>
			<!-- end of the column-header row -->

			<!-- Start of the data in the data table -->
			<%!
			String elidedString(String theString, int maxLen) {
			   if (theString == null) return null;
			   int strLen = theString.length();
			   if ((maxLen < 5) || (strLen<=maxLen)) return theString;
			   int keepCount=maxLen - 3;
			   int endCount=keepCount - (keepCount/2);
			   StringBuffer b = new StringBuffer(maxLen);
			   b.append(theString.substring(0, (keepCount)/2)).append("...").append(theString.substring(strLen-endCount));
			   return b.toString();
			   }
			   %>
			<% 

if (numDataItems==0)  
{ %>
			<tr class="evenrow">
				<td colspan=9><%= msgs.get("KEY_NONE") %></td>
			</tr>
			<%
}
//int start = manager.getStartConnection(), finish = manager.getEndConnection();
else {
	int lenColHeader0 = Math.max(msgs.get("KEY_CONN_ID").length(), 30);
	int lenColHeader1 = Math.max(msgs.get("KEY_CONN_SPEC").length(), 26);
    for (int i = start; i <= finish; i++) {
     String encodedID = manager.getEncodedHostConnectionID(i);
    //System.out.println("manageconnectoins: uniqueid = " + encodedID);
    String[] rowData=null;

	if (i % 2 == 0) { %>
			<tr class="evenrow">
				<%} else {%>
			<tr class="oddrow">
				<%} %>
				<%  rowData = manager.getHostConnectionBasics(i); 
				    int column = 0; %>
				<td class="listItem"> <label for="<%="input" + i %>" ><input id="<%="input" + i %>" name="SELECTED_ROW" value="<%= encodedID %>" type="checkbox"
					onclick="checkBigCheck(this.checked)"> <div style="display:none" > <%=rowData[column]%> </div> </label></td>
				
				<td class="listItem"><a href="#" onclick="openWindow('<%=encodedID%>');" style="cursor: hand"
					title="<%=msgs.get("KEY_SHOW_DETAILS")+ "   " + rowData[column]%>"> <%=elidedString(rowData[column++], lenColHeader0)%></a></td>
				
				<td class="listItem"><a style="text-decoration:none; color:black; cursor:text;" href="#" title="<%=rowData[column]%>"> <%=elidedString(rowData[column++], lenColHeader1)%></a></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
                <td class="listItem"><%=rowData[column++]%></td>
                				
			</tr>
			<%  } }  %>

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table --> <!-- No need for the footer table on this page  so comment it out-->
		
	</tr>
</table>
<!-- End of the layout manager table--></form>
</body>
</html>


