<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminScopeManager manager = bean.getScopeManager(); // ScopeManager contains all the logic for scope management
	String function = HATSAdminConstants.FUNCTION_SELECT_SR;
	int numDataItems = manager.getNumServerRegions();
	int start = manager.getStartServerRegion();
	int finish = manager.getNumServerRegions() -1;
	int total = numDataItems;
	String unsortedImage = "<img src=\"images/sort_none.gif\" alt = \"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
		/* Calculate the sorting preference and order */
	int sortOrder = bean.getScopeManager().getSortOrderForServerRegionPanel();
  	int descending = sortOrder % 2;
  	String modifier = null;
  	String alt = " alt=\"";
  	if (descending == 1) {
  		modifier = "descend";
  		alt += msgs.get("KEY_DESCENDING") + "\"";
  	} else {
  		modifier = "ascend";
  		alt += msgs.get("KEY_ASCENDING") + "\"";
  	}
  	sortOrder = sortOrder - descending;
  	/* end of sorting calculations */
	if (true)  { // Change this to a check for bidi languages 
	%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>
<HEAD>
<title><%= msgs.get("KEY_SELECT_SR") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>
<script language="javascript1.2">
<!--Insert the JavaScript here if any that is required for form validation/processing-->
</script>

<form method="post" action="<%= response.encodeURL("admin")%>"><input type="hidden"
	name="<%=HATSAdminConstants.PARAM_FUNCTION%>"
	value="<%=HATSAdminConstants.FUNCTION_SELECT_SR%>"> 

<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4>
	<tr class="header">
		<td><%= msgs.get("KEY_SELECT_SR") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_SELECT_SR") %> <admin:HelpLink topicID="SELECT_SR"/> </td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>

		<admin:Scope />

		<admin:BrowseButtons function="<%=function%>" start="<%=start%>" finish="<%=finish%>" total="<%=total%>" />

		<table class="data-table" width="100%" cellspacing="1" cellpadding="4" border="1" summary=<%= msgs.get("KEY_INSTRUCTIONS_SELECT_SR") %>>
		<caption><%= msgs.get("KEY_APPLICATIONS_CAPTION") %></caption>
		
		 <!-- start of data table -->
			<tr class="columnheader"> <!-- Row for all the column headers -->

				<% if(numDataItems > 0) { // Extra td needed for checkbox, radio etc %>
				<td width=1%></td>
				<% } %>
				<th ><a	href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SELECT_SR + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_SR_APPNAME)%>"
					title="<%=msgs.get("KEY_APPNAME")%>"> <%=msgs.get("KEY_APPNAME")%></a><%=(sortOrder == manager.SORT_SR_APPNAME? "<img src=\"images/sort_" + modifier + ".gif\"" + alt + ">" : unsortedImage)%></th>
				<th ><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SELECT_SR + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_SR_APPSERVERNAME)%>"
					title="<%=msgs.get("KEY_APPSERVERNAME")%>"> <%=msgs.get("KEY_APPSERVERNAME")%></a><%=(sortOrder == manager.SORT_SR_APPSERVERNAME ? "<img src=\"images/sort_" + modifier + ".gif\""  + alt + ">": unsortedImage)%></th>
				<th ><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SELECT_SR + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_SR_NODENAME)%>"
					title="<%=msgs.get("KEY_NODENAME")%>"> <%=msgs.get("KEY_NODENAME")%></a><%=(sortOrder == manager.SORT_SR_NODENAME ? "<img src=\"images/sort_" + modifier + ".gif\""  + alt + ">": unsortedImage)%></th>
			    
			    <th ><a	href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SELECT_SR + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_SR_CELLNAME)%>"
					title="<%=msgs.get("KEY_CELLNAME")%>"> <%=msgs.get("KEY_CELLNAME")%></a><%=(sortOrder == manager.SORT_SR_CELLNAME ? "<img src=\"images/sort_" + modifier + ".gif\"" + alt + ">" : unsortedImage)%></th>
				 

	</tr>   <!-- end of the column-header row -->
<!-- Start of the data in the data table -->
<% 
if (numDataItems==0)  
{ %>
			<tr class="evenrow">
				<td colspan=4><%= msgs.get("KEY_NONE") %></td>
			</tr>
			<%
}

else  { 
	boolean checkedFlag=false;
	for (int i = start; i <= finish; i++) {  //start of for loop for displaying the data
	String[] srBasics = manager.getServerRegionBasics(i);
		if (i % 2 == 0) {%>
			<tr class="evenrow">
				<%} else {%>
			<tr class="oddrow">
				<%}%> 
				<td > <label for="<%="input" + i %>" ><input id="<%="input" + i %>"
					name="<%= HATSAdminConstants.PARAM_SELECTED_SR %>"
					value="<%= HATSAdminBean.encode(manager.getSRAdminServerName(i)) %>"
					type="radio" 
					<% if (!checkedFlag) { %> checked 
					<% checkedFlag=true;
					} %>>
					<div style="display:none" > <%=srBasics[0] + " : " + srBasics[1]
												 + " : " + srBasics[2] + " : " + srBasics[3] %> </div>
					</label>
				</td>
				
				<td> <%= srBasics[0] %></td>
				<td> <%= srBasics[1] %></td>
				<td> <%= srBasics[2] %></td>
				<td> <%= srBasics[3] %></td>
				<% }  %>
				
			</tr>
			
			

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table -->



		<!-- start of data-footer table with any buttons if needed -->
		<table class="data-footer" width=100% cellspacing=1 cellpadding=7>
			<tr>
				<td><input class="button" type="SUBMIT"
					name="<%=HATSAdminConstants.OPERATION_OK%>"
					value="<%=msgs.get("KEY_OK")%>"> <input class="button"
					type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>"
					value="<%=msgs.get("KEY_CANCEL")%>"></td>
			</tr>
		</table>
		<!-- end of data-footer table -->

		<% }  // end of (numDataItems !=0)  %>
		</td>
	</tr>
</table> <!-- End of the layout manager table-->
</form>
</body>
</html>


