<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information
	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminLicenseManager manager = bean.getLicenseManager();
	String function = HATSAdminConstants.FUNCTION_LICENSE_USAGE;
	int numDataItems = manager.getNumLicenseUsageInfos();
	int start = manager.getStartLicenseUsageInfo();
	int finish =manager.getFinishLicenseUsageInfo();
	int total = numDataItems;

	// System.out.println("PAGE: LicenseInfos: numDataItems = " + numDataItems);

	/* Calculate the sorting preference and order */
	String unsortedImage = "<img src=\"images/sort_none.gif\" alt = \" width=\"9\"  height=\"13\"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
	int sortOrder = manager.getSortLicensePreference();
  	int descending = sortOrder % 2;
  	String modifier = null;
  	String alt = " alt=\"";
  	if (descending == 1) {
  		modifier = "descend";
  		alt += msgs.get("KEY_DESCENDING") + "\"";
  	} else {
  		modifier = "ascend";
  		alt += msgs.get("KEY_ASCENDING") + "\"";
  	}
  	sortOrder = sortOrder - descending;

  	/* end of sorting calculations */


	if (true)  { // Change this to a check for bidi languages 
	%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>

<HEAD>
<title><%= msgs.get("KEY_LICENSE_USAGE") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>

<form method="post" action="<%= response.encodeURL("admin")%>"><input type="hidden"	name="<%=HATSAdminConstants.PARAM_FUNCTION%>"	value="<%=HATSAdminConstants.FUNCTION_LICENSE_USAGE%>">
	<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4 >
	<tr class="header">
		<td><%= msgs.get("KEY_LICENSE_USAGE") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_LICENSE_USAGE") %> <admin:HelpLink topicID="LICENSE_USAGE" /> </td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>

		<admin:Scope />

		<!-- start of top-buttons/refresh link table -->
		<table width=100% cellpadding=7>
			<tr>
               <td><a href="javascript:document.forms[0].submit();"><IMG border="0" src="images/refresh.gif"  width="16"  height="16"
					alt='<%=msgs.get("KEY_REFRESH")%>' onclick="submit();"
					style="cursor: hand"></a></td>

			</tr>
		</table>
		<!-- end of top-buttons/refresh link table -->
		
		<admin:BrowseButtons function="<%=function%>" start="<%=start%>" finish="<%=finish%>" total="<%=total%>" />	
		<table class="data-table" width="100%" cellspacing="1" cellpadding="4" border="1" summary="<%= msgs.get("KEY_INSTRUCTIONS_LICENSE_USAGE") %> ">
			<caption><%= msgs.get("KEY_LICENSE_USAGE") %></caption>
			<!-- start of data table -->
			<tr class="columnheader">
				<!-- Row for all the column headers -->

				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_LICENSE_USAGE + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_LICENSE_APPNAME)%>"
					title="<%=msgs.get("KEY_APPNAME_DESC")%>"> <%=msgs.get("KEY_APPNAME")%></a><%=(sortOrder == manager.SORT_LICENSE_APPNAME ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage )%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_LICENSE_USAGE + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_LICENSE_ACTIVE_SESSIONS)%>"
					title="<%=msgs.get("KEY_ACTIVE_SESSIONS_DESC")%>"> <%=msgs.get("KEY_ACTIVE_SESSIONS")%></a><%=(sortOrder == manager.SORT_LICENSE_ACTIVE_SESSIONS? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">": unsortedImage )%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_LICENSE_USAGE + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_LICENSE_LIFE_MAX)%>"
					title="<%=msgs.get("KEY_LIFE_MAX_DESC")%>"> <%=msgs.get("KEY_LIFE_MAX")%></a><%=(sortOrder == manager.SORT_LICENSE_LIFE_MAX? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">": unsortedImage ) %></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_LICENSE_USAGE + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_LICENSE_HOUR_MAX)%>"
					title="<%=msgs.get("KEY_HOUR_MAX_DESC")%>"><%= msgs.get("KEY_HOUR_MAX") %></a><%=(sortOrder == manager.SORT_LICENSE_HOUR_MAX? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage )%></th>


			</tr>
			<!-- end of the column-header row -->

			<!-- Start of the data in the data table -->
			<% 

if (numDataItems==0)  
{ %>
			<tr class="evenrow">
				<td colspan=4><%= msgs.get("KEY_NONE") %></td>
			</tr>
			<%
}

else for (int i = start; i <= finish; i++) {
    
    
    String[] rowData= manager.getLicenseUsageInfoAsArray(i); 
    int column = 0; 
    
	if (i % 2 == 0) { %>
			<tr class="evenrow">
				<%} else {%>
			<tr class="oddrow">
				<%} %>
				     
				
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
			</tr>
			<%  }  %>

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table -->
		</td> 
	</tr>
</table>
<!-- End of the layout manager table--></form>
</body>
</html>


