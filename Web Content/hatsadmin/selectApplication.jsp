<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*, java.text.DateFormat"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminScopeManager manager = bean.getScopeManager(); // ScopeManager contains all the logic for scope management
	int selectedApplicationIndex = manager.getCurrentApplicationIndex();
    String function = HATSAdminConstants.FUNCTION_SELECT_APPLICATION;
	int numDataItems = manager.getNumApplications();
	int start = manager.getStartApplication();
	int finish = manager.getNumApplications()-1;
	int total= numDataItems;
	/* Calculate the sorting preference and order */
	String unsortedImage = "<img src=\"images/sort_none.gif\" width=\"9\"  height=\"13\" alt = \"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
	int sortOrder = bean.getScopeManager().getSortOrderForApplicationPanel();
  	int descending = sortOrder % 2;
  	String modifier = null;
  	String alt = " alt=\"";
  	if (descending == 1) {
  		modifier = "descend";
  		alt += msgs.get("KEY_DESCENDING") + "\"";
  	} else {
  		modifier = "ascend";
  		alt += msgs.get("KEY_ASCENDING") + "\"";
  	}
  	sortOrder = sortOrder - descending;
  	/* end of sorting calculations */
 
			
		
	if (true)  { // Change this to a check for bidi languages 
	%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>


<HEAD>
<title><%= msgs.get("KEY_SELECT_APPLICATION") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>
<script language="javascript1.2">
<!--Insert the JavaScript here if any that is required for form validation/processing-->
</script>

<form method="post" action="<%= response.encodeURL("admin")%>">
<input type="hidden" name="<%=HATSAdminConstants.PARAM_FUNCTION%>" value="<%=HATSAdminConstants.FUNCTION_SELECT_APPLICATION%>"> 
<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4>
	<tr class="header">
		<td><%= msgs.get("KEY_SELECT_APPLICATION") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_SELECT_APPLICATION") %> <admin:HelpLink topicID="SELECT_APPLICATION" /> </td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>

		<admin:Scope />




		<admin:BrowseButtons function="<%=function%>" start="<%=start%>" finish="<%=finish%>" total="<%=total%>" />

		<table class="data-table" width="100%" cellspacing="1" cellpadding="4" border="1" summary=<%= msgs.get("KEY_SELECT_APPLICATION") %>>
		<caption><%= msgs.get("KEY_APPLICATIONS_CAPTION") %></caption>
		 <!-- start of data table -->
			<tr class="columnheader"> <!-- Row for all the column headers -->

				<% if(numDataItems > 0) { // Extra td needed for checkbox, radio etc %>
				<td width=1%></td>
				<% } %>
				<th ><a	href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SELECT_APPLICATION + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_APP_APPNAME)%>"
					title="<%=msgs.get("KEY_APPNAME")%>"> <%=msgs.get("KEY_APPNAME")%></a><%=(sortOrder == manager.SORT_APP_APPNAME? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\" " + alt + ">" :unsortedImage)%></th>
				<th ><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SELECT_APPLICATION + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_APP_APPSERVERNAME)%>"
					title="<%=msgs.get("KEY_APPSERVERNAME")%>"> <%=msgs.get("KEY_APPSERVERNAME")%></a><%=(sortOrder == manager.SORT_APP_APPSERVERNAME ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\" "  + alt + ">": unsortedImage)%></th>
				<th ><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SELECT_APPLICATION + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_APP_NODENAME)%>"
					title="<%=msgs.get("KEY_NODENAME")%>"> <%=msgs.get("KEY_NODENAME")%></a><%=(sortOrder == manager.SORT_APP_NODENAME ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\" "  + alt + ">": unsortedImage)%></th>
			    
			    <th ><a	href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_SELECT_APPLICATION + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_APP_CELLNAME)%>"
					title="<%=msgs.get("KEY_CELLNAME")%>"> <%=msgs.get("KEY_CELLNAME")%></a><%=(sortOrder == manager.SORT_APP_CELLNAME ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\" " + alt + ">" : unsortedImage)%></th>
				 

	</tr>   <!-- end of the column-header row -->
<!-- Start of the data in the data table -->
<% 
if (numDataItems==0)  
{ %>
			<tr class="evenrow">
				<td colspan=4><%= msgs.get("KEY_NONE") %></td>
			</tr>
			<%
}
//int start = manager.getStartConnection(), finish = manager.getEndConnection();
else  { 
	int currentScopeIndex = manager.getCurrentScopeIndex(); 
	for (int i = start; i <= finish; i++) {  //start of for loop for displaying the data

	
	if (i % 2 == 0) {%>
			<tr class="evenrow">
				<%} else {%>
			<tr class="oddrow">
				<%}%> 
				<td>
				<label for="<%="input" + i %>">
					<input id="<%="input" + i %>" 
					name="<%= HATSAdminConstants.PARAM_SELECTED_APPLICATION %>"	
					value="<%= HATSAdminBean.encode(manager.getAdminServerName(i)) %>"
					type="radio" 
					<%= (i==selectedApplicationIndex || i==0) ? "checked" : "" %> >
					<div style="display:none" > <%=manager.getAppName(i) + " : " + manager.getAppServerName(i)
												 + " : " + manager.getNodeName(i) + " : " + manager.getCellName(i) %> </div>
					</label>
				</td>
				
				<td> <%= manager.getAppName(i) %></td>
				<% if (manager.isClustered(i) == false)  { %>
				<td> <%= manager.getAppServerName(i) %></td>
				<td> <%= manager.getNodeName(i) %></td>
				<td> <%= manager.getCellName(i) %></td>
				<% } else { %>
				<td colspan=3 align="center"> <%= manager.getClusterName(i) %></td>
				<% } %>
				
			</tr>
			<%}%>
			

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table -->



		<!-- start of data-footer table with any buttons if needed -->
		<table class="data-footer" width=100% cellspacing=1 cellpadding=7>
			<tr>
				<td>
				<input class="button" type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_OK%>"	value="<%=msgs.get("KEY_OK")%>"> 
				<input class="button" type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>"	value="<%=msgs.get("KEY_CANCEL")%>">
				</td>
			</tr>
		</table>
		<!-- end of data-footer table -->

		<% }  // end of (numDataItems !=0)  %>
		</td>
	</tr>
</table> <!-- End of the layout manager table-->
</form>
</body>
</html>


