<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*, com.ibm.hats.runtime.RuntimeConstants, java.util.*,java.text.Collator"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
    // Get the msgs object for generating messages and get the bean from the session
    // for obtaining all the required information

    // See if the locale has been previously saved away in the session.
    Locale userLocale = (Locale)session.getAttribute( RuntimeConstants.PARAM_USER_LOCALE );

    HatsMsgs msgs = HATSAdminServlet.getMessages(request);
    HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
    HATSAdminConnectionManager manager = bean.getConnectionManager();
    
    // If the locale cannot be retrieved from the session
    if ( null == userLocale )
    {
        // then retrieve the locale from the messages
        userLocale = HatsLocale.getInstance().getFullySupportedLocale(msgs.getLocale());
    }
    else
    {
        // Create a new msgs object containing the correct locale information
        msgs = new HatsMsgs( msgs.getResourcePath(), msgs.getComponent(), userLocale );
    }
    
    Collator collator = Collator.getInstance(userLocale);
%>

<%!
    	class TranslatedLanguageName implements Comparable {
			Locale locale;
			String code;
			String key;
			String value;
			Collator collator;
			public TranslatedLanguageName(Locale locale, String code, String key, String value, Collator collator) {
				this.locale = locale; this.code = code; this.key = key; this.value = value; this.collator = collator;
			}
			public int compareTo(Object other) {
				return collator.compare(value, ((TranslatedLanguageName)other).value);
			}
		}
%>
<%    if (true)  { // Change this to a check for bidi languages 
%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>
<HEAD>
<title><%= msgs.get("KEY_PREFERENCES") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>


<form method="post" action="<%= response.encodeURL("admin")%>">
<input type="hidden" name="<%=HATSAdminConstants.PARAM_FUNCTION%>" value="<%=HATSAdminConstants.FUNCTION_PREFERENCES%>"> 
<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width="95%" border="1" cellspacing="1" cellpadding="4">
	<tr class="header">
		<td><%= msgs.get("KEY_PREFERENCES") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_PREFERENCES") %></td>
	</tr>
</table>
<!--End of the instructions table-->
<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border="0" cellspacing="0"
	cellpadding="10" width="95%">
	<tr>
		<td>

		<admin:Scope/>
	 <!-------------------start of input table ------------->
		<table class="input-table" border="1" cellpadding="2" cellspacing="1"
			width="100%">
			<tr class="header" valign="top">
				<td colspan="3"><%=msgs.get("KEY_PREFERENCES") %></td>
			</tr>
            <% if ( request.isRequestedSessionIdFromCookie() ) { %>
			<tr class="input-table-text">
				<td><label for="input1"><%=msgs.get("KEY_SELECT_LANGUAGE")%></label></td>
				<td nowrap><select id="input1"
					name="<%=HATSAdminConstants.PARAM_SELECTED_LANGUAGE%>">
					<% String[] codes = HatsLocale.getInstance().getSupportedLanguageCodes();
			String[] countries = HatsLocale.getInstance().getSupportedCountryCodes();
			Locale[] locales = HatsLocale.getInstance().getSupportedLocales();
			TranslatedLanguageName[] languageNames = new TranslatedLanguageName[codes.length];
			for (int i = 0; i < codes.length; i++) {
				String combined = codes[i] + (countries[i].equals("") ? "" : "_") + countries[i];
				String key = "LOCALE_" + combined.toUpperCase();
				languageNames[i] = new TranslatedLanguageName(locales[i], combined, key, msgs.get(key), collator);
			}
			try { 
			Arrays.sort(languageNames);
			} catch (Exception e) {e.printStackTrace();}
			for (int i = 0; i < languageNames.length; i++) {
				%>
					<option
						<%=(userLocale.equals(languageNames[i].locale) ? "selected" : "")%>
						value="<%=languageNames[i].code%>"><%=languageNames[i].value%></option>
					<%}%>
				</select></td>
				<td> <admin:HelpLink topicID="PREFERENCES" /> <%= msgs.get("KEY_SELECT_LANGUAGE_SHORTDESC")%>
				</td>
			</tr>
            <% } %>
			<tr class="input-table-text">
			     <td><%=msgs.get("KEY_SET_NEW_FINISH")%></td>
                             <td><label for="input2" ><%= msgs.get("KEY_NUMBER_TO_DISPLAY")  %> </label> &nbsp;<input id="input2" type="text"	name="<%=HATSAdminConstants.PARAM_NEW_FINISH%>"
					value="<%=(manager.getMaxFinishHostConn() + 1)%>" size="5"
					></td>
                             <td><%= msgs.get("KEY_NEW_FINISH_DESC")%> </td>


                        </tr>
		</table>


		<table class="data-footer" width="100%" cellspacing="1"
			cellpadding="7">
			<tr>
				<td><input class="button" type="SUBMIT"
					name="<%=HATSAdminConstants.OPERATION_OK%>"
					value="<%=msgs.get("KEY_OK")%>">
                                    <input class="button"
					type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>"
					value="<%=msgs.get("KEY_CANCEL")%>"></td>
			</tr>
		</table>
		<!-- end of data-footer table --></td>
	</tr>
</table>
<!-- End of the layout manager table--></form>
</body>
</html>


