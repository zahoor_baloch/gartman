<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*, java.text.DateFormat, com.ibm.hats.runtime.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information
	
	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
    HatsMsgs studioMsgs = new HatsMsgs("studio", msgs.getLocale());
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminLicenseManager manager = bean.getLicenseManager();
	boolean enabledFlag = (bean.isMonitor() ? false: true );
	if (true)  { // Change this to a check for bidi languages 
	%>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>

<HEAD>
<title><%= msgs.get("KEY_SET_LICENSE_OPTIONS") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<script language="Javascript">

   // Set the enable state of the Num Licenses input field, based on the selected license type
   function setEnableStates() {
        if (document.forms[0].elements["licenseType"][0].checked) {
			document.forms[0].elements["<%=HATSAdminConstants.PARAM_NUM_LICENSES%>"].disabled = "";
		} else {
			document.forms[0].elements["<%=HATSAdminConstants.PARAM_NUM_LICENSES%>"].disabled = "true";
		}
   }

   function updateLicenseNumber() {
       // If processor selected
       if (!document.forms[0].elements["licenseType"][0].checked) {
           document.forms[0].elements["<%=HATSAdminConstants.PARAM_NUM_LICENSES%>"].disabled = "";
           document.forms[0].elements["<%=HATSAdminConstants.PARAM_NUM_LICENSES%>"].value = "-1";
       }       

       return true;
   }

</script>

<body>
<form method="post" action="<%= response.encodeURL("admin")%>" onsubmit="updateLicenseNumber()">
 	<input type="hidden"	name="<%=HATSAdminConstants.PARAM_FUNCTION%>" value="<%=HATSAdminConstants.FUNCTION_SET_LICENSE_OPTIONS%>"> 
	<admin:UserMessage />

	<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
	<table width=95% border=1 cellspacing=1 cellpadding=4>
		<tr class="header">
			<td><%= msgs.get("KEY_SET_LICENSE_OPTIONS") %></td>
		</tr>
		<tr>
			<td class="instructions">
			<%= msgs.get("KEY_INSTRUCTIONS_SET_LICENSE_OPTIONS") %>
			<%= bean.isOperator() || bean.isAdministrator() ? msgs.get("KEY_PRESS_OK") : "" %>
			</td>
		</tr>
	</table>
	<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>
		  <admin:Scope />
	
	
	<!-- end of top-buttons/refresh link table -->
	<!------------------------------------------------------------>

	<table class="input-table" border="1" cellpadding="2" cellspacing="1" width="100%">
	 <caption><%= msgs.get("KEY_SET_LICENSE_OPTIONS") %></caption>
		<tr class="header">
			<td colspan="3"></td>
		</tr>
		<tr class="input-table-text">
			<td>
			<fieldset>
			<legend><b><%= studioMsgs.get("EnableRuntimePage.license_type")%></b></legend>
			
            <table border=0 cellpadding=2 cellspacing=5>
              <tr>
                <td nowrap>
<%
	int numLicenses = manager.getNumLicenses();
    boolean isUser = (numLicenses != -1 && numLicenses < 50000);
%>
                  <input id="radio1" onclick="setEnableStates()" type="radio" name="licenseType" value="USER" <%= (isUser ? "checked" : "") %> /> <label for="radio1" ><%= studioMsgs.get("EnableRuntimePage.user")  %></label>
                <td nowrap>
                  <label for="input1" ><%= studioMsgs.get("EnableRuntimePage.number_licenses")  %> </label> <input id="input1" type="text" name="<%=HATSAdminConstants.PARAM_NUM_LICENSES%>" value="<%= manager.getNumLicenses() %>" <%= enabledFlag ? "" : "disabled" %> size="5"><br>
              <tr>
                <td nowrap>
                  <input id="radio2" onclick="setEnableStates()" type="radio" name="licenseType" value="PROCESSOR"  <%= (!isUser ? "checked" : "") %> /> <label for="radio2" ><%= studioMsgs.get("EnableRuntimePage.processor")%></label>
                <td>&nbsp;
            </table>
			</fieldset>
				<br><input id="input2" type="checkbox" name="<%=HATSAdminConstants.PARAM_LICENSE_TRACKING_FLAG%>" <%= manager.isLicenseTrackingOn() ? "checked" : "" %> <%= enabledFlag ? "" : "disabled" %>> <label for="input2" > <%=msgs.get("KEY_LICENSE_TRACKING")%> </label>
				<br><label for="input3" > <%=msgs.get("KEY_LICENSE_FILENAME")%> </label> <input id="input3"  type="text" name="<%=HATSAdminConstants.PARAM_LICENSE_FILENAME%>" value="<%= manager.getLicenseFileName() %>" <%= enabledFlag ? "" : "disabled" %>> 
			   </td>
			<td> <admin:HelpLink topicID="SET_LICENSE_OPTIONS" /> <%= msgs.get("KEY_LICENSE_OPTIONS_DESC") %></td>
		</tr>
	</table>

	<% if (bean.isOperator() || bean.isAdministrator() ) { %>
	<table class="data-footer" width=100% cellspacing=1 cellpadding=7>
		<tr>
			<td><input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.OPERATION_OK%>"	value="<%=msgs.get("KEY_OK")%>">
				 <input class="button" type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>" value="<%=msgs.get("KEY_CANCEL")%>">
			</td>
		</tr>
	</table>
	<!-- end of data-footer table -->
	<% } %>
	
	</td>
	</tr>
</table>

<script language="Javascript">
   setEnableStates();
</script>

<!-- End of the layout manager table--></form>
</body>
</html>
