<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminServlet.setWASSecurityEnabled(true);
	String submitText = msgs.get("KEY_SUBMIT");

	
	if (true)  { // Change this to a check for bidi languages 
	%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>
<HEAD>
<title><%= msgs.get("KEY_LOGIN") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>
<body background="images/login-background.jpg">

   
<p> 
<table width=100%> <tr> <td align="center">
<IMG border="0" alt="" src="images/hatslogo.gif">
</td> </tr> </table>
<FORM ACTION='j_security_check' METHOD='POST'>
<table height=100% width=100%>
<tr>
<td align="center" valign="middle">
<TABLE class="input-table" border="1" width="30%" cellspacing="1" cellpadding="3" >
	
	<TBODY>
	 
	
		
		<TR class="header">
			<TD colspan="2"><%= msgs.get("KEY_LOGIN") %></TD>
		</TR>

		<TR class="input-table-text">


			<TD><%= msgs.get("KEY_USERNAME") %></TD>
			<TD><INPUT type="text" size="20" name="j_username"></TD>
		</TR>
		<TR class="input-table-text">
			<TD><%= msgs.get("KEY_PASSWORD") %></TD>
			<TD><INPUT type="password" size="20" name="j_password"></TD>
		</TR>
		<TR class="footer" align="center">
			<TD colspan="2">
			<P><INPUT class="button" type="SUBMIT" name="submit" value="<%= submitText %>">
			</P>
			</TD>
		</TR>
	</TBODY>
</TABLE>
</td>
</tr>
</table>
</FORM>
</body></html>

