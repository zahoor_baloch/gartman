<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ibm.hats.util.HatsMsgs, com.ibm.hats.runtime.admin.*" %>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%
	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
%>

<html lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<head>
  <title> <%= msgs.get("KEY_ADMINCONSOLE_TITLE") %>  </title>
  <meta name="GENERATOR" content="IBM WebSphere Studio">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="pragma" content="no-cache">

</head>

<frameset rows="75,*" border="1" >
  <frame title="<%= msgs.get("KEY_TITLE_HEADER_FRAME") %>" name="header" src="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_HEADER)%>"  marginwidth="0" noresize >
  <frameset cols="250,*" framespacing=3>
   <frame title="<%= msgs.get("KEY_TITLE_MENU_FRAME") %>"name="menu" src="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MENU)%>" marginheight="20" marginwidth="20" frameborder=0>
   <frame title="<%= msgs.get("KEY_TITLE_CONTENT_FRAME") %>"name="content" src="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_OVERVIEW)%>" marginheight="20" marginwidth="20" frameborder=0>
  </frameset>
</frameset>
<body>
<noframes>
<%= msgs.get("KEY_NO_FRAMES_MESSAGE") %>
</noframes>

</body>

</html>