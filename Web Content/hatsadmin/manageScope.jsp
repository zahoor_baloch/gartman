<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminScopeManager manager = bean.getScopeManager(); // ScopeManager contains all the logic for scope management
	String function = HATSAdminConstants.FUNCTION_MANAGE_SCOPE;
	int start = manager.getStartScope();
	int finish = manager.getFinishScope();
	int numDataItems = manager.getNumHosts();
	int total = numDataItems;
	
	/* Calculate the sorting preference and order */
	String unsortedImage = "<img src=\"images/sort_none.gif\" width=\"9\"  height=\"13\"  alt = \"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
	int sortOrder = bean.getScopeManager().getSortOrderForScopePanel();
  	int descending = sortOrder % 2;
  	String modifier = null;
  	String alt = " alt=\"";
  	if (descending == 1) {
  		modifier = "descend";
  		alt += msgs.get("KEY_DESCENDING") + "\"";
  	} else {
  		modifier = "ascend";
  		alt += msgs.get("KEY_ASCENDING") + "\"";
  	}
  	sortOrder = sortOrder - descending;
  	/* end of sorting calculations */

	if (true)  { // Change this to a check for bidi languages 
%>

<html lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>

<HEAD>
<title><%= msgs.get("KEY_MANAGE_SCOPE") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>

<form method="post" action="<%= response.encodeURL("admin")%>">
<input type="hidden" name="<%=HATSAdminConstants.PARAM_FUNCTION%>" value="<%=HATSAdminConstants.FUNCTION_MANAGE_SCOPE%>"> 
<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4>
	<tr class="header">
		<td><%= msgs.get("KEY_MANAGE_SCOPE") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_MANAGE_SCOPE") %> <admin:HelpLink topicID="MANAGE_SCOPE" /> </td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10	width=95%>
	<tr>
		<td>

		<admin:Scope />

		<table width=100% cellpadding=7>
			<!-- start of top-buttons/refresh link table -->
			<tr>
			<td width=25 ><a href="javascript:document.forms[0].submit();"><img border=0 src="images/refresh.gif" width="16" height="13" alt="<%=msgs.get("KEY_REFRESH")%>" onclick="submit();"
					style="cursor: hand"></a></td> 				
				<td><input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.OPERATION_NEW%>" value="<%=msgs.get("KEY_NEW")%>"> 
				<% if (manager.getNumHosts() != 0) {%>
				<input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.OPERATION_DELETE%>"	value='<%=msgs.get("KEY_DELETE")%>'> 
			    <input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.OPERATION_EDITPORT%>"	value='<%=msgs.get("KEY_EDITPORT")%>'> 
				<% } %></td>

			</tr>
		</table>
		<!-- end of top-buttons/refresh link table -->


		<admin:BrowseButtons function="<%=function%>" start="<%=start%>" finish="<%=finish%>" total="<%=total%>" />

		<!-- start of data table -->
		<table class="data-table" width=100% cellspacing=1 cellpadding=4 border=1 summary=<%= msgs.get("KEY_MANAGE_SCOPE_SUMMARY") %>> 
		<caption><%= msgs.get("KEY_MANAGE_SCOPE_CAPTION") %></caption>
			<tr class="columnheader"> <!-- Row for all the column headers -->

				<% if(numDataItems > 0) { // Extra td needed for checkbox, radio etc %>
				<td width=1%></td>
				<% } %>
				<th ><a	href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_SCOPE + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_SCOPE_HOST)%>" title="<%=msgs.get("KEY_HOST")%>"> <%=msgs.get("KEY_HOST")%></a><%=(sortOrder == manager.SORT_SCOPE_HOST ? "<img src=\"images/sort_" + modifier + ".gif\"" + "width=\"9\"  height=\"13\""  + alt + ">" : unsortedImage)%></th>
				<th ><a	href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_MANAGE_SCOPE + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_SCOPE_PORT)%>" title="<%=msgs.get("KEY_PORT")%>"> <%=msgs.get("KEY_PORT")%></a><%=(sortOrder == manager.SORT_SCOPE_PORT ? "<img src=\"images/sort_" + modifier + ".gif\""  + "width=\"9\"  height=\"13\""  + alt + ">": unsortedImage)%></th>

			</tr>   <!-- end of the column-header row -->
<!-- Start of the data in the data table -->
<% 
if (numDataItems==0)  
{ %>
			<tr class="data-table-row">
				<td colspan=3><%= msgs.get("KEY_NONE") %></td>
			</tr>
<%
}

else  { 
	int currentScopeIndex = manager.getCurrentScopeIndex();
	int defaultScopeIndex = manager.getDefaultScopeIndex(); 
	// System.out.println("defaultIndex = " + defaultScopeIndex +  ", currentIndex = " + currentScopeIndex);
	for (int i = start; i < finish+1; i++) { 	 //start of for loop for displaying the data
 	String encodedID = HATSAdminBean.encode(manager.getUniqueIDForScopeAt(i));
	if (i % 2 == 0) {%>
			<tr class="evenrow">
				<%} else {%>
			<tr class="oddrow">
				<%}%>
				
				<td > <label for="<%="input" + i %>" ><input id="<%="input" + i %>" name="<%= HATSAdminConstants.PARAM_SELECTED_SCOPE %>" value="<%= encodedID %>" type="radio"
				<% if ( i == currentScopeIndex) {   %>
					checked <% } %>> <div style="display:none" > <%= manager.getHostName(i) + " : " + manager.getPort(i) %></div> </label></td>
				 
				<td width="50%">
				<% if (i==defaultScopeIndex) { %>
				<%=manager.getHostName(i) + "<i>   (" + msgs.get("KEY_DEFAULT") + ")</i>" %>
 			    <% } else {%>
 			    <%=manager.getHostName(i) %>
 			    <% }%>
				</td>
				<td ><%=manager.getPort(i)%></td>
				
			</tr>
			<%}%>
			<!-- This page has an extra checkbox in addition to the data table -->
			<tr class="data-table-row">
			<td colspan=3>
				<INPUT id="input2" type="checkbox" name="<%= HATSAdminConstants.PARAM_SELECT_APPLICATION %>" <%=manager.isManagingAllApplications() ? "checked" : "" %> > <label for="input2"><%= msgs.get("KEY_SELECT_APPLICATION_CHECKBOX") %> </label>
			</td>
			</tr>

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table -->



		<!-- start of data-footer table with any buttons if needed -->
		<table class="data-footer" width=100% cellspacing=1 cellpadding=5>
			<tr>
				<td>
				<input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.OPERATION_OK%>"	value="<%=msgs.get("KEY_OK")%>"> 
				<input class="button" type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>"	value="<%=msgs.get("KEY_CANCEL")%>">
				</td>
			</tr>
		</table>
		<!-- end of data-footer table -->

		<% }  // end of (numDataItems !=0)  %>
		</td>
	</tr>
</table> <!-- End of the layout manager table-->
</form>
</body>
</html>


