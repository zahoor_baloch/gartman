<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminScopeManager manager = bean.getScopeManager(); // ScopeManager contains all the logic for scope management
	
	
	int numHosts = manager.getNumHosts();
	if (true)  { // Change this to a check for bidi languages 
	%>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>

<HEAD>
<title><%= msgs.get("KEY_NEW_SCOPE") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
<script language="javascript1.2">
function setFocus() {
	if (document.forms[0].<%=HATSAdminConstants.PARAM_NEW_HOSTNAME%> != null) {
		document.forms[0].<%=HATSAdminConstants.PARAM_NEW_HOSTNAME%>.focus();
	}
	
	if (document.forms[0].<%= HATSAdminConstants.PARAM_DEFAULT_PORT %> != null) {
		document.forms[0].<%= HATSAdminConstants.PARAM_DEFAULT_PORT %>.focus();
	}

}

</script>
</HEAD>

<body  onload="setFocus()">
<form method="post" action="<%= response.encodeURL("admin")%>">
<input type="hidden" name="<%=HATSAdminConstants.PARAM_FUNCTION%>"	value="<%=HATSAdminConstants.FUNCTION_NEW_SCOPE%>"> 
<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4>
	<tr class="header">
		<td><%= msgs.get("KEY_NEW_SCOPE") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_NEW_SCOPE") %> <admin:HelpLink topicID="NEW_SCOPE" /> </td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10	width=95% >
	<tr>
		<td>

		<admin:Scope />

		<table class="data-table" width=100% cellspacing=1 cellpadding=4 border=1 summary=<%= msgs.get("KEY_NEW_SCOPE_SUMMARY") %>> <!-- start of data table -->
			<caption><%= msgs.get("KEY_MANAGE_SCOPE_CAPTION") %></caption>
	
			<tr class="columnheader"> <!-- Row for all the column headers -->

				<% if(numHosts > 0) { // Extra td needed for checkbox, radio etc %>
				<td width=1%></td>
				<% } %>
				<th><label for="input1"><%=msgs.get("KEY_HOST")%></label></th>
				<th><label for="input2"><%=msgs.get("KEY_PORT")%></label></th>

	</tr>   <!-- end of the column-header row -->
<!-- Start of the data in the data table -->
<% 
int length = manager.getNumHosts();
if (length==0)  
{ %>
			<tr class="evenrow">
				<td colspan=3><%= msgs.get("KEY_NONE") %></td>
			</tr>
			<%
}
//int start = manager.getStartConnection(), finish = manager.getEndConnection();
else  { 
	 int editScopeIndex = manager.getIndexForScopeToBeEdited();
	 if (editScopeIndex == -1) {%>
	
	<tr class="oddrow"> 
				<td> <!-- Blank cell to adjust for the radio buttons --> </td>
				<td> <INPUT id="input1" type="text" name="<%= HATSAdminConstants.PARAM_NEW_HOSTNAME %>" size="50"> </td>
				<td> <INPUT id="input2" type="text" name="<%= HATSAdminConstants.PARAM_NEW_PORT %>" size="50"> </td>
	</tr>
	<% } 
	int currentScopeIndex = manager.getCurrentScopeIndex();
	int defaultScopeIndex = manager.getDefaultScopeIndex(); 
	for (int i = 0; i < length; i++) {  //start of for loop for displaying the data
	if (i % 2 == 0) {%>
			<tr class="evenrow">
				<%} else {%>
			<tr class="oddrow">
				<%}%>
				<td >
				<input	name="<%= HATSAdminConstants.PARAM_SELECTED_SCOPE %>" value="<%= manager.getHostName(i) + ":" + manager.getPort(i) %>"	type="radio" disabled
				<% if ( i == currentScopeIndex) {  %>
					checked <% } %>></td>
				<% if (editScopeIndex == -1 || i != defaultScopeIndex) { // Display the localhost in italics %>
				<td ><%=manager.getHostName(i)%></td>
				<td ><%=manager.getPort(i)%></td>
				<% } else { %>
				<td ><I><%=manager.getHostName(i)%> </I></td>
				<td ><INPUT type="text" name="<%= HATSAdminConstants.PARAM_DEFAULT_PORT %>" value="<%=manager.getPort(defaultScopeIndex)%>" size="50" ></td>
				<% } %>
			</tr>
			<%}%>

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table -->


		<!-- start of data-footer table with any buttons if needed -->
		<table class="data-footer" width=100% cellspacing=1 cellpadding=7>
			<tr>
				<td>
				<input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.OPERATION_OK%>"	value="<%=msgs.get("KEY_OK")%>"> 
				<input class="button"	type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>" value="<%=msgs.get("KEY_CANCEL")%>">
				</td>
			</tr>
		</table>
		<!-- end of data-footer table -->

		<% }  // end of (length !=0)  %>
		</td>
	</tr>
</table> <!-- End of the layout manager table-->
</form>
</body>
</html>


