<!doctype HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

    <%@ page contentType="text/html; charset=utf-8"%>
    <%@ page language="java" %>
    <%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
    <%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

    <% 
            // Get the msgs object for generating messages and get the bean from the session
            // for obtaining all the required information

            HatsMsgs msgs = HATSAdminServlet.getMessages(request);
            HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
            HATSAdminConnectionManager manager = bean.getConnectionManager();
            String function = HATSAdminConstants.FUNCTION_VIEW_POOLDEFS;
            int numDataItems = manager.getNumPoolDefinitions();
            int start = manager.getStartPoolDef();
            int finish = manager.getFinishPoolDef();
            int total = numDataItems;
			String unsortedImage = "<img src=\"images/sort_none.gif\"  width=\"9\"  height=\"13\" alt = \"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
            /* Calculate the sorting preference and order */
            int sortOrder = manager.getSortPoolDefPreference();
            int descending = sortOrder % 2;
            String modifier = null;
            String alt = " alt=\"";
            if (descending == 1) {
                    modifier = "descend";
                    alt += msgs.get("KEY_DESCENDING") + "\"";
            } else {
                    modifier = "ascend";
                    alt += msgs.get("KEY_ASCENDING") + "\"";
            }
            sortOrder = sortOrder - descending;

            /* end of sorting calculations */
            if (true)  { // Change this to a check for bidi languages 
            %>
   
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>

    <HEAD>
    <title><%= msgs.get("KEY_VIEW_POOLDEFS") %></title>
    <META name="GENERATOR" content="IBM WebSphere Studio">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
    </HEAD>

    <body>
    <script language="javascript1.2">
        <!-- Hiding JavaScript    
        
        function openWindow(id) {
            var temp = window.open('<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_POOLDEF_DETAILS + "&" + HATSAdminConstants.PARAM_ID + "=")%>' + id, id, 'width=680,height=450,menubar=0,scrollbars=0,status=0,toolbar=0,resizable=yes');
            temp.focus();
            return false;
        }   
        
        // Done hiding --> 
        
    </script>

    <form method="post" action="<%= response.encodeURL("admin")%>">
        <input type="hidden" name="<%=HATSAdminConstants.PARAM_FUNCTION%>" value="<%=HATSAdminConstants.FUNCTION_VIEW_POOLDEFS%>">
        <admin:UserMessage />
    <!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
        <table width="95%" border="1" cellspacing="1" cellpadding="4">
            <tr class="header">
                <td><%= msgs.get("KEY_VIEW_POOLDEFS") %></td>
            </tr>
            <tr>
                <td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_VIEW_POOLDEFS") %> <admin:HelpLink topicID="POOL_DEFINITIONS" /> </td>
            </tr>
        </table>
    <!--End of the instructions table-->
        <p>
    <!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
        <table class="layout-manager" border="0" cellspacing="0" cellpadding="10"
            width="95%">
            <tr>
                <td>

                                <admin:Scope />


                    <table width="100%" cellpadding="7">
    <!-- start of top-buttons/refresh link table -->
                        <tr>
                           <td><a href="javascript:document.forms[0].submit();"><img border="0"src="images/refresh.gif"  width="16"  height="16" alt="<%=msgs.get("KEY_REFRESH")%>" onclick="submit();"  style="cursor: hand"></a> </td>

                        </tr>
                    </table>
    <!-- end of top-buttons/refresh link table -->
		<admin:BrowseButtons function="<%=function%>" start="<%=start%>" finish="<%=finish%>" total="<%=total%>" />

                    <table class="data-table" width="100%" cellspacing="1" cellpadding="4" border="1" summary="<%= msgs.get("KEY_INSTRUCTIONS_VIEW_POOLDEFS") %> ">
                    		<caption><%= msgs.get("KEY_VIEW_POOLDEFS") %></caption>
	
    <!-- start of data table -->
                        <tr class="columnheader">
    <!-- Row for all the column headers -->

                            <th><a
                        href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_VIEW_POOLDEFS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_POOLDEF_POOLDEF_NAME)%>"
                        title="<%=msgs.get("KEY_POOLDEF_NAME_DESC")%>"><%=msgs.get("KEY_POOLDEF_NAME")%></a><%=(sortOrder == manager.SORT_POOLDEF_POOLDEF_NAME? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>
                            <th><a
                        href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_VIEW_POOLDEFS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_POOLDEF_TYPE)%>"
                        title="<%=msgs.get("KEY_TYPE_DESC")%>"><%=msgs.get("KEY_TYPE")%></a><%=(sortOrder == manager.SORT_POOLDEF_TYPE? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">": unsortedImage)%></th>
                            <th><a
                        href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_VIEW_POOLDEFS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_POOLDEF_CONNSPEC)%>"
                        title="<%=msgs.get("KEY_CONNSPEC_DESC")%>"><%=msgs.get("KEY_CONNSPEC")%></a><%=(sortOrder == manager.SORT_POOLDEF_CONNSPEC? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">":unsortedImage) %></th>
                            <th><a
                        href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_VIEW_POOLDEFS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_POOLDEF_LOGONSPEC)%>"
                        title="<%=msgs.get("KEY_LOGONSPEC_DESC")%>"><%= msgs.get("KEY_LOGONSPEC") %></a><%=(sortOrder == manager.SORT_POOLDEF_LOGONSPEC ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>
                            <th><a
                        href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_VIEW_POOLDEFS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_POOLDEF_USERPOOL)%>"
                        title="<%=msgs.get("KEY_USERPOOL_DESC")%>"><%= msgs.get("KEY_USERPOOL") %></a><%=(sortOrder == manager.SORT_POOLDEF_USERPOOL? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>


                        </tr>
    <!-- end of the column-header row -->
    <!-- Start of the data in the data table -->			
    <%!
			String elidedString(String theString, int maxLen) {
			   if (theString == null) return null;
			   int strLen = theString.length();
			   if ((maxLen < 5) || (strLen<=maxLen)) return theString;
			   int keepCount=maxLen - 3;
			   int endCount=keepCount - (keepCount/2);
			   StringBuffer b = new StringBuffer(maxLen);
			   b.append(theString.substring(0, (keepCount)/2)).append("...").append(theString.substring(strLen-endCount));
			   return b.toString();
			   }
			   %>
    <% 

    if (numDataItems==0)  
    { %>

                        <tr class="data-table-row">
                            <td colspan="5"><%= msgs.get("KEY_NONE") %></td>
                        </tr>
                            <%
    }
    //int start = manager.getStartPoolDef(), finish = manager.getEndPoolDef();
    else {
    int lenColHeader0 = Math.max(msgs.get("KEY_POOLDEF_NAME").length(), 30);
	int lenColHeader2 = Math.max(msgs.get("KEY_CONNSPEC").length(), 26);
    
    for (int i = start; i <= finish; i++) {
        String encodedID = manager.getEncodedPoolDefID(i);
            if (i % 2 == 0) {%>

                        <tr class="evenrow">
                            <%} else {%>
                        <tr class="oddrow">
                            <%} %>
                                    <% String[] rowData = manager.getPoolDefinitionBasics(i); 
                                       int column = 0; %>

                            <td class="listItem"><a href="#"
                                            onclick="openWindow('<%=encodedID%>');"
                                            title="<%=msgs.get("KEY_SHOW_DETAILS")+ "   " + rowData[column]%>"><%=elidedString(rowData[column++], lenColHeader0)%></a></td>
                            <td class="listItem"><%=rowData[column++]%></td>
                            <td class="listItem"><a style="text-decoration:none; color:black; cursor:text;" href="#" title="<%=rowData[column]%>"> <%=elidedString(rowData[column++], lenColHeader2)%></a></td>
                            <td class="listItem"><%=rowData[column++]%></td>
                            <td class="listItem"><%=rowData[column++]%></td>

                        </tr>
                            <%  } }  %>


    <!-- end of data rows in the data table -->
                    </table>
    <!-- end of the data table --> <!-- No need for the footer table on this page  so comment it out-->
                    <% /* %>

                    <table class="data-footer" width="100%" cellspacing="1" cellpadding="7">
                        <tr>
                            <td><input class="button" type="SUBMIT"
                                            name="<%=HATSAdminConstants.OPERATION_OK%>"
                                            value="<%=msgs.get("KEY_OK")%>"><input class="button"
                                            type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>"
                                            value="<%=msgs.get("KEY_CANCEL")%>"></td>
                        </tr>
                    </table>
                    <% */ %> <!-- end of data-footer table --></td>
            </tr>
        </table>
    <!-- End of the layout manager table-->
    </form>
    </body>
    </html>


