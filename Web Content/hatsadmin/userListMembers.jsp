<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminUserManager manager = bean.getUserManager();
	String function = HATSAdminConstants.FUNCTION_USER_LIST_MEMBERS;
	int userListIndex = manager.getIndexForSelectedUserList();
	int numDataItems = manager.getNumUserListMembers(userListIndex);
	int start = manager.getStartUserPoolMember();
	int finish = manager.getFinishUserPoolMember();
	int total = numDataItems;
	/* Calculate the sorting preference and order */
	String unsortedImage = "<img src=\"images/sort_none.gif\" alt = \" width=\"9\"  height=\"13\"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
	int sortOrder = manager.getSortUserListMemberPreference();
  	int descending = sortOrder % 2;
  	String modifier = null;
  	String alt = " alt=\"";
  	if (descending == 1) {
  		modifier = "descend";
  		alt += msgs.get("KEY_DESCENDING") + "\"";
  	} else {
  		modifier = "ascend";
  		alt += msgs.get("KEY_ASCENDING") + "\"";
  	}
  	sortOrder = sortOrder - descending;

  	/* end of sorting calculations */

	if (true)  { // Change this to a check for bidi languages 
	%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>

<HEAD>
<title><%= msgs.get("KEY_USER_LIST_MEMBERS") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>


<form method="post" action="<%= response.encodeURL("admin")%>"><input type="hidden"
	name="<%=HATSAdminConstants.PARAM_FUNCTION%>"
	value="<%=HATSAdminConstants.FUNCTION_USER_LIST_MEMBERS%>"> 
	<admin:UserMessage/>

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4>
	<tr class="header">
		<td><%= msgs.get("KEY_USER_LIST_MEMBERS") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_USER_LIST_MEMBERS") %> <admin:HelpLink topicID="USER_LIST_MEMBERS" /> </td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>
	
			<admin:Scope />



		<table width=100% cellpadding=7>
			<!-- start of top-buttons/refresh link table -->
			<tr>
          
				<td><a href="javascript:document.forms[0].submit();" ><img border="0" src="images/refresh.gif"  width="16"  height="16"
					alt="<%=msgs.get("KEY_REFRESH")%>" onclick="submit();"
					style="cursor: hand"></a></td>

			</tr>
		</table>
		<!-- end of top-buttons/refresh link table -->

		<table class="data-table" width="100%" cellspacing="1" cellpadding="4" border="1" summary="<%= msgs.get("KEY_INSTRUCTIONS_USER_LIST_MEMBERS") %> ">
		<caption><%= msgs.get("KEY_USER_LIST_MEMBERS") %></caption>
			<!-- start of data table -->
			<tr class="columnheader">
				<!-- Row for all the column headers -->
			
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LIST_MEMBERS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_USERLISTMEMBER_NAME)%>"
					title="<%=msgs.get("KEY_USERLISTMEMBER_NAME_DESC")%>"> <%=msgs.get("KEY_NAME")%></a><%=(sortOrder == manager.SORT_USERLISTMEMBER_NAME ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LIST_MEMBERS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_USERLISTMEMBER_STATE)%>"
					title="<%=msgs.get("KEY_USERLISTMEMBER_STATE_DESC")%>"> <%=msgs.get("KEY_STATE")%></a><%=(sortOrder == manager.SORT_USERLISTMEMBER_STATE? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">": unsortedImage)%></th>


			</tr>
			<!-- end of the column-header row -->

			<!-- Start of the data in the data table -->
			<% 
			

if (numDataItems==0)  
{ %>
			<tr class="evenrow">
				<td colspan=5><%= msgs.get("KEY_NONE") %></td>
			</tr>
			<%
}
//int start = manager.getStartConnection(), finish = manager.getEndConnection();
else  {
	String[][] data=null;
	 
	 data = manager.getUserPoolMembers(manager.getIndexForSelectedUserList());
	 for (int i = 0; i < numDataItems; i++) {
	 String[] rowData= new String[2];
	 rowData = data[i];
	 if (i % 2 == 0) { %>
			<tr class="evenrow">
				<%} else {%>
			<tr class="oddrow">
				<%} %>

				
				 <%  int column = 0; %>
				
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>

			</tr>
			<%  } } %>

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table --> <!-- No need for the footer table on this page  so comment it out-->
		<% /* %>
		<table class="data-footer" width=100% cellspacing=1 cellpadding=7>
			<tr>
				<td><input class="button" type="SUBMIT"
					name="<%=HATSAdminConstants.OPERATION_OK%>"
					value="<%=msgs.get("KEY_OK")%>"> <input class="button"
					type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>"
					value="<%=msgs.get("KEY_CANCEL")%>"></td>
			</tr>
		</table>
		<% */ %> <!-- end of data-footer table --></td>
	</tr>
</table>
<!-- End of the layout manager table--></form>
</body>
</html>


