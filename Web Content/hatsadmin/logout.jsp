<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);

	if (true)  { // Change this to a check for bidi languages 
	%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>
<HEAD>
<title><%= msgs.get("KEY_LOGOUT") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>
<body background="images/login-background.jpg">

   
<p>
<table border="0" cellpadding="0" cellspacing="0" width="100%"	background="images/background.jpg">
	<tr>
		<td align="left" width="60%"><img src="images/hatslogo.gif"	width="600" height="52" alt="<%=msgs.get("KEY_ADMINCONSOLE_TITLE")%>"></td>
		<td align="right" width="40%"><img src="images/IBM-logo.jpg" width="112" height="52" alt="<%=msgs.get("IBM_LOGO")%>" ></td>
	</tr>
</table>
<TABLE  align="center" border="0" width="100%" cellspacing="1" cellpadding="1" >
<tr align="center">
<td>
<admin:UserMessage />
</td>
</tr>
</TABLE>

<table height=100% width=100%>
<tr>
<td align="center" valign="middle">
<TABLE class="input-table" border="1" width="30%" cellspacing="1" cellpadding="3" >
		
	<TBODY>
	

		<TR class="header">
			<TD><%= msgs.get("KEY_LOGOUT") %></TD>
		</TR>

		<TR class="input-table-text">


			<TD><%= msgs.get("KEY_LOGOUT_SUCCESS") %></TD>
			
		</TR>
		
	</TBODY>
</TABLE>
</td>
</tr>
</table>

</body></html>

