<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
	// Get the msgs object for generating messages and get the bean from the session
	// for obtaining all the required information

	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	HATSAdminUserManager manager = bean.getUserManager();
	String function = HATSAdminConstants.FUNCTION_USER_LISTS;
	int numDataItems = manager.getNumUserLists();
	int start = manager.getStartUserPool();
	int finish = manager.getFinishUserPool();
	int total = numDataItems;
	
	
	//System.out.println("userlists.jsp: numDataItems is " + numDataItems);
		/* Calculate the sorting preference and order */
	String unsortedImage = "<img src=\"images/sort_none.gif\" alt = \" width=\"9\"  height=\"13\"" + msgs.get("KEY_CLICK_TO_SORT") + "\">";
	int sortOrder = manager.getSortUserListPreference();
  	int descending = sortOrder % 2;
  	String modifier = null;
  	String alt = " alt=\"";
  	if (descending == 1) {
  		modifier = "descend";
  		alt += msgs.get("KEY_DESCENDING") + "\"";
  	} else {
  		modifier = "ascend";
  		alt += msgs.get("KEY_ASCENDING") + "\"";
  	}
  	sortOrder = sortOrder - descending;

  	/* end of sorting calculations */

	if (true)  { // Change this to a check for bidi languages 
	%>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>


<HEAD>
<title><%= msgs.get("KEY_USER_LISTS") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>

<body>


<form method="post" action="<%= response.encodeURL("admin")%>"><input type="hidden"
	name="<%=HATSAdminConstants.PARAM_FUNCTION%>"
	value="<%=HATSAdminConstants.FUNCTION_USER_LISTS%>"> <admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width=95% border=1 cellspacing=1 cellpadding=4 >
	<tr class="header">
		<td><%= msgs.get("KEY_USER_LISTS") %></td>
	</tr>
	<tr>
		<td class="instructions"><%= msgs.get("KEY_INSTRUCTIONS_USER_LISTS") %> <admin:HelpLink topicID="USER_LISTS" /> </td>
	</tr>
</table>
<!--End of the instructions table-->

<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border=0 cellspacing=0 cellpadding=10
	width=95%>
	<tr>
		<td>

		<admin:Scope />





		<table width=100% cellpadding=7>
			<!-- start of top-buttons/refresh link table -->
			<tr>
          	<td><a href="javascript:document.forms[0].submit();"><img border="0" src="images/refresh.gif"  width="16"  height="16"
					alt="<%=msgs.get("KEY_REFRESH")%>" onclick="submit();"
					style="cursor: hand"></a></td>

			</tr>
		</table>
		<!-- end of top-buttons/refresh link table -->

		<admin:BrowseButtons function="<%=function%>" start="<%=start%>" finish="<%=finish%>" total="<%=total%>" />

		<table class="data-table" width="100%" cellspacing="1" cellpadding="4" border="1" summary="<%= msgs.get("KEY_INSTRUCTIONS_USER_LISTS") %> ">
			<caption><%= msgs.get("KEY_USER_LISTS") %></caption>
			<!-- start of data table -->
			<tr class="columnheader">
				<!-- Row for all the column headers -->
			
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LISTS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_USERLIST_POOLNAME)%>"
					title="<%=msgs.get("KEY_USERLIST_NAME_DESC")%>"> <%=msgs.get("KEY_USERLIST_NAME")%></a><%=(sortOrder == manager.SORT_USERLIST_POOLNAME ? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LISTS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_USERLIST_EXECUTION_COUNT)%>"
					title="<%=msgs.get("KEY_EXCEPTION_COUNT_DESC")%>"> <%=msgs.get("KEY_EXCEPTION_COUNT")%></a><%=(sortOrder == manager.SORT_USERLIST_EXECUTION_COUNT? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\""  + alt + ">": unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LISTS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_USERLIST_FREE_REQUESTS)%>"
					title="<%=msgs.get("KEY_FREE_REQUESTS_DESC")%>"> <%=msgs.get("KEY_FREE_REQUESTS")%></a><%=(sortOrder == manager.SORT_USERLIST_FREE_REQUESTS? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">": unsortedImage) %></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LISTS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_USERLIST_GET_REQUESTS)%>"
					title="<%=msgs.get("KEY_GET_REQUESTS_DESC")%>"><%= msgs.get("KEY_GET_REQUESTS") %>
				</a><%=(sortOrder == manager.SORT_USERLIST_GET_REQUESTS? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>
				<th><a
					href="<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LISTS + "&" + HATSAdminConstants.PARAM_OPERATION + "=" + HATSAdminConstants.OPERATION_SORT + "&" + HATSAdminConstants.PARAM_SORT + "=" + manager.SORT_USERLIST_USERCOUNT)%>"
					title="<%=msgs.get("KEY_USERCOUNT_DESC")%>"><%= msgs.get("KEY_USERCOUNT") %>
				</a><%=(sortOrder == manager.SORT_USERLIST_USERCOUNT? "<img src=\"images/sort_" + modifier + ".gif\" width=\"9\"  height=\"13\"" + alt + ">" : unsortedImage)%></th>


			</tr>
			<!-- end of the column-header row -->

			<!-- Start of the data in the data table -->
			<% 
			

if (numDataItems==0)  
{ %>
			<tr class="evenrow">
				<td colspan=5><%= msgs.get("KEY_NONE") %></td>
			</tr>
			<%
}
//int start = manager.getStartConnection(), finish = manager.getEndConnection();
else for (int i = start; i < finish+1; i++) {
	String encodedID="";
    encodedID = HATSAdminBean.encode(manager.getUniqueIDForUserListAt(i));
    String[] rowData=new String[5];
    if (i % 2 == 0) { %>
			<tr class="evenrow">
				<%} else {%>
			<tr class="oddrow">
				<%} %>

				<% 
				    rowData = manager.getUserPoolBasics(i); 
				   int column = 0; %>
				<td class="listItem"><a href='<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_USER_LIST_MEMBERS + "&" + HATSAdminConstants.PARAM_ID + "=" + encodedID) %>'> <%=rowData[column++]%></a> </td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>
				<td class="listItem"><%=rowData[column++]%></td>

			</tr>
			<%  }  %>

			<!-- end of data rows in the data table -->
		</table>
		<!-- end of the data table --> <!-- No need for the footer table on this page  so comment it out-->
		<!-- end of data-footer table --></td>
	</tr>
</table>
<!-- End of the layout manager table--></form>
</body>
</html>


