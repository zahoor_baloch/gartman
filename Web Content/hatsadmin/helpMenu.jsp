<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*" %>
<% 
	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
%>
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
 
<head>
<title><%=msgs.get("KEY_MENU_TITLE")%></title>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/adminconsole.css" type="text/css" />
</head>


<body class="navtree">
<script type="text/javascript" language="JavaScript" src="scripts/aptree.js">
</script>

<script type="text/javascript" language="JavaScript1.2">

setShowExpanders(true);
setExpandDepth(1);
setKeepState(false);
setShowHealth(false);
setInTable(false);
setTargetFrame("content");
openFolder = "images/open_folder.gif";
closedFolder = "images/closed_folder.gif";
plusIcon = "images/lplus.gif";
minusIcon = "images/lminus.gif";
blankIcon = "images/blank20.gif";

</script>










<script type="text/javascript" language="JavaScript1.2">
admin_domain = addRoot('images/Domain.gif', '','');

gettingstartedcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_MANAGEMENT_SCOPE") %>", "");
managementscope = addItem(gettingstartedcategory, "images/onepix.gif", "<%= msgs.get("KEY_MANAGE_SCOPE") %>", "<%= response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "MANAGE_SCOPE") %>");
newscope = addItem(gettingstartedcategory, "images/onepix.gif", "<%= msgs.get("KEY_NEW_SCOPE") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "NEW_SCOPE") %>");
selectapplication = addItem(gettingstartedcategory, "images/onepix.gif", "<%= msgs.get("KEY_SELECT_APPLICATION") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "SELECT_APPLICATION") %>");
selectjvm = addItem(gettingstartedcategory, "images/onepix.gif", "<%= msgs.get("KEY_SELECT_JVM") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "SELECT_JVM") %>");
<% if (Util.isZosPlatform()) {  %>
selectserverregion = addItem(gettingstartedcategory, "images/onepix.gif", "<%= msgs.get("KEY_SELECT_SR") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "SELECT_SR") %>");
<% } %>

connmanagementcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_CONNECTION_MANAGEMENT") %> ", "");
hostconnections = addItem(connmanagementcategory,"images/onepix.gif","<%= msgs.get("KEY_HOST_CONNECTIONS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "HOST_CONNECTIONS") %>");
databaseconnections = addItem(connmanagementcategory,"images/onepix.gif","<%= msgs.get("KEY_DB_CONNECTIONS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "DB_CONNECTIONS")%>");
viewconnpools = addItem(connmanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_CONN_POOLS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "CONNECTION_POOLS") %>");
viewpooldefs  = addItem(connmanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_POOL_DEFS") %>","<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "POOL_DEFINITIONS") %>");


usermanagementcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_USER_MANAGEMENT") %>", "");
viewuserlists = addItem(usermanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_USER_LISTS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "USER_LISTS") %>");
viewuserlists = addItem(usermanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_USER_LIST_MEMBERS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "USER_LIST_MEMBERS") %>");



licensemanagementcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_LICENSE_MANAGEMENT") %>", "");
viewlicenseinfo = addItem(licensemanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_LICENSE_USAGE") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "LICENSE_USAGE") %>");
viewlicenseinfo = addItem(licensemanagementcategory, "images/onepix.gif", "<%= msgs.get("KEY_SET_LICENSE_OPTIONS") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "SET_LICENSE_OPTIONS") %>");
<% /* if (bean.isOperator() || bean.isAdministrator()) { %>
securitycategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_SECURITY") %>", "");
applicationpasswords = addItem(securitycategory, "images/onepix.gif", "<%= msgs.get("KEY_ENCRYPTION_KEY") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "ENCRYPTION_KEY") %>");
<% } */ %>
troubleshootingcategory = addItem(admin_domain, "images/onepix.gif", "<%= msgs.get("KEY_TROUBLESHOOTING") %>", "");
viewLog = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_VIEW_LOG") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "VIEW_LOG") %>");
viewLog = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_LOG_OUTPUT") %>", "<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "LOG_OUTPUT")%>");
settraceoptions  = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_SERVER_TRACING") %>","<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "SERVER_TRACING") %>");
settraceoptions  = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_IO_TRACING") %>","<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "IO_TRACING") %>");
settraceoptions  = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_USER_MACRO_TRACING") %>","<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "USER_MACRO_TRACING") %>");
settraceoptions  = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_DISPLAY_TERMINAL") %>","<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" +  HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "DISPLAY_TERMINAL_TRACING") %>");
settraceoptions  = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_IBM_SERVICE_TRACE_OPTIONS") %>","<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "IBM_SERVICE_TRACING_OPTIONS") %>");
settraceoptions  = addItem(troubleshootingcategory, "images/onepix.gif", "<%= msgs.get("KEY_TRACE_OUTPUT") %>","<%=response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "="  + HATSAdminConstants.FUNCTION_HELP + "&" + HATSAdminConstants.PARAM_TOPIC + "=" + "TRACE_OUTPUT") %>");


</script>


<script type="text/javascript" language="JavaScript1.2">
initialize();
</script>

</body>
</html>
