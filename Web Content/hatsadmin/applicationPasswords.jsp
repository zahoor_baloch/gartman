<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.*"%>
<%@ taglib uri="/WEB-INF/tld/hatsadmin.tld" prefix="admin" %>

<% 
    // Get the msgs object for generating messages and get the bean from the session
    // for obtaining all the required information
    HatsMsgs msgs = HATSAdminServlet.getMessages(request);
    HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
    HATSAdminSecurityManager manager = bean.getSecurityManager();
   	boolean enabledFlag = (bean.isMonitor() ? false: true );
    if (true)  { // Change this to a check for bidi languages 
%>

<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } else { %>
<HTML dir="RTL" lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<% } %>
<HEAD>
<title><%= msgs.get("KEY_APPLICATION_PASSWORDS") %></title>
<META name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<LINK rel="stylesheet" href="css/hatsadmin.css" type="text/css">
</HEAD>
<body>

<form method="post" action="<%= response.encodeURL("admin")%>">
<input type="hidden" name="<%=HATSAdminConstants.PARAM_FUNCTION%>" value="<%=HATSAdminConstants.FUNCTION_APPLICATION_PASSWORDS%>">

<admin:UserMessage />

<!--This is the instructions table and is independant of the other page content and contains the page heading and the instructions for the page -->
<table width="95%" border="1" cellspacing="1" cellpadding="4">
	<tr class="header">
		<td><%= msgs.get("KEY_APPLICATION_PASSWORDS") %></td>
	</tr>
	<tr>
		<td colspan="2" class="instructions">
		<%= msgs.get("KEY_INSTRUCTIONS_APPLICATION_PASSWORDS") %>
		<%= bean.isOperator() || bean.isAdministrator() ? msgs.get("KEY_PRESS_OK") : "" %>
		</td>
	</tr>
</table>
<!--End of the instructions table-->


<p><!-- This is the envelope for everything else on the page. It contains a single row and single cell -->
<table class="layout-manager" border="0" cellspacing="0" cellpadding="10" width="95%">
	<tr>
		<td>
		<admin:Scope /> 
		<!-------------------start of input table ------------->
		<table class="input-table" border="1" cellpadding="2" cellspacing="1" width="100%">
			<tr class="header" valign="top">
				<td colspan="3"><%=msgs.get("KEY_APPLICATION_PASSWORDS") %></td>
			</tr>
			<tr class="input-table-text">
				<td> <label for="input1" ><%=msgs.get("KEY_ENCRYPTION_KEY")%> </label> </td>
				<td nowrap>
				<input id="input1" type="text" name="<%=HATSAdminConstants.PARAM_ENCRYPTION_KEY%>" <%= enabledFlag ? "" : "disabled" %>> <br>
				<input id="input2" type="checkbox" name="<%=HATSAdminConstants.PARAM_RETAIN_ENCRYPTION_KEY%>" <%= enabledFlag ? "" : "disabled" %>> <label for="input2"> <%= msgs.get("KEY_RETAIN_ENCRYPTION_KEY")%> </label>
				</td>
				<td> <admin:HelpLink topicID="ENCRYPTION_KEY" /> <%= msgs.get("KEY_ENCRYPTION_KEY_SHORTDESC")%>
				</td>
			</tr>
		</table>
		<% if (bean.isOperator() || bean.isAdministrator()) { %>
		<table class="data-footer" width="100%" cellspacing="1"
			cellpadding="7">
			<tr>
				<td>
				<input class="button" type="SUBMIT"	name="<%=HATSAdminConstants.OPERATION_OK%>"	value="<%=msgs.get("KEY_OK")%>"> 
				<input class="button" type="SUBMIT" name="<%=HATSAdminConstants.OPERATION_CANCEL%>"	value="<%=msgs.get("KEY_CANCEL")%>">
				</td>
			</tr>
		</table>
		<!-- end of data-footer table -->
		
		<% } %>
		</td>
	</tr>
</table>
<!-- End of the layout manager table-->
</form>
</body>
</html>


