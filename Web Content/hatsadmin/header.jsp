<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.util.*, com.ibm.hats.runtime.admin.* "%>
<% 
	HatsMsgs msgs = HATSAdminServlet.getMessages(request);
	HATSAdminBean bean = (HATSAdminBean)session.getAttribute(HATSAdminConstants.BEAN_ID);
	String logoutDest = "ibm_security_logout";
	//AdminTODO testtodo

%>
 
<HTML lang="<%= msgs.get("KEY_ADMIN_LANG")%>" >
<HEAD>
<style type="text/css">
.banner-table  { background-color: #767776; background-image: none; border-right: 1px solid #000000; border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; }
.top-navigation { color: #000000; font-size:70.0%; background-color:#ADB0EC; font-family: Arial,Helvetica, sans-serif; padding-left: 10px; padding-right: 5px;}
a:link.top-nav-item {color: #000000;}
a.top-nav-item  { color: #000000; font-family: Arial,Helvetica,sans-serif; font-weight:bold; text-decoration: none  } 
a:active.top-nav-item  { color: #000000; font-family: sans-serif; }  
a:hover.top-nav-item {  text-decoration: underline}
</style>

</HEAD>

<body leftmargin="0" topmargin="0" marginwidth="0"	marginheight="0">
<FORM METHOD=POST ACTION="<%= logoutDest %> ">
<INPUT TYPE="HIDDEN" name="logoutExitPage" VALUE="logout.jsp">
<table border="0" cellpadding="0" cellspacing="0" width="100%"	background="images/background.jpg">
	<tr>
		<td align="left" width="60%"><img src="images/hatslogo.gif"	width="600" height="52" alt="<%=msgs.get("KEY_ADMINCONSOLE_TITLE")%>"></td>
		<td align="right" width="40%"><img src="images/IBM-logo.jpg" width="112" height="52" alt="<%=msgs.get("IBM_LOGO")%>" ></td>
	</tr>
</table>




<table class="banner-table" border="0" cellpadding="3"
	cellspacing="0" width="100%">
	<tr valign="top">
		<td class="top-navigation">
			<a class="top-nav-item" href="<%= response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_OVERVIEW) %> "
			target="content"> <%= msgs.get("KEY_HOME") %></a>&nbsp;&nbsp; | &nbsp;&nbsp; 
		<% if ( request.isRequestedSessionIdFromCookie() ) { %>
			<a class="top-nav-item" href="<%= response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_PREFERENCES) %> "
			target="content"> <%= msgs.get("KEY_PREFERENCES") %></a>&nbsp;&nbsp;| &nbsp;&nbsp; 
		<% } %>
		<% if (HATSAdminServlet.isWASSecurityEnabled()) { %>
			<a href="logout.jsp" onClick="submit();" class="top-nav-item" 
			target="_parent"> <%= msgs.get("KEY_LOGOUT") %></a>&nbsp;&nbsp; |	&nbsp;&nbsp; 
		<% } %>
			<a class="top-nav-item" href="<%= response.encodeURL("admin?" + HATSAdminConstants.PARAM_FUNCTION + "=" + HATSAdminConstants.FUNCTION_HELP) %> "
			target="help"> <%= msgs.get("KEY_HELP") %></a>&nbsp;&nbsp;	&nbsp;&nbsp;</td>
		<td class="top-navigation" align=right></td>
	</tr>
</table>

</form>
</body>
</HTML>


