<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->

<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css">
<!-- embedded stylesheet to coordinate button and link colors -->
<STYLE TYPE="text/css">
A.TOPBAR:link {	color: white; }
A.TOPBAR:active {color: white; }
A.TOPBAR:visited {color: white; }
A.BOTTOMBAR:link {color: #55839A;  }
A.BOTTOMBAR:active {color: #55839A; }
A.BOTTOMBAR:visited {color: #55839A; }

table.ApplicationKeypad { 
	color: blue; 
	white-space: nowrap;	
}

td.ApplicationKeypad {
	color: blue; 
	white-space: nowrap;	
}

input.HostButton {
  background-color: #484848;
  color: white;
  white-space: pre;
}

input.HostPFKey {
  background-color: #0267CC;
  color: white;
  white-space: pre;

}

input.ApplicationButton {
  background-color: #0267CC;
  color: white; 
  white-space: pre;
  width: 15em;
}

select.ApplicationDropDown {
  width: 15em;
}

A.ApplicationKeyLink:link, A.ApplicationKeyLink:visited{
	color: #ffffff;
}

A.ApplicationKeyLink:hover{
	color: #0065de;
}

.COMPANYTITLE{
    color: #FFFFFF;
    font-size: large;
    font-style: italic;
}

.COMPANYSLOGAN{
    color: #FFFFFF;
    font-size: 1.1em;
    font-style: italic;
}

</STYLE>
</HEAD>
<BODY>
<TABLE width="100%" height="100%" cellpadding="4" cellspacing="0" border="0" bgcolor="#0065DE">
    <TBODY>
        <TR>
            <TD align="left" valign="top" bgcolor="#000000"><span class="COMPANYTITLE">My Company</span></TD>
            <TD width="100%" align="right" valign="bottom" bgcolor="#0065DE"><span class="COMPANYSLOGAN">Innovative technologies designed for the future</span></TD>
            <TD width="40" bgcolor="#0065DE">&nbsp;</TD>
        </TR>
        <TR>          
  			<TD align="left" bgcolor="#000000">   
            <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
            <!-- a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>-->
  			</TD>  		
            <TD width="100%" align="right" valign="bottom" bgcolor="#0065DE">&nbsp;</TD>
            <TD width="40">&nbsp;</TD>
        </TR>
        <TR valign="bottom">
            <TD align="left" bgcolor="#000000" height="24" valign="bottom"></TD>
                <TD width="100%">
				<TABLE background="../common/images/Simple1ButtonBackground.jpg" width="100%" border="0" cellpadding="0" cellspacing="0">
    				<TBODY>
        			<TR>
            			<TD width="60" background="../common/images/Simple1ButtonBackgroundBlue.jpg" nowrap="nowrap"></TD>
            			<TD valign="middle" align="left" background="../common/images/Simple1ButtonBackgroundBlue.jpg">
        					<IMG src="../common/images/Simple1ButtonStart.jpg" alt="" border="0" width="14" height="24" align="right" hspace="0" vspace="0"> 
        				</TD>
            			<TD align="center" background="../common/images/Simple1ButtonBackground.jpg" nowrap="nowrap">
        					<A CLASS="TOPBAR" href="http://www.ibm.com">Home</A>
        				</TD>
            			<TD width="14" background="../common/images/Simple1ButtonBackgroundBlue.jpg" >
        					<IMG src="../common/images/Simple1ButtonEnd.jpg" alt="" align="left" border="0" width="14" height="24" hspace="0" vspace="0"> 
        				</TD>
         				<TD width="60" background="../common/images/Simple1ButtonBackgroundBlue.jpg" ></TD>
            			<TD width="14" background="../common/images/Simple1ButtonBackgroundBlue.jpg">
        					<IMG src="../common/images/Simple1ButtonStart.jpg" alt="" border="0" width="14" height="24" align="right" hspace="0" vspace="0"> 
        				</TD>
            			<TD align="center" nowrap="nowrap">        
        					<A CLASS="TOPBAR" href="http://www.ibm.com">Links</A>
        				</TD>
            			<TD background="../common/images/Simple1ButtonBackgroundBlue.jpg" >
        					<IMG width="14" src="../common/images/Simple1ButtonEnd.jpg" alt="" align="left" border="0" height="24" hspace="0" vspace="0"> 
        				</TD>
            			<TD width="40" background="../common/images/Simple1ButtonBackgroundBlue.jpg" ></TD>
            			<TD width="14" background="../common/images/Simple1ButtonBackgroundBlue.jpg">
        					<IMG src="../common/images/Simple1ButtonStart.jpg" alt="" border="0" width="14" height="24" align="right" hspace="0" vspace="0"> 
        				</TD>
            			<TD align="center" nowrap="nowrap">        
       		 				<A CLASS="TOPBAR" href="http://www.ibm.com">About My Company</A>
        				</TD>
            			<TD width="14" background="../common/images/Simple1ButtonBackgroundBlue.jpg" >
        					<IMG src="../common/images/Simple1ButtonEnd.jpg" alt="" align="left" border="0" width="14" height="24" hspace="0" vspace="0"> 
        				</TD>
            			<TD width="40" background="../common/images/Simple1ButtonBackgroundBlue.jpg"></TD>
            			<TD valign="middle" align="left"  width="14" height="24" background="../common/images/Simple1ButtonBackgroundBlue.jpg">
        					<IMG width="14" src="../common/images/Simple1ButtonStart.jpg" alt="" border="0" height="24" align="right" hspace="0" vspace="0"> 
        				</TD>
            			<TD align="center" nowrap="nowrap">        
        					<A CLASS="TOPBAR" href="http://www.ibm.com">Stock Quote</A>
        				</TD>
            			<TD width="14" background="../common/images/Simple1ButtonBackgroundBlue.jpg" >
        					<IMG src="../common/images/Simple1ButtonEnd.jpg" alt="" align="left" border="0" width="14" height="24" hspace="0" vspace="0"> 
        				</TD>
         				<TD width="60" background="../common/images/Simple1ButtonBackgroundBlue.jpg" ></TD>
        			</TR>                
    				</TBODY>
				</TABLE>        
        	</TD>
           <TD width="40">&nbsp;</TD>
        </TR>
        <TR>
            <TD background="../common/images/simple1bottom1.jpg" height="9"></TD>
            <TD width="100%" background="../common/images/simple1bottom2.jpg"></TD>
            <TD width="40" background="../common/images/simple1bottom2.jpg"></TD>
        </TR>
        <TR bgcolor="#313031">
        	<TD align="center" valign="top" height="100%">
        	<DIV align="center">
            	<TABLE height="100%" cellpadding="3" cellspacing="3" border="0" >
        			<TBODY>
					<TR>
						<TD bgcolor="#313031" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">My Company Home Page</A>		
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#313031" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">My Company Map</A> 
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#313031" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">My Company Employees</A> 
						</TD>
					</TR>			
					<TR>
						<TD bgcolor="#313031" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">Jobs at My Company</A> 
						</TD>
					</TR>						
					<TR>
						<TD bgcolor="#313031" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">My Company Articles</A>
						</TD>
					</TR>						
					<TR>
						<TD bgcolor="#313031" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">Support</A>
							<BR>
						</TD>
					</TR>
					<TR>
						<TD height="100%" bgcolor="#313031" valign="top">
							<HATS:ApplicationKeypad/>
						</TD>
					</TR>														
        			</TBODY>
        		</TABLE>
        	</DIV>
         </TD>
        <TD width="100%" height="100%" valign="top" bgcolor="white">
        <a name="navskip"></a>
 		<HATS:Transform skipBody="true">
    	<DIV align="center">
    	<P> Host screen transformation will be shown here </P></DIV>
 		</HATS:Transform>
         </TD>
         <TD width="40"></TD>
        </TR>        
        <TR>
            <TD align="left" bgcolor="#313031" height="16"></TD>
            <TD colspan="2" width="100%" align="center" valign="bottom" bgcolor="#313031">
            <A CLASS="TOPBAR" href="http://www.ibm.com">Contact Us</A>&nbsp;&nbsp;&nbsp;&nbsp;
            <A CLASS="TOPBAR" href="http://www.ibm.com">Human Resources</A>&nbsp;&nbsp;&nbsp;&nbsp;
            <A CLASS="TOPBAR" href="http://www.ibm.com">Employee Information</A>&nbsp;&nbsp;&nbsp;&nbsp;
            </TD>
        </TR>               
        </TBODY>
</TABLE>
</BODY>
</HTML>

