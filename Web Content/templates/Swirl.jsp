<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=1.6,0,04/07/28,16:52:09  -->

<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css">
<style type="text/css">
INPUT.ApplicationButton {
	border: black solid 1px;
	color: white;
	background-color: #408080;
}

INPUT.HostButton, INPUT.HostPFKey {
	border: black solid 1px;
	color: white;
	background-color: #0f4857;
}

table.ApplicationKeypad {
	border: #666666 solid 1px;
}

table.HostKeypad {
	border: #666666 solid 1px;
}

A.HATSLINK:link, A.HATSLINK:visited, A.ApplicationKeyLink:link, A.ApplicationKeyLink:visited, A.HostKeyLink:link, A.HostKeyLink:visited {
	color: #1d88a3;
}

A.HATSLINK:hover, A.ApplicationKeyLink:hover, A.HostKeyLink:hover {
	color: #5555ff;
}

.HATSSTATUSHEADER {
	border-color: black;
	background-color: #0f4857;
}

.COMPANYTITLE {
    color: white;
    font-size: 1.9em; 
    font-weight: bold; 
}

.MASTERTABLE{
    border: solid #cccccc 1px;
} 
 
</style>

</HEAD>
<body marginheight="2" marginwidth="2" topmargin="2" leftmargin="2">
<table width="792" border="0" cellpadding="0" cellspacing="0" CLASS="MASTERTABLE">
<tr>
<td valign="middle" height="56" background="../common/images/iceskate002ba800.gif">
<span CLASS="COMPANYTITLE"> &nbsp;My Company </span>
</td>
</tr>
<!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
<!--      
<TR>
	<TD>
	<a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
  	</TD>
</TR>
-->
<tr>
<td valign="top" align="center">
<br>
<a name="navskip"></a>
 <HATS:Transform skipBody="true">
    <P> Host screen transformation will be shown here </P>
 </HATS:Transform>
</td>
</tr>
<tr>
<td align="center">
<HATS:ApplicationKeypad settings="layout:horizontal"/>
</td>
</tr>
</table>
</BODY>
</HTML>
