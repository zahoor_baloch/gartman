<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->
<HEAD>
  <TITLE><HATS:Util type="applicationName" /></TITLE> <HATS:Util type="baseHref" />
  <META name="GENERATOR" content="IBM WebSphere Studio">
  <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- See the User's and Administration Guide for information on using templates and stylesheets -->
  <!-- Global Style Sheet -->
  <LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
  <LINK rel="stylesheet" href="../common/stylesheets/slickWhiteBackground.css" type="text/css">
  <LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css">
  <!-- embedded stylesheet to coordinate button and link colors -->
  <STYLE type="text/css">
     A.TOPBAR:link {color:white; font-size: 1em;}
     A.TOPBAR:active {color:white; font-size: 1em;}
     A.TOPBAR:visited {color:white; font-size: 1em;}
     
     A.BOTTOMBAR:link {color:black}
     A.BOTTOMBAR:active {color:black}
     A.BOTTOMBAR:visited {color:black}
     
     A.ApplicationKeyLink {
        color: white;
     }
     
     A.HATSLINK:link, A.HATSLINK:visited, A.HostKeyLink:link, A.HostKeyLink:visited {
        color: #841412;
     }
     
     input.ApplicationButton, select.ApplicationDropDown {
        width: 14em;
     }
     
     .COMPANYTITLE{
        color: #FFFFFF;
        font-size: xx-large;
         font-style: italic;
     }
     
     .COMPANYSLOGAN{
         color: #FFFFFF;
         font-size: large;
         font-style: italic;
     }
        
    .LINKSHEADER{
       color:  black;
       font-size: medium;     
     }
  </STYLE>
</HEAD>
  <BODY>
  <TABLE width="100%" cellpadding="4" cellspacing="0"  border="0">
    <TBODY>
      <TR>
        <TD colspan="3" align="left"  background="../common/images/EagleInFlight.jpg" height="100">
          <SPAN class="COMPANYTITLE">
          &nbsp;&nbsp;My Company
          </SPAN>
        </TD>
      </TR>
      <TR>
        <TD background="../common/images/FloweringTree.jpg"    height="100%" align="left" valign="top" width="225">
          <DIV align="center">
            <TABLE height="100%" cellpadding="3" cellspacing="3" border="0">
              <TBODY>
              <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
              <!-- 
              <TR valign="top"> 
            	<TD align="left"  colspan="2" >
        		<a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0"></a>
        		</TD>
       		  </TR> 
       		  -->
              <TR>
                <TD nowrap="nowrap" valign="top" width="225">
                <DIV align="center">
                  <STRONG>			
                  <span CLASS="LINKSHEADER">My Company Links </span><BR>
                  <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Home Page</A> <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Map</A> <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Employees</A> <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Jobs at My Company</A> <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Articles</A> <BR>
                  <BR>
                  <span CLASS="LINKSHEADER">My Products</span><BR>
                  <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A> <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A> <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A> <BR>
                  <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A> <BR>                  
                  </STRONG>                  
                  </DIV>
                  &nbsp; <BR>
                  &nbsp; <BR>
                  <HATS:ApplicationKeypad/>
                </TD>
              </TR>
              </TBODY>
            </TABLE>
          </DIV>
        </TD>
        <TD height="100%" valign="top" width="78%">
        <a name="navskip"></a>
        <HATS:Transform skipBody="true">
        <DIV align="center"><P> Host screen transformation will be shown here </P></DIV>
        </HATS:Transform>        
        </TD>
        </TR>
    </TBODY>
  </TABLE>
  </BODY>
</HTML>
