<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->

<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/blacktheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoBlack.css" type="text/css">
<!-- embedded stylesheet to coordinate button and link colors -->
<STYLE type="text/css">
A.BOTTOMBAR:link {color:white}
A.BOTTOMBAR:active {color:white}
A.BOTTOMBAR:visited {color:white}

table.ApplicationKeypad
{ 
	color: blue; 
	white-space: nowrap;	
}

td.ApplicationKeypad
{
	color: blue; 
	white-space: nowrap;	
}

A.ApplicationKeyLink:link, A.ApplicationKeyLink:visited {
	color: white;
}

input.HostButton {
  background-color: #C29130;
  color: white;
  white-space: pre;
}

input.HostPFKey {
  background-color: #CC6633;
  color: white;
  white-space: pre;
}

input.ApplicationButton {
  background-color: #CC6633;
  color: white;
  white-space: pre;
  width: 15em;
}

select.ApplicationDropDown {
  background-color: #9A6600;
  color: white;
  width: 15em;
}

.HATSSTATUSHEADER, .HATSSTATUSFOOTER {
	color: white;
	background-color: #9b5e26;
}

.COMPANYTITLE{
   font-size: x-large;
   color:#FFFFFF;
   font-style: italic;
}

.COMPANYSLOGAN{
   font-size: medium;
   color:#FFFFFF;
   font-style: italic;
}

.LINKSTITLE{
   font-size: small;
   color:#FFC262;
}

</STYLE>
</HEAD>
<BODY>
<TABLE bgcolor="black" width="100%" cellspacing="0" border="0" cellpadding="0">
    <TBODY>
        <TR>
            <TD  colspan="2" align="left" bgcolor="#CC6633"><SPAN CLASS="COMPANYTITLE">My Company</SPAN></TD>
        </TR>
        <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
        <!-- 
        <TR>
            <TD> <a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a></TD>
            <TD colspan="2" align="center" valign="bottom" bgcolor="#000000"><SPAN CLASS="COMPANYSLOGAN">Your company slogan goes here...</SPAN></TD>
        </TR>
        -->
        <TR valign="top">
            <TD align="left" bgcolor="#000000">&nbsp;</TD>
                <TD width="100%" valign="top">
				<TABLE bgcolor="black" width="100%" border="0" cellpadding="3" cellspacing="3">
    				<TBODY>
    				<TR>
            			<TD bgcolor="#CD9933" align="center" nowrap="nowrap">
        					<A CLASS="BOTTOMBAR" href="http://www.ibm.com">First Link</A>
        				</TD>
            			<TD bgcolor="#656600" align="center" nowrap="nowrap">        
        					<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Second Link</A>
        				</TD>
            			<TD bgcolor="#983233" align="center" nowrap="nowrap">        
       		 				<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Third Link</A>
        				</TD>
            			<TD bgcolor="#9A6600" align="center" nowrap="nowrap">        
        					<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Fourth Link</A>
        				</TD>
         			</TR>                
    				</TBODY>
				</TABLE>        
        	</TD>
        </TR>
        <TR bgcolor="#000000">
        	<TD height="100%" align="center" valign="top">
        	<DIV align="center">
            	<TABLE background="../common/images/Corporate3left.jpg" height="100%" cellpadding="3" cellspacing="3" border="0">
        			<TBODY>
					<TR>
         				<TD nowrap="nowrap" valign="top">
							<STRONG CLASS="LINKSTITLE">My Company Links</STRONG>
							<BR>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Home Page</A>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Map</A>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Employees</A>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">Jobs at My Company</A>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Articles</A>
							<BR>
							<BR>
							<STRONG CLASS="LINKSTITLE">My Products</STRONG>
							<BR>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A>
							<BR><A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A>
							<BR>&nbsp;
							<BR>&nbsp;
							<BR>
							<HATS:ApplicationKeypad/>
						</TD>
					</TR>									
        			</TBODY>
        		</TABLE>
        	</DIV>
         </TD>
        <TD width="100%" height="100%" bgcolor="black" valign="top">
        <a name="navskip"></a>
 <HATS:Transform skipBody="true">
    <DIV align="center"><P> Host screen transformation will be shown here </P></DIV>
 </HATS:Transform>
 </TD>
         </TR>        
        </TBODY>
</TABLE>
</BODY>
</HTML>
