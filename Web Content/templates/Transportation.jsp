<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->
<HEAD>
  <title><HATS:Util type="applicationName" /></title> <HATS:Util type="baseHref" />
  <META name="GENERATOR" content="IBM WebSphere Studio">
  <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
   
  <!-- See the User's and Administration Guide for information on using templates and stylesheets -->
  <!-- Global Style Sheet --> 
  <LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
  <LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css">
  <!-- embedded stylesheet to coordinate button and link colors -->
  <STYLE type="text/css">
  A.BOTTOMBAR:link {color:black}
  A.BOTTOMBAR:active {color:black}
  A.BOTTOMBAR:visited {color:black}
  table.ApplicationKeypad
  {
      background: inherit;
      white-space: nowrap;
  }
  td.ApplicationKeypad
  {
      background: inherit;
      white-space: nowrap;
  }

  input.ApplicationButton, select.ApplicationDropDown {
       width: 15em;
  }
  input.HostButton {
    background-color: #C29130;
    color: white;
    white-space: pre;
  }

  .COMPANYTITLE {
    color:  #660000;
    font-size: xx-large;
    font-style: italic;
  }
  .LINKSHEADER {
    color:  #660000;
    font-size: medium;
  }
  </STYLE>
</HEAD>
  <BODY>
  <TABLE width="100%" cellpadding="4" cellspacing="0" border="0" >
    <TBODY>
    <TR>
      <TD colspan="3" align="left"  background="../common/images/TransportationHeader.jpg" height="125">
        <SPAN class="COMPANYTITLE">
        &nbsp; &nbsp;&nbsp;&nbsp;My Company
        </SPAN>
      </TD>
    </TR>
    <TR>
    <TD background="../common/images/TrainSignalSidebar.jpg"  width="215" height="100%" align="center" valign="top">
        <DIV align="center">
          <TABLE height="100%"  border="0" width="215">
            <TBODY>
            <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
            <!-- 
            <TR>
				<TD>
				<a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
  				</TD>
  			</TR>
  			-->
            <TR>
              <TD nowrap="nowrap"  height="500" valign="top">
              <BR>
              <STRONG>
              <span CLASS="LINKSHEADER">My Company Links </span><BR>
                <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Home Page</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Map</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Employees</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Jobs at My Company</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Articles</A> <BR>
                <BR>
                <span CLASS="LINKSHEADER">My Products </span><BR>
                <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A> <BR><br><br>
                </STRONG>
               
                &nbsp; <BR>
                &nbsp; <BR>
                <HATS:ApplicationKeypad/></TD>

            </TR>
            </TBODY>
          </TABLE>
        </DIV>
      </TD>
    <TD nowrap="nowrap" height="100%" width="5" align="left" background="../common/images/ThinGreyVertical.jpg"></TD>
      <TD width="100%" height="100%" valign="top" align="center"><HATS:Transform skipBody="true">
      <a name="navskip"></a>
        <DIV align="center">
          <P>
          Host screen transformation will be shown here
          </P>
        </DIV>
        </HATS:Transform>
      </TD>
    </TR>
    </TBODY>
  </TABLE>
  </BODY>
</HTML>