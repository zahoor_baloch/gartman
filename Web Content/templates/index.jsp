<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.ibm.hats.common.*"%><html xmlns="http://www.w3.org/1999/xhtml">
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="java.net.*"%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gartman HATS Phase1</title>

<link href="../common/css/style.css" rel="stylesheet" type="text/css" />
<link href="../common/css/menu.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
//window.onload = init;
var js_isPin = 'test';
function init() {	
	if(document.getElementById("isMouseMoveAllowed").value != "false"){
		document.onmousemove = getCursorXY;		
	}
}

function getCursorXY(e) {
	//alert("getCursorXy");
	var isIE = document.all;	
	if (!e) e = window.event;
	
	if (e)
	{ 
	
	document.getElementById('cursorX').value = isIE ? (e.clientX + document.body.scrollLeft) : e.pageX;
	document.getElementById('cursorY').value = isIE ? (e.clientY + document.body.scrollTop) : e.pageY;

	if(document.getElementById('cursorX').value < 95 || document.getElementById('cursorX').value > 263 || document.getElementById('cursorY').value < 68 || document.getElementById('cursorY').value > 616 )
	{
	
	hideTree();
	}
	
	else if (document.getElementById('cursorX').value >= 95 || document.getElementById('cursorX').value <= 263 || document.getElementById('cursorY').value >= 68 || document.getElementById('cursorY').value <= 616){
	showTree();
	
	}
	}
}

function inity() {	
	
	
	if(document.getElementById("isMouseMoveAllowed").value != "false"){	
		document.onmouseover = getCurXY;
		
	}
	
}

function getCurXY(e) {	
	//alert('in index ');
	var isIE = document.all;		
	if (!e) e = window.event;
	
	if (e)
	{ 
	
	document.getElementById('curX').value = isIE ? (e.clientX + document.body.scrollLeft) : e.pageX;
	document.getElementById('curY').value = isIE ? (e.clientY + document.body.scrollTop) : e.pageY;

	
	if( document.getElementById('cursorX').value < 1320 || document.getElementById('cursorX').value > 1340 ){
		//var hatsFrame = parent.hatsFrame;
		var hatsFrame = window.hatsFrame;
		if(typeof hatsFrame != 'undefined'){
			
				hatsFrame.hideRightTree();
			
		}
	}
	}	
}
</script>

<script language="javascript">
window.history.forward(1);

function signOff() {
	var hatsFrame = parent.hatsFrame; 
	hatsFrame.ms('macrorun_LogoutMacro', 'hatsportletid');
}

function hide() {
	/*document.getElementById("hatsFrame").width="1208";
	window.menuFrame.document.getElementById("MenuContent").className = "contentbgmainleftempty";
	window.menuFrame.document.getElementById("MenuContent2").style.display = "none";
	window.menuFrame.document.getElementById("showbox").style.display = "none";
	window.menuFrame.document.getElementById("hidebox").style.display = "block";
	window.hatsFrame.document.getElementById("colcenterover").className = "contentbgmainrightfull";
	document.getElementById("menuFrame").width="27";*/
}
function show() {
	/*document.getElementById("hatsFrame").width="1050";
	window.menuFrame.document.getElementById("MenuContent").className = "contentbgmainleft";
	window.menuFrame.document.getElementById("MenuContent2").style.display = "block";
	//window.menuFrame.document.getElementById("showbox").style.display = "block";
	//window.menuFrame.document.getElementById("hidebox").style.display = "none";
	window.hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright";
	document.getElementById("menuFrame").width="185";*/
}

function setValuesAndGoTo1(val1) {
	var hatsFrame = parent.hatsFrame; 
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	hatsFrame.ms('macrorun_MasterMenuLevel01Macro', 'hatsportletid');
}
function setValuesAndGoTo2(val1, val2) {
	var hatsFrame = parent.hatsFrame; 
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	hatsFrame.ms('macrorun_MasterMenuLevel02Macro', 'hatsportletid');
}
function setValuesAndGoTo3(val1, val2, val3) {
	var hatsFrame = parent.hatsFrame; 
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV3.value = val3;
	hatsFrame.ms('macrorun_MasterMenuLevel03Macro', 'hatsportletid');
}
function setValuesAndGoTo4(val1, val2, val3, val4) {
	var hatsFrame = parent.hatsFrame;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV3.value = val3;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV4.value = val4;
	hatsFrame.ms('macrorun_MasterMenuLevel04Macro', 'hatsportletid');
}
function setValuesAndGoTo5(val1, val2, val3, val4, val5) {
	var hatsFrame = parent.hatsFrame;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV3.value = val3;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV4.value = val4;
	hatsFrame.document.forms["HATSForm"].hatsgv_promptGV5.value = val5;
	hatsFrame.ms('macrorun_MasterMenuLevel05Macro', 'hatsportletid');
}

function showTree() {
	if(document.getElementById("isMouseMoveAllowed").value != "false"){
		init();
	}
	var menuFrame = window.menuFrame;
	var hatsFrame = window.hatsFrame;
		
		if(typeof menuFrame != 'undefined' && menuFrame.document.getElementById('treeMenu_ltr_27') != null){
			
			menuFrame.document.getElementById('treeMenu_ltr_27').setAttribute('id', 'treeMenu_ltr_185');
		}
		
		menuFrame.document.getElementById('MenuContent').style.display = "block";	
		if(js_isPin != "true"){
			document.getElementById("hatsFrame").width="975";
			document.getElementById("hatsFrame").align="left";
			menuFrame.document.getElementById("MenuContent2").style.display = "block";
			hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright4";
		}else{
			document.getElementById("hatsFrame").width="975";
			document.getElementById("hatsFrame").align="left";
			menuFrame.document.getElementById("MenuContent2").style.display = "block";
			hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright3";
		}
		document.getElementById("menuFrame").style.width = "260px";
		if(js_isPin != "true"){
			hatsFrame.document.getElementById('mainbtnbar').style.display="none";
		}		
}

function hideTree() {	

	var menuFrame = window.menuFrame;
	var hatsFrame = window.hatsFrame;
			
	document.getElementById("isMouseMoveAllowed").value = "true";
	hatsFrame.document.getElementById("gdb").setAttribute('onmousemove', 'initt();');	
	document.getElementById("indexbody").setAttribute('onmousemove', 'inity();');
	
	if(typeof menuFrame != 'undefined' && menuFrame.document.getElementById("treeMenu_ltr_185") != null){	
		menuFrame.document.getElementById("treeMenu_ltr_185").setAttribute('id', 'treeMenu_ltr_27');		
	
	menuFrame.document.getElementById('MenuContent').style.display = "none";
	document.getElementById("hatsFrame").width="1208";
	menuFrame.document.getElementById("MenuContent2").style.display = "none";
	if(js_isPin != "true"){
		hatsFrame.document.getElementById("colcenterover").className = "contentbgmainrightfull";
	}else{
		hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright";
	}
	document.getElementById("menuFrame").style.width = "27px";
	}
}



</script>
<style type="text/css">
	
	#menuFrame{
		display: block;
		width: 25px;	
	}
		
</style>
</head>

<body id="indexbody">
<div class="clear2px"></div>
<input type="hidden" id="cursorX" size="3"/>
<input type="hidden" id="cursorY" size="3"/>

<input type="hidden" id="curX" size="3"/>
<input type="hidden" id="curY" size="3"/>

<input type="hidden" id="checkMaster" value="check" />
<input type="hidden" id="check" value="check" />
<input type="hidden" id="cursorXY" value="test" />
<input type="hidden" id="isMouseMoveAllowed" value="false" />
<input type="hidden" id="curXY" value="test" />
<input type="hidden" id="menuFrameWidth" value="test" />



<div class="logoheader">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="9"><img src="../common/images/header_left.gif" width="9" height="55" /></td>
    <td valign="top" background="../common/images/header_rpt.gif">
    <div class="logo2" style="float:left;"><img src="../common/images/logo.jpg" width="217" height="43" /></div>
    <div class="logo3" style="float:right;"><img src="../common/images/logo2.jpg" width="68" height="50" /></div>
    </td>
    <td width="9"><img src="../common/images/header_right.gif" width="9" height="55" /></td>
  </tr>
</table>

</div>

<div class="main">

<div class="container">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" align="center">
            <%//System.out.println("inner html "+ session.getAttribute("html"));
             %>
			<div id="menuFrameDiv"> <% if (session.getAttribute("html") != null) {
				out.print(session.getAttribute("html")); } %> </div>
            </td>
            <td width="2"></td>
            <td align="center">
<%
	String URL = "";
	String serverName  = request.getServerName();
	int serverPortNum  = request.getServerPort();
	String contextPath = request.getContextPath();
	URL = "http://" + serverName + ":" + serverPortNum + contextPath + "/entry";
	/*System.out.println("serverName"+serverName);
	System.out.println("serverPortNum"+ serverPortNum );
	System.out.println("contextPath"+contextPath);
	System.out.println("URL"+URL);*/
	/* InetAddress ownIP=InetAddress.getLocalHost();
	//int portn = request.getServerPort();
	System.out.println("IP of my system is := "+ ownIP.getHostAddress());
	String linkToHost = "http://"+ownIP.getHostAddress()+":9080/Gartman/entry";
	System.out.println(linkToHost);*/

%>
			<iframe align="middle" id="hatsFrame" name="hatsFrame" src="<%=URL %>" width="1208" scrolling="no" frameborder="0" height="527"></iframe>
			<!--<iframe align="middle" id="hatsFrame" name="hatsFrame" src="http://as400.gartman.com:33000/Gartman/entry" width="1208" scrolling="no" frameborder="0" height="527"></iframe>-->
<!--			 <iframe align="middle" id="hatsFrame" name="hatsFrame" src="http://as400.gartman.com:33000/Gartman/entry" width="1050" scrolling="no" frameborder="0" height="527"></iframe> -->
<!-- http://as400.gartman.com:33000/Gartman/entry			<iframe align="middle" id="hatsFrame" name="hatsFrame" src="" width="805" scrolling="no" frameborder="0" height="517"></iframe>-->

            </td>
          </tr>
        </table>

</div>

<div class="footer">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="../common/images/footer_left.gif" width="10" height="26" /></td>
    <td background="../common/images/footer_rpt.gif"><div class="fottxt">&copy; 2010 Gartman. All rights reserved.</div></td>
    <td width="10"><img src="../common/images/footer_right.gif" width="10" height="26" /></td>
  </tr>
</table>

</div>
</div>
<%System.out.println("..............index"); %>
</body>

</html>
