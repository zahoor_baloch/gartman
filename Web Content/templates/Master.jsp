<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@ page import="com.ibm.hats.common.*"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../common/css/style.css" rel="stylesheet" type="text/css" />
<link href="../common/css/menu.css" rel="stylesheet" type="text/css" />

<!--Tree Menu Start-->
<link rel="stylesheet" href="../common/css/jquery.treeview.css" />
<script type="text/javascript" src="../common/js/jquery.min.js"></script>
<script src="../common/lib/jquery.cookie.js" type="text/javascript"></script>
<script src="../common/js/jquery.treeview.js" type="text/javascript"></script>

<script type="text/javascript" src="../common/js/demo.js"></script>
<!--Tree Menu End-->

<%
/*String isPin = "";
try{
isPin = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isPin", true).getString(0);
}catch(Exception ex){
System.out.println("In Master.jsp..."+ex);*/
//}
//System.out.println("In MAster.jsp......isPin..."+isPin);
%>

<script>
	var hatsFrame = parent.hatsFrame;
	
	
	var js_isPin = '';
	
	if(typeof hatsFrame.js_isPin != 'undefined'){		
		js_isPin = hatsFrame.js_isPin;
	}else{
		js_isPin = '';
	}
		
	function showBothTrees(){
				
		var isMaster = parent.document.getElementById("checkMaster").value;
		
		if(isMaster === "Master" && js_isPin != "true"){
			parent.document.getElementById("check").value="Master";
			parent.document.getElementById("checkMaster").value = "Default";
			hatsFrame.document.getElementById("pin").style.display = "block";
			
			parent.document.getElementById("hatsFrame").width="1075";
			parent.document.getElementById("hatsFrame").align="left";
			document.getElementById('treeMenu_ltr_27').setAttribute('id', 'treeMenu_ltr_185');
			document.getElementById("MenuContent2").style.display = "block";
			document.getElementById('MenuContent').style.display = "block";		
			parent.document.getElementById("menuFrame").style.width = "160px";
			hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright_2";
					
			hatsFrame.document.getElementById('treeMenu_rtl_05').setAttribute('id', 'treeMenu_rtl_10');		
			hatsFrame.document.getElementById("mainbtnbar").style.display="block";
			hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright_2";
			parent.document.getElementById("menuFrameWidth").value = "160";
			treeTimeout();		
		}else{
			parent.document.getElementById("checkMaster").value = "Default";
		}	
	}
	
	var hatsFrame = parent.hatsFrame;
		
function hideBothTrees() {
	
	//alert('in master hidebthtrees');
	hatsFrame.document.getElementById("pin").style.display = "none";
	document.getElementById("treeMenu_ltr_185").setAttribute('id', 'treeMenu_ltr_27');
	document.getElementById('MenuContent').style.display = "none";
	parent.document.getElementById("hatsFrame").width="1208";
	document.getElementById("MenuContent2").style.display = "none";
	hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright";
	parent.document.getElementById("menuFrame").style.width = "27px";
	
	hatsFrame.document.getElementById('treeMenu_rtl_10').setAttribute('id', 'treeMenu_rtl_05');
	hatsFrame.document.getElementById("mainbtnbar").style.display="none";
	hatsFrame.document.getElementById("colcenterover").className = "contentbgmainrightfull";
	
	parent.document.getElementById("isMouseMoveAllowed").value = "true";
	hatsFrame.document.getElementById("gdb").setAttribute('onmousemove', 'initt();');	
	parent.document.getElementById("indexbody").setAttribute('onmousemove', 'inity();');
		//alert(parent.document.getElementById("menuFrame").style.width );	  	
}
function treeTimeout(){	
	setTimeout("hideBothTrees()",3000);	
}

function showRightTreePin() {	
	hatsFrame.iniit();
	parent.document.getElementById("indexbody").setAttribute('onmousemove', 'inity();');
		if(hatsFrame.document.getElementById('treeMenu_rtl_05') != null){
			hatsFrame.document.getElementById('treeMenu_rtl_05').setAttribute('id', 'treeMenu_rtl_10');
		}
		
			hatsFrame.document.getElementById("unpin").style.display = "block";
			
		if(parent.document.getElementById("isMouseMoveAllowed").value != "false" && document.getElementById('treeMenu_ltr_27') != null){
			hatsFrame.document.getElementById("mainbtnbar").style.display="block";
			hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright";
		}else if(parent.document.getElementById("isMouseMoveAllowed").value != "false" && document.getElementById('treeMenu_ltr_27') == null){
			hatsFrame.document.getElementById("mainbtnbar").style.display="block";			
			hatsFrame.document.getElementById("colcenterover").className = "contentbgmainright3";
		}
	
}

</script>

<script src="http://ajax.googleapis.com/ajax/libs/dojo/1.7.2/dojo/dojo.js"
               data-dojo-config="async: true"></script>  
<script language="JavaScript">     
        
        require(["dojo/ready"], function(ready){
     		ready(function(){
     		
         		if(js_isPin == "true"){
	        		showRightTreePin();
	        	}else{
	        		showBothTrees();
	        	}
     		});
		});
 


function hideDIV() {
parent.hideTree();
}

function showDIV() {
parent.showTree();
}

function setValuesAndGoTo1(val1) {
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	parent.hatsFrame.ms('macrorun_MasterMenuLevel01Macro', 'hatsportletid');
}
function setValuesAndGoTo2(val1, val2) {
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_macroName.value = "setValuesAndGoTo2("+val1+","+val2+")";
	parent.hatsFrame.ms('macrorun_MasterMenuLevel02Macro', 'hatsportletid');
}
function setValuesAndGoTo3(val1, val2, val3) {
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV3.value = val3;
	parent.hatsFrame.ms('macrorun_MasterMenuLevel03Macro', 'hatsportletid');
}
function setValuesAndGoTo4(val1, val2, val3, val4) {
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV3.value = val3;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV4.value = val4;
	parent.hatsFrame.ms('macrorun_MasterMenuLevel04Macro', 'hatsportletid');
}
function setValuesAndGoTo5(val1, val2, val3, val4, val5) {
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV3.value = val3;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV4.value = val4;
	parent.hatsFrame.document.forms["HATSForm"].hatsgv_promptGV5.value = val5;
	parent.hatsFrame.ms('macrorun_MasterMenuLevel05Macro', 'hatsportletid');
}

</script>

	<!-- Started By AK -->
<style type="text/css">
#treeMenu_ltr_185{
	display: block;
	width:259px;
	min-height:495px;
	height: auto ;
	background: url(../common/images/opn-close.gif) -28% 0 no-repeat;
	cursor: pointer;
	cursor: hand;
}
		
#treeMenu_ltr_27{
	display: block;
	width:25px;
	min-height:495px;
	height: auto;
	background: url(../common/images/opn-close.gif) 97% 0 no-repeat;
	cursor: pointer;
	cursor: hand;
}
</style>
	<!-- Ended By AK -->

</head>

<body>

<div class="topbar_headingnnew">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="middle" style="padding-right: 3px;">					
			<div id="treeMenu_ltr_27"  onmouseover="showDIV();">
		    	<div id="MenuContent" style="display: none;">
				<div id="MenuContent2" style="display: none;">
				<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left" valign="top">
						<div class="menumain" style="display: block; overflow:auto; height:497px;  margin-top:20px;">
						<ul id="red" class="treeview-red">
				                  		<li ><span class="testtxt2"><a title = "Financial Applications" href="#">Financial Applications</a></span>															
				<ul>															
					<li><span class="testtxt3"><a title = "Accounts Payable" href="#">Accounts Payable</a></span>														
					<ul>														
						<li><span class="testtxt3"><a title = "Accounts Payable File Maintenance" href="#">Accounts Payable File Maintenance</a></span>													
						<ul>													
							<li><span><a title= "Vendor Master File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 1);" target="_parent">Vendor Master File Maintenance</a></span></li>												
							<li><span><a title = "A/P Detail File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 2);" target="_parent">A/P Detail File Maintenance</a></span></li>												
							<li><span><a title = "Recurring Entry File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 3);" target="_parent">Recurring Entry File Maintenance</a></span></li>												
							<li><span><a title = "A/P Interface File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 4);" target="_parent">A/P Interface File Maintenance</a></span></li>												
							<li><span><a title = "Voucher Stub File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 5);" target="_parent">Voucher Stub File Maintenance</a></span></li>												
							<li><span><a title = "A/P Y-T-D File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 6);" target="_parent">A/P Y-T-D File Maintenance</a></span></li>												
							<li><span><a title = "Purchasing Terms File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 7);" target="_parent">Purchasing Terms File Maintenance</a></span></li>												
							<li><span><a title = "Direct Deposit Control File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 8);" target="_parent">Direct Deposit Control File Maintenance</a></span></li>												
							<li><span><a title = "A/P Check Date Control File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 9);" target="_parent">A/P Check Date Control File Maintenance</a></span></li>												
							<li><span><a title = "Vendor Class File Maintenance" href="javascript:setValuesAndGoTo3(1, 1, 10);" target="_parent">Vendor Class File Maintenance</a></span></li>												
							<li><span><a title = "Flag Vendors as Inactive" href="javascript:setValuesAndGoTo3(1, 1, 11);" target="_parent">Flag Vendors as Inactive</a></span></li>												
							<li><span><a title = "End of Month" href="javascript:setValuesAndGoTo3(1, 1, 12);" target="_parent">End of Month</a></span></li>												
						</ul>
						</li>													
						<li><span class="testtxt3"><a title = "Accounts Payable Transaction Processing" href="#">Accounts Payable Transaction Processing</a></span>													
						<ul>													
							<li><span class="testtxt3"><a title = "Making Entries" href="#">Making Entries</a></span>												
							<ul>												
								<li><span><a title = "A/P Detail Entry" href="javascript:setValuesAndGoTo3(1, 2, 1);" target="_parent">A/P Detail Entry</a></span></li>											
								<li><span><a title = "A/P Detail Edit/Update-After #1" href="javascript:setValuesAndGoTo3(1, 2, 2);" target="_parent">A/P Detail Edit/Update-After #1</a></span></li>											
								<li><span><a title = "Create A/P Voucher from P/O" href="javascript:setValuesAndGoTo3(1, 2, 3);" target="_parent">Create A/P Voucher from P/O</a></span></li>											
								<li><span><a title = "Copy Recurring Invoices" href="javascript:setValuesAndGoTo3(1, 2, 4);" target="_parent">Copy Recurring Invoices</a></span></li>											
							</ul>
							</li>												
							<li><span class="testtxt3"><a title = "Writing Checks" href="#">Writing Checks</a></span>
							<ul>												
								<li><span><a title = "Select Invoices for Payment" href="javascript:setValuesAndGoTo3(1, 2, 5);" target="_parent">Select Invoices for Payment </a></span></li>											
								<li><span><a title = "Edit Selected Invoices" href="javascript:setValuesAndGoTo3(1, 2, 7);" target="_parent">Edit Selected Invoices      </a></span></li>											
								<li><span><a title = "Print Cash Requirements/Edit" href="javascript:setValuesAndGoTo3(1, 2, 8);" target="_parent">Print Cash Requirements/Edit</a></span></li>											
								<li><span><a title = "Print Checks and Update" href="javascript:setValuesAndGoTo3(1, 2, 9);" target="_parent">Print Checks and Update     </a></span></li>
								
							</ul>	
							</li>											
							<li><span class="testtxt3"><a title = "Direct Pay" href="#">Direct Pay</a></span>
							<ul>												
								<li><span><a title = "Select Invoice for Direct Pay" href="javascript:setValuesAndGoTo3(1, 2, 13);" target="_parent">Select Invoice for Direct Pay</a></span></li>											
								<li><span><a title = "Update Direct Pay" href="javascript:setValuesAndGoTo3(1, 2, 14);" target="_parent">Update Direct Pay            </a></span></li>
							</ul>
							</li>											
							<li><span class="testtxt3"><a title = "EDI 810" href="#">EDI 810</a></span>
							<ul>												
								<li><span><a title = "Edit In-bound 810 File" href="javascript:setValuesAndGoTo3(1, 2, 18);" target="_parent">Edit In-bound 810 File</a></span></li>
							</ul>
							</li>											
							<li><span class="testtxt3"><a title = "Charge Backs" href="#">Charge Backs</a></span>
							<ul>												
								<li><span><a title = "Work with Charge Back Invoices" href="javascript:setValuesAndGoTo3(1, 2, 20);" target="_parent">Work with Charge Back Invoices</a></span></li>											
								<li><span><a title = "Print and Post Credit Memos" href="javascript:setValuesAndGoTo3(1, 2, 21);" target="_parent">Print and Post Credit Memos   </a></span></li>
							</ul>
							</li>	
						</ul>
						</li>										
						<li><span class="testtxt3"><a title = "" href="#"> Accounts Payable Reports               </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 1);" target="_parent">Vendor Master File List       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 2);" target="_parent">Print 50 Unused Vendor No.'s  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 3);" target="_parent">Aged Analysis Report          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 4);" target="_parent">Vendor Labels Selections      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 5);" target="_parent">Recurring Entry List          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 6);" target="_parent">Division Balances Report      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 7);" target="_parent">Y-T-D Purchases-Vendor Ranking</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 8);" target="_parent">Use Tax Report                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 9);" target="_parent">Cash Requirements/Due Date    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 10);" target="_parent">Due Date Summary (no options) </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 11);" target="_parent"> Print 1099's                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 12);" target="_parent"> Purchasing Terms listing      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 13);" target="_parent"> Vendor Inactive List           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 14);" target="_parent"> Y-T-D Purchases-Vendor(Detail) </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 15);" target="_parent"> M-T-D Purchase Journal         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 16);" target="_parent"> A/P Cash Projection            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 17);" target="_parent"> Suppliers by Location          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 19);" target="_parent"> Y-T-D Purchases-Vendor(Summary)</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 20);" target="_parent"> Selective Purchase Journal     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 21);" target="_parent"> Accounts Payable Balance       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 22);" target="_parent"> A/P Selective Check Register   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 3, 21);" target="_parent"> Vendor Charge Back Report      </a></span></li>
						</ul>
						</li>												
						<li><span class="testtxt3"><a title = "" href="#">Accounts Payable Inquiries             </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 1);" target="_parent">A/P Detail Inquiry         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 2);" target="_parent">A/P History Inquiry        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 3);" target="_parent">A/P Check History Inquiry  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 4);" target="_parent">A/P Due Date Inquiry       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 5);" target="_parent">P/O to A/P Inquiry         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 6);" target="_parent">Inventory Receipts History </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 7);" target="_parent">Inventory History Tracking </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 8);" target="_parent">Purchase History Inquiry   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 11);" target="_parent"> P/O Inquiry by Vendor      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 12);" target="_parent"> P/O Inquiry by Vendor-Items</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 13);" target="_parent"> P/O Inquiry by Item                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 14);" target="_parent"> View Vendor Balances               </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 15);" target="_parent"> Open & Vouchered P/O to A/P Inquiry</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 16);" target="_parent"> Vendor Analysis                    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 17);" target="_parent"> View Company Balances              </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 18);" target="_parent"> A/P Invoice History by Batch       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 4, 19);" target="_parent"> A/P Check History by Batch         </a></span></li>
							</ul>
							</li>												
						<li><span class="testtxt3"><a title = "" href="#">eInvoices (EDI 810)                    </a></span>	
						<ul>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 1);" target="_parent">eInvoice Control File Maintenance    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 2);" target="_parent">eInvoice Change/Delete Reason Maint  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 3);" target="_parent">eInvoice Processing                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 4);" target="_parent">A/P Detail Entry (maintenance)       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 5);" target="_parent">A/P Detail Edit/Update (after #4)    </a></span></li>												
																			
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 7);" target="_parent">Display eInvoice Change History      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 8);" target="_parent">Print UnProcessed eInvoices (summary)</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 9);" target="_parent">Print UnProcessed eInvoices (detail) </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 10);" target="_parent"> Re-Open Processed eInvoice(s)        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 5, 11);" target="_parent"> Purge Processed eInvoices            </a></span></li>
							</ul>
							</li>												
						<li><span class="testtxt3"><a title = "" href="#"> P/O to A/P                             </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 6, 1);" target="_parent">Open P/O to A/P Report             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 6, 2);" target="_parent">P/O to A/P Detail History Report   </a></span></li>												
																			
																			
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 6, 5);" target="_parent">Open P/O to A/P Inquiry            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 6, 6);" target="_parent">P/O to A/P Detail History Inquiry  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 6, 7);" target="_parent">Purchase History Inquiry           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(1, 6, 8);" target="_parent">Open & Vouchered P/O to A/P Inquiry</a></span></li>												
							</ul>
							</li>
							</ul>
							</li>												
					<li><span class="testtxt3"><a title = "" href="#">General Ledger</a></span>	
					<ul>													
						<li><span class="testtxt3"><a title = "" href="#">File Maintenance Menu                      </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 1);" target="_parent">Chart of Accounts                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 2);" target="_parent">Control File                     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 3);" target="_parent">Transaction Source               </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 4);" target="_parent">Budget File                      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 5);" target="_parent">Detail File                      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 6);" target="_parent">Sub Accounts                     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 7);" target="_parent">Format File                      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 8);" target="_parent">Summarize Detail File            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 9);" target="_parent">Default Location File            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 10);" target="_parent"> Enter Cash Accounts              </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 11);" target="_parent"> EOD G/L Close Selection Setup    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 12);" target="_parent"> EOD G/L Balance Sheet Setup      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 1, 13);" target="_parent"> EOD G/L Trial Balance Sheet Setup</a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Transaction Processing Menu                </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 2, 1);" target="_parent">Create/Edit Journal Entry Batch       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 2, 2);" target="_parent">Print Journal Entry Batch             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 2, 3);" target="_parent">Post Journal Entry Batch to Files     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 2, 4);" target="_parent">Recurring Journal Entries             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 2, 9);" target="_parent">Copy GLIMPORT.txt from IFS to GLIMPORT</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 2, 10);" target="_parent"> Create GL batch from GLIMPORT         </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Reports Menu                               </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 1);" target="_parent">List Chart of Accounts            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 2);" target="_parent">Audit Journal                     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 3);" target="_parent">Financial Statements              </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 4);" target="_parent">Account Detail Listing            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 5);" target="_parent">Trial Balance                     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 6);" target="_parent">General Ledger by Company         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 7);" target="_parent">General Ledger by Location        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 8);" target="_parent">Journal Transaction Source Summary</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 9);" target="_parent">General ledger budget file list   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 10);" target="_parent"> Display Financial Statements      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 11);" target="_parent"> Validate Master to Detail         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 3, 12);" target="_parent"> Consolidated Ledger by Company    </a></span></li>
							</ul>		</li>										
						<li><span class="testtxt3"><a title = "" href="#">Inquiry Menu                               </a></span>	
						<ul>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 4, 1);" target="_parent">Account Analysis       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 4, 2);" target="_parent">Account Detail         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 4, 3);" target="_parent">Batch/Reference Inquiry</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 4, 4);" target="_parent">G/L Analysis           </a></span></li>
							</ul>
							</li>												
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(2, 5);" target="_parent">Close Period                               </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(2, 6);" target="_parent">Un-close Period                            </a></span></li>													
						<li><span class="testtxt3"><a title = "" href="#">G/L Interface Menu                         </a></span>
					<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Distribution</a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 1);" target="_parent">Price Exception File         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 2);" target="_parent">A/R to G/L Table             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 3);" target="_parent">Invoicing-G/L Table          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 4);" target="_parent">Sales to Inventory/COGS Table</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 5);" target="_parent"> Inter-Company                </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 6);" target="_parent"> Inventory Adj Reason         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 7);" target="_parent"> Method of Payment            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 8);" target="_parent"> Tax Master File              </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 9);" target="_parent"> Route Master                 </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Accounts Payable</a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 13);" target="_parent"> A/P Interface file Maintenance</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 14);" target="_parent"> Vendor Maintenance            </a></span></li>
								</ul>
								</li>											
							<li><span class="testtxt3"><a title = "" href="#">General Ledger          </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 16);" target="_parent"> Location Maintenance</a></span></li>
								</ul>
								</li>											
							<li><span class="testtxt3"><a title = "" href="#">Payroll                      </a></span>	
							<ul>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 18);" target="_parent"> Payroll - G/L Interface  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 19);" target="_parent"> Payroll - A/P Interface  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 20);" target="_parent"> Deduction Default Master </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(2, 7, 21);" target="_parent"> Employee Deduction Master</a></span></li>
								</ul></li>
									</ul></li>					
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(2, 10);" target="_parent"> Year End Close to New Year Balances Forward     </a></span></li>	</ul></li>												
					<li><span class="testtxt3"><a title = "" href="#">Payroll</a></span>
					<ul>														
						<li><span class="testtxt3"><a title = "" href="#">Master File Maintenance             </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Master Files                  </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 1);" target="_parent">Employee Master            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 2);" target="_parent">Employee Deduction Master  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 3);" target="_parent">Employee Earnings/Taxes YTD</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 4);" target="_parent">Employee Deductions YTD    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 5);" target="_parent">Deduction Default Master   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 6);" target="_parent">Payroll - G/L Interface    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 7);" target="_parent">Workers Comp Rates         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 8);" target="_parent">State Unemployment Taxes   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 9);" target="_parent">Loc/Dept/Shift Maintenance </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 10);" target="_parent"> Payroll - A/P Interface    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 11);" target="_parent"> City Tax YTD Earnings & Tax</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 12);" target="_parent"> Job Master                 </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Tables                        </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 13);" target="_parent"> Pay Frequency             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 14);" target="_parent"> Pay Types                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 15);" target="_parent"> Direct Deposit            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 16);" target="_parent"> Deduction Codes           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 17);" target="_parent"> State Income Taxes        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 18);" target="_parent"> Federal Taxes             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 19);" target="_parent"> City Taxes                </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 20);" target="_parent"> Department Master         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 21);" target="_parent"> Accrual Codes             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 22);" target="_parent"> Accrual Pay Types         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 1, 23);" target="_parent"> Accrual Clearing Pay Types</a></span></li>
								</ul></li></ul></li>											
						<li><span class="testtxt3"><a title = "" href="#">Payroll Processing                  </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Current Processing               </a></span>	
							<ul>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 1);" target="_parent">Enter/Edit Time Records       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 2);" target="_parent">Print Payroll Registers       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 3);" target="_parent">Print Checks/Labels           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 4);" target="_parent">Post Payroll To Master Files  </a></span></li>											
																			
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 6);" target="_parent">Enter Time Cards              </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 7);" target="_parent">Print Time Card Edit          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 8);" target="_parent">Enter Time/Badge              </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 9);" target="_parent">Post Time Cards To History    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 10);" target="_parent"> Direct Deposit Prenotification</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 11);" target="_parent"> Direct Deposit Test Run       </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Historical Processing             </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 13);" target="_parent"> Reprint Registers From History</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 14);" target="_parent"> Employee Check Listing        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 15);" target="_parent"> Print Deduction Recap Report  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 16);" target="_parent"> Print Departmental Registers  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 2, 20);" target="_parent"> Print Payroll Worksheet       </a></span></li>
								</ul></li></ul>	</li>										
						<li><span class="testtxt3"><a title = "" href="#">Reports                             </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">File Listings                  </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 1);" target="_parent">Employee Master File        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 2);" target="_parent">Employee Earnings YTD File  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 3);" target="_parent">Employee Deductions YTD File</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 4);" target="_parent">Workers Compensation File   </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#"> Other Reports                    </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 5);" target="_parent">Deduction Balance Report      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 6);" target="_parent">Print Roster                  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 7);" target="_parent">Deduction Recap Report        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 8);" target="_parent">Employee Check Listing        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 9);" target="_parent">Payroll Worksheet             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 10);" target="_parent"> Reprint Registers from History</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 11);" target="_parent"> Selective Reprint from History</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 12);" target="_parent"> Print Time Cards           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 13);" target="_parent"> Print Federal Tax Liability</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 14);" target="_parent"> Anniversary/Birthday List  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 15);" target="_parent"> State/City Tax Report      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 16);" target="_parent"> Selective Pay Raise History</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 17);" target="_parent"> City Tax Detail Report     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 18);" target="_parent"> Print Employee Badges      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 19);" target="_parent"> Workers Comp Report        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 20);" target="_parent"> Time Card Detail Report    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 3, 21);" target="_parent"> Over Time Report           </a></span></li>
								</ul></li></ul></li>											
						<li><span class="testtxt3"><a title = "" href="#">Inquiry                             </a></span>	
						<ul>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 4, 1);" target="_parent">Payroll Register Inquiry</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 4, 2);" target="_parent">YTD by Pay Type Inquiry </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 4, 3);" target="_parent">Time/Attendance Inquiry </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 4, 4);" target="_parent">Time/Absentee Inquiry   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(3, 4, 5);" target="_parent"> Five Year Comparative   </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">  Monthly, Quarterly, Yearly Processes</a></span>
						<ul>
				<!-- start working		-->
						<li><span class="testtxt3"><a title = "" href="#">Quarterly</a></span>		
							<ul>	
				    		<li><span><a title = "" href="javascript:setValuesAndGoTo3(3,5,1);" target="_parent">Print Government Reports (941/FUTA/SUTA)</a></span></li>	
				    		</ul>	
						</li>
				<li><span class="testtxt3"><a title = "" href="#">Yearly</a></span>	<ul>		
					<li><span><a title = "" href="javascript:setValuesAndGoTo3(3,5,2);" target="_parent"> Create New Year Tax Tables From Old Table</a></span></li>		</ul></li>
				<li><span class="testtxt3"><a title = "" href="#">W2's</a></span>	<ul>		
					<li><span class="testtxt3"><a title = "" href="#">W2 Menu</a></span> <ul>		
						<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,3,1);" target="_parent"> Enter Additional W2 Amounts	</a></span></li>
						<li><span class="testtxt3"><a title = "" href="#">W2 Forms</a></span>
						<ul>	
							<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,3,3);" target="_parent"> Print W-2's     </a></span></li> </ul></li>        
						<li><span class="testtxt3"><a title = "" href="#">IRS Diskette</a></span>	<ul>
							<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,3,4);" target="_parent"> Create W-2 Diskette Employee File </a></span></li></ul></li>
						<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,3,5);" target="_parent"> W-2 Diskette Employer File Maintenance    </a></span></li>        	
						<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,3,6);" target="_parent"> W-2 Diskette Employee File Maintenance  </a></span></li>          	
						<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,3,7);" target="_parent"> Create and Save W-2 Diskette File To Diskette   </a></span></li>  	
						<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,3,8);" target="_parent"> Print Diskette File Totals  </a></span></li>
					</ul></li></ul></li>                      	
				<li><span class="testtxt3"><a title = "" href="#">Quarterly</a></span> <ul>			
					<li><span class="testtxt3"><a title = "" href="#">Sending SUTA to State-ICESA format</a></span> <ul> 		
						<li><span class="testtxt3"><a title = "" href="#">Maintenance</a></span>	<ul>
							<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,4,1);" target="_parent"> SUTA Control File</a></span></li></ul> </li>
						<li><span class="testtxt3"><a title = "" href="#">Extract </a></span> 	<ul>
							<li><span><a title = "" href="javascript:setValuesAndGoTo4(3,5,4,13);" target="_parent"> SUTA (ICESA Format)</a></span></li> </ul></li>
					</ul></li></ul></li>
				<li><span class="testtxt3"><a title = "" href="#">Monthly</a></span>	<ul>		                       
				                                       	<li><span><a title = "" href="javascript:setValuesAndGoTo3(3,5,10);" target="_parent"> Insurance Report </a></span></li>		                                                                  
				                                        <li><span><a title = "" href="javascript:setValuesAndGoTo3(3,5,11);" target="_parent"> State/City Tax Report </a></span></li> </ul> </li>    		
				<li><span class="testtxt3"><a title = "" href="#">Yearly Maintenance</a></span>	<ul>		
					<li><span><a title = "" href="javascript:setValuesAndGoTo3(3,5,15);" target="_parent"> Zero Y-T-D Withholdings for Specified Deduction</a></span></li>  	</ul></li>	
				
				<!-- end working-->
				</ul></li>
						</ul></li>													
																			
					<li><span class="testtxt3"><a title = "" href="#">Accounts Receivable</a></span>
					<ul>														
						<li><span class="testtxt3"><a title = "" href="#"> A/R File Maintenance          </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">  Maintenance                 </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 1);" target="_parent">Customer Master File     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 2);" target="_parent">Open Item File           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 3);" target="_parent">Terms Master File        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 4);" target="_parent">Sales Rep Master File    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 5);" target="_parent">Sales Tax File           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 6);" target="_parent">Mail List File           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 7);" target="_parent">A/R to G/L Table         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 8);" target="_parent">Customer Master Sales Rep</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 9);" target="_parent">Credit Management        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 10);" target="_parent"> Territory/Sales Rep File </a></span></li>											
								<!--<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 11);" target="_parent"> Claims File              </a></span></li>
								
								--><li><span class="testtxt3"><a title = "" href="#">  Claims File       </a></span>
									<ul>
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 11, 1);" target="_parent"> Enter or Update Claims File                  </a></span></li>
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 11, 2);" target="_parent"> Print Claims File </a></span></li>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 11, 4);" target="_parent">Reorganize the Claims Master File  </a></span></li>									
									</ul>
								</li>
								
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 12);" target="_parent"> Closing Date File        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 13);" target="_parent"> Customer Terms File          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 14);" target="_parent"> A/R Credit Code File         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 15);" target="_parent"> Customer/Item Authority      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 16);" target="_parent"> Customer Billing Codes       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 17);" target="_parent"> Bill to File Maintenance     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 18);" target="_parent"> Invoicing - G/L Table        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 19);" target="_parent"> Buying Group File Maintenance</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 20);" target="_parent"> View Customers on Hold       </a></span></li>
								<li><span class="testtxt3"><a title = "" href="#">  More Maintenance             </a></span>											
									<ul>
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 1);" target="_parent">Profile Master                 </a></span></li>
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 2);" target="_parent">Customer Profile               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 3);" target="_parent">Billing Code Master            </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 4);" target="_parent">Customer Rank Table            </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 5);" target="_parent">Customer Sales Rep by Sales Rep</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 6);" target="_parent">Customer Sales Rep by County   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 7);" target="_parent">Change Area Codes              </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 8);" target="_parent">EOM/EOM Statements Maintenance </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 9);" target="_parent">Sales Manager                  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 10);" target="_parent"> Marketing Code Master          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 11);" target="_parent"> Customer Global Changes        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 12);" target="_parent"> NSF Checks                     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 13);" target="_parent"> Credit Letter Template            </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 14);" target="_parent"> Customer class II code            </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 15);" target="_parent"> Customer/Division Budget          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 16);" target="_parent"> Customer Inventory Guarantee      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 17);" target="_parent"> Customer Order Type               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 18);" target="_parent"> Customer Ship Via Sales Rep       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 19);" target="_parent"> Customer Mail Codes Global Changes</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 20);" target="_parent"> Customer Job Maintenance          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 21);" target="_parent"> Customer Billing code history     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 1, 21, 22);" target="_parent"> Buying Authority Inquiry          </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">  A/R Update Menu III               </a></span>
									<ul>										
										<li><span class="testtxt3"><a title = "" href="#">  Maintenance                         </a></span>	
										<ul>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(4, 1, 21, 23, 1);" target="_parent">Post Sales Commissions to History</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(4, 1, 21, 23, 2);" target="_parent">Ship to Global Changes           </a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(4, 1, 21, 23, 3);" target="_parent">Work With Wish List           	</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(4, 1, 21, 23, 4);" target="_parent">Work With Saved Carts           	</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(4, 1, 21, 23, 5);" target="_parent">Work With Gift Cards            	 </a></span></li>
											</ul></li>
											</ul></li></ul>	</li>							
							<li><span class="testtxt3"><a title = "" href="#"> Other Functions </a></span>	
							<ul>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 22);" target="_parent"> End of Month</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 1, 23);" target="_parent"> End of Year </a></span></li>
								</ul></li>
								</ul></li>											
						<li><span class="testtxt3"><a title = "" href="#"> A/R Transaction Processing    </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 2, 1);" target="_parent">Cash Receipts Entry                       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 2, 2);" target="_parent">Cash Receipts Edit/Update (after #1 above)</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 2, 3);" target="_parent">A/R Detail Entry (adjustments, etc.)      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 2, 4);" target="_parent">A/R Detail Edit/Update (after #3 above)   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 2, 5);" target="_parent"> Recurring Entry Batch Maintenance         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 2, 6);" target="_parent"> Recurring Entry Edit/Post                 </a></span></li>												
							<li><span class="testtxt3"><a title = "" href="#">Bad Debt Menu                             </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 2, 7, 1);" target="_parent">Work with Open Invoices/Bad Debts</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 2, 7, 2);" target="_parent">Unpurged Aged Trial Balance      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 2, 7, 3);" target="_parent">Print Statements                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(4, 2, 7, 4);" target="_parent">Inquiry                          </a></span></li>
								</ul></li>											
								</ul></li>
						<li><span class="testtxt3"><a title = "" href="#"> Accounts Receivable Reports   </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#"> Customer Master               </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 1);" target="_parent">List With Full Information </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 2);" target="_parent">List With Single Line      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 3);" target="_parent">Labels                     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 4);" target="_parent">Credit Reference Inq/Report</a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#"> Open Items                        </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 5);" target="_parent">Aged Trial Balance (unpurged)  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 6);" target="_parent">Aged Trial Balance (purged)    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 7);" target="_parent">Aged Trial Summary by Customer </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 8);" target="_parent">Aged Trial Summary by Sales Rep</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 9);" target="_parent">Selective Aged Trial Balance   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 10);" target="_parent"> Statements                     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 11);" target="_parent"> Past Due Notices               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 12);" target="_parent"> Sales Tax Expiration           </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#"> Mail List & Mail Codes          </a></span>	
							<ul>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 13);" target="_parent"> Listing                     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 14);" target="_parent"> Labels                      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 15);" target="_parent"> Customer Labels by Mail Code</a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Credit Reports                  </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 16);" target="_parent"> Credit Status Report        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 17);" target="_parent"> Credit Limit Report         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 19);" target="_parent"> Credit Report by Cust Sales </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Other Reports                   </a></span>	
							<ul>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 19);" target="_parent"> A/R to G/L Table List       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 20);" target="_parent"> Terms File List             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 21);" target="_parent"> Sales Tax Master File List  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 3, 22);" target="_parent"> Sales Rep Master File List  </a></span></li>
								</ul></li>		 
								</ul></li>									
						<li><span class="testtxt3"><a title = "" href="#">Accounts Receivable Inquiries </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 1);" target="_parent">Online Ageing                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 2);" target="_parent">History                       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 3);" target="_parent">Customer Master               </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 4);" target="_parent">Customer Analysis             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 5);" target="_parent">Customer Sales                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 6);" target="_parent">Credit Window                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 7);" target="_parent">Collection Detail             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 8);" target="_parent">NSF Checks                    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 9);" target="_parent">View Buying Group Members     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 10);" target="_parent"> View Customers by Bill To     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 11);" target="_parent"> View Customers by Price Exc Rf</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 12);" target="_parent"> Customer Note Pad             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 13);" target="_parent"> View Customers by Billing Code  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 14);" target="_parent"> View Buying Group by Customer   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 15);" target="_parent"> Work With A/R and Order Balances</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 16);" target="_parent"> Customer Ranking                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 17);" target="_parent"> Cash Receipts History           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 18);" target="_parent"> Movers & Shakers                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 19);" target="_parent"> Cash Receipts History by Batch  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 4, 20);" target="_parent"> Company Balances                </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Finance Charges               </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 5, 1);" target="_parent">Select Accounts to be Charged   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 5, 2);" target="_parent">Maintenance Selected Records    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 5, 3);" target="_parent">Print and Post Invoices         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 5, 4);" target="_parent"> Re-print list of finance charges</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 5, 7);" target="_parent"> Interest Rate Table</a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Charter Member Invoicing      </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 6, 1);" target="_parent">Select Accounts to be Charged</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 6, 2);" target="_parent">Print Edit Listing           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 6, 3);" target="_parent">Print and Post Invoices      </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Billing Code Invoicing        </a></span>	
						<ul>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 7, 1);" target="_parent">Billing Code Invoicing Entry</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 7, 2);" target="_parent">Print Edit Listing          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 7, 3);" target="_parent">Print and Post Invoices     </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Accounts Receivable Reports II</a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Other Reports                    </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 1);" target="_parent">Inactive Customer List        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 2);" target="_parent">Print Labels for Top xx Accts </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 3);" target="_parent">Terms Usage Analysis          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 4);" target="_parent">Credit Ranking                </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 5);" target="_parent">Customer Mailing Labels (3-up)</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 6);" target="_parent">Customer Comments List        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 7);" target="_parent">Cash Receipts Register - Hist </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 8);" target="_parent">Ineligible A/R Maintenance    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 9);" target="_parent">Print Ineligible A/R Report   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 10);" target="_parent"> Print A/R Hist by Credit Code </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 11);" target="_parent"> Print A/R Dispute Report      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 12);" target="_parent"> Print A/R Dispute Summary     </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Other Inquiries                  </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 13);" target="_parent"> Open A/R Balances by location</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 14);" target="_parent"> Print A/R Summary            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 8, 15);" target="_parent"> Print Cust. Billing code hist</a></span></li>
								</ul></li>
								</ul></li>											
						<li><span class="testtxt3"><a title = "" href="#">Charge Backs                  </a></span>
						<ul>
				<!--		confilicting menu commented													-->
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 9, 1);" target="_parent">Order Management</a></span></li>										
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 9, 2);" target="_parent">Select Orders for Invoicing</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 9, 3);" target="_parent">Print/Post Invoices        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 9, 4);" target="_parent">Cash Receipts Entry        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 9, 5);" target="_parent"> Cash Receipts Edit/Update  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 9, 6);" target="_parent"> Aged Trial Report          </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#"> Trip Menu                     </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 1);" target="_parent">Master file maintenance                          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 2);" target="_parent">Trip customer file maintenance                   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 3);" target="_parent">Trip item file maintenance                       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 4);" target="_parent">Master file list                                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 5);" target="_parent">Trip inquiry                                     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 6);" target="_parent">Enter trip transactions                          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 7);" target="_parent">Trip/customer sales report-TP100-sales only      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 8);" target="_parent">Trip/customer sales report-TP110-all transactions</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 9);" target="_parent">Trip detail report                               </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 10);" target="_parent"> Trip statement                                   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 11);" target="_parent"> Trip End of Year                                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 12);" target="_parent"> Trip file purge                                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 10, 13);" target="_parent"> Trip Setup                                       </a></span></li>
							</ul></li> 												
						<li><span class="testtxt3"><a title = "" href="#"> Recurring Billing             </a></span>	
						<ul>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 11, 1);" target="_parent">Recurring Billing Maintenance</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 11, 2);" target="_parent">Select Recurring entries     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 11, 3);" target="_parent">Print edit/update entries    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 11, 5);" target="_parent"> Select Orders for Invoicing</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 11, 6);" target="_parent"> Print pre-invoicing edit     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 11, 7);" target="_parent"> Print and Post Invoices      </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#"> Sales Tax Menu                </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Maintenance                      </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 1);" target="_parent">Sales Tax Master              </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 2);" target="_parent">States Sharing Exempt Status  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 3);" target="_parent">Location Master               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 4);" target="_parent">Ship Via                      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 5);" target="_parent"> Route Master                  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 6);" target="_parent"> Customer Master               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 7);" target="_parent"> Item master                   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 8);" target="_parent"> Sales Tax Return Definition   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 9);" target="_parent"> Change Sales Tax on an Invoice</a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Reports                           </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 13);" target="_parent"> Sales tax report from Receipts</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 14);" target="_parent"> State sales tax report        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 15);" target="_parent"> County sales tax report       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 16);" target="_parent"> City sales tax report         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 17);" target="_parent"> Sales Tax Return Reports      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 18);" target="_parent"> Taxable Sales with Discounts  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 12, 19);" target="_parent"> Create Tax File for Download  </a></span></li>
								</ul></li>
								</ul></li>											
						<li><span class="testtxt3"><a title = "" href="#"> Cash Drawer Menu              </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Maintenance          </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 13, 1);" target="_parent">Cash Drawer       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 13, 2);" target="_parent">Method of Payment </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 13, 3);" target="_parent">Payment on Account</a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Inquiries            </a></span>
							<ul>
																			
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 13, 7);" target="_parent"> View Cash Drawer  </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Reports                       </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 13, 13);" target="_parent"> Cash Drawer Balancing     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 13, 15);" target="_parent"> Cash Drawer History Report</a></span></li>
								</ul></li>
								</ul>	</li>										
						<li><span class="testtxt3"><a title = "" href="#"> Storage Processing</a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 14, 1);" target="_parent">Storage Fees Maintenance   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 14, 2);" target="_parent">Storage Fees History       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 14, 3);" target="_parent">Create Storage Billing     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 14, 5);" target="_parent"> Select Orders for Invoicing</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 14, 6);" target="_parent"> Print pre-invoicing edit   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(4, 14, 7);" target="_parent"> Print and Post Invoices    </a></span></li>
						</ul>	</li>
						</ul></li>												
					<li><span class="testtxt3"><a title = "" href="#"> Bank Reconciliation</a></span>
					<ul>														
						<li><span class="testtxt3"><a title = "" href="#">Statement Processing           </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(5, 1);" target="_parent">Enter/Edit Bank Statement   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(5, 5);" target="_parent"> Positive Pay File Build</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(5, 7);" target="_parent"> Delete Pos Pay TRANSMIT File</a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">System Files Maintenance            </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(5, 13);" target="_parent"> Bank Account File Maintenance   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(5, 14);" target="_parent"> Bank Account Listing            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(5, 15);" target="_parent"> Deposit Bank Account Maintenance</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(5, 16);" target="_parent"> List Outstanding Checks         </a></span></li>
							</ul></li>
							</ul></li>												
					<li><span class="testtxt3"><a title = "" href="#"> Fixed Assets</a></span>
					<ul>														
						<li><span class="testtxt3"><a title = "" href="#">Fixed Assets          </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 1);" target="_parent">File Maintenance   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 2);" target="_parent">Master File Listing</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 3);" target="_parent">Book Value Report  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 4);" target="_parent">Loan/Rental Report </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 5);" target="_parent"> Class Maintenance  </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Depreciation                       </a></span>	
						<ul>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 13);" target="_parent"> Current Month Depreciation     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 14);" target="_parent"> Post Current Month Depreciation</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 15);" target="_parent"> Depreciation History Inquiry   </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Special Functions                  </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(6, 21);" target="_parent"> Fixed Asset Year End Update    </a></span></li>
							</ul></li>		
							</ul></li>										
					<li><span class="testtxt3"><a title = "" href="#"> Claims</a></span>	
					<ul>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(7, 1);" target="_parent">Claims File Maintenance</a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(7, 2);" target="_parent">Print Claims           </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(7, 13);" target="_parent"> Claim Billing Control</a></span></li>													
						<li><span class="testtxt3"><a title = "" href="#">Vendor Rebates                 </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(7, 16);" target="_parent"> Aged Memo Bill Report      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(7, 17);" target="_parent"> Work With Rebate Receivable</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(7, 18);" target="_parent"> Aged Rebate Report         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(7, 19);" target="_parent"> Rebate Inquiry             </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Customer Rebates               </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(7, 20);" target="_parent"> Customer Rebate Inquiry    </a></span></li>
							</ul></li>
							</ul></li>												
				</ul>
				</li>															
				<li ><span class="testtxt2"><a title = "" href="#">Distribution Applications</a></span>															
				<ul>															
					<li><span class="testtxt3"><a title = "" href="#"> Inventory</a></span>
					<ul>														
						<li><span class="testtxt3"><a title = "" href="#">Menus</a></span>
						<ul>													
						<li><span class="testtxt3"><a title = "" href="#">Inventory File Maintenance Menu</a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 1);" target="_parent">Item Master              </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 2);" target="_parent">Item Balance             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 3);" target="_parent">Bin Location/Order Point </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 4);" target="_parent">Buyer Master             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 5);" target="_parent">Family Code              </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 6);" target="_parent">Product Code             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 7);" target="_parent">Vendor Master            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 8);" target="_parent">Item History             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 9);" target="_parent">Item Price File          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 10);" target="_parent"> Item Specifications      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 11);" target="_parent"> Hazardous Material Descr.</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 12);" target="_parent"> Inventory Transfer File  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 13);" target="_parent"> Drop an Item                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 14);" target="_parent"> Group Item Size File          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 15);" target="_parent"> Item Master Global Changes    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 16);" target="_parent"> Buyers Guide Seasonal Factor  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 17);" target="_parent"> Product Price File            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 18);" target="_parent"> Class Code File               </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 19);" target="_parent"> Sales to Inventory/COGS Table </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 20);" target="_parent"> Item Balance Global Changes   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 21);" target="_parent"> Laminate Ranking Table        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 1, 22);" target="_parent"> Update A,B,C Codes to Lam Item</a></span></li>												
							<li><span class="testtxt3"><a title = "" href="#"> Inventory Update Menu II      </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 1);" target="_parent">Products Excluded from Gen Sl</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 2);" target="_parent">Stocking Location            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 3);" target="_parent">Item Ranking (ABC) Table     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 4);" target="_parent">Major Product Group          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 5);" target="_parent"> Non-Lam - Update A,B,C Codes </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 6);" target="_parent"> Change Bin Locations         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 7);" target="_parent"> EOD Buying Report Setup-IM540</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 8);" target="_parent"> EOD Inven. Value Setup-IMVALU</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 9);" target="_parent"> Report Code                  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 10);" target="_parent"> Inventory Size Code          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 11);" target="_parent"> Vendor Source of Supply      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 12);" target="_parent"> Item Unit of Measure         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 13);" target="_parent"> Ineligible Inventory Setup     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 14);" target="_parent"> EOD Stock Status Setup-IM150   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 15);" target="_parent"> Item Master Global Pricing Chgs</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 16);" target="_parent"> NMFC Master File Maintenance   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 17);" target="_parent"> Customer/Vendor UPC label print</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 18);" target="_parent"> Item xref Maint. (Buying ONLY) </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 19);" target="_parent"> Sell Group Maintenance         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 20);" target="_parent"> Stocking Item Maintenance      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 1, 23, 21);" target="_parent"> Inventory Category Maintenance </a></span></li>
								</ul></li>
								</ul></li>											
						<li><span class="testtxt3"><a title = "" href="#">Inventory Transaction Processing Menu</a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Adjustments</a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 1);" target="_parent">Inventory Adjustments Entry By Batch      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 2);" target="_parent">Inventory Adjustments Edit/Update By Batch</a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Transfers</a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 3);" target="_parent">Inventory Transfers Receipts Entry        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 4);" target="_parent">Inventory Transfers Receipts Update       </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Scrap</a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 5);" target="_parent"> Pull Scrap                                </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 6);" target="_parent"> Print Edit and Update Scrap               </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Other                                        </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 7);" target="_parent"> Interactive Inventory Adjustments         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 8);" target="_parent"> Write Down Inventory                      </a></span></li>
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 2, 9);" target="_parent"> Kit/De-kit                      </a></span></li>
																			</ul></li>
																			</ul></li>
						<li><span class="testtxt3"><a title = "" href="#">Inventory Reports Menu               </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#"> Master Listings              </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 1);" target="_parent">Item Master               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 2);" target="_parent">Buyer Master              </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 3);" target="_parent">Product Code Master       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 4);" target="_parent">Family Code Master        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 5);" target="_parent">Item Master Price List    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 6);" target="_parent">Item Price File List      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 7);" target="_parent">Item Specifications List  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 8);" target="_parent">Item Price List w/Prf %   </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Inquiries                    </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 9);" target="_parent">Inventory Receipts History</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 10);" target="_parent"> Inventory (w/windows)     </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Reports                           </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 13);" target="_parent"> Stock Status (1 line per item)</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 14);" target="_parent"> Stock Status (includes details</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 15);" target="_parent"> Inventory (PO & Order details)</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 16);" target="_parent"> Inventory Turns               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 17);" target="_parent"> Inventory Value               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 18);" target="_parent"> Inventory Ageing              </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 19);" target="_parent"> Buying Report                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 20);" target="_parent"> Inventory Transfers           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 21);" target="_parent"> 12 Months Sales               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 3, 22);" target="_parent"> Custom Inventory Menu</a></span></li>											
								<li><span class="testtxt3"><a title = "" href="#"> More Inventory Reports        </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 1);" target="_parent">Item Price Change Worksheet   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 2);" target="_parent">Laminate ranking              </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 3);" target="_parent">Item ranking                  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 4);" target="_parent">Inventory Receipts History    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 5);" target="_parent">Class Code File               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 6);" target="_parent">End of Day Buying Rpt Setup   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 7);" target="_parent">Inventory Receipts Register   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 8);" target="_parent">Inventory Recpt Hist By Source</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 9);" target="_parent">Inventory Holds Ageing        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 10);" target="_parent"> Inventory Analysis (IM640)    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 11);" target="_parent"> Inventory Transfers (IT055)   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 12);" target="_parent"> Specials List                 </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 13);" target="_parent"> Print Bin Locations by Section</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 14);" target="_parent"> Print Bin Locations by Bin    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 15);" target="_parent"> Print Pick List               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 16);" target="_parent"> Print Roll Goods Pick List    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 17);" target="_parent"> Aged Inventory Summary (IM165)</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 18);" target="_parent"> Inventory Holds List          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 19);" target="_parent"> Inventory Turns (cur mo only) </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 20);" target="_parent"> Factory/Warehouse             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 21);" target="_parent"> Buying Report (IM540)         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 3, 23, 22);" target="_parent"> Inventory Adj by Reason Code  </a></span></li>										
									<li><span class="testtxt3"><a title = "" href="#"> More Inventory Reports        </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 1);" target="_parent">Item Master Price Change Wksh      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 2);" target="_parent">Stock Status by Dye Lot            </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 3);" target="_parent">Inventory Turns (IM321)            </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 4);" target="_parent">Empty Bin Report                   </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 5);" target="_parent">Inventory Rec/History by Vend      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 6);" target="_parent">Inactive Item List                 </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 7);" target="_parent">Non-Movement Inventory report      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 8);" target="_parent">Cycle Count History Report         </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 9);" target="_parent">Last Counted Report                </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 10);" target="_parent"> Pre-Inventory count sheets         </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 11);" target="_parent"> Stock Status/as of Prior Date      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 12);" target="_parent"> Back Order Report by Vendor (BO025)</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 13);" target="_parent"> Stocking Item List                 </a></span></li>
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 14);" target="_parent"> Hits Report                        </a></span></li>
										<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 3, 23, 23, 15);" target="_parent"> Inactive Items with Stock          </a></span></li>
										</ul></li>		
										</ul></li>	
										</ul></li>
										</ul></li>						
						<li><span class="testtxt3"><a title = "" href="#">Inventory Inquiry Menu               </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 1);" target="_parent">Inventory Receipts History </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 2);" target="_parent">Inventory Inquiry w/Windows</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 3);" target="_parent">Purchase Order by Item     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 4);" target="_parent">Purchase Order by Vendor   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 5);" target="_parent">Inventory History Tracking </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 6);" target="_parent">Inventory Value            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 7);" target="_parent">Inventory Detail           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 8);" target="_parent">Back Order Inquiry & Attach</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 9);" target="_parent">Customer Inventory Holds   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 10);" target="_parent"> Inventory Status           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 11);" target="_parent"> Laminate Inventory Cut     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 12);" target="_parent"> Inventory Holds            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 13);" target="_parent"> Unit of Measure                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 14);" target="_parent"> Quantity Breaks                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 15);" target="_parent"> Product Line Analysis            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 16);" target="_parent"> Product Line Summary             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 17);" target="_parent"> Item Summary                     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 18);" target="_parent"> View Inventory by Product Code   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 19);" target="_parent"> Vendor Inventory Summary         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 20);" target="_parent"> Item Ranking                     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 21);" target="_parent"> Purchase History                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 4, 22);" target="_parent"> View Complementary Item Where Usd</a></span></li>												
							<li><span class="testtxt3"><a title = "" href="#"> More Inventory Inquiries         </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 4, 23, 1);" target="_parent">Create Inv Analysis Work File               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 4, 23, 2);" target="_parent">View Work File From 1 Above                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 4, 23, 3);" target="_parent">Customer Inventory Guarantee Inquiry by Item</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 4, 23, 4);" target="_parent">Direct Purchase Order Inquiry               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 4, 23, 5);" target="_parent"> Who is buying an item?                      </a></span></li>
								</ul></li>	
								</ul></li>										
						<li><span class="testtxt3"><a title = "" href="#">Physical Inventory Menu              </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#"> Counts                           </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 1);" target="_parent">Print Count Sheet             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 2);" target="_parent">Enter Counts                  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 3);" target="_parent">Inventory Detail Inquiry      </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#"> Re-Counts                        </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 5);" target="_parent">Enter Items to be Re-counted  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 6);" target="_parent">Print Re-count Work Sheet     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 7);" target="_parent">Enter Re-counts               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 8);" target="_parent">Force Re-count (items w/var)  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 9);" target="_parent"> Count Sheet Selection Maint.  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 10);" target="_parent"> Print Inventory Variance      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 11);" target="_parent"> Finalize Physical Inventory   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 12);" target="_parent"> Print Count Sheet by Item Rank</a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Other reports                     </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 13);" target="_parent"> Print Complete Inventory      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 14);" target="_parent"> Complete Inventory Summary    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 15);" target="_parent"> Print Items with No Count     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 16);" target="_parent"> Print Items with Zero Balance </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 17);" target="_parent"> Re-Print Count Sheet          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 18);" target="_parent"> Print Duplicated Items        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 19);" target="_parent"> Maintain Default Selection    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 20);" target="_parent"> Maintain Items to Summarize   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 21);" target="_parent"> Set Flag Code                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 22);" target="_parent"> Flag Transferred Inventory    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 5, 23);" target="_parent"> Custom Physical Inventory Menu</a></span></li>
								 
								 </ul></li>	
								 </ul></li>										
						<li><span class="testtxt3"><a title = "" href="#"> Purchase Order Entry/Update Menu     </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Purchase Order                  </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 1);" target="_parent">Enter Or Update A P/O       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 2);" target="_parent">Print Purchase Order        </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">On-Line P/O                     </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 4);" target="_parent">Create P/O Work File        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 5);" target="_parent">Update P/O Files            </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Receipts                        </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 6);" target="_parent">Enter P/O Receipts By Item  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 7);" target="_parent">Enter P/O Receipts          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 8);" target="_parent">Make Changes To Receipts    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 9);" target="_parent">Update Receipts To Inventory</a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Manifest                        </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#"> Manifest Receiving Menu     </a></span>
								<ul>											
									<li><span class="testtxt3"><a title = "" href="#">Manifest Receipts               </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 1);" target="_parent">Make changes to manifest dtl.</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 2);" target="_parent">Manifest receipts edit/update</a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">P/O Receipts                    </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 3);" target="_parent">Make changes to receipts     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 4);" target="_parent">Update receipts to inventory </a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Match Back Orders               </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 5);" target="_parent">Match back orders to receipts</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 6);" target="_parent">Update matched back orders   </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 7);" target="_parent">Enter pre-receive by item    </a></span></li>									
									    <li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 8);" target="_parent"> Manifest maintenance</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 9);" target="_parent"> Enter pre-receive shipments  </a></span></li>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 10);" target="_parent"> Make changes to Pre Receive  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 11);" target="_parent"> Receipts History Maint       </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 12);" target="_parent"> Manifest Consolidation       </a></span></li>
										</ul></li>	
										<li><span class="testtxt3"><a title = "" href="#">Reports                   </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 13);" target="_parent">Manifest listing      				</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 14);" target="_parent">PO/Manifest Delivery Summary   	</a></span></li>
										</ul></li>	
									<li><span class="testtxt3"><a title = "" href="#">Labels                   </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 17);" target="_parent">Pre Receiving Tags by Manifest     </a></span></li>									
										
										</ul></li>
									<li><span class="testtxt3"><a title = "" href="#">EDI                    </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 18);" target="_parent">Process Inbound 856 EDI File      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 19);" target="_parent">EDI 856 Error File Maintenance 	</a></span></li>
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 10, 22);" target="_parent">Laminate Item Tag Maintenance  	</a></span></li>
										</ul></li>	
										</ul></li>	
										
														
								<li><span class="testtxt3"><a title = "" href="#"> Laminate Receiving Menu     </a></span>
								<ul>											
									<li><span class="testtxt3"><a title = "" href="#">Manifest receipts               </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 1);" target="_parent">Make changes to manifest     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 2);" target="_parent">Manifest receipts edit/update</a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">P/O receipts                    </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 3);" target="_parent">Make changes to receipts     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 4);" target="_parent">Update receipts to inventory </a></span></li>
										</ul>
										</li>									
									<li><span class="testtxt3"><a title = "" href="#">Match back orders               </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 5);" target="_parent">Match back orders to receipts</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 6);" target="_parent">Update matched back orders   </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 8);" target="_parent">Manifest maintenance         </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 9);" target="_parent">Pre Receive Shipments        </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 10);" target="_parent"> Receipts History Maint       </a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Reports                           </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 13);" target="_parent"> Manifest listing              </a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Labels                            </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 17);" target="_parent"> Pre Receiving Tags by Manifest</a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">EDI Process                       </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 18);" target="_parent"> Process Uploaded PC file      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 11, 19);" target="_parent"> Process EZLINK PO creation    </a></span></li>
										</ul></li>	
										</ul></li>								
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 12);" target="_parent"> Directs Receiving           </a></span></li>	
								</ul></li>										
							<li><span class="testtxt3"><a title = "" href="#">Match Back Orders                </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 13);" target="_parent"> Match Back Orders To Receipts</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 14);" target="_parent"> Update Matched Back Orders   </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Maintenance                      </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 15);" target="_parent"> Vendor Master Maintenance    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 16);" target="_parent"> Item Master Maintenance      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 17);" target="_parent"> View B/O Not On P/O          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 18);" target="_parent"> Create P/O From B/O          </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Inquiries                        </a></span>	
							<ul>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 19);" target="_parent"> P/O Inquiry By Vendor        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 20);" target="_parent"> P/O Inquiry By Vendor-Items  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 21);" target="_parent"> P/O Inquiry By Item          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 6, 22);" target="_parent"> Inventory Inquiry            </a></span></li>											
								<li><span class="testtxt3"><a title = "" href="#"> More P/O Update Menu         </a></span>
								<ul>											
									<li><span class="testtxt3"><a title = "" href="#">Maintenance                     </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 1);" target="_parent">P/O Terms Files              </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 2);" target="_parent">P/O Ship Via                 </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 3);" target="_parent">P/O Freight                  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 4);" target="_parent">Re-order Location Consolidatn</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 5);" target="_parent">Global Purchase Order Close  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 6);" target="_parent">PO TO AP Detail-EOD          </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 7);" target="_parent">Manifest Master file         </a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Reversal                        </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 8);" target="_parent">Enter P/O Returns            </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 9);" target="_parent">Make Changes/Update Returns  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 10);" target="_parent"> Mill Inquiry                 </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 11);" target="_parent"> PO/Manifest Delivery Summary </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 12);" target="_parent"> Assign a P/O Number          </a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Other                           </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 13);" target="_parent"> Convert a Sales Order       </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 14);" target="_parent"> Back Order Inquiry & Attach </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 15);" target="_parent"> View B/Os Attached to a P/O </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 16);" target="_parent"> P/O for Review              </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 17);" target="_parent"> P/O Loads                   </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 18);" target="_parent"> P/O Vendor Loads            </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 19);" target="_parent"> P/O Release History         </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 20);" target="_parent"> Seasonal Factor File Maint. </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 21);" target="_parent"> Purchase Authorization Level</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 6, 23, 22);" target="_parent"> P/O Manager Maintenance     </a></span></li>									
										<li><span class="testtxt3"><a title = "" href="#"> More P/O Update Menu        </a></span>
										<ul>									
											<li><span class="testtxt3"><a title = "" href="#">Transfers                          </a></span>
											<ul>								
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 6, 23, 23, 1);" target="_parent">Setup for Transfer Replenishment</a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 6, 23, 23, 2);" target="_parent">Post Transfer Replenishment     </a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 6, 23, 23, 5);" target="_parent">Create Transfers & Print        </a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 6, 23, 23, 6);" target="_parent">View & Maintain Transfer Work Fl</a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 6, 23, 23, 7);" target="_parent">Post Transfers                  </a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 6, 23, 23, 11);" target="_parent"> View All Items with Sales Flag</a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(11, 6, 23, 23, 12);" target="_parent"> View an Item with Sales Flag    </a></span></li>
												</ul></li>
												</ul></li>
												</ul></li>	
												</ul></li>	
												</ul></li>	
												</ul></li>				
						<li><span class="testtxt3"><a title = "" href="#"> Purchase Order Reports Menu          </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Reports                       </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 1);" target="_parent">Print Purchase Order       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 2);" target="_parent">Purchase Order             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 3);" target="_parent">Open Order List By Customer</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 4);" target="_parent">Open Order List By Item    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 5);" target="_parent">Commitment By Vendor       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 6);" target="_parent">Commitment By Item         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 7);" target="_parent">Open P/O                   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 8);" target="_parent">P/O List By Vnd/PO/or Br   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 9);" target="_parent">P/O List By Due Date       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 10);" target="_parent"> Back Order Report (BO200)  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 11);" target="_parent"> Back Order Report By User  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 12);" target="_parent"> Purchasing Terms Listing   </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Inquiries                </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 19);" target="_parent"> P/O Inquiry By Vendor</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 20);" target="_parent"> P/O Inquiry By Item  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 7, 21);" target="_parent"> Inventory Inquiry    </a></span></li>											
								<li><span class="testtxt3"><a title = "" href="#"> PO To A/P Menu</a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 7, 23, 1);" target="_parent">Open P/O to A/P Report             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 7, 23, 2);" target="_parent">P/O to A/P Detail History Report   </a></span></li>										
																			
																			
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 7, 23, 5);" target="_parent">Open P/O to A/P Inquiry            </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 7, 23, 6);" target="_parent">P/O to A/P Detail History Inquiry  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 7, 23, 7);" target="_parent">Purchase History Inquiry           </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(11, 7, 23, 8);" target="_parent">Open & Vouchered P/O to A/P Inquiry</a></span></li>										
									</ul></li>			
									</ul></li>
									</ul></li>							
																			
																			
																			
																			
																			
																			
																			
						<li><span class="testtxt3"><a title = "" href="#"> Bar Code Label Menu                  </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 1);" target="_parent">Print Bar Coded Bin Locations                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 2);" target="_parent">Maintain Bin Location File                   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 3);" target="_parent">Print Bar Coded Item numbers                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 4);" target="_parent">Configure Intermec 3400 printers             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 5);" target="_parent">Print Generic Inventory labels               </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 6);" target="_parent">Print Inventory tags by bin                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 7);" target="_parent">Print Shipping labels for staging            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 8);" target="_parent">Staging Open Order listing by Ship Date      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 9);" target="_parent">Change bin locations on inventory            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 10);" target="_parent"> Print barcoded User ID tags                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 11);" target="_parent"> Open Order listing for Roll Goods            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 12);" target="_parent"> Print Shipping Labels from Pick Ticket Scan  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 8, 13);" target="_parent"> Configure Intermec PL4 / Zebra QL420 printers</a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#"> Lot Control Menu                     </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 9, 1);" target="_parent">Define location usage as it pertains to inventory.                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 9, 2);" target="_parent">Define the location search (pecking) order for inventory selection.</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 9, 3);" target="_parent">Define controls by family                                          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 9, 4);" target="_parent">Define controls by product line.                                   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(11, 9, 5);" target="_parent"> Define controls by item (SKU)                                      </a></span></li>
							</ul></li>												
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(11, 10);" target="_parent"> Customer Loans                       </a></span></li>	
						</ul></li>
						</ul></li>
																		
					<li><span class="testtxt3"><a title = "" href="#"> Order Entry/Invoicing         </a></span>
					<ul>														
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 1);" target="_parent">Order Management</a></span></li>												
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 2);" target="_parent">Print a Pick Ticket          </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 3);" target="_parent">Print Open Order List        </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 4);" target="_parent">Print Open Order List by Item</a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 5);" target="_parent">Select Orders for Invoicing  </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 6);" target="_parent">Print pre-invoicing edit     </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 7);" target="_parent">Print and Post Invoices      </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 8);" target="_parent">Print Accumulated Invoices   </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 9);" target="_parent">Print an Invoice Copy        </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 10);" target="_parent"> Enter/Change Invoice Message </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 11);" target="_parent"> Floor Covering Cut Processing</a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 12);" target="_parent"> Batch Order Entry            </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 13);" target="_parent"> Stage/Ship Orders            </a></span></li>													
						<li><span class="testtxt3"><a title = "" href="#"> Pricing Menu                 </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Menus                      </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">Pricing File Maintenance</a></span>
								<ul>											
									<li><span class="testtxt3"><a title = "" href="#">File Maintenance             </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 1);" target="_parent">Miscellaneous Charge Desc </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 2);" target="_parent">Volume/Group File         </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 3);" target="_parent">Tax Master File           </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 4);" target="_parent">Terms Master File         </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 5);" target="_parent">Customer Terms File       </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 6);" target="_parent">Customer Misc Charges     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 7);" target="_parent">Customer Line Item Charges</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 8);" target="_parent">Dealer/Exception Pricing  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 8);" target="_parent">Ship Via File             </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 10);" target="_parent"> Item Master File          </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 11);" target="_parent"> Item Price File           </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 12);" target="_parent"> Product Price File        </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 13);" target="_parent"> Family Code File             </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 14);" target="_parent"> Item/Product Special Price   </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 15);" target="_parent"> Item/Product Special Charges </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 16);" target="_parent"> Cuts @ Roll                  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 17);" target="_parent"> Vendor/Price Code Setup      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 18);" target="_parent"> Roll Sale Setup              </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 19);" target="_parent"> Delete Expired Price Exc Recs</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 20);" target="_parent"> Price List Template Maint    </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 21);" target="_parent"> Which Location To Use/Pricing</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 1, 22);" target="_parent"> Which Loc To Use/Pricng/Cust </a></span></li>									
										<li><span class="testtxt3"><a title = "" href="#"> More Pricing Options         </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 1);" target="_parent">Mfg. Misc. Cost Description    </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 2);" target="_parent">Determine Customer Pricing     </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 3);" target="_parent">Price Exception Global Changes </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 4);" target="_parent">Price Exception History Inquiry</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 5);" target="_parent">Price Code File Maintenance    </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 6);" target="_parent">Price Exception Global Copy    </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 7);" target="_parent">Item Price Global Changes      </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 8);" target="_parent">Price Matrix Code Maintenance  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 9);" target="_parent">Item Master Price Changes      </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 10);" target="_parent"> Trip Menu                      </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 11);" target="_parent"> Delivery Charge Increments     </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 12);" target="_parent"> Price Check                    </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 13);" target="_parent"> Sell Group Master              </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 14);" target="_parent"> Sell Group Detail              </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 15);" target="_parent"> View Sell Group Prices         </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 16);" target="_parent"> Price Exception w/Profit(PE395)</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo5(12, 14, 1, 23, 17);" target="_parent"> Order Add On Maintenance       </a></span></li>
											</ul></li>
											</ul></li>
											</ul></li>								
								<li><span class="testtxt3"><a title = "" href="#">Pricing Reports         </a></span>
								<ul>											
									<li><span class="testtxt3"><a title = "" href="#">Reports                          </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 1);" target="_parent">Volume/Group Discount File    </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 2);" target="_parent">Price Exception File (PE110)  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 3);" target="_parent">Price Exception (PE111)       </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 4);" target="_parent">Misc Charges Descriptions     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 5);" target="_parent">Price Exception Usage (PE330) </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 6);" target="_parent">Price Exception (PE350)       </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 7);" target="_parent">Customer Price Sheets (PE380) </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 8);" target="_parent">Customer Price Report (PE370) </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 9);" target="_parent">Salesman Price Sheets (PE380S)</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 10);" target="_parent"> Price List From Template      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 11);" target="_parent"> Item Price List               </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 12);" target="_parent"> Item Price File List          </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 13);" target="_parent"> Item Price List w/Prf %        </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 14);" target="_parent"> Product Price List             </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 15);" target="_parent"> Item Special Price List        </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 16);" target="_parent"> Rebates Recalculated Report    </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 17);" target="_parent"> Price Exception Expire (PE146) </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 18);" target="_parent"> Price Exception w/Sales (PE250)</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 19);" target="_parent"> Specials List                  </a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Inquiries                          </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 20);" target="_parent"> Price Exception File           </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 21);" target="_parent"> Create Price Catalog Excel File  (file name=PL200PRICE)</a></span></li>									
										            									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 22);" target="_parent"> Rebate Report                  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 2, 23);" target="_parent"> Customer Price Sheet (PE390)   </a></span></li>
										</ul></li>
										</ul></li>									
								<li><span class="testtxt3"><a title = "" href="#">Trip Menu               </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 1);" target="_parent">Master file maintenance                          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 2);" target="_parent">Trip customer file maintenance                   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 3);" target="_parent">Trip item file maintenance                       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 4);" target="_parent">Master file list                                 </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 5);" target="_parent">Trip inquiry                                     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 6);" target="_parent">Enter trip transactions                          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 7);" target="_parent">Trip/customer sales report-TP100-sales only      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 8);" target="_parent">Trip/customer sales report-TP110-all transactions</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 9);" target="_parent">Trip detail report                               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 10);" target="_parent"> Trip statement                                   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 11);" target="_parent"> Trip End of Year                                 </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 12);" target="_parent"> Trip file purge                                  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 14, 3, 13);" target="_parent"> Trip Setup                                       </a></span></li>
									</ul></li>
									</ul></li>
									</ul></li>										
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 15);" target="_parent"> Print Truck Manifest         </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 16);" target="_parent"> Transfer Receipts Entry      </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 17);" target="_parent"> Transfer Receipts Update     </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 18);" target="_parent"> Print COD Report by Route    </a></span></li>													
						<li><span class="testtxt3"><a title = "" href="#">Other reports                    </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 19);" target="_parent"> Re-print Orders for Invoicing</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 20);" target="_parent"> Print Open Order Labels      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 21);" target="_parent"> Re-Print Sales Order         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(12, 22);" target="_parent"> Open Orders by User          </a></span></li>												
							<li><span class="testtxt3"><a title = "" href="#"> More O/E-Inv Options         </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 1);" target="_parent">Customer Order Template Maint.   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 2);" target="_parent">Batch Order Template Maint.      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 3);" target="_parent">Add cost to an order/invoice     </a></span></li>											
								<li><span class="testtxt3"><a title = "" href="#">Cash Drawer Menu                 </a></span>
								<ul>											
									<li><span class="testtxt3"><a title = "" href="#">Maintenance          </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 4, 1);" target="_parent">Cash Drawer       </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 4, 2);" target="_parent">Method of Payment </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 4, 3);" target="_parent">Payment on Account</a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Inquiries            </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 4, 7);" target="_parent"> View Cash Drawer  </a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Reports                       </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 4, 13);" target="_parent"> Cash Drawer Balancing     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 4, 15);" target="_parent"> Cash Drawer History Report</a></span></li>
										</ul></li>	
										</ul></li>								
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 5);" target="_parent"> Open Order Report (OO166)        </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 6);" target="_parent"> Orders for Review                </a></span></li>											
								<li><span class="testtxt3"><a title = "" href="#">SPIFF Menu                       </a></span>	
								<ul>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 7, 1);" target="_parent">Dealer Employee Setup (Vendor Master)</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 7, 2);" target="_parent">Register a Spiff                     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 7, 3);" target="_parent">Enter a Spiff on an Order            </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 7, 4);" target="_parent">Enter a Spiff on an Invoice          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 7, 11);" target="_parent"> Spiff Display                        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(12, 23, 7, 13);" target="_parent"> Spiff Report                         </a></span></li>
									</ul></li>										
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 8);" target="_parent">Orders by Route Report           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 9);" target="_parent">Re-price Open Orders             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 10);" target="_parent"> Mill Inquiry                     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 11);" target="_parent"> Daily Fill Rate Percentage Report</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 13);" target="_parent"> Order Status Inquiry             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 14);" target="_parent"> Back Orders Coded Ship Complete  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 15);" target="_parent"> Cancelled Orders                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 16);" target="_parent"> Cancelled Orders by Customer     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 17);" target="_parent"> Cancelled Orders by Item         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 18);" target="_parent"> Work with Review Reason Codes    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 19);" target="_parent"> Sales Exception Report           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 20);" target="_parent"> Sales & GP by Rep, Div & Location</a></span></li>											
								<li><span class="testtxt3"><a title = "" href="#">On-Demand Invoicing                  </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(12, 23, 22);" target="_parent"> On-Demand Invoice Post           </a></span></li>
									</ul></li>
									</ul></li>
									</ul></li>
									</ul></li>										
					<li><span class="testtxt3"><a title = "" href="#"> Sales Analysis                </a></span>
					<ul>														
						<li><span class="testtxt3"><a title = "" href="#">Reports                        </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,1);" target="_parent">(DMS) Sales Reporting       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,2);" target="_parent">Top 10 Customers/Items      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,3);" target="_parent">Item Ranking                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,4);" target="_parent">Salesman Comparative Sales  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,5);" target="_parent">Rebate/Promo                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,6);" target="_parent">Invoice Copies              </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,7);" target="_parent">Salesman by Product         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,8);" target="_parent">Customer by Product         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,9);" target="_parent">Product Sales               </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,10);" target="_parent"> Sales Tax from History      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,11);" target="_parent"> Sales Analysis by Class Code</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,12);" target="_parent"> Customer Ranking            </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Inquiries                     </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,13);" target="_parent"> Sales Inquiry by Customer </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,14);" target="_parent"> Sales Inquiry by Item     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,15);" target="_parent"> Customer Sales Analysis   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,16);" target="_parent"> Item Sales Analysis       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,17);" target="_parent"> Sales Rep Analysis        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,18);" target="_parent"> Vendor Sales Analysis     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,19);" target="_parent"> Vendor Analysis           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,20);" target="_parent"> Work with Open Orders     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,21);" target="_parent"> Work with Sales History   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(13,22);" target="_parent"> Inventory History Tracking</a></span></li>												
							<li><span class="testtxt3"><a title = "" href="#"> More Sales Analysis       </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#"> Other Functions                  </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 1);" target="_parent">Segment Analysis              </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 2);" target="_parent">Product Sales Analysis        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 3);" target="_parent">Line Item Sales by Location   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 4);" target="_parent">Line Item Sales by Customer   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 5);" target="_parent">Invoices by User              </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 6);" target="_parent">Sales Analysis Report (SH695) </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 7);" target="_parent">Line item Misc Charge report  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 8);" target="_parent">Salesman Product Sales Report </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 9);" target="_parent">Print Invoice Register-History</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 10);" target="_parent"> Salesman Line Item (SA040)    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 11);" target="_parent"> Customer/Product Sales By Date</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 12);" target="_parent"> Year to Date Customer Sales   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 13);" target="_parent"> Sales Analysis by G/L          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 14);" target="_parent"> Sales Summary by G/L (SH760)   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 15);" target="_parent"> Sales by State/County          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 16);" target="_parent"> Customer/Product Sales Report  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 17);" target="_parent"> Analysis by Prod Code by Div   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 18);" target="_parent"> Salesman Analysis-G/L (SH785/6)</a></span></li>										
									<li><span class="testtxt3"><a title = "" href="#"> Sales Budget Menu              </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 19, 1);" target="_parent">Company Budget Maintenance      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 19, 2);" target="_parent">Location Budget Maintenance     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 19, 3);" target="_parent">Sales Rep Budget Maintenance    </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 19, 4);" target="_parent">Customer/Vendor Budget-Potential</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 19, 5);" target="_parent"> Default Budget Percentages      </a></span></li>
										</ul></li>									
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 20);" target="_parent"> Salesman/Customer/Class Report </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 21);" target="_parent"> Customer G/L Summary           </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(13, 23, 22);" target="_parent"> Weekly Sales Analysis          </a></span></li>										
									<li><span class="testtxt3"><a title = "" href="#"> More Sales Analysis            </a></span>
									<ul>											
										<li><span class="testtxt3"><a title = "" href="#"> Additional Reports               </a></span>
										<ul>										
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 1);" target="_parent">Order Report by Ship Date     </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 2);" target="_parent">Invoice Report-Cost Breakdown </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 3);" target="_parent">Backorder Report-Cost Breakdwn</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 4);" target="_parent">Ship Report by Customer Class </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 5);" target="_parent">Order Report by State/Salesman</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 6);" target="_parent">Sales by Location/Division    </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 7);" target="_parent">Non-Product Summary           </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 8);" target="_parent">Credit Reason                 </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 9);" target="_parent">Invoice Summary by Salesman   </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 10);" target="_parent"> Factory Warehouse by Salesman </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 11);" target="_parent"> County History Report         </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 12);" target="_parent"> Salesman Sales Summary Report </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 13);" target="_parent"> Customer Summary (CI170)       </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 14);" target="_parent"> Rebate Report (SH950)          </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 15);" target="_parent"> Order Report by Customer Class </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 16);" target="_parent"> Sales by Division Within Slsmn </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 17);" target="_parent"> Ship Report by Class/Ship-To   </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 18);" target="_parent"> Sales by Loc/Div (DMS SA Fmt)  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 19);" target="_parent"> Top performers (reps) by rpt cd</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 20);" target="_parent"> Sales By Slsm/Lo/Dv(DMS SA Fmt)</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 21);" target="_parent"> Invoice Report-Cost by Class   </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(13, 23, 23, 22);" target="_parent"> 13 Months Sales                </a></span></li>								
											<li><span class="testtxt3"><a title = "" href="#"> More Sales Analysis            </a></span>
											<ul>									
												<li><span class="testtxt3"><a title = "" href="#"> Additional Reports                 </a></span>
												<ul>								
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 1);" target="_parent">Division G/L Sales Summary      </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 2);" target="_parent">Sales Summary (SH725)           </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 3);" target="_parent">Sales Analysis by Class Code II </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 4);" target="_parent">Credit Memos Created from RAs   </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 5);" target="_parent">Vendor Daily Analysis           </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 6);" target="_parent">Sales by serial number          </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 7);" target="_parent">Sales by Bill-to w/i Item       </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 8);" target="_parent">Invoice list by date/weight     </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 9);" target="_parent">Route Analysis by invoice date  </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 10);" target="_parent"> Sales by Division/Product Code  </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 11);" target="_parent"> Sales by Customer/Div/Prod Code </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 12);" target="_parent"> Sales by Sales Rep/Div/Prod Code</a></span></li>
													</ul></li>						
												<li><span class="testtxt3"><a title = "" href="#">Additional Inquiries                 </a></span>
												<ul>								
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 13);" target="_parent"> Sales Inquiry by Price Chg Reas  </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 14);" target="_parent"> Sales Inquiry by Spiff           </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 15);" target="_parent"> Sales Inquiry by Manager/Class Cd</a></span></li>
													</ul></li>						
												<li><span class="testtxt3"><a title = "" href="#">Additional Reports                   </a></span>
												<ul>								
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 19);" target="_parent"> Sales by Div/Product (SH990RPXD) </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 20);" target="_parent"> Sales by Rep/Cust/Class (SH795)  </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 21);" target="_parent"> Sales Rep/Cust/Vend Budget vs Sls</a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 22);" target="_parent"> Sales Rep/Div/Budget (SH995RPXS) </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(13, 23, 23, 23, 23);" target="_parent"> Customer Sales/Category (SR010R) </a></span></li>
													</ul></li>
													</ul></li>	
													</ul></li>
													</ul></li>
													</ul></li>	
													</ul></li>
													</ul></li>
													</ul>	</li>				
					<li><span class="testtxt3"><a title = "" href="#"> Sales x Press                 </a></span>
					<ul>															
						<li><span class="testtxt3"><a title = "" href="#">Maintenance                    </a></span>
						<ul>														
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 1);" target="_parent">Sales Rep Activity          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 2);" target="_parent">Sales Rep Calls             </a></span></li>												
								</ul></li>											
																			
																			
																			
																			
																			
						<li><span class="testtxt3"><a title = "" href="#">Inquiries                      </a></span>	
						<ul>	
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 3);" target="_parent"> Sales Order       			</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 4);" target="_parent"> Invoice History       		</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 5);" target="_parent"> Customer Ranking 		 		</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 6);" target="_parent"> Purchase Order   		 		</a></span></li>										
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 9);" target="_parent"> Sales Detail Inquiry        	</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 10);" target="_parent">Sales Rep System              </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 11);" target="_parent">Sales Rep Comm from/Receipts  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 12);" target="_parent">View Back Orders by Rep       </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Reports                          </a></span>	
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 13);" target="_parent"> Salesman Line Item           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 14);" target="_parent"> Salesman Comparative Sales   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 15);" target="_parent"> Salesman by Product Code     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 16);" target="_parent"> Salesman Commission by Profit</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 17);" target="_parent"> Salesman Commission          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 18);" target="_parent"> Salesman Activity            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 19);" target="_parent"> Salesman Activity (12 months)</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 20);" target="_parent"> Salesman by Class Code       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 21);" target="_parent"> Salesman by Customer Ranking </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(14, 22);" target="_parent"> Slsmn by Cust Ranking/Address</a></span></li>												
							<li><span class="testtxt3"><a title = "" href="#"> Sales Rep Menu #2            </a></span>		
							<ul>											
								<li><span class="testtxt3"><a title = "" href="#">Reports                            </a></span>
								<ul>												
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(14, 23, 1);" target="_parent">Customer Comparative (SH435)    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(14, 23, 2);" target="_parent">Customer Master (Full Info)     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(14, 23, 3);" target="_parent">Customer Master (Single Line)   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(14, 23, 4);" target="_parent">Territory Sls By Cust/Family    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(14, 23, 5);" target="_parent"> Sales by Customer/Div/Prod Code </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(14, 23, 6);" target="_parent"> Sales by Sales Rep/Div/Prod Code</a></span></li>
									</ul></li>
									</ul></li>
									</ul></li>
										</ul>	</li>									
					<li><span class="testtxt3"><a title = "" href="#"> Miscellaneous File Maintenance</a></span>
					<ul>															
						<li><span class="testtxt3"><a title = "" href="#">File Maintenance               </a></span>
						<ul>														
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 1);" target="_parent">Customer Item File          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 2);" target="_parent">Product Code Sales/Budget   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 3);" target="_parent">Customer/Inventory/Sales Loc</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 4);" target="_parent">Shipping Instructions       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 5);" target="_parent">Budget File                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 6);" target="_parent">Budget Analysis History     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 7);" target="_parent">Customer Class Code Desc    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 8);" target="_parent">Reports Parameters          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 9);" target="_parent">Collector                   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 10);" target="_parent"> Customer Order Type         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 11);" target="_parent"> Cash Drawer                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 12);" target="_parent"> County File                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 13);" target="_parent"> County Budget                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 14);" target="_parent"> Order Type Codes             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 15);" target="_parent"> Credit Reason                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 16);" target="_parent"> Inventory Adj Reason         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 17);" target="_parent"> Method of Payment            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 18);" target="_parent"> Pick Ticket Printer          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 19);" target="_parent"> Directional Pull Priority    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 20);" target="_parent"> Credit Message User          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 21);" target="_parent"> Sales Analysis Color Coding  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(15, 22);" target="_parent"> G/L to Exclude from Sales Rpt</a></span></li>
																			
							<li><span class="testtxt3"><a title = "" href="#"> More Misc File Maintenance   </a></span>	
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">File Maintenance                  </a></span>
								<ul>												
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 1);" target="_parent">Family Code Type File          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 2);" target="_parent">Price Change Reason File       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 3);" target="_parent">User Message File              </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 4);" target="_parent">Create NACM Tape               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 5);" target="_parent">Vendor Budget                  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 6);" target="_parent">Report Code Budget             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 7);" target="_parent">Setup Pick Ticket/Whse printers</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 8);" target="_parent">Salesman/Report Code Budget    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 9);" target="_parent">Sales Categories               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 10);" target="_parent"> Create Experian NACM Tape      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 11);" target="_parent"> Create CCI File                </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 12);" target="_parent"> SH890 Report Setup             </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Reports                            </a></span>
								<ul>												
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 13);" target="_parent"> Sales/Budget by Report Code    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 14);" target="_parent"> Sales/Budget by Sls Rep/Rpt Cd </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 15);" target="_parent"> Sales by Category              </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 16);" target="_parent"> Salesman/Division Budget Rpt   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 17);" target="_parent"> Price Over-Ride Security       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 18);" target="_parent"> Sales/Budget by Sls Rep/Cls Cd </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 19);" target="_parent"> Cut Machine control maint.     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 20);" target="_parent"> Sales/Budget by Loc/Rep/Div/Cls</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 21);" target="_parent"> Global Bin Location Change     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(15, 23, 22);" target="_parent"> Vendor Cross Ref Maintenance   </a></span></li>										
									<li><span class="testtxt3"><a title = "" href="#"> More Misc File Maintenance     </a></span>
									<ul>											
										<li><span class="testtxt3"><a title = "" href="#">File Maintenance                    </a></span>
										<ul>										
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 1);" target="_parent">Convert Old to New Vendor        </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 2);" target="_parent">User Defined Fields Description  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 3);" target="_parent">UDF Validation Table             </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 4);" target="_parent">UDF Code Validation Table        </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 5);" target="_parent">Re-average Cost on Avg Cost Items</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 6);" target="_parent">Order Source File                </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 7);" target="_parent">Setup Sales Period Headers       </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 8);" target="_parent">Create Credit LINK File(CLFILE)  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 9);" target="_parent">Setup Orders for Review Msg User </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 10);" target="_parent"> Customer xref Maint (SA ONLY)    </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 11);" target="_parent"> Customer Hold Reason Codes       </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 12);" target="_parent"> Report Printer                   </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 13);" target="_parent"> View All Open Batches            </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 14);" target="_parent"> Productivity Department Xref     </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 15);" target="_parent"> Gross Profit Margin Authorization</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 16);" target="_parent"> Customer Contact Document Types  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 17);" target="_parent"> Create NACM Midwest file         </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 18);" target="_parent"> Work with NACM Midwest comments  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 19);" target="_parent"> Restrict status chg of EDI orders</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 20);" target="_parent"> Purge customers                  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 21);" target="_parent"> Order Cancellation Reason        </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 22);" target="_parent"> Item xref Maintenance            </a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(15, 23, 23, 23);" target="_parent"> Text Master     		            </a></span></li>
											</ul></li>		
											</ul></li>
											</ul></li>	
											</ul></li>
											</ul></li>
										</ul>	</li>					
					<li><span class="testtxt3"><a title = "" href="#"> Miscellaneous File Reports    </a></span>
					<ul>															
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 1);" target="_parent">Ship Via description          </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 2);" target="_parent">Performance Tracking Report   </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 3);" target="_parent">Budget analysis by product    </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 4);" target="_parent">Budget analysis by salesman   </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 5);" target="_parent">Inter-Company File            </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 6);" target="_parent">User tracking orders taken    </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 7);" target="_parent">Daily Cuts Report             </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 8);" target="_parent">Roll Goods Pick List Selection</a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 9);" target="_parent">Cut Table/RF Activity reports </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 10);" target="_parent"> Budget analysis by vendor     </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 11);" target="_parent"> View batch history            </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 12);" target="_parent"> Cut Table status report       </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(16, 13);" target="_parent"> Document Delivery System      </a></span></li>
						</ul></li>														
					<li><span class="testtxt3"><a title = "" href="#"> Warehouse x Press             </a></span>
					<ul>														
						<li><span class="testtxt3"><a title = "" href="#">File Maintenance              </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 1);" target="_parent">Shipping Instructions      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 2);" target="_parent">Warehouse Zone Setup       </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 3);" target="_parent">Bin Location/Order Point   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 4);" target="_parent">Bin Location/Section Setup </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 5);" target="_parent">Section Setup by Item      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 6);" target="_parent">Ship Via                   </a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#">Menus                         </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#"> Inventory Inquiry          </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 1);" target="_parent">Inventory Receipts History </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 2);" target="_parent">Inventory Inquiry w/Windows</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 3);" target="_parent">Purchase Order by Item     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 4);" target="_parent">Purchase Order by Vendor   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 5);" target="_parent">Inventory History Tracking </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 6);" target="_parent">Inventory Value            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 7);" target="_parent">Inventory Detail           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 8);" target="_parent">Back Order Inquiry & Attach</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 9);" target="_parent">Customer Inventory Holds   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 10);" target="_parent"> Inventory Status           </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 11);" target="_parent"> Laminate Inventory Cut     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 12);" target="_parent"> Inventory Holds            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 13);" target="_parent"> Unit of Measure                  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 14);" target="_parent"> Quantity Breaks                  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 15);" target="_parent"> Product Line Analysis            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 16);" target="_parent"> Product Line Summary             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 17);" target="_parent"> Item Summary                     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 18);" target="_parent"> View Inventory by Product Code   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 19);" target="_parent"> Vendor Inventory Summary         </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 20);" target="_parent"> Item Ranking                     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 21);" target="_parent"> Purchase History                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 7, 22);" target="_parent"> View Complementary Item Where Usd</a></span></li>											
								<li><span class="testtxt3"><a title = "" href="#"> More Inventory Inquiries         </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 7, 23, 1);" target="_parent">Create Inv Analysis Work File               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 7, 23, 2);" target="_parent">View Work File From 1 Above                 </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 7, 23, 3);" target="_parent">Customer Inventory Guarantee Inquiry by Item</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 7, 23, 4);" target="_parent">Direct Purchase Order Inquiry               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 7, 23, 5);" target="_parent"> Who is buying an item?                      </a></span></li>
									</ul></li>
									</ul></li>										
							<li><span class="testtxt3"><a title = "" href="#"> Physical Inventory         </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">Counts                           </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 1);" target="_parent">Print Count Sheet             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 2);" target="_parent">Enter Counts                  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 3);" target="_parent">Inventory Detail Inquiry      </a></span></li>
									</ul></li>
																			
								<li><span class="testtxt3"><a title = "" href="#">Re-Counts                        </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 5);" target="_parent">Enter Items to be Re-counted  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 6);" target="_parent">Print Re-count Work Sheet     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 7);" target="_parent">Enter Re-counts               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 8);" target="_parent">Force Re-count (items w/var)  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 9);" target="_parent">Count Sheet Selection Maint.  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 10);" target="_parent"> Print Inventory Variance      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 11);" target="_parent"> Finalize Physical Inventory   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 12);" target="_parent"> Print Count Sheet by Item Rank</a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Other reports                     </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 13);" target="_parent"> Print Complete Inventory      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 14);" target="_parent"> Complete Inventory Summary    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 15);" target="_parent"> Print Items with No Count     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 16);" target="_parent"> Print Items with Zero Balance </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 17);" target="_parent"> Re-Print Count Sheet          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 18);" target="_parent"> Print Duplicated Items        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 19);" target="_parent"> Maintain Default Selection    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 20);" target="_parent"> Maintain Items to Summarize   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 21);" target="_parent"> Set Flag Code                 </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 22);" target="_parent"> Flag Transferred Inventory    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 8, 23);" target="_parent"> Custom Physical Inventory Menu</a></span></li>
									</ul></li>	
									</ul></li>									
							<li><span class="testtxt3"><a title = "" href="#"> Purchase Order Entry/Update</a></span>	
							<ul>											
								<li><span class="testtxt3"><a title = "" href="#">Purchase Order                  </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 1);" target="_parent">Enter Or Update A P/O       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 2);" target="_parent">Print Purchase Order        </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">On-Line P/O                     </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 4);" target="_parent">Create P/O Work File        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 5);" target="_parent">Update P/O Files            </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Receipts                        </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 6);" target="_parent">Enter P/O Receipts By Item  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 7);" target="_parent">Enter P/O Receipts          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 8);" target="_parent">Make Changes To Receipts    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 9);" target="_parent">Update Receipts To Inventory</a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Manifest                        </a></span>	
								<ul>										
									<li><span class="testtxt3"><a title = "" href="#"> Manifest Receiving Menu     </a></span>
									<ul>										
										<li><span class="testtxt3"><a title = "" href="#">Manifest Receipts               </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 1);" target="_parent">Make changes to manifest dtl.</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 2);" target="_parent">Manifest receipts edit/update</a></span></li>
											</ul></li>							
										<li><span class="testtxt3"><a title = "" href="#">P/O Receipts                    </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 3);" target="_parent">Make changes to receipts     </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 4);" target="_parent">Update receipts to inventory </a></span></li>
											</ul></li>								
										<li><span class="testtxt3"><a title = "" href="#">Match Back Orders               </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 5);" target="_parent">Match back orders to receipts</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 6);" target="_parent">Update matched back orders   </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 7);" target="_parent">Enter pre-receive by item    </a></span></li>								
										    <li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 8);" target="_parent"> Manifest maintenance</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 9);" target="_parent">Enter pre-receive shipments  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 10);" target="_parent"> Make changes to Pre Receive  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 11);" target="_parent"> Receipts History Maint       </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 10, 12);" target="_parent"> Manifest Consolidation       </a></span></li>
											</ul></li>
											</ul></li>								
									<li><span class="testtxt3"><a title = "" href="#"> Laminate Receiving Menu     </a></span>
									<ul>										
										<li><span class="testtxt3"><a title = "" href="#">Manifest receipts               </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 1);" target="_parent">Make changes to manifest     </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 2);" target="_parent">Manifest receipts edit/update</a></span></li>
											</ul></li>								
										<li><span class="testtxt3"><a title = "" href="#">P/O receipts                    </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 3);" target="_parent">Make changes to receipts     </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 4);" target="_parent">Update receipts to inventory </a></span></li>
											</ul></li>								
										<li><span class="testtxt3"><a title = "" href="#">Match back orders               </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 5);" target="_parent">Match back orders to receipts</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 6);" target="_parent">Update matched back orders   </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 8);" target="_parent">Manifest maintenance         </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 9);" target="_parent">Pre Receive Shipments        </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 10);" target="_parent"> Receipts History Maint       </a></span></li>
											</ul></li>								
										<li><span class="testtxt3"><a title = "" href="#">Reports                           </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 13);" target="_parent"> Manifest listing              </a></span></li>
											</ul></li>								
										<li><span class="testtxt3"><a title = "" href="#">Labels                            </a></span>									
										<ul>
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 17);" target="_parent"> Pre Receiving Tags by Manifest</a></span></li>
											</ul></li>								
										<li><span class="testtxt3"><a title = "" href="#">EDI Process                       </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 18);" target="_parent"> Process Uploaded PC file      </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 11, 19);" target="_parent"> Process EZLINK PO creation    </a></span></li>
											</ul></li>	
											</ul></li>							
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 12);" target="_parent"> Directs Receiving           </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Match Back Orders                </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 13);" target="_parent"> Match Back Orders To Receipts</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 14);" target="_parent"> Update Matched Back Orders   </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Maintenance                      </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 15);" target="_parent"> Vendor Master Maintenance    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 16);" target="_parent"> Item Master Maintenance      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 17);" target="_parent"> View B/O Not On P/O          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 18);" target="_parent"> Create P/O From B/O          </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Inquiries                        </a></span>	
								<ul>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 19);" target="_parent"> P/O Inquiry By Vendor        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 20);" target="_parent"> P/O Inquiry By Vendor-Items  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 21);" target="_parent"> P/O Inquiry By Item          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 9, 22);" target="_parent"> Inventory Inquiry            </a></span></li>										
									<li><span class="testtxt3"><a title = "" href="#"> More P/O Update Menu         </a></span>
									<ul>										
										<li><span class="testtxt3"><a title = "" href="#">Maintenance                     </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 1);" target="_parent">P/O Terms Files              </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 2);" target="_parent">P/O Ship Via                 </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 3);" target="_parent">P/O Freight                  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 4);" target="_parent">Re-order Location Consolidatn</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 5);" target="_parent">Global Purchase Order Close  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 6);" target="_parent">PO TO AP Detail-EOD          </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 7);" target="_parent">Manifest Master file         </a></span></li>
											</ul></li>								
										<li><span class="testtxt3"><a title = "" href="#">Reversal                        </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 8);" target="_parent">Enter P/O Returns            </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 9);" target="_parent">Make Changes/Update Returns  </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 10);" target="_parent"> Mill Inquiry                 </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 11);" target="_parent"> PO/Manifest Delivery Summary </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 12);" target="_parent"> Assign a P/O Number          </a></span></li>
											</ul></li>								
										<li><span class="testtxt3"><a title = "" href="#">Other                           </a></span>
										<ul>									
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 13);" target="_parent"> Convert a Sales Order       </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 14);" target="_parent"> Back Order Inquiry & Attach </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 15);" target="_parent"> View B/Os Attached to a P/O </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 16);" target="_parent"> P/O for Review              </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 17);" target="_parent"> P/O Loads                   </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 18);" target="_parent"> P/O Vendor Loads            </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 19);" target="_parent"> P/O Release History         </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 20);" target="_parent"> Seasonal Factor File Maint. </a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 21);" target="_parent"> Purchase Authorization Level</a></span></li>								
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 9, 23, 22);" target="_parent"> P/O Manager Maintenance     </a></span></li>								
											<li><span class="testtxt3"><a title = "" href="#"> More P/O Update Menu        </a></span>
											<ul>								
										 		<li><span class="testtxt3"><a title = "" href="#">Transfers                          </a></span>
										 		<ul>							
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(17, 9, 23, 23, 1);" target="_parent">Setup for Transfer Replenishment</a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(17, 9, 23, 23, 2);" target="_parent">Post Transfer Replenishment     </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(17, 9, 23, 23, 5);" target="_parent">Create Transfers & Print        </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(17, 9, 23, 23, 6);" target="_parent">View & Maintain Transfer Work Fl</a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(17, 9, 23, 23, 7);" target="_parent">Post Transfers                  </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(17, 9, 23, 23, 11);" target="_parent"> View All Items with Sales Flag  </a></span></li>						
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(17, 9, 23, 23, 12);" target="_parent"> View an Item with Sales Flag    </a></span></li>
													</ul></li>
													
												<li><span class="testtxt3"><a title = "" href="#"> Maintenance                          </a></span>
												<ul>
													<li><span><a title = "" href="javascript:setValuesAndGoTo5(17, 9, 23, 23, 13);" target="_parent"> FOB Descriptions      			</a></span></li>
												</ul>
												</li>	
												
													</ul></li>
													 </ul></li>
													  </ul></li>	
													  </ul></li>
												
													  </ul></li>					
							<li><span class="testtxt3"><a title = "" href="#"> Purchase Order Reports     </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">Reports                       </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 1);" target="_parent">Print Purchase Order       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 2);" target="_parent">Purchase Order             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 3);" target="_parent">Open Order List By Customer</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 4);" target="_parent">Open Order List By Item    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 5);" target="_parent">Commitment By Vendor       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 6);" target="_parent">Commitment By Item         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 7);" target="_parent">Open P/O                   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 8);" target="_parent">P/O List By Vnd/PO/or Br   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 9);" target="_parent">P/O List By Due Date       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 10);" target="_parent"> Back Order Report (BO200)  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 11);" target="_parent"> Back Order Report By User  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 12);" target="_parent"> Purchasing Terms Listing   </a></span></li>										
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Inquiries                </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 19);" target="_parent"> P/O Inquiry By Vendor</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 20);" target="_parent"> P/O Inquiry By Item  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 10, 21);" target="_parent"> Inventory Inquiry    </a></span></li>										
									<li><span class="testtxt3"><a title = "" href="#"> PO To A/P Menu       </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 10, 23, 1);" target="_parent">Open P/O to A/P Report             </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 10, 23, 2);" target="_parent">P/O to A/P Detail History Report   </a></span></li>									
																			
																			
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 10, 23, 5);" target="_parent">Open P/O to A/P Inquiry            </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 10, 23, 6);" target="_parent">P/O to A/P Detail History Inquiry  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 10, 23, 7);" target="_parent">Purchase History Inquiry           </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 10, 23, 8);" target="_parent">Open & Vouchered P/O to A/P Inquiry</a></span></li>									
										</ul></li>
										</ul></li>	
										</ul></li>								
							<li><span class="testtxt3"><a title = "" href="#"> Routing                    </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#"> File Maintenance            </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 1);" target="_parent">Route Master             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 2);" target="_parent">Route by way of          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 3);" target="_parent">Location Routing         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 4);" target="_parent">Customer Delivery Info   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 5);" target="_parent">Customer Route           </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 6);" target="_parent"> Route Consolidation      </a></span></li>
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 10);" target="_parent"> See who is on a route    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 11);" target="_parent"> Update Open Order Routing</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 12);" target="_parent"> Test Routing             </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Transaction Processing/Reports    </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 13);" target="_parent"> Process Truck Route           </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 14);" target="_parent"> Print Truck Manifest          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 15);" target="_parent"> Print Customer Delivery charge</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 16);" target="_parent"> Floor Covering Cut Processing </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 17);" target="_parent"> Enter Truck Trip Information  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 18);" target="_parent"> Receive Inventory Transfers   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 19);" target="_parent"> Update Inventory Transfers    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 20);" target="_parent"> Enter Inventory Adjustments   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 11, 21);" target="_parent"> Update Inventory Adjustments  </a></span></li>
									</ul></li>
									</ul></li>										
							<li><span class="testtxt3"><a title = "" href="#"> Inventory Transfers        </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">Create Warehouse Transfers      </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 12, 1);" target="_parent">Create Inventory Transfers  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 12, 2);" target="_parent">Edit and Update Transfers   </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Receive Inventory Transfers     </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 12, 3);" target="_parent">Receive Inventory Transfers </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 12, 4);" target="_parent">Update Transfer Receipts    </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Pooled Transfers                </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 12, 5);" target="_parent">Work with Pooled Transfers  </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Transfer Reports                </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 12, 10);" target="_parent"> Report from Transfer History</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 12, 11);" target="_parent"> Report from Open Orders     </a></span></li>
									</ul></li>	
									</ul></li>	
									</ul></li>								
						<li><span class="testtxt3"><a title = "" href="#">Transaction Processing/Reports   </a></span>
						<ul>
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 13);" target="_parent"> Order Management             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 14);" target="_parent"> Print Load Sheet             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 15);" target="_parent"> Order Status Update          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 16);" target="_parent"> Floor Covering Cut Processing</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 17);" target="_parent"> Print Truck Manifest - OO110 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 18);" target="_parent"> Pull Roadnet orders          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 19);" target="_parent"> Laminate Cut Processing      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 20);" target="_parent"> Enter Inventory Adjustments  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 21);" target="_parent"> Update Inventory Adjustments </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 22);" target="_parent"> Process Truck Route          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 23);" target="_parent"> Reprint Shipping Labels      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 25);" target="_parent"> Print Truck Manifest - OO110P</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(17, 26);" target="_parent"> Truck Manifest - OO165       </a></span></li>
							
							
							<li><span class="testtxt3"><a title = "" href="#"> More Warehouse Management                        </a></span>
								<ul>
									<li><span class="testtxt3"><a title = "" href="#"> Menus                         </a></span>
										<ul>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 1);" target="_parent">Bar Code Label Menu 	</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo4(17, 27 , 2);" target="_parent">R/F Warehouse Menu 		</a></span></li>
										</ul>
									</li>
									<li><span class="testtxt3"><a title = "" href="#"> Reports                         </a></span>
										<ul>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 7);" target="_parent">Print pick tickets by Route 		</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 8);" target="_parent">Print Will Call Console activity 	</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 9);" target="_parent">Inventory Consolidation report 		</a></span></li>
										</ul>
									</li>
									<li><span class="testtxt3"><a title = "" href="#"> File Maintenance                        </a></span>
										<ul>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 13);" target="_parent">Daily Route Maintenance 		</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 14);" target="_parent">SCAC Maintenance 				</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 15);" target="_parent">Open routes for allocations 	</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 16);" target="_parent">Will call operators console 	</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 17);" target="_parent">Flag item to be counted 		</a></span></li>
										</ul>
									</li>
									<li><span class="testtxt3"><a title = "" href="#"> Inquires                        </a></span>
										<ul>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 19);" target="_parent">View all active RF users 		</a></span></li>
											<li><span><a title = "" href="javascript:setValuesAndGoTo3(17, 27 , 20);" target="_parent">Dashboard 						</a></span></li>
										</ul>
									</li>
								</ul>
							</li>
							
																			
							</ul></li>	
							</ul></li>											
					<li><span class="testtxt3"><a title = "" href="#"> Pricing                       </a></span>
					<ul>															
						<li><span class="testtxt3"><a title = "" href="#">Menus                      </a></span>
						<ul>														
							<li><span class="testtxt3"><a title = "" href="#">Pricing File Maintenance</a></span>
							<ul>													
								<li><span class="testtxt3"><a title = "" href="#">File Maintenance             </a></span>
								<ul>												
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 1);" target="_parent">Miscellaneous Charge Desc </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 2);" target="_parent">Volume/Group File         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 3);" target="_parent">Tax Master File           </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 4);" target="_parent">Terms Master File         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 5);" target="_parent">Customer Terms File       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 6);" target="_parent">Customer Misc Charges     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 7);" target="_parent">Customer Line Item Charges</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 8);" target="_parent">Dealer/Exception Pricing  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 9);" target="_parent">Ship Via File             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 10);" target="_parent"> Item Master File          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 11);" target="_parent"> Item Price File           </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 12);" target="_parent"> Product Price File        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 13);" target="_parent"> Family Code File             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 14);" target="_parent"> Item/Product Special Price   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 15);" target="_parent"> Item/Product Special Charges </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 16);" target="_parent"> Cuts @ Roll                  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 17);" target="_parent"> Vendor/Price Code Setup      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 18);" target="_parent"> Roll Sale Setup              </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 19);" target="_parent"> Delete Expired Price Exc Recs</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 20);" target="_parent"> Price List Template Maint    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 21);" target="_parent"> Which Location To Use/Pricing</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 1, 22);" target="_parent"> Which Loc To Use/Pricng/Cust </a></span></li>										
									<li><span class="testtxt3"><a title = "" href="#"> More Pricing Options         </a></span>	
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 1);" target="_parent">Mfg. Misc. Cost Description    </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 2);" target="_parent">Determine Customer Pricing     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 3);" target="_parent">Price Exception Global Changes </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 4);" target="_parent">Price Exception History Inquiry</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 5);" target="_parent">Price Code File Maintenance    </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 6);" target="_parent">Price Exception Global Copy    </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 7);" target="_parent">Item Price Global Changes      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 8);" target="_parent">Price Matrix Code Maintenance  </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 9);" target="_parent">Item Master Price Changes      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 10);" target="_parent"> Trip Menu                      </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 11);" target="_parent"> Delivery Charge Increments     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 12);" target="_parent"> Price Check                    </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 13);" target="_parent"> Sell Group Master              </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 14);" target="_parent"> Sell Group Detail              </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 15);" target="_parent"> View Sell Group Prices         </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 16);" target="_parent"> Price Exception w/Profit(PE395)</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(18, 1, 23, 17);" target="_parent"> Order Add On Maintenance       </a></span></li>
										</ul></li>
										</ul></li>	
										</ul></li>								
							<li><span class="testtxt3"><a title = "" href="#">Pricing Reports         </a></span>	
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">Reports                          </a></span>
								<ul>												
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 1);" target="_parent">Volume/Group Discount File    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 2);" target="_parent">Price Exception File (PE110)  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 3);" target="_parent">Price Exception (PE111)       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 4);" target="_parent">Misc Charges Descriptions     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 5);" target="_parent">Price Exception Usage (PE330) </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 6);" target="_parent">Price Exception (PE350)       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 7);" target="_parent">Customer Price Sheets (PE380) </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 8);" target="_parent">Customer Price Report (PE370) </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 9);" target="_parent">Salesman Price Sheets (PE380S)</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 10);" target="_parent"> Price List From Template      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 11);" target="_parent"> Item Price List               </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 12);" target="_parent"> Item Price File List          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 13);" target="_parent"> Item Price List w/Prf %        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 14);" target="_parent"> Product Price List             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 15);" target="_parent"> Item Special Price List        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 16);" target="_parent"> Rebates Recalculated Report    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 17);" target="_parent"> Price Exception Expire (PE146) </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 18);" target="_parent"> Price Exception w/Sales (PE250)</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 19);" target="_parent"> Specials List                  </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Inquiries                          </a></span>
								<ul>												
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 20);" target="_parent"> Price Exception File           </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 21);" target="_parent"> Create Price Catalog Excel File (file name=PL200PRICE)</a></span></li>										
									             										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 22);" target="_parent"> Rebate Report                  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 2, 23);" target="_parent"> Customer Price Sheet (PE390)   </a></span></li>
									</ul></li>
									</ul></li>										
							<li><span class="testtxt3"><a title = "" href="#">Trip Menu               </a></span>	
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 1);" target="_parent">Master file maintenance                          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 2);" target="_parent">Trip customer file maintenance                   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 3);" target="_parent">Trip item file maintenance                       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 4);" target="_parent">Master file list                                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 5);" target="_parent">Trip inquiry                                     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 6);" target="_parent">Enter trip transactions                          </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 7);" target="_parent">Trip/customer sales report-TP100-sales only      </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 8);" target="_parent">Trip/customer sales report-TP110-all transactions</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 9);" target="_parent">Trip detail report                               </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 10);" target="_parent"> Trip statement                                   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 11);" target="_parent"> Trip End of Year                                 </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 12);" target="_parent"> Trip file purge                                  </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(18, 3, 13);" target="_parent"> Trip Setup                                       </a></span></li>
								</ul></li>	
								</ul></li>
								 </ul></li>										
				</ul>	
				</li>														
				<li ><span class="testtxt2"><a title = "" href="#">Cross Applications</a></span>															
				<ul>															
					<li><span><a title = "" href="javascript:setValuesAndGoTo1(20);" target="_parent"> Address Book             </a></span></li>														
					<li><span class="testtxt3"><a title = "" href="#"> Cross Application Support</a></span>
					<ul>														
						<li><span class="testtxt3"><a title = "" href="#">File Maintenance            </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Location Menu            </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">File Maintenance                     </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 1);" target="_parent">Company Master                   </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 2);" target="_parent">Location Master                  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 3);" target="_parent">Inter-Company                    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 4);" target="_parent">Show Location on Inquiry         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 5);" target="_parent">Seq to Search Inventory (pecking)</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 6);" target="_parent">Inventory Hub/Spoke Table Maint  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 7);" target="_parent">Which Location to Use/Pricing    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 8);" target="_parent">Which Location to Use/G/L        </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 9);" target="_parent">Which Locations Transfer         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 10);" target="_parent"> Raw Material Location Table      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 11);" target="_parent"> Seq to Search Inv for Transfers  </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 12);" target="_parent"> Stocking Location                </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 13);" target="_parent"> Set Location by User          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 14);" target="_parent"> Customer - Inventory to Sales </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 1, 15);" target="_parent"> Customer/Location(s) to Search</a></span></li>										
									</ul></li>
									</ul></li>										
																			
																			
																			
																			
																			
																			
																			
							<li><span class="testtxt3"><a title = "" href="#">Buying Authority Menu    </a></span>	
							<ul>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 2, 1);" target="_parent">Products Excluded from General Sales                                   </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 2, 2);" target="_parent">Customer/Item Authority (is the customer limited to certain items)     </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 2, 3);" target="_parent">Billing Code Master (is buying authority limited to installed displays)</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 2, 4);" target="_parent">Customer Billing Codes (does the customer have the display)            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 2, 5);" target="_parent"> User File (is the user limited to certain suppliers)                   </a></span></li>
								</ul></li>											
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 3);" target="_parent">A/P Voucher Control      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 4);" target="_parent">Division                 </a></span></li>												
							<li><span class="testtxt3"><a title = "" href="#"> G/L Interface Menu       </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">Distribution                    </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 1);" target="_parent">Price Exception File         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 2);" target="_parent">A/R to G/L Table             </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 3);" target="_parent">Invoicing-G/L Table          </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 4);" target="_parent">Sales to Inventory/COGS Table</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 5);" target="_parent">Inter-Company                </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 6);" target="_parent">Inventory Adj Reason         </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 7);" target="_parent">Method of Payment            </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 8);" target="_parent">Tax Master File              </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 9);" target="_parent">Route Master                 </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Accounts Payable                  </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 13);" target="_parent"> A/P Interface file Maintenance</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 14);" target="_parent"> Vendor Maintenance            </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">General Ledger                    </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 16);" target="_parent"> Location Maintenance          </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#">Payroll                           </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 18);" target="_parent"> Payroll - G/L Interface       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 19);" target="_parent"> Payroll - A/P Interface       </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 20);" target="_parent"> Deduction Default Master      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 5, 21);" target="_parent"> Employee Deduction Master     </a></span></li>
									</ul></li>	
									</ul></li>									
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 6);" target="_parent">Consolidation Maintenance</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 7);" target="_parent">Batch Master             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 8);" target="_parent">System Control           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 9);" target="_parent">User                     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 10);" target="_parent"> Work With Help Text      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 11);" target="_parent"> Dial-in Customer Setup   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 12);" target="_parent"> Automatic Numbering      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 13);" target="_parent"> Work Calendar Maintenance   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 14);" target="_parent"> State/Province File         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 15);" target="_parent"> G/L Number Global Changes   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 16);" target="_parent"> Terms Code Global Changes   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 17);" target="_parent"> Country File                </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 18);" target="_parent"> Currency Codes              </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 19);" target="_parent"> No Work Copy Setup          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 20);" target="_parent"> Program Security Maintenance</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 21);" target="_parent"> Postal File Maintenance     </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(21, 22);" target="_parent"> Job Tracking Admin Menu         </a></span></li>												
							<li><span class="testtxt3"><a title = "" href="#"> Cross App Menu II           </a></span>
							<ul>												
								<li><span class="testtxt3"><a title = "" href="#">File Maintenance               </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 23, 1);" target="_parent">Customer/User Profile Master		</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 23, 2);" target="_parent">User Control Setup         		</a></span></li>		
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 23, 3);" target="_parent">NiteWatchr Contacts		 		</a></span></li>								
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 23, 13);" target="_parent">Create Excel @ IFS from G/S file	</a></span></li>
									<li><span><a title = "" href="javascript:setValuesAndGoTo3(21, 23, 22);" target="_parent">O/E View Cost Table				</a></span></li>										
									</ul></li>			
									</ul></li>	
									</ul></li>	
									</ul></li>					
					<li><span class="testtxt3"><a title = "" href="#"> Custom Miscellaneous Menu</a></span>	
					<ul>													
						<li><span class="testtxt3"><a title = "" href="#">** Bin Location Changes           </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(22, 1);" target="_parent">Bin Location Maintenanc        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(22, 2);" target="_parent">Update Bin Location Changes    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(22, 9);" target="_parent">CIM910ON - Onthank Usage Report</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo2(22, 10);" target="_parent"> Halebian Pres. Rep Excel File  </a></span></li>
							</ul></li>
							</ul></li>												
					<li><span><a title = "" href="javascript:setValuesAndGoTo1(23);" target="_parent"> Process End Of Day       </a></span></li>														
				</ul>
				</li>															
				<li ><span class="testtxt2"><a title = "" href="#">Other</a></span>															
				<ul>															
					<li><span><a title = "" href="javascript:setValuesAndGoTo1(8);" target="_parent">Clock In/Out</a></span></li>														
					<li><span class="testtxt3"><a title = "" href="#"> Manager     </a></span>
					<ul>														
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 1);" target="_parent">Company Analysis              </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 2);" target="_parent">General Ledger Analysis       </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 3);" target="_parent">Order Management              </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 4);" target="_parent">Credit Management             </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 5);" target="_parent">Open Order Summary Inquiry    </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 6);" target="_parent">Open A/R Balances by Location </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 7);" target="_parent">Inventory Value Inquiry       </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 8);" target="_parent">Segment Master Maintenance    </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 9);" target="_parent">Setup Customer Segments       </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 10);" target="_parent"> Sales Rep System              </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 11);" target="_parent"> Top 10 Customers/Items (daily)</a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 12);" target="_parent"> Sales Rep Maintenance         </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 13);" target="_parent"> Setup Sales Managers           </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 14);" target="_parent"> Setup Customers Viewed By a Rep</a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 15);" target="_parent"> Executive Menu                 </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 16);" target="_parent"> Item Ranking                   </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 17);" target="_parent"> Customer Ranking               </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 18);" target="_parent"> Vendor Ranking                 </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 19);" target="_parent"> Display User Sign-Ons          </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(9, 20);" target="_parent"> View Inventory Analysis Report </a></span></li>													
						<li><span class="testtxt3"><a title = "" href="#"> Sales Manager Menu             </a></span>
						<ul>													
							<li><span class="testtxt3"><a title = "" href="#">Maintenance                     </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 1);" target="_parent">Sales Rep Master             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 2);" target="_parent">Change Sales Rep in Cust Mast</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 3);" target="_parent">Territory/Sales Rep Assignmnt</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 4);" target="_parent">Commission Master            </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 5);" target="_parent">Special Sales Rep Assignment </a></span></li>											
																			
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 7);" target="_parent">Order Type/Sales Rep Assignmt</a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 8);" target="_parent">Change Sales Rep on Invoice  </a></span></li>
								</ul></li>											
							<li><span class="testtxt3"><a title = "" href="#">Inquiries</a></span>
							</li>
							<li><span class="testtxt3"><a title = "" href="#">Reports                              </a></span>
							<ul>												
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 13);" target="_parent"> Salesman Master File             </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 14);" target="_parent"> Salesman Commission Master       </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 16);" target="_parent"> Salesman Commission by Profit    </a></span></li>											
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 21, 21);" target="_parent"> Post Sales Commissions to History</a></span></li>											
								<li><span class="testtxt3"><a title = "" href="#"> Sales Budget Menu                </a></span>
								<ul>											
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 22, 1);" target="_parent">Company Budget Maintenance      </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 22, 2);" target="_parent">Location Budget Maintenance     </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 22, 3);" target="_parent">Sales Rep Budget Maintenance    </a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 22, 4);" target="_parent">Customer/Vendor Budget-Potential</a></span></li>										
									<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 22, 5);" target="_parent"> Default Budget Percentages      </a></span></li>
									</ul></li>										
								<li><span class="testtxt3"><a title = "" href="#"> Sales Rep Menu                   </a></span>
								<ul>											
									<li><span class="testtxt3"><a title = "" href="#">Maintenance                    </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 1);" target="_parent">Sales Rep Activity          </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 2);" target="_parent">Sales Rep Calls             </a></span></li>									
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Inquiries                      </a></span>
									<ul>		
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 3);" target="_parent"> Sales Order			        </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 4);" target="_parent"> Invoice History            	</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 5);" target="_parent"> Customer Ranking				</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 6);" target="_parent"> Purchase Order     			</a></span></li>																		
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 9);" target="_parent"> Sales Detail Inquiry       	</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 10);" target="_parent"> Sales Rep System            	</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 11);" target="_parent"> Sales Rep Comm from/Receipts	</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 12);" target="_parent"> View Back Orders by Rep     	</a></span></li>
										</ul></li>									
									<li><span class="testtxt3"><a title = "" href="#">Reports                          </a></span>
									<ul>										
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 13);" target="_parent"> Salesman Line Item           </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 14);" target="_parent"> Salesman Comparative Sales   </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 15);" target="_parent"> Salesman by Product Code     </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 16);" target="_parent"> Salesman Commission by Profit</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 17);" target="_parent"> Salesman Commission          </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 18);" target="_parent"> Salesman Activity            </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 19);" target="_parent"> Salesman Activity (12 months)</a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 20);" target="_parent"> Salesman by Class Code       </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 21);" target="_parent"> Salesman by Customer Ranking </a></span></li>									
										<li><span><a title = "" href="javascript:setValuesAndGoTo4(9, 21, 23, 22);" target="_parent"> Slsmn by Cust Ranking/Address</a></span></li>									
										<li><span class="testtxt3"><a title = "" href="#"> Sales Rep Menu #2            </a></span>
										<ul>									
											<li><span class="testtxt3"><a title = "" href="#">Reports                            </a></span>
											<ul>								
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(9, 21, 23, 23, 1);" target="_parent">Customer Comparative (SH435)    </a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(9, 21, 23, 23, 2);" target="_parent">Customer Master (Full Info)     </a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(9, 21, 23, 23, 3);" target="_parent">Customer Master (Single Line)   </a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(9, 21, 23, 23, 4);" target="_parent">Territory Sls By Cust/Family    </a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(9, 21, 23, 23, 5);" target="_parent"> Sales by Customer/Div/Prod Code </a></span></li>							
												<li><span><a title = "" href="javascript:setValuesAndGoTo5(9, 21, 23, 23, 6);" target="_parent"> Sales by Sales Rep/Div/Prod Code</a></span></li>
												</ul></li>	
												</ul></li>			
												</ul></li>	
												</ul></li>	
												</ul></li>	
												</ul></li>
						<li><span class="testtxt3"><a title = "" href="#"> Custom Manager Menu            </a></span>	
						<ul>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 22, 1);" target="_parent">Enter Time Cards                  </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 22, 2);" target="_parent">Time Card Detail Report           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 22, 3);" target="_parent">Create President Report Excel File</a></span></li>
							</ul></li>												
						<li><span class="testtxt3"><a title = "" href="#"> Manager Menu II                </a></span>
						<ul>													
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 1);" target="_parent">View Vinyl Productivity          </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 2);" target="_parent">View Hard Surface Productivity   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 3);" target="_parent">View Warehouse Dashboard         </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 4);" target="_parent">View Invoice Batches             </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 5);" target="_parent">View ABC Summary                 </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 6);" target="_parent">Route Analysis by invoice date   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 7);" target="_parent">New Customers                    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 8);" target="_parent">New Vendors                      </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 9);" target="_parent">New Items                        </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 10);" target="_parent"> Executive Information Definitions</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 11);" target="_parent"> Maintain Executive Information   </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 12);" target="_parent"> Vendor Daily Sales               </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 13);" target="_parent"> Location Analysis           </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 14);" target="_parent"> View Potential Problem Areas</a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 15);" target="_parent"> Company Balances            </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 16);" target="_parent"> Sales Rep / Vendor Sales    </a></span></li>												
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 17);" target="_parent"> Displaly Financial Statement</a></span></li>
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 18);" target="_parent"> Specials List					</a></span></li>
							<li><span><a title = "" href="javascript:setValuesAndGoTo3(9, 23, 19);" target="_parent"> Low Margin Inquiry			</a></span></li>
							</ul></li>		
							</ul></li>										
					<li><span class="testtxt3"><a title = "" href="#"> Contacts    </a></span>	
					<ul>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(10, 1);" target="_parent">Account Management			</a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(10, 2);" target="_parent">Account Extract           	</a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(10, 3);" target="_parent">Account Inquiry		   	</a></span></li>												
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(10, 4);" target="_parent">View Recent Comments     	 </a></span></li>													
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(10, 5);" target="_parent">Contacts                 	 </a></span></li>
						<li><span><a title = "" href="javascript:setValuesAndGoTo2(10, 6);" target="_parent">Account Type Maintenance  	 </a></span></li>
						<!--<li><span><a title = "" href="javascript:setValuesAndGoTo2(10, 10);" target="_parent">Job Tracking Menu  	 	 </a></span></li>
						-->
						<li><span class="testtxt3"><a title = "" href="#">Job Tracking Menu</a></span>
							<ul>													
								<li><span><a title = "" href="javascript:setValuesAndGoTo3(10, 10, 1);" target="_parent">Job Master</a></span></li>
								
								<li><span class="testtxt3"><a title = "" href="#"> Reports</a></span>
									<ul>
										<li><span><a title = "" href="javascript:setValuesAndGoTo3(10, 10, 13);" target="_parent">Open quotes </a></span></li>
										<li><span><a title = "" href="javascript:setValuesAndGoTo3(10, 10, 14);" target="_parent">Quotes converted to orders </a></span></li>
										<li><span><a title = "" href="javascript:setValuesAndGoTo3(10, 10, 15);" target="_parent">Invoices by Spec Rep</a></span></li>
									</ul>
								</li>				
							</ul>
						</li>
					</ul></li>													
				</ul></li>
						</ul>
						</div>
						</td>
					</tr>
				</table>
				</div>
				</div>
		    </div>
		
		
		</td>
	</tr>
</table>
</div>
<input id="test" type="hidden"></input>
<%System.out.println("..............Master"); %>


</body>
</html>