<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=1.25,0,04/07/28,16:51:44  -->

<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css"> 
<!-- embedded stylesheet to coordinate button and link colors -->
<STYLE type="text/css">

TABLE.HostKeypad {
	background-color: #a9bafc;
}

.COMPANYTITLE {
    color: #FFFFFF;
    font-style: italic;
    font-size: xx-large;
}

</STYLE>
</HEAD>
<BODY>
<TABLE width="100%" cellpadding="4" cellspacing="0" border="0">
    <TBODY>
        <TR>
            <TD height="69" background="../common/images/Corporate1top.gif"> <span class="COMPANYTITLE">My Company</span></TD>
        </TR>
        <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
        <!-- 
        <TR>         
        	<TD valign="top">
        	<a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
        	</TD>
        </TR> 
        -->   
        <TR bgcolor="#7DA6CB">
        <TD width="100%" height="100%" colspan="2" align="center" valign="top" bgcolor="white">
        <br>
        <a name="navskip"></a>
 <HATS:Transform skipBody="true">
    <P> Host screen transformation will be shown here </P>
 </HATS:Transform> 
         </TD>
         </TR>
         <TR>
         <TD align="center" bgcolor="white">
         	<br><HATS:ApplicationKeypad settings="layout:horizontal"/>
         </TD>
        </TR>        
        </TBODY>
</TABLE>
</BODY>
</HTML>



