<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->
<HEAD>
  <TITLE><HATS:Util type="applicationName" /></TITLE> <HATS:Util type="baseHref" />
  <META name="GENERATOR" content="IBM WebSphere Studio">
  <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- See the User's and Administration Guide for information on using templates and stylesheets -->
  <!-- Global Style Sheet -->
  <LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
  <LINK rel="stylesheet" href="../common/stylesheets/slickWhiteBackground.css" type="text/css">
  <LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css">
  <!-- embedded stylesheet to coordinate button and link colors -->
  <STYLE type="text/css">
      A.TOPBAR:link {color:white; }
      A.TOPBAR:active {color:white; }
      A.TOPBAR:visited {color:white; }
      A.BOTTOMBAR:link {color:blue}
      A.BOTTOMBAR:active {color:blue}
      A.BOTTOMBAR:visited {color:blue}
      A.ApplicationKeyLink {
          color: white;
      }
      A.HATSLINK:link, A.HATSLINK:visited, A.HostKeyLink:link, A.HostKeyLink:visited {
          color: #841412;
      }
      input.ApplicationButton, select.ApplicationDropDown {
          width: 15em;
      }
      .COMPANYTITLE{
          color: #0000FF;
          font-size: xx-large;
          font-style: italic;
      }
      .COMPANYSLOGAN{
          color: #FFFFFF;
          font-size: large;
          font-style: italic;
      }
      .LINKSHEADER{    
          color: #000067;
          font-style: italic;
      }
  </STYLE>
</HEAD>
  <BODY>
  <TABLE width="100%" cellpadding="4" cellspacing="0"  border="0">
    <TBODY>
      <TR>
        <TD colspan="3" align="left"  background="../common/images/EagleInFlight.jpg" height="100">
          <SPAN class="COMPANYTITLE">
          &nbsp;&nbsp;&nbsp;My Company
          </SPAN>
        </TD>
      </TR>
      <TR>
        <TD>
          <TABLE width="100%" cellpadding="0" cellspacing="0" bgcolor="white" border="0">
            <TBODY>
            <TR>
            <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
            <!-- 
               <TD align="left"  width="155" height="27" valign="top" bgcolor="white">
                 <a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
              </TD>
              -->
              <TD >
                <TABLE background="../common/images/WhiteButtonBackground.jpg" align="left" width="100%" border="0" cellpadding="0" cellspacing="0">
                  <TBODY>
                  <TR>
                    <TD background="../common/images/WhiteButtonBackground.jpg"   >
                      <IMG src="../common/images/BlueTabStart.jpg" width="28" height="28" alt="" border="0" align="right" hspace="0" vspace="0">
                    </TD>
                    <TD align="center" background="../common/images/BlueTabBackground.jpg"  nowrap="nowrap">
                      <A CLASS="TOPBAR" href="http://www.ibm.com">First Link</A>
                    </TD>
                    <TD  background="../common/images/WhiteButtonBackground.jpg" >
                      <IMG src="../common/images/BlueTabEnd.jpg" width="28" height="28" alt="" align="left" border="0"  hspace="0" vspace="0">
                    </TD>
                    <TD   background="../common/images/WhiteButtonBackground.jpg">
                      <IMG src="../common/images/BlueTabStart.jpg" width="28" height="28" alt="" border="0" align="right" hspace="0" vspace="0">
                    </TD>
                    <TD align="center" background="../common/images/BlueTabBackground.jpg"  nowrap="nowrap">
                      <A CLASS="TOPBAR" href="http://www.ibm.com">Second Link</A>
                    </TD>
                    <TD background="../common/images/WhiteButtonBackground.jpg">
                      <IMG  src="../common/images/BlueTabEnd.jpg" width="28" height="28" alt="" align="left" border="0"   hspace="0" vspace="0">
                    </TD>
                    <TD  background="../common/images/WhiteButtonBackground.jpg" >
                      <IMG src="../common/images/BlueTabStart.jpg" width="28" height="28" alt="" border="0" align="right"  hspace="0" vspace="0">
                    </TD>
                    <TD align="center" background="../common/images/BlueTabBackground.jpg"  nowrap="nowrap">
                      <A CLASS="TOPBAR" href="http://www.ibm.com">Third Link</A>
                    </TD>
                    <TD background="../common/images/WhiteButtonBackground.jpg"  >
                      <IMG  src="../common/images/BlueTabEnd.jpg" width="28" height="28" alt="" align="left" border="0"  hspace="0" vspace="0">
                    </TD>
                    <TD  background="../common/images/WhiteButtonBackground.jpg"  >
                      <IMG src="../common/images/BlueTabStart.jpg" width="28" height="28" alt="" border="0" align="right"  hspace="0" vspace="0">
                    </TD>
                    <TD align="center" background="../common/images/BlueTabBackground.jpg" nowrap="nowrap">
                      <A CLASS="TOPBAR" href="http://www.ibm.com">Fourth Link</A>
                    </TD>
                    <TD  background="../common/images/WhiteButtonBackground.jpg">
                      <IMG  src="../common/images/BlueTabEnd.jpg" width="28" height="28" alt="" align="left"  border="0"  hspace="0" vspace="0">
                    </TD>
                  </TR>
                  </TBODY>
                </TABLE>
              </TD>
            </TR>
            </TBODY>
          </TABLE>
        </TD>
      </TR>
    <TR>
      <TD>
        <TABLE>
          <TBODY>
          <TR>
            <TD  width="155" height="100%" align="left" valign="top">
              <DIV align="center">
                <TABLE height="100%" cellpadding="0" cellspacing="0" border="0">
                  <TBODY>
                  <TR>
                    <TD nowrap="nowrap"  width="155" valign="top">
                      <STRONG CLASS="LINKSHEADER">My Company Links</STRONG> <BR>
                      <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Home Page</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Map</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Employees</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Jobs at My Company</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Articles</A> <BR>
                      <BR>
                      <STRONG CLASS="LINKSHEADER">My Products</STRONG> <BR>
                      <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A> <BR>
                      &nbsp; <BR>
                      &nbsp; <BR>
                      <HATS:ApplicationKeypad/>
                    </TD>
                  </TR>
                  </TBODY>
                </TABLE>
              </DIV>
            </TD>
            <TD nowrap="nowrap" height="100%" width="5" align="left" background="../common/images/ThinBlueVertical.jpg">
            </TD>
            <TD width="100%" height="100%" valign="top" >
            <a name="navskip"></a>
              <HATS:Transform skipBody="true">
              <DIV align="center">
                <P>
                Host screen transformation will be shown here 
                </P>
              </DIV>
              </HATS:Transform>
            </TD>
          </TR>
          </TBODY>
        </TABLE>
      </TD>
    </TR>
    </TBODY>
  </TABLE>
  </BODY>
</HTML>
