<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@ page import="com.ibm.hats.common.*"%>

<head>
<%
String isPin = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isPin", true).getString(0);
String isMasterMenu = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isMasterMenu", true).getString(0).trim();
String isSignOn = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("checkGV", true).getString(0).trim();
%>
<script src="http://ajax.googleapis.com/ajax/libs/dojo/1.7.2/dojo/dojo.js"
               data-dojo-config="async: true"></script>    
<script type="text/javascript">

var js_isPin = '';    
  
require(["dojo/ready"], function(ready){
     ready(function(){      
     
		   js_isPin = getCookie("isPin");
			var menuFrame = parent.menuFrame;
			        	
			if(js_isPin == "true" && '<%=isSignOn %>' != 'Sign On'){
				
					showRightTreePin();
					        		
			}else if('<%=isMasterMenu %>' == 'Master Menu (MASTER)'){
			     showBothTrees2();
		   	}
		   	parent.js_isPin = js_isPin;
     });
});
        


function getCookie(isPin)
{
var i,x,y,ARRcookies=document.cookie.split(";");
for (i=0;i<ARRcookies.length;i++)
  {
  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  x=x.replace(/^\s+|\s+$/g,"");
  if (x==isPin)
    {
    return unescape(y);
    }
  }
}



function setCookie(isPin,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var pin =escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=isPin + "=" +pin;
}

function setPinCookie()
{
setCookie("isPin",'true',365);
var isPin=getCookie("isPin");
//alert(isPin);
}

function setUnPinCookie()
{
setCookie("isPin",'false',365);
var isPin=getCookie("isPin");
//alert(isPin);
}

function pinFunctionKeys(){
//alert(document.forms["HATSForm"].hatsgv_isPin);
//document.forms["HATSForm"].hatsgv_isPin.value = "true";
js_isPin = "true";
parent.js_isPin = "true";
document.getElementById("pin").style.display = "none";
document.getElementById("unpin").style.display = "block";
setPinCookie();
showRightTreePin();
//ms('[enter]','HATSForm');

}

function unpinFunctionKeys(){
js_isPin = "false";
parent.js_isPin = "false";
//document.forms["HATSForm"].hatsgv_isPin.value = "";
document.getElementById("unpin").style.display = "none";
document.getElementById("pin").style.display = "block";
setUnPinCookie();
hideRightTree();
//ms('[enter]','HATSForm');

}

function initt() {		
	if(parent.document.getElementById("isMouseMoveAllowed").value != "false"){
		document.onmousemove = getDocXY;
	}
}

function getDocXY(e) {	
	var isIE = document.all;	
	if (!e) e = window.event;
		
	if (e)
	{ 	
		document.getElementById('DocX').value = isIE ? (e.clientX + document.body.scrollLeft) : e.pageX;
		document.getElementById('DocY').value = isIE ? (e.clientY + document.body.scrollTop) : e.pageY;
		
		if(document.getElementById('DocX').value > 1 && document.getElementById('DocX').value < 1050)
		{		
			
			parent.hideTree();
			
			hideRightTree();					
		}	
	}	
}

function iniit() {	
	if(parent.document.getElementById("isMouseMoveAllowed").value != "false"){
		document.onmouseover = getPosXY;
	}
}

function getPosXY(e) {	
	var isIE = document.all;	
	if (!e) e = window.event;
	
	if (e)
	{ 
		/*document.getElementById('PosX').value = isIE ? (e.clientX + document.body.scrollLeft) : e.pageX;
		document.getElementById('PosY').value = isIE ? (e.clientY + document.body.scrollTop) : e.pageY;*/
		
		if(document.getElementById('DocX').value <= 1044 )		{
			//alert('got');
			//if(parent.document.getElementById("isMouseMoveAllowed").value!="false"){
				hideRightTree();
			//}
		}
	}
}
</script>

<title>Gartman</title>
<HATS:Util type="baseHref" />

<link href="../common/css/style.css" rel="stylesheet" type="text/css" />
<link href="../common/css/menu.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../common/stylesheets/industry.css"
	type="text/css" />
<!--[if lt IE 7]>
<style type="text/css">
input.RHBLANK,input.RHBLUE,input.RHGREEN,input.RHRED,input.RHCYAN,
input.RHWHITE,input.RHMAGENTA,input.RHBROWN,input.RHYELLOW,input.RHGRAY 
{ background-image: none !important; }
</style>
<![endif]-->

<!-- Started By AK -->
<style type="text/css">

#treeMenu_rtl_10{
	display: block;
	width:159px;
	min-height:495px;
	height: auto ;
	float:left;
	text-align:center;
	/*background: url(../common/images/opn-close2.gif) -322px 0 no-repeat;*/
	cursor: pointer;
	cursor: hand;				
}
		
#treeMenu_rtl_05{
	display: block;
	width:27px;
	min-height:495px;
	height: auto;
	float:left;
	background: url(../common/images/opn-close2.gif) 25% 0 no-repeat;
	cursor: pointer;
	cursor: hand;
}

div.mainbtnbar {
	/*margin:25px 0 0 0px;*/
	margin:5px 0 0 0px;
	padding:0;
	display:block;
	padding: 0 5px;
}

/*div.mainbtn a*/

a.functionbtn1{
	margin:0px;
	width:149px;
	height:21px;
	padding:7px 0px 0px 0px;
	font:bold 10px Arial, Helvetica, sans-serif;
	color:#FFF;
	text-align:center;
	text-decoration:none;
	vertical-align:text-top;
	display:block;
	text-decoration:none;
	background: #000 url(../common/images/menu_btn.gif) no-repeat center bottom;
	white-space:nowrap;
}

/*div.mainbtn a:hover*/

a.functionbtn1:hover{
	color:#fff;
	background: #fdaa34 url(../common/images/menu_btn.gif) no-repeat center top;
}
a.functionbtn2{	
	margin:0px;
	width:149px;
	height:21px;
	padding:-2px 0px 0px 0px;
	font:bold 10px Arial, Helvetica, sans-serif;
	color:#FFF;
	text-align:center;
	/*text-decoration:none;*/
	vertical-align:text-top;
	display:block;
	/*text-decoration:none;*/
	/*background: #000 url(../common/images/menu_btn.gif) no-repeat center bottom;*/
	white-space:nowrap;	
	background-color: red;
}

/*div.mainbtn a:hover*/

a.functionbtn2:hover{
	color:#fff;
	/*background: #fdaa34 url(../common/images/menu_btn.gif) no-repeat center top;*/
}
#mainCon {
	margin-right:2px; 
}
				
</style>
<!-- Ended By AK -->

<script type="text/javascript" src="../common/scripts/industry.js" ></script>
<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>
<script language="javascript">
function showRightTree() {	
	iniit();
	if(js_isPin != "true"){
		if(document.getElementById('treeMenu_rtl_05') != null){
			document.getElementById('treeMenu_rtl_05').setAttribute('id', 'treeMenu_rtl_10');
			
		}
		
			document.getElementById("pin").style.display = "block";
			
		if(parent.document.getElementById("isMouseMoveAllowed").value != "false"){
			document.getElementById("mainbtnbar").style.display="block";
			document.getElementById("colcenterover").className = "contentbgmainright";
			
		}	
	}
}

function showRightTreePin() {	
	iniit();
parent.document.getElementById("indexbody").setAttribute('onmousemove', 'inity();');	
	var menuFrame = parent.menuFrame;
	
		if(document.getElementById('treeMenu_rtl_05') != null){
			document.getElementById('treeMenu_rtl_05').setAttribute('id', 'treeMenu_rtl_10');
			
		}
		
			document.getElementById("unpin").style.display = "block";
			parent.document.getElementById("isMouseMoveAllowed").value = "true"; 
			
		if(typeof menuFrame != 'undefined'){
			if(parent.document.getElementById("isMouseMoveAllowed").value != "false" && menuFrame.document.getElementById('treeMenu_ltr_27') != null){
				document.getElementById("mainbtnbar").style.display="block";			
				document.getElementById("colcenterover").className = "contentbgmainright";
			}else if(parent.document.getElementById("isMouseMoveAllowed").value != "false" && menuFrame.document.getElementById('treeMenu_ltr_27') == null){
				document.getElementById("mainbtnbar").style.display="block";			
				document.getElementById("colcenterover").className = "contentbgmainright3";
			}
		}else{
			document.getElementById("mainbtnbar").style.display="block";			
				document.getElementById("colcenterover").className = "contentbgmainright";
		}
		//alert(document.getElementById("mainbtnbar").style.display);
}

function hideRightTree() {

	var menuFrame = parent.menuFrame;
	if(js_isPin != "true"){
		parent.document.getElementById("isMouseMoveAllowed").value = "true";
		document.getElementById("gdb").setAttribute('onmousemove', 'initt();');	
		parent.document.getElementById("indexbody").setAttribute('onmousemove', 'inity();');
		if(document.getElementById("pin")!=null){
			document.getElementById("pin").style.display = "none";
		}
		
		if(document.getElementById('treeMenu_rtl_10') != null){
			document.getElementById('treeMenu_rtl_10').setAttribute('id', 'treeMenu_rtl_05');
		}	
		if(document.getElementById("mainbtnbar")!=null){
			document.getElementById("mainbtnbar").style.display="none";
		}
		document.getElementById("colcenterover").className = "contentbgmainrightfull";
	}
		
}

function signOff() {
	var hatsFrame = parent.hatsFrame; 
	hatsFrame.ms('macrorun_LogoutMacro', 'hatsportletid');
}

function cursorWait() {
    //window.onbeforeunload=ChangeToHourGlass();
    if(screenName == "ExecutiveMenu" || screenName == "CompanyAnalysis_Combo") {    	
    	return;
    } else {    	
    	document.getElementById("topcontent").style.display ='none';
    	document.getElementById("loadingimgtop").style.display ='block';
    	document.getElementById("loadingimg").style.display ='block';
    } 
}

function checkWidth() {
	if (parent.document.getElementById("hatsFrame").width == 1050)
	{document.getElementById("colcenterover").className = "contentbgmainrightnew2"; }
	else if (parent.document.getElementById("hatsFrame").width == 1208) 
	{document.getElementById("colcenterover").className = "contentbgmainrightnew2";}
}

</script>

<script language="JavaScript">
function setCursorAndGo(val) {
	setCursorPosition(val, 'HATSForm');
	ms('[enter]', 'HATSForm');
}

function setPromptsCursorAndGo(val, sid) {
	//if() {
//	} else if() {
//	}
//	setCursorPosition(val, 'HATSForm');
//	ms('[enter]', 'HATSForm');
	
	var i,fwd="",pos="",temp='[pagedn]',temp1='[down]';
    for(i=0;i<sid;i++) { fwd=fwd+temp; }
    for(i=1;i<val;i++) { pos=pos+temp1; }
   	document.forms["HATSForm"].hatsgv_gvSid.value  = fwd;
    document.forms["HATSForm"].hatsgv_gvPosition.value = pos;
    ms("macrorun_GoToScreenAndDetails","hatsportletid");
}

function setMenu(cursorPos){
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[enter]', 'HATSForm');
}
</script>

<script language="JavaScript">
//top.parent.document.getElementById('hatsFrame').contentWindow.addEventListener("beforeunload", function() {alert("unloading")}, false);
//window.onbeforeunload = function () {
	//alert("hi");
//	if(screenName == "ExecutiveMenu" || screenName == "CompanyAnalysis_Combo") {
 //   	return;
  //  } else {
   // 	document.getElementById("topcontent").style.display ='none';
   // 	document.getElementById("loadingimgtop").style.display ='block';
   // 	document.getElementById("loadingimg").style.display ='block';
   // }
//};

</script>

</head>

<body id="gdb">

<input type="hidden" id="DocX" size="3"/>
<input type="hidden" id="DocY" size="3"/>

<input type="hidden" id="PosX" size="3"/>
<input type="hidden" id="PosY" size="3"/>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="left" valign="top">
			<div id="mainCon">
				<table class="topbar_headingnew" width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="92%" align="center">
							<table width="auto" cellpadding="0" cellspacing="0" border="0">
								<tr>
<%								 								
								String checkMenu = "";
								HostScreen hostScreen = ((TransformInfo) request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getHostScreen();
								//System.out.println("hostScreen"+hostScreen.getFieldAtPos(01,003));
								if(hostScreen.getFieldAtPos(01,003) != null){
									checkMenu = hostScreen.getFieldAtPos(01,003).getFieldString().trim();
								}
								
								//System.out.println("checkMenu"+checkMenu);
								if(checkMenu.equalsIgnoreCase("Item Menu")){
%>
									<td>
										<div class="topbar_tab">
											<a href="#" onclick="setMenu('003');return false">Item Menu</a>
											<a href="#" onclick="setMenu('015');return false">Customer Menu</a>
											<a href="#" onclick="setMenu('031');return false">Sales Menu</a>
											
										</div>
									</td>
<%
								}else{
									String checkDots = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("checkDots", true).getString(0).trim();																													
									if(!(checkDots.equalsIgnoreCase("......"))){					
%>
									<td>
											<div class="topbar_headingtxtleft">
													<HATS:Component col="1" alternate=""
							widget="com.ibm.hats.transform.widgets.FieldWidget"
							alternateRenderingSet="" erow="1" textReplacement=""
							widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:false|fieldStyleClass:topbar_headingtxtleft|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:false|omitExtraneousTables:false|reverseColorForTD:true|layout:TABLE|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:[enter]|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
							type="com.ibm.hats.transform.components.FieldComponent"
							ecol="132" componentSettings="" row="1" />
											</div>
										</td>		
														
<%
									}else{
%>
										<td width="350">			
										</td>													
<%
									}
								}									
%>											
													
								</tr>			
							</table>							
						</td>
						<td width="8%">
							<table width="auto" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td>
										<div class="topbar_headingtxtright"><span><a title= "SignOff" href="javascript:signOff();" target="_parent">
										<%String signOnText = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("signOnText", true).getString(0);%>
										<%if (signOnText.length()>0 && signOnText.equalsIgnoreCase("Sign On")) {
										//System.out.println("Sign On Text:............." + signOnText); 
										out.print("");
										} else { %>

										<% out.print("SignOff");
										}%>
										</a></span>
										</div>
									</td>
								</tr>
							</table>
		
						</td>
					</tr>
				
					<tr>
						<td colspan="2" align="left" valign="top">
							<table width="100%" height="100%" class="contentbgnew5" border="0" cellspacing="0" cellpadding="0">
				                <tr>
				                  <td align="left">
				                  <div id="topcontent" style="display: block;">                                 			                 	
				                	<div class="contentbgmainrightfull" id="colcenterover" align="center">				                  	
				                  		<HATS:Transform skipBody="true">
				    						<p> Host screen transformation will be shown here </p>
				 						</HATS:Transform>
				 					</div>				                   
				 				  </div>
				                   	
				                   	<div id="loadingimgtop" style="display: none;">
										<table width="100%" height="495" cellpadding="0" cellspacing="0">
										<tr>
											<td align="center" valign="middle">
												<div id="loadingimg"><img border="0" src="../common/images/ajax-loader.gif" width="100" height="100"/></div>
											</td>
										</tr>
										</table>
									</div>				                   	
				                  </td>
				            	</tr>
				        	</table>
						</td>
					</tr>
				</table>				  		            
			</div>
		
		
				
		
		</td>
		<td align="left" width="27" valign="top" style="border: 1px solid #ccc;" id="fKeyBar">
		<div id="pin" onclick="pinFunctionKeys()" style="display: none;"><img src="../common/images/pin1.png"></img></div>
				<div id="unpin" onclick="unpinFunctionKeys()" style="display: none;"><img src="../common/images/pin2.png"></img></div>
			<div id="treeMenu_rtl_05" onmouseover="showRightTree()">
	
				
	
				 <div class="mainbtnbar" id="mainbtnbar" style="display: none">
				 <%
				 	String isPopup = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isPopup", true).getString(0).trim();
				 	//System.out.println("isPopup......."+isPopup);
				 	if(isPopup.equalsIgnoreCase("Yes")){
				 %>
				 		<script type="text/javascript">			 		
							var functionTable = "<table>";
							var funclinks=document.getElementsByTagName("a"); 
								for(var i=0;i<funclinks.length;i++){			
									if(funclinks[i].className=="functionbtn"){
											var functionKeyDesc = funclinks[i].innerHTML;
											
											
											if(functionKeyDesc.length>35){
												var str = functionKeyDesc.split("&");
												var str1 = "";
												for(var k=0; k<str.length; k++){
													str1 = str1 + str[k]+" ";
												}
												var str2 = str1.replace(/(nbsp;)*/g,"");
																								
												functionTable = functionTable + "<tr><td><a href=\""+ funclinks[i] +"\" class=functionbtn3>"+str2+"</a></td></tr>";		
											}else{
												functionTable = functionTable + "<tr><td><a href=\""+ funclinks[i] +"\" class=functionbtn1>"+functionKeyDesc+"</a></td></tr>";
											}
											
											//functionTable = functionTable + "<tr><td><a href=\""+ funclinks[i] +"\" class=functionbtn1>"+funclinks[i].innerHTML+"</a></td></tr>";
											//functionTable = functionTable + "<tr><td><a href=\""+ funclinks[i] +"\" class=functionbtn1>"+functionKeyDesc+"</a></td></tr>";											
									}
								}														
								 
						   functionTable = functionTable + "<tr><td><a href=\"javascript:ms('[enter]', 'HATSForm')\" class=functionbtn1>Enter</a></td></tr>";
						   functionTable = functionTable + "<tr><td><a href=\"javascript:ms('[attn]', 'HATSForm')\" class=functionbtn1>ESC</a></td></tr></table>";
						   						 
						   document.getElementById("mainbtnbar").innerHTML = functionTable;
						</script>	
				 <%	
				 	}else{
				 		if(isPopup.equalsIgnoreCase("No")){
				 %>
				 			<HATS:Component col="1" alternate=""
			widget="com.ibm.hats.transform.widgets.LinkWidget"
			alternateRenderingSet="" erow="27" textReplacement=""
			widgetSettings="style:|captionType:DESCRIPTION|separator: | |layout:TABLE|trimCaptions:true|target:|columnsPerRow:1|emenubarStyleClass:HATSEMENUBAR|linkStyleClass:functionbtn1|"
			type="com.ibm.hats.transform.components.FunctionKeyComponent"
			ecol="132"
			componentSettings="delimiter:=| = |searchType:allDelimitersFirst|reqValueAfterDescription:  |startDelimiter:PF#|F#|Alt+F1|reqValueBeforeLeadingToken: |"
			row="9" />
							<a href="javascript:ms('[enter]', 'HATSForm')" title="Enter" class="functionbtn1">Enter</a>
							<a href="javascript:ms('[attn]', 'HATSForm')" title="ESC" class="functionbtn1">ESC</a>
							
				 <%		
				 		}				 	
				 	}				 	
				 %>		
				</div>
			</div>
		</td>
	</tr>
</table>

<%
	String checkGV = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("checkGV", true).getString(0).trim();	
	if(checkGV.equalsIgnoreCase("Sign On")) {
%>
		<script>
		document.getElementById('fKeyBar').style.display = 'none';
		document.getElementById("colcenterover").className = "contentbgmainrightfull";
		</script>
<%
	}
%>
<script>
function hideBothTrees_2() {
				var menuFrame = parent.menuFrame; 
				document.getElementById("pin").style.display = "none";
				
				menuFrame.document.getElementById("treeMenu_ltr_185").setAttribute('id', 'treeMenu_ltr_27');
				menuFrame.document.getElementById('MenuContent').style.display = "none";
				parent.document.getElementById("hatsFrame").width="1208";
				menuFrame.document.getElementById("MenuContent2").style.display = "none";		
				document.getElementById("colcenterover").className = "contentbgmainrightfull";
				parent.document.getElementById("menuFrame").style.width = "27px";
				
				document.getElementById('treeMenu_rtl_10').setAttribute('id', 'treeMenu_rtl_05');	
				document.getElementById("mainbtnbar").style.display="none";			
				document.getElementById("colcenterover").className = "contentbgmainrightfull";
				parent.document.getElementById("isMouseMoveAllowed").value = "true";
				document.getElementById("gdb").setAttribute('onmousemove', 'initt();');
				parent.document.getElementById("indexbody").setAttribute('onmousemove', 'inity();');
					
}
			
function treeTimeOut_2(){	
	setTimeout("hideBothTrees_2()",3000);
}

function showBothTrees2(){
		
	var menuFrame = parent.menuFrame; 
	
	var checkMaster = parent.document.getElementById("checkMaster").value;
			if(	checkMaster == "Default" && js_isPin != "true"){
				
					parent.document.getElementById("check").value="Default";
					parent.document.getElementById("checkMaster").value = "Default";
					document.getElementById("pin").style.display = "block";
					
					
					parent.document.getElementById("hatsFrame").width="1075";
					parent.document.getElementById("hatsFrame").align="left";
					
					if(menuFrame.document.getElementById('treeMenu_ltr_27') != null){
						menuFrame.document.getElementById('treeMenu_ltr_27').setAttribute('id', 'treeMenu_ltr_185');
					}
					menuFrame.document.getElementById("MenuContent2").style.display = "block";
					menuFrame.document.getElementById('MenuContent').style.display = "block";		
					parent.document.getElementById("menuFrame").style.width = "160px";
					document.getElementById("colcenterover").className = "contentbgmainright_2";
										
					document.getElementById('treeMenu_rtl_05').setAttribute('id', 'treeMenu_rtl_10');		
					document.getElementById("mainbtnbar").style.display="block";
					document.getElementById("colcenterover").className = "contentbgmainright_2";
					
										
					treeTimeOut_2();
			}else{
				parent.document.getElementById("checkMaster").value="Master";
			}
}
</script>

<%		
	if(isMasterMenu.length()>0 && isMasterMenu.equalsIgnoreCase("Master Menu (MASTER)")) {		
%>
	<script>	 
			parent.document.getElementById("isMouseMoveAllowed").value = "true";			
	</script>
<%
	}else{
		if (isSignOn.length()>0 && isSignOn.equalsIgnoreCase("Sign On")) {			
%>
			<script>
				top.parent.document.getElementById("isMouseMoveAllowed").value ="false";
				top.parent.document.getElementById("indexbody").removeAttribute("onmousemove");
				document.getElementById("gdb").removeAttribute("onmousemove");
				top.parent.document.onmouseover = "";
				document.onmouseover = "";			
			</script>
<%	
		}else{		
%>
			<script>
				parent.document.getElementById("isMouseMoveAllowed").value = "true";
				document.getElementById("gdb").setAttribute('onmousemove', 'initt();');
				parent.document.getElementById("indexbody").setAttribute('onmousemove', 'inity();');
			</script>
<%		
		}
	}
%>
<script>
var funclinks_2 = document.getElementsByTagName("a"); 
		for (var k=0; k<funclinks_2.length; k++){
			if(funclinks_2[k].innerHTML == "Help"){				
				funclinks_2[k].href="javascript:ms('[help]', 'HATSForm')";
			}
	  }
</script>
<%	
	System.out.println("..............default");	 	
%>

<%	
	System.out.println("..............default");	 	
%>
</body> 
</html>