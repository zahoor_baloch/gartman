<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->

<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css">
<!-- embedded stylesheet to coordinate button and link colors -->
<STYLE type="text/css">
A.TOPBAR:link {color:white}
A.TOPBAR:active {color:white}
A.TOPBAR:visited {color:white}

A.BOTTOMBAR:link {color:black}
A.BOTTOMBAR:active {color:black}
A.BOTTOMBAR:visited {color:black}

body {
	background-color: white;
}

table.ApplicationKeypad {
	text-align: center;
	width: 100%;
	padding: 0px;
	margin: 0px;
}

A.ApplicationKeyLink {
	color: black;	
	text-align: left;
}

INPUT.ApplicationButton {
	width: 15em;
}

select.ApplicationDropDown {
	background-color: #7DA6CB;
	width: 15em;
}

.HATSSTATUSHEADER {
	background-color: #184571;
	color: white;
}

.HATSSTATUSFOOTER {
	background-color: #cccccc;
	color: black;
}

.HATSFOOTERSTATUSHEADER, .HATSFOOTERSTATUSINFO {
	color: black;
}

.COMPANYTITLE{
   font-size: xx-large;
   color:#FFFFFF;
   font-style: italic;
}

.LINKSTITLE{
   font-size: 1em;
   color:#000000;
}


</STYLE>
</HEAD>
<BODY>
<TABLE width="100%" cellpadding="4" cellspacing="0" border="0">
    <TBODY>
        <TR>
            <TD colspan="3" height="69" background="../common/images/Corporate1top.gif"> <SPAN CLASS="COMPANYTITLE">My Company</SPAN></TD>
        </TR>
        <TR>
         <TD height="100%" align="center" valign="top">
        	<DIV align="center">
            	<TABLE height="100%" cellpadding="4" cellspacing="0" border="0" >
        			<TBODY>
					<TR>
					    <TD bgcolor="#6D7D3C">					    
					    	<IMG src="../common/images/Corporate1arrow.jpg" alt="Corporate1arrow.jpg" border="0" width="20" height="23" align="right" hspace="0" vspace="0">
					    </TD>
         				<TD bgcolor="#BE7D49" width="100%" nowrap="nowrap">
							<STRONG CLASS="LINKSTITLE">My Company Links</STRONG>
						</TD>
        			</TR>
        			<!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
                    <!-- 
        			<TR> 
        				<TD valign="top" bgcolor="#184571" colspan="2" nowrap="nowrap" >
        			        <a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
        			    </TD>
        			</TR>  
        			--> 
					<TR>
						<TD bgcolor="#184571" colspan="2" nowrap="nowrap">
						    &nbsp;<br><A CLASS="TOPBAR" href="http://www.ibm.com">My Company Home Page</A><br>&nbsp;
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#184571" colspan="2" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">My Company Map</A><br>&nbsp;						
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#184571" colspan="2" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">My Company Employees</A><br>&nbsp;
						</TD>
					</TR>		
					<TR>
						<TD bgcolor="#184571" colspan="2" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">Jobs at My Company</A><br>&nbsp;
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#184571" colspan="2" nowrap="nowrap">
							<A CLASS="TOPBAR" href="http://www.ibm.com">My Company Articles</A><br>&nbsp;
						</TD>
					</TR>													
					<TR>
					    <TD bgcolor="#6D7D3C" >
					    	<IMG src="../common/images/Corporate1arrow.jpg" alt="Corporate1arrow.jpg" border="0" width="20" height="23" align="right" hspace="0" vspace="0">
					    </TD>					
						<TD bgcolor="#BE7D49" width="100%" nowrap="nowrap">
							<STRONG CLASS="LINKSTITLE">My Products</STRONG>
						</TD>
					</TR>			
					<TR>
						<TD bgcolor="#7DA6CB" colspan="2" nowrap="nowrap">
							&nbsp;<br><A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A><br>&nbsp;
						</TD>
					</TR>						
					<TR>
						<TD bgcolor="#7DA6CB" colspan="2" nowrap="nowrap">
							<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A><br>&nbsp;
						</TD>
					</TR>						
					<TR>
						<TD bgcolor="#7DA6CB" colspan="2" nowrap="nowrap">
							<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A><br>&nbsp;
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#7DA6CB" colspan="2">
							<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A><br>&nbsp;
						</TD>
					</TR>					
					<TR>
						<TD bgcolor="#7DA6CB" colspan="2" align="left" width="100%" height="100%" valign="bottom">
							<HATS:ApplicationKeypad/>
						</TD>
					</TR>
        			</TBODY>
        		</TABLE>
        	</DIV>
         </TD>
        <TD width="100%" height="100%" colspan="2" valign="top">
        <a name="navskip"></a>
 <HATS:Transform skipBody="true">
    <DIV align="center"><P> Host screen transformation will be shown here </P></DIV>
 </HATS:Transform>

         </TD>
        </TR>        
        </TBODY>
</TABLE>
</BODY>
</HTML>
