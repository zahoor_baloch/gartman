<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->

<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/whitetheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoWhite.css" type="text/css">
<style type="text/css">
body { background-color:#ccf; }
form { margin: 0px; }
TABLE.HostKeypad { border-style: none; background-color: white; margin: 0px 0px 0px 4px; }
.roundedcornermodule { margin: 0px; }
.extraBackground {background-color: #ccf;margin: 0px;padding: 0px;}
.keySection {margin: 0px;padding: 0px;background-color:white }
.renderSection{margin-top: 4px; background-color:white}
.purple { background-color: white; width: 100%;}
.purple .topright { background:transparent url(../common/images/purple_right_t.gif) no-repeat scroll right top;}
.purple .bottomright { background:transparent url(../common/images/purple_right_b.gif) no-repeat scroll right bottom; }
.purple .topleft { background:transparent url(../common/images/purple_left_t.gif) no-repeat scroll left top; }
.purple .bottomleft { background:transparent url(../common/images/purple_left_b.gif) no-repeat scroll left bottom; }
</style>
</HEAD>
<BODY>
<TABLE width="100%">
<!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
<!--      
<tr>
<td>
 <a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
 </td>
</tr>  
-->
<TR>
<TD>
<img src="../common/images/modern_image_pda.gif" alt="Modern Image PDA" width="37" height="32" >
</TD>
<TD align="right" vAlign="top">
<HATS:ApplicationKeypad settings="layout:horizontal"/>
</TD>
</TR>
</TABLE>
<div class="roundedcornermodule purple" >
<div class="topright">
<div class="topleft">
<div class="bottomright">
<div class="bottomleft">
<a name="navskip"></a>
 <HATS:Transform skipBody="true">
    <P> Host screen transformation will be shown here </P>
 </HATS:Transform>
</div>
</div>
</div>
</div>
</div>
</BODY>
</HTML>