<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>

<head>
<title>Gartman</title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio" />
<META http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link href="../common/css/style.css" rel="stylesheet" type="text/css" />

<script>
function show(object,val) { 
document.getElementById(object).style.visibility = val; 
} 

</script>

</head>

<body>




<br />

<div class="logoheader">

<div class="clear"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="9"><img src="../common/images/gartman_default/header_left.gif" width="9" height="64" /></td>
    <td valign="top" background="../common/images/gartman_default/header_rpt.gif">
    <div class="logo2" style="float:left;"><img src="../common/images/gartman_default/logo.jpg" width="217" height="43" /></div>
    <div class="logo3" style="float:right;"><img src="../common/images/gartman_default/logo2.jpg" width="68" height="54" /></div>
    </td>
    <td width="9"><img src="../common/images/gartman_default/header_right.gif" width="9" height="64" /></td>
  </tr>
</table>

</div>



<div class="main">

<div class="container">

<div class="topbar_heading">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="6"><img src="../common/images/gartman_default/heading_left.gif" width="6" height="25" /></td>
    <td background="../common/images/gartman_default/heading_rpt.gif"><div class="topbar_headingtxt1">Welcome to Gartman.</div></td>
    <td width="6"><img src="../common/images/gartman_default/heading_right.gif" width="6" height="25" /></td>
  </tr>
</table>
</div>

<div class="contentbg">
<div class="clear80px"></div>

<div>
<HATS:Transform skipBody="true">
    <P> Host screen transformation will be shown here </P>
 </HATS:Transform>
</div>

</div>

</div>

<div class="footer">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="../common/images/gartman_default/footer_left.gif" width="10" height="26" /></td>
    <td background="../common/images/gartman_default/footer_rpt.gif"><div class="fottxt">&copy; 2010 Gartman. All rights reserved.</div></td>
    <td width="10"><img src="../common/images/gartman_default/footer_right.gif" width="10" height="26" /></td>
  </tr>
</table>

</div>

</div>

</body> 
</html>