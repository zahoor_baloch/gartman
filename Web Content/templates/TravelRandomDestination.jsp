<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->
<HEAD>
  <title><HATS:Util type="applicationName" /></title> <HATS:Util type="baseHref" />
  <META name="GENERATOR" content="IBM WebSphere Studio">
  <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <script language="JavaScript">
  function GetRandom(start,end)
  {
      var range = end - start + 1;
      var result = start + Math.floor(Math.random()*range);
      return result;
  }
  </script>
  <!-- See the User's and Administration Guide for information on using templates and stylesheets -->
  <!-- Global Style Sheet -->
  <LINK rel="stylesheet" href="../common/stylesheets/monochrometheme.css" type="text/css">
  <LINK rel="stylesheet" href="../common/stylesheets/reverseVideoMono.css" type="text/css">
  <!-- embedded stylesheet to coordinate button and link colors -->
  <STYLE TYPE="text/css">
  A.BOTTOMBAR:link {color:white}
  A.BOTTOMBAR:active {color:white}
  A.BOTTOMBAR:visited {color:white}
  table.ApplicationKeypad
  {
      background: inherit;
      white-space: nowrap;
  }
  td.ApplicationKeypad
  {
      background: inherit;
      white-space: nowrap;
  }

  input.ApplicationButton, select.ApplicationDropDown {
       width: 15em;
  }
  input.HostButton {
    background-color: #C29130;
    color: white;
    white-space: pre;
  }

  .COMPANYTITLE {
    color:  white;
    font-size: xx-large;
    font-style: italic;
  }
  .LINKSHEADER {
    color:  black;
    font-size: large;
  }
  </STYLE>
</HEAD>
  <BODY>
  <TABLE width="100%" cellpadding="4" cellspacing="0" border="0" >
    <TBODY>
    <TR>
      <TD colspan="3" align="left"  background="../common/images/EagleInFlight.jpg" height="100">
        <SPAN class="COMPANYTITLE">
        &nbsp; &nbsp;My Company
        </SPAN>
      </TD>
    </TR>
    <TR>
    <TD background="../common/images/GraduatedBlueVertical.jpg"  width="215" height="100%" align="center" valign="top">
        <DIV align="center">
          <TABLE height="100%"  border="0">
            <TBODY>
            <!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
            <!-- 
            <TR>
			<TD>
				<a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
  			</TD>
  			</TR>
  			-->
            <TR>
              <TD nowrap="nowrap"  height="500" valign="top"  width="260">
              <BR>
              <STRONG>
              <span CLASS="LINKSHEADER">My Company Links </span><BR>
                <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Home Page</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Map</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Employees</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Jobs at My Company</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Articles</A> <BR>
                <BR>
                <span CLASS="LINKSHEADER">My Products </span><BR>
                <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A> <BR>
                <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A> <BR><br><br>
                </STRONG>
               <DIV align="center">
               <script language="JavaScript">
              var choice = GetRandom(1,4);
              if (choice == 1)
              {
                  document.write("<IMG src='../common/images/CityPhoto.jpg' width='113' height='106' alt='Scenic Photo will display here'>");
              }
              else if ( choice == 2)
              {
                   document.write("<IMG src='../common/images/TropicalPhoto.jpg' width='136' height='112' alt='Scenic Photo will display here'>");
              }
              else if (choice == 3)
              {
                  document.write("<IMG src='../common/images/DesertPhoto.jpg' width='139' height='114' alt='Scenic Photo will display here'>");
              }
              else if (choice == 4)
              {
                  document.write("<IMG src='../common/images/MountainPhoto.jpg' width='176' height='134' alt='Scenic Photo will display here'>");
              }

               </script>
               </DIV>
                &nbsp; <BR>
                &nbsp; <BR>
                <HATS:ApplicationKeypad/></TD>

            </TR>
    </TBODY>
          </TABLE>
        </DIV>
      </TD>
      <TD width="100%" height="100%" valign="top" align="center"><HATS:Transform skipBody="true">
      <a name="navskip"></a>
        <DIV align="center">
          <P>
          Host screen transformation will be shown here
          </P>
        </DIV>
        </HATS:Transform>
      </TD>
    </TR>
    </TBODY>
  </TABLE>
  </BODY>
</HTML>