<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->
<HEAD>
  <title><HATS:Util type="applicationName" /></title> <HATS:Util type="baseHref" />
  <META name="GENERATOR" content="IBM WebSphere Studio">
  <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- See the User's and Administration Guide for information on using templates and stylesheets -->
  <!-- Global Style Sheet -->
  <LINK rel="stylesheet" href="../common/stylesheets/tantheme.css" type="text/css">
  <LINK rel="stylesheet" href="../common/stylesheets/reverseVideoTan.css" type="text/css">
  <!-- embedded stylesheet to coordinate button and link colors -->
  <STYLE TYPE="text/css">
      A.BOTTOMBAR:link {color:white}
      A.BOTTOMBAR:active {color:white}
      A.BOTTOMBAR:visited {color:white}
      A.TOPBAR:link {color:black}
      A.TOPBAR:active {color:black}
      A.TOPBAR:visited {color:black}
      table.ApplicationKeypad
      {
          background: inherit;
          white-space: nowrap;
      }
      td.ApplicationKeypad
      {
          background: inherit;
          white-space: nowrap;
      }

      input.ApplicationButton, select.ApplicationDropDown {
        width: 15em;
      }
      input.HostButton {
        background-color: #C29130;
        color: black;
        white-space: pre;
      }
      input.HostPFKey {
        background-color: #7CBC85;
        color: black;
        font-size: .8em;
        white-space: pre;
      }
      input.ApplicationButton {
        background-color: #BFD7AC;
        color: black;
        white-space: pre;
        width: 15em;
      }
      table.HostKeypad {
        background-color: #E3C993;
      }
      .COMPANYTITLE {
        color:  black;
        font-size: xx-large;
        font-style: italic;
      }
       .COMPANYSLOGAN {
        color:  black;
        font-size: medium;
        font-style: italic;
      }
      .LINKSHEADER {
        color:  #FFC262;
        font-size: medium;
      }
  </STYLE>
</HEAD>
  <BODY>
  <TABLE width="100%" cellpadding="0" cellspacing="0"  border="0">
    <TBODY>
    <TR>
      <TD>
        <TABLE width="100%" cellpadding="0" cellspacing="0" border="0" >
          <TBODY>
          <TR>
            <TD nowrap="nowrap" colspan="3" height="100" background="../common/images/InsuranceGreenCenter.jpg" align="left" >
              <span CLASS="COMPANYTITLE">
              &nbsp; My Company
              </span>
              <BR>
              <span CLASS="COMPANYSLOGAN">
              &nbsp; &nbsp; &nbsp;My Company Slogan
              </span>
            </TD>
          </TR>
                </TBODY>
        </TABLE>
      </TD>
    </TR>
    <TR>
      <TD>
        <TABLE width="100%" bgcolor="#036634" cellpadding="0" cellspacing="0"  border="0" >
          <TBODY>
          <TR >           
            <TD align="left"  valign="bottom" >
            <!--Remove the comments from the following line if you wish a link to go directly to the transformation-->
            <!-- <a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>-->
            </TD>   
            <TD  width="100%">
              <TABLE bgcolor="#036634" width="100%" border="0" cellpadding="5" cellspacing="10">
                <TBODY>
                <TR>
                  <TD bgcolor="#FFC262" align="center" nowrap="nowrap"><A CLASS="TOPBAR" href="http://www.ibm.com">First Link</A></TD>
                  <TD bgcolor="#FFC262" align="center" nowrap="nowrap"><A CLASS="TOPBAR" href="http://www.ibm.com">Second Link</A></TD>
                  <TD bgcolor="#FFC262" align="center" nowrap="nowrap"><A CLASS="TOPBAR" href="http://www.ibm.com">Third Link</A></TD>
                  <TD bgcolor="#FFC262" align="center" nowrap="nowrap"><A CLASS="TOPBAR" href="http://www.ibm.com">Fourth Link</A></TD>
                </TR>
                </TBODY>
              </TABLE>
            </TD>
          </TR>
          <TR>
            <TD background="../common/images/GreenToTanVertical.jpg"  width="155" height="100%" align="center" valign="top">
              <DIV align="center">
                <TABLE height="100%" cellpadding="3" cellspacing="3" border="0" >
                  <TBODY>
                  <TR>

                    <TD nowrap="nowrap"  valign="top">
                    <STRONG>
                    <SPAN CLASS="LINKSHEADER">My Company Links</SPAN> <BR>
                      <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Home Page</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Map</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Employees</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Jobs at My Company</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Articles</A> <BR>
                      <BR>
                      <SPAN CLASS="LINKSHEADER">My Products</SPAN> <BR>
                      <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A> <BR>
                      <A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A> <BR>
                     </STRONG>
                      &nbsp; <BR>
                      &nbsp; <BR>
                      <HATS:ApplicationKeypad/>
                      </TD>
                  </TR>
                  </TBODY>
                </TABLE>
              </DIV>
            </TD>
            <TD  bgcolor="#E3C993" width="100%" height="100%" valign="top" >
            <a name="navskip"></a>
            <HATS:Transform skipBody="true">
            <DIV align="center"><P> Host screen transformation will be shown here </P></DIV>
            </HATS:Transform>
            </TD>
          </TR>
          </TBODY>
        </TABLE>
        </TD>
        </TR>
        </TBODY>
        </TABLE>
  </BODY>
</HTML>