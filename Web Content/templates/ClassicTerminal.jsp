<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=1.18,0,04/07/28,16:51:42  -->

<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/blacktheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoBlack.css" type="text/css">
<% 
  String userAgent = (String) request.getHeader("user-agent");
  if ((userAgent.indexOf("Windows CE") != -1)) { %>
<style type="text/css">
select.HATSDROPDOWN {background-color: #092509;color: lime;}
</style>
<% } %>
</HEAD>
<BODY> 
<!--Remove the comments from the following line if you wish a link to go directly to the transformation-->
<!-- <a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a> -->
<DIV align="center">

<table width="760" align="center" border="0" cellspacing="4">
<tr>
<td align="center">
<a name="navskip"></a>
 <HATS:Transform skipBody="true">
    <P> Host screen transformation will be shown here </P>
 </HATS:Transform>
 </td>
 </tr>
<tr>
<td align="center" width="100%">
<HATS:ApplicationKeypad settings="layout:horizontal"/>
</td>
</tr>
</table>
</DIV>
</BODY>
</HTML>