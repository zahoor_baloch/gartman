<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- LANG attribute indicates the display language code of the template: Ex: en, fr, de" -->
<HTML LANG="en">

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<!-- CMVC keyword, do NOT translate the following line -->
<!--cmvc_en_version_level=%I%,%S%,%E%,%U%  -->

<HEAD>
<title><HATS:Util type="applicationName" /></title>
<HATS:Util type="baseHref" />
<META name="GENERATOR" content="IBM WebSphere Studio">
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- See the User's and Administration Guide for information on using templates and stylesheets -->
<!-- Global Style Sheet -->
<LINK rel="stylesheet" href="../common/stylesheets/blacktheme.css" type="text/css">
<LINK rel="stylesheet" href="../common/stylesheets/reverseVideoBlack.css" type="text/css">
<!-- embedded stylesheet to coordinate button and link colors -->
<STYLE type="text/css">

A.BOTTOMBAR:link {color:black}
A.BOTTOMBAR:active {color:black}
A.BOTTOMBAR:visited {color:black}

table.ApplicationKeypad {
	text-align: center;
	width: 100%;
	padding: 0px;
	margin: 0px;
}

td.ApplicationKeypad
{
	color: white; 
	white-space: nowrap;	
}

input.ApplicationButton {
  background-color: maroon;
  color: white;
  white-space: pre;
  width: 15em;
}

select.ApplicationDropDown {
  background-color: #EED39E;
  color: black;
  width: 15em;
}

INPUT.HostPFKey, INPUT.HostButton {
	color: white;
	background-color: maroon;
}

INPUT.HostButton {
	background-color: black;
}

A.HATSLINK:link, A.HATSLINK:visited {
	color: maroon;
}


.COMPANYTITLE{
  color: #FFFFFF;
  font-style: italic;
  font-size: xx-large;
}
.LINKSTITLE{
  color: #FFFFFF;
  font-size: 1.2em;
}


</STYLE>

</HEAD>
<BODY>
<TABLE width="100%" cellpadding="4" cellspacing="0" border="0" >
    <TBODY>
        <TR>
            <TD colspan="3" height="69" background="../common/images/Corporate2top.jpg"> <span CLASS="COMPANYTITLE">My Company</span></TD>
        </TR>
        <TR bgcolor="#EED39E">
         <TD height="100%" align="center">
        	<DIV align="center">
            	<TABLE height="100%" cellpadding="0" cellspacing="0" border="0" >
        			<TBODY>
        			<!--Remove the comments from the following lines if you wish a link to go directly to the transformation-->
                    <!-- 
        			<TR> 
        				<TD valign="top">
        			     <a href="../entry#navskip"><img src="..\common\images\fastfwd.gif" alt="Skip to host application" border="0" width="20" height="23"></a>
        			     </TD>
        			</TR>  
        			-->  
        			<TR>
					    <TD bgcolor="#000000">
					    	<IMG src="../common/images/Corporate2arrow.jpg" alt="" border="0" width="20" height="23" align="right" hspace="0" vspace="0">
					    </TD>
         				<TD bgcolor="#000000" width="100%" nowrap="nowrap">
							<STRONG CLASS="LINKSTITLE">&nbsp;My Company Links&nbsp;&nbsp;</STRONG>
						</TD>
        			</TR>
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
						    <BR>&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Home Page</A>		
							<BR>&nbsp;
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
							&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Map</A> 
							<BR>&nbsp;
						</TD>
					</TR> 
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
							&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Employees</A> 
							<BR>&nbsp;
						</TD>
					</TR>		
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
							&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Jobs at My Company</A> 
							<BR>&nbsp;
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
							&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">My Company Articles</A> 
							<BR>&nbsp;
						</TD>
					</TR>													
					<TR>
					    <TD bgcolor="#000000" >
					    	<IMG src="../common/images/Corporate2arrow.jpg" alt="" border="0" width="20" height="23" align="right" hspace="0" vspace="0">
					    </TD>					
						<TD bgcolor="#000000" width="100%" nowrap="nowrap">
							<STRONG CLASS="LINKSTITLE">&nbsp;My Products</STRONG>
						</TD>
					</TR>			
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
							<BR>&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Main Product</A> 
							<BR>&nbsp;
						</TD>
					</TR>						
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
							&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Additional Products</A>
							<BR>&nbsp;
						</TD>
					</TR>						
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
							&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Downloads</A>
							<BR>&nbsp;
						</TD>
					</TR>
					<TR>
						<TD bgcolor="#EED39E" colspan="2">
							&nbsp;<A CLASS="BOTTOMBAR" href="http://www.ibm.com">Support</A>
							<BR>&nbsp;
						</TD>
					</TR>					
					<TR>
						<TD colspan="2" align="center">
							<HATS:ApplicationKeypad/>
						</TD>
					</TR>
					<TR>
						<TD height="60">
					</TD>
					</TR>									
        			</TBODY>
        		</TABLE>
        	</DIV>
         </TD>
        <TD width="100%" height="100%" colspan="2" bgcolor="black" valign="top">
        <a name="navskip"></a>
 <HATS:Transform skipBody="true">
    <DIV align="center"><P> Host screen transformation will be shown here </P></DIV>
 </HATS:Transform>
         </TD>
        </TR>        
        </TBODY>
</TABLE>
</BODY>
</HTML>

