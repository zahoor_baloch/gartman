// Licensed Materials - Property of IBM
//
// AIMCHSR00
// (C)Copyright IBM Corp. 2003, 2008  All Rights Reserved

//.============================================================================
//.Function:  Defines and processes keyboard related constants and events
//.============================================================================

/***********************************************************
 * NOTICE: DO NOT MODIFY THE FOLLOWING VARIABLES!!
 ***********************************************************/
var CODE_BACKSPACE = 8;
var CODE_TAB       = 9;
var CODE_ENTER     = 13;
var CODE_PAUSE     = 19;
var CODE_ESC       = 27;
var CODE_PAGEUP    = 33;
var CODE_PAGEDOWN  = 34;
var CODE_END       = 35;
var CODE_HOME      = 36;
var CODE_INSERT    = 45;
var CODE_DELETE    = 46;
var CODE_A         = 65;
var CODE_B         = 66;
var CODE_C         = 67;
var CODE_D         = 68;
var CODE_E         = 69;
var CODE_F         = 70;
var CODE_G         = 71;
var CODE_H         = 72;
var CODE_I         = 73;
var CODE_J         = 74;
var CODE_K         = 75;
var CODE_L         = 76;
var CODE_M         = 77;
var CODE_N         = 78;
var CODE_O         = 79;
var CODE_P         = 80;
var CODE_Q         = 81;
var CODE_R         = 82;
var CODE_S         = 83;
var CODE_T         = 84;
var CODE_U         = 85;
var CODE_V         = 86;
var CODE_W         = 87;
var CODE_X         = 88;
var CODE_Y         = 89;
var CODE_Z         = 90;
var CODE_F1        = 112;
var CODE_F2        = 113;
var CODE_F3        = 114;
var CODE_F4        = 115;
var CODE_F5        = 116;
var CODE_F6        = 117;
var CODE_F7        = 118;
var CODE_F8        = 119;
var CODE_F9        = 120;
var CODE_F10       = 121;
var CODE_F11       = 122;
var CODE_F12       = 123;
var HostKey        = 1;
var ApplicationKey = 2;
var CODE_PLUS      = 107;
var CODE_MINUS     = 109;

/***********************************************************
* NOTICE: DO NOT MODIFY THE ABOVE VARIABLES!!
***********************************************************/

var defaultKeyMappings = [
  // KEYCODE,       ALT, CTRL, SHIFT, MNEMONIC
  //============ command key mappings ================
  [CODE_ENTER,      0,    0,     0, '[enter]'     ],
  [CODE_PAUSE,      0,    0,     0, '[attn]'      ],
  [CODE_ESC,        0,    0,     0, '[clear]'     ],
  [CODE_ESC,        0,    0,     1, '[sysreq]'    ],
  [CODE_R,          0,    1,     0, '[reset]'     ],
  [CODE_PAGEUP,     0,    0,     0, '[pageup]'    ],
  [CODE_PAGEUP,     1,    0,     0, 'refresh'     ],
  [CODE_PAGEDOWN,   0,    0,     0, '[pagedn]'    ],
  [CODE_PAGEDOWN,   1,    0,     0, '[pa3]'       ],
  [CODE_END,        1,    0,     0, '[pa2]'       ],
  [CODE_INSERT,     1,    0,     0, 'default'     ],
  [CODE_DELETE,     1,    0,     0, '[pa1]'       ],
  [CODE_D,          0,    1,     0, 'disconnect'  ],
  [CODE_H,          0,    1,     0, '[help]'      ],
  [CODE_P,          0,    1,     0, '[printhost]' ],
  [CODE_J,          0,    1,     0, 'printjobs'   ],
  [CODE_ENTER,      1,    0,     0, 'reverse'     ],
  [CODE_ENTER,      1,    1,     0, 'reverse'     ],
  [CODE_K,          0,    1,     0, 'toggle'      ],
  [CODE_S,          0,    1,     0, 'ResetButton' ],
  //============ function key mappings ===============
  [CODE_F1,         0,    0,     0, '[pf1]'       ],
  [CODE_F1,         0,    0,     1, '[pf13]'      ],
  [CODE_F2,         0,    0,     0, '[pf2]'       ],
  [CODE_F2,         0,    0,     1, '[pf14]'      ],
  [CODE_F3,         0,    0,     0, '[pf3]'       ],
  [CODE_F3,         0,    0,     1, '[pf15]'      ],
  [CODE_F4,         0,    0,     0, '[pf4]'       ],
  [CODE_F4,         0,    0,     1, '[pf16]'      ],
  [CODE_F5,         0,    0,     0, '[pf5]'       ],
  [CODE_F5,         0,    0,     1, '[pf17]'      ],
  [CODE_F6,         0,    0,     0, '[pf6]'       ],
  [CODE_F6,         0,    0,     1, '[pf18]'      ],
  [CODE_F7,         0,    0,     0, '[pf7]'       ],
  [CODE_F7,         0,    0,     1, '[pf19]'      ],
  [CODE_F8,         0,    0,     0, '[pf8]'       ],
  [CODE_F8,         0,    0,     1, '[pf20]'      ],
  [CODE_F9,         0,    0,     0, '[pf9]'       ],
  [CODE_F9,         0,    0,     1, '[pf21]'      ],
  [CODE_F10,        0,    0,     0, '[pf10]'      ],
  [CODE_F10,        0,    0,     1, '[pf22]'      ],
  [CODE_F11,        0,    0,     0, '[pf11]'      ],
  [CODE_F11,        0,    0,     1, '[pf23]'      ],
  [CODE_F12,        0,    0,     0, '[pf12]'      ],
  [CODE_F12,        0,    0,     1, '[pf24]'      ],
  [CODE_ENTER,      0,    1,     0, '[qfldext]'   ],   
  [CODE_ENTER,      0,    0,     1, '[fldext]'    ],   
  [CODE_PLUS,       0,    1,     0, '[field+]'    ],
  [CODE_MINUS,      0,    0,     1, '[qfld-]'     ],   
  [CODE_MINUS,      0,    1,     0, '[field-]'    ]
];

//***********************************************************************************
//* Due to limitations in how the Macintosh Safari browser maps keys to ordinal
//* values, we have the need to remove the Function key defnitions from the default
//* keymap.  If the mac safari browser is detected in keyboard init, we'll swap the
//* table defined below out for the default table defined above.
//***********************************************************************************
var macSafariKeyMappings = [
  // KEYCODE,       ALT, CTRL, SHIFT, MNEMONIC
  //============ command key mappings ================
  [CODE_ENTER,      0,    0,     0, '[enter]'     ],
  [CODE_PAUSE,      0,    0,     0, '[attn]'      ],
  [CODE_ESC,        0,    0,     0, '[clear]'     ],
  [CODE_ESC,        0,    0,     1, '[sysreq]'    ],
  [CODE_R  ,        0,    1,     0, '[reset]'     ],
  [CODE_PAGEUP,     1,    0,     0, 'refresh'     ],
  [CODE_PAGEDOWN,   0,    0,     0, '[pagedn]'    ],
  [CODE_PAGEDOWN,   1,    0,     0, '[pa3]'       ],
  [CODE_END,        1,    0,     0, '[pa2]'       ],
  [CODE_INSERT,     1,    0,     0, 'default'     ],
  [CODE_DELETE,     1,    0,     0, '[pa1]'       ],
  [CODE_D,          0,    1,     0, 'disconnect'  ],
  [CODE_H,          0,    1,     0, '[help]'      ],
  [CODE_P,          0,    1,     0, '[printhost]' ],
  [CODE_J,          0,    1,     0, 'printjobs'   ],
  [CODE_ENTER,      1,    0,     0, 'reverse'     ],
  [CODE_K,          0,    1,     0, 'toggle'      ],
  [CODE_S,          0,    1,     0, 'ResetButton' ],
  [CODE_ENTER,      0,    1,     0, '[qfldext]'   ],   
  [CODE_ENTER,      0,    0,     1, '[fldext]'    ],   
];

var applicationKeys =[
  'disconnect',
  'refresh',
  'printjobs',
  'toggle',
  'default',
  'reverse',
  'ResetButton'
];


var IMEkeysForKeyUp = [
//-Keycode----ALT-CTRL-Shift----//
[  CODE_ENTER,  0,  0,  0  ],
[  CODE_M,      0,  1,  0  ],
[  CODE_ENTER,  0,  1,  0  ]
];

var IMEkeysForKeyDown = [
//-Keycode----ALT-CTRL-Shift----//
[  25,          1,  0,  0  ] //ALT + SBCS/DBCS key
];

function isIMEkeysForKeyUp(event)
{
 var code = event.keyCode ? event.keyCode : event.which;
 var IMEkeysCount = IMEkeysForKeyUp.length;
     for( var i=0; i < IMEkeysCount; i++) {
     if( code == IMEkeysForKeyUp[i][0] &&
         event.altKey == IMEkeysForKeyUp[i][1] &&
         event.ctrlKey == IMEkeysForKeyUp[i][2] &&
         event.shiftKey == IMEkeysForKeyUp[i][3])
         return true;
     }
     return false;
}

function isIMEkeysForKeyDown(event)
{
 var code = event.keyCode ? event.keyCode : event.which;
 var IMEkeysCount = IMEkeysForKeyDown.length;
     for( var i=0; i < IMEkeysCount; i++) {
     if( code == IMEkeysForKeyDown[i][0] &&
         event.altKey == IMEkeysForKeyDown[i][1] &&
         event.ctrlKey == IMEkeysForKeyDown[i][2] &&
         event.shiftKey == IMEkeysForKeyDown[i][3])
         return true;
     }
     return false;
}


var preventDefault = false;
var keyHandlingEnabled = true;
var HostButtonList = new Array();
var ApplicationButtonList = new Array();
var toggleKeyCodeIndex,toggleKeyCode;
var NoHostKeypadButtons = true;
var bDebug = false;
var beInited = false;
var PortletKBInited = new Array();
var HATSPortletKBInited= new Array()
var toggleButtons = new Array();
var toggleButtonArraySize = 0;
var toggleLinks = new Array();
var toggleLinkArraySize = 0;
var toggleSearched = false;
var pjw;
var isKeySent = false;
var kbdSupportAll = false;

var autoSubmitField = null;
var autoSubmitFieldChanged = false;

var activeFormName = null;
var isIMEstate = false; //$IC58284 add$//

// onhelp handler
function helpfunction()
{ //khp-a
  if(isOtherBrowser) return true;
  keyboard_init();
  if (preventDefault)
  {
    if (bDebug)
       alert("Hot-key F1(CTRL/SHIFT-F1) no longer brings up the help window!!");
    preventDefault = false;
    return false;
  }
}

// oncontextmenu event handler
function contextfunction(e)
{ //khp-a
  if(isOtherBrowser) return true;
  keyboard_init();
  if (preventDefault)
  {
    if(bDebug)
      alert("Blocking context menu in NS6 the similar way as blocking help window in IE...");
    preventDefault = false;
    return false;
  }
}

if (!isIE && !isIEMobile) { 
  document.onkeypress = keypresshandler;
  document.onkeydown = keydownhandler;
  document.onkeyup = keyuphandler;
  document.onhelp = helpfunction;
  document.oncontextmenu = contextfunction;
  document.onclick = clickhandler;
}

// Toggles key handling on/off
// When keyHandlingEnabled is true, AID keys are processed.
// When keyHandlingEnabled is false, AID keys will not be processed.
function toggleKeyboard(pID)
{
  setHatsFocus(pID);
  if(isOtherBrowser) return;
  keyboard_init();

  keyHandlingEnabled = !keyHandlingEnabled;
  if(bDebug)
    alert("keyHandlingEnabled status after toggled=" + keyHandlingEnabled);
  if (keyHandlingEnabled)
  {
    hatsForm.KeyboardToggle.value = "1";
  }
  else
  {
    hatsForm.KeyboardToggle.value = "0";
  }
  ms('toggle',pID);
}

// onclick event handler
function clickhandler(e)
{
  if (beensubmitted) return false;
  e = (e) ? e : ((window.event) ? window.event : "");
  var element = (isIE) ? e.srcElement : e.target;

  // Return if the event is not for a hats form
  if(hatsForm !=null && element!=null && element.form!=undefined && hatsForm.name!=element.form.name) 
       return true;

  if(isIMEstate) isIMEstate = false; 

  forceChange(element);
  if(element.disabled==false){
    if(element.type == "radio"){
      pool = element.name.split("_");
      if(pool.length>=3)
      {
        if((pool[0].indexOf("in")!=-1) &&(!isNaN(pool[1]))&&(!isNaN(pool[2]))){
          if(!(element.checked)) return;
          if(element.disabled==false)
            element.blur();
          if(element.disabled==false)
            element.focus();
        }
      }
    }
  }

  // Code to handle positioning on protected text (non HTML anchor tag implementation)
  
  if (element.id && element.id != "" && element.id.substring(0, 2) == "p_") {
    var t = element.id.indexOf("_", 2);
    var pos = null;
    var formName = null;
    if (t != -1) {
      pos = element.id.substring(2, t);
      formName = element.id.substring(t + 1, element.id.length);
    } else {
      pos = element.id.substring(2, element.id.length);
      formName = (hatsForm != null ? hatsForm.name : null);
    }

    if (formName != null) {
      var action = element.getAttribute("action");
      if (action == null || action == "") {
        setCursorPosition(pos, formName);
      } else {
        msb(action, pos, formName);
      }
    }
  } else {
    if(carettrackingenabled||statuswindowenabled)
        updateCursorPosition(false,null); //A0 added null
  }
}

// Method to return the HTML Form object containing the given DOM node

function getParentForm(node) {
        while (node.parentNode && node.parentNode != null && node.parentNode != node) {
                if (node.parentNode.tagName && node.parentNode.tagName.toUpperCase() == "FORM") {
                        return node.parentNode;
                } else {
                        node = node.parentNode;
                }
        }

        return null;
}

// keyUp event handler
function keyuphandler(e)
{
  if(isOtherBrowser && !OPERA) return true;                

  e = (e) ? e : ((window.event) ? window.event : "");
  var element = (isIE) ? e.srcElement : e.target;

  // Return if the event is not for a hats form
  if(hatsForm !=null && element!=null && element.form!=undefined && hatsForm.name!=element.form.name) 
       return true;

  if((enableDBCSSession == true) && (element.tagName == "INPUT")) 
  {                                                               
    //var code = e.keyCode ? e.keyCode : e.which;                 
    //if(isIMEstate && code == CODE_ENTER)                        
      if(isIMEstate && isIMEkeysForKeyUp(e))                      
          isIMEstate = false;                                     
  }                                                               

  if((enableDBCSSession == true) && (element.tagName == "INPUT") && element.onkeypress != null){
      element.onkeypress(e);
      
      if (isHInput(element.type, element.name)) {
          var kc = e.keyCode ? e.keyCode : e.which;
          if (!isNonDataKeyCodeForKeyUp(e)) {
              element.setAttribute("MDT", "1");
          }
      }
  }

  if((enableBIDI == "true") && (element.type != "password") && (element.tagName == "INPUT")){
    str = element.value;
    var length = str.length;
    for(i = 0;i < length ;i++) {
      code = str.charCodeAt(i);
      if(((code > 64) & (code < 91)) || ((code > 96) & (code < 123))){
        element.dir = "ltr";
        break;
      }
      else if(code > 1487){
        element.dir = "rtl";
        break;
      }
    }
  }
  if (beensubmitted) return true;
  if(carettrackingenabled||statuswindowenabled) {
      if(isNonDataKeyCodeForKeyUp(e))
          updateCursorPosition(false,null);
      else
          updateCursorPosition(true,null);
  }
}

// keyPress event handler
function keypresshandler(e)
{

  e = (e) ? e : ((window.event) ? window.event : "");
  var element = e.srcElement ? e.srcElement : e.target;

  // Return if the event is not for a hats form
  if(hatsForm !=null && element!=null && element.form!=undefined && hatsForm.name!=element.form.name) 
       return true;

  
  if (element != null) {
      if (isHInput(element.type, element.name)) {
                  var kc = e.keyCode ? e.keyCode : e.which;
                  if (!isNavigationKeyCode(e)) {
                          element.setAttribute("MDT", "1");
                          //alert("3.value of element " + element + " set to : " + element.getAttribute("MDT"));
                  }
      }
  }

  refreshSafe = false;
  if(isOtherBrowser && !OPERA)  
  {
    if (e.keyCode == CODE_ENTER)
    {
       ms('[enter]');
       return false;
    }
    return true;
  }
  keyboard_init();
  if (isIE || !isNS6)  return;
  if (preventDefault)
  {
    e.preventDefault();
    if(!(e.keyCode == CODE_F10 && (e.shiftKey)))preventDefault = false;
    return false;
  }
}

//Performs keydown event on specified formName.
function keydownhandlerHSR(e, formName)
{
  hatsForm = document.forms[formName];
  if (hatsForm != null)
    activeFormName = formName;
  keydownhandler(e);
}

// keyDown event handler
function keydownhandler(e)
{

  e = (e) ? e : ((window.event) ? window.event : "");
  var element = (isIE && !MAC) ? e.srcElement : e.target;


  // Return if the event is not for a hats form
  if(hatsForm !=null && element!=null && element.form!=undefined && hatsForm.name!=element.form.name) 
       return true;

  if((enableDBCSSession == true) && (element.tagName == "INPUT"))            
  {                                                                          
      var code = e.keyCode ? e.keyCode : e.which;                            
      //isIMEstate = code == 229;                                            
      if(code == 229) isIMEstate = true;                                     
      else if(isIMEstate && isIMEkeysForKeyDown(e)) isIMEstate = false;      
      else if(isIMEstate && !isNonDataKeyCodeForKeyUp(e)) isIMEstate = false;

      if(isIMEstate)                                                         
          refreshSafe = false;                                               
  }                                                                          

  if((enableDBCSSession == true) && (element.tagName == "INPUT") && element.onkeypress != null)
  {
    element.onkeypress(e);
    if(e.returnValue == false) return;
  }
  if(hatsForm==null) return true;
  if(isOtherBrowser && !OPERA) 
  {
    return true;
  }
  keyboard_init();
  preventDefault = false;
  if (hatsForm != null)
  {
    if (hatsForm.name == activeFormName)
    {
      if(!beInited) return true;
    }
    else
    {
      if(!HATSPortletKBInited[hatsForm.name]) return true;
    }
  }

  if (!(isIE||isNS6||OPERA)) return true;                        
  var keyCode = (isIE)?(window.event.keyCode):(e.keyCode);
  var altKeyDown = (isIE)?(window.event.altKey):(e.altKey);
  var ctrlKeyDown = (isIE)?(window.event.ctrlKey):(e.ctrlKey);
  var shiftKeyDown = (isIE)?(window.event.shiftKey):(e.shiftKey);
  if( isToggleKey(keyCode,altKeyDown,ctrlKeyDown,shiftKeyDown) && (ApplicationButtonList['toggle']) )
  {
     toggleKeyboard(hatsForm.name);
     blockDefaultKeyAction();
     return false;
  }

  if (!keyHandlingEnabled) return true;
  
  if(carettrackingenabled||statuswindowenabled) {
      if(isNonDataKeyCodeForKeyUp(e))
          updateCursorPosition(false,null);
      else
          updateCursorPosition(true,null);
  }
  
  var target = (isIE && !MAC)?(document.activeElement):(e.target);
  if(isIE)
  {
    if( target.tagName.toLowerCase()=="select")
    {
      if(target.onchange)
      {
        var a =" "+target.onchange;
        if(a.indexOf('ms')!=-1)
        {
          if ( ((keyCode+""=="38") || (keyCode+""=="40"))
               && !(altKeyDown && ctrlKeyDown && shiftKeyDown) )
          {
            autoSubmitField=target;
            autoSubmitFieldChanged=true;
            if ( keyCode+""=="38") target.selectedIndex-=1;
            else if ( keyCode+""=="40") target.selectedIndex+=1;
            else target.selectedIndex+=1;
            return false;
          }
          else if (keyCode+""=="9") //&& !(altKeyDown && ctrlKeyDown && shiftKeyDown)
          {
            forceChange(null);
          }
        }
      }
    }
  }

  if(isIE && (altKeyDown & (keyCode == CODE_ENTER)))
  {
    try
    {
      if(document.all.reverse == null) {
        blockDefaultKeyAction();
        return;
      }
    }
    catch(excp) {
      blockDefaultKeyAction();
      return;
    }
  }

  var mappingCount, code;
  mappingCount = defaultKeyMappings.length;

  for (i=0; i<mappingCount; i++)
  {
    code = defaultKeyMappings[i][0];

    if ((code == keyCode)
        && !(defaultKeyMappings[i][1]^altKeyDown)
        && !(defaultKeyMappings[i][2]^ctrlKeyDown)
        && !(defaultKeyMappings[i][3]^shiftKeyDown))
    {
      if((target!=null)&&(defaultKeyMappings[i][4]=="[enter]")) {
        if((target.tagName.toLowerCase()=="a") &&
          ((target.href.toLowerCase().indexOf("javascript")==-1) ||
           (target.href.toLowerCase().indexOf("setcur")==-1)))
           return true;

        if((target.tagName.toLowerCase()=="input")&&
        ((target.getAttribute("type").toLowerCase()=="button")||(target.getAttribute("type").toLowerCase()=="submit"))) {
          return true;
        }

        if(isIE){
          if( target.tagName.toLowerCase()=="select"){
            if(target.onchange)
            {
              var a =" "+target.onchange;
              if(a.indexOf('ms')!=-1){
                target.onchange();
                return false;
              }
            }
          }
        }
      }
      
      if(defaultKeyMappings[i][4]=="[qfldext]") {         
              processQuickFieldExit(element,"");          
              blockDefaultKeyAction();                    
          } else if(defaultKeyMappings[i][4]=="[qfld-]") {
              processQuickFieldExit(element,"-");         
              blockDefaultKeyAction();                    
      }
      else {
              if(enableBIDI=="true" && gobject != null)
                updateBiDiCursorPos(gobject);

              sendKeyIfEnabled(defaultKeyMappings[i][4]);
      }
      return true;
    }
  }
  return true;
}


function processQuickFieldExit(element,prefix)
{
    var value=element.value;
    var name=element.name;

    
    if (element.tagName != "INPUT")
                return;

    // calculate starting pos and length of the field
    var p=name.split("_");
    var pos=parseInt(p[1]);
    var len=parseInt(p[2]);

    // clear the field from the caret position to the end
    var caretPos = parseInt(hatsForm.CARETPOS.value);
    var endChar=pos+len-1;
    if (caretPos<=endChar)
    { 
        var newValue = value.substring(0, caretPos-pos);
        element.value=prefix+newValue; 
        forceChange(element);
    }
    var nextElement=tabToNextInputField(element,pos,len);
}

// If autoSubmitField != target, force onchange() call on autoSubmitField
function forceChange(target)
{
  if (autoSubmitField != null) {
    if (autoSubmitFieldChanged) {
      if (autoSubmitField != target) {
        autoSubmitField.onchange();
      }
    }
  }
}

// Sends AID key mnemonic only if enabled
function sendKeyIfEnabled(mnemonic)
{
  if(isOtherBrowser && !OPERA) return;                
  keyboard_init();
  if (bDebug) alert("check before sending key... " + mnemonic + "");

  if ( (kbdSupportAll) ||
       (NoHostKeypadButtons && !ApplicationButtonList[mnemonic]) ||
       (HostButtonList[mnemonic] | ApplicationButtonList[mnemonic]) )
  {
    blockDefaultKeyAction();
    if(mnemonic == "printjobs")
    {
       openPrintWindow(PrintURL);
    }
    else
    {
       ms(mnemonic,hatsForm.name);
       isKeySent = true;
    }
  }
}

// Prevents default key action
function blockDefaultKeyAction()
{
  if(isOtherBrowser) return;
  keyboard_init();
  if (isIE)
  {
    window.event.keyCode = 0;
    window.event.returnValue = false;
  }
  preventDefault = true;
}

// Returns an index of where "toggle" keycode is from defaultKeyMappings array
function getToggleKeyCodeIndex()
{
  if(isOtherBrowser) return 0;
  var mappingCount, code;
  mappingCount = defaultKeyMappings.length;

  for (i=0; i < mappingCount; i++)
  {
    code = defaultKeyMappings[i][0];
    if (defaultKeyMappings[i][4] == "toggle")
    {
       toggleKeyCodeIndex = i;
       return toggleKeyCodeIndex;
    }
  }
}

// Returns "toggle" keycode
function getToggleKeyCode()
{
  if(isOtherBrowser) return 0;
  return defaultKeyMappings[getToggleKeyCodeIndex()][0];
}

// Checks whether the keyCode is a "toggle" keycode
function isToggleKey(keyCode,altKeyDown,ctrlKeyDown,shiftKeyDown)
{
  if(isOtherBrowser) return false;
  keyboard_init();
  var code;
  var i = toggleKeyCodeIndex;

  if (defaultKeyMappings == null || defaultKeyMappings[i] == null)
  {
    return false;
  }

  code = defaultKeyMappings[i][0];
  if ((code == keyCode)
      && !(defaultKeyMappings[i][1]^altKeyDown)
      && !(defaultKeyMappings[i][2]^ctrlKeyDown)
      && !(defaultKeyMappings[i][3]^shiftKeyDown))
  {
    return true;
  }
  return false;
}

// Initializes keyboard setting
function keyboard_init()
{
  if((isOtherBrowser && !OPERA) || hatsForm == null ) return;           
  if(SAFARI)
    defaultKeyMappings = macSafariKeyMappings;
  if(hatsForm.name == activeFormName)
  {
    if(beInited==true) return;
    beInited = true;
  }
  else
  {
    if(HATSPortletKBInited[hatsForm.name] == null) HATSPortletKBInited[hatsForm.name] = false;
    if(HATSPortletKBInited[hatsForm.name] == true)
    {
      return;
    }
    //setFormObj();  //the form obj must to be reloaded, if this portlet has not done keyboard_init.
    for(pID in HATSPortletKBInited)
    {
      HATSPortletKBInited[pID] = false;
      if(pID == hatsForm.name)
      {
         HATSPortletKBInited[pID] = true;
      }
    }
  }
  toggleKeyCode = getToggleKeyCode();
  InitButtonList();

  if (bDebug) debug_yoyo();
  if (hatsForm.KeyboardToggle.value=="0") keyHandlingEnabled = false;
  else if (hatsForm.KeyboardToggle.value=="1") keyHandlingEnabled = true;
}

// Determines whether a mnemonic is an application key or host key.
// Returns 0 for mnemonic doesn't exist in defaultKeyMappings table
function whatKindOfKey(mnemonic)
{
  if(isOtherBrowser) return 0;
  keyboard_init();
  for (var i=0 ; i < defaultKeyMappings.length ; i++)
  {
    if(mnemonic == defaultKeyMappings[i][4])
    {
      if(isApplicationKey(mnemonic))
        return ApplicationKey;
      else
        return HostKey;
    }
  }
  return 0;
}


// event.ctrlKey, event.altKey, Arrow keys, home, end, backspace, insert, tab, enter, delete , alt, ctrl, Shift, Fn, Caps Lock, Num Lock, Scroll Lock, Menu
function isNonDataKeyCodeForKeyUp(event) {
        event = (event) ? event : ((window.event) ? window.event : "");
        var keyCode = event.keyCode ? event.keyCode : event.which;
        var ret = event.ctrlKey || event.altKey ||(keyCode >= 37 && keyCode <= 40) || keyCode == 36 || keyCode == 35 || keyCode == 8 || keyCode == 45 || keyCode == 9 || keyCode == 13 || keyCode == 46
                      || keyCode == 18 || keyCode == 17 || keyCode == 16 || keyCode == 255 || keyCode == 20 || keyCode == 144 || keyCode == 145 || keyCode == 93;
        return ret;
}

// Checks whether a mnemonic is an application key or not
function isApplicationKey(mnemonic)
{
  if(isOtherBrowser) return false;
  keyboard_init();
  for (var i=0 ; i < applicationKeys.length ; i++)
  {
    if(mnemonic == applicationKeys[i])
       return true;
  }
  return false;
}

// Initializes button list
function InitButtonList()
{
  if(isOtherBrowser) return;
  var cells,elementNext,cellName;
  var debugOut="";

  cells = document.getElementsByTagName("input");
  for ( var j=0 ; j < cells.length ; j++)
  {
    if(cells[j].getAttribute("accesskey")!= "hatsportletid")
    {
      if(cells[j].getAttribute("accesskey")!= hatsForm.name)
      {
        continue;
      }
    }
    elementNext = cells[j];
    cellName = elementNext.name;
    if (elementNext.type == "button")
    {
      if (bDebug) debugOut = debugOut + "\n" + "   button cellName = " + cellName;
      switch(whatKindOfKey(cellName))
      {
        case 1://HostKey:
           NoHostKeypadButtons = false;
           if (!(elementNext.disabled))
           {
               HostButtonList[cellName]= true;
               if (bDebug) debugOut = debugOut + ":=> HostKey \n";
           }
           break;
        case 2://ApplicationKey:
           if (!(elementNext.disabled))
           {
               ApplicationButtonList[cellName]= true;
               if (bDebug) debugOut = debugOut + ":=> ApplicationKey \n";
           }
           break;
      }
    }
  }
  cells = document.getElementsByTagName("a");
  for ( var j=0 ; j < cells.length ; j++)
  {
    if(cells[j].getAttribute("accesskey") != "hatsportletid")
    {
      if(cells[j].getAttribute("accesskey") != hatsForm.name)
      {
        continue;
      }
    }
    elementNext = cells[j];
    cellName = elementNext.name;
    if (bDebug) debugOut = debugOut + "\n" + "   link cellName = " + cellName;
    switch(whatKindOfKey(cellName))
    {
      case 1://HostKey:
        NoHostKeypadButtons = false;
        if (elementNext.href != "")
        {
            HostButtonList[cellName]= true;
            if (bDebug) debugOut = debugOut + ":=> HostKey \n";
        }
        break;
      case 2://ApplicationKey:
        if (elementNext.href != "")
        {
            ApplicationButtonList[cellName]= true;
            if (bDebug) debugOut = debugOut + ":=> ApplicationKey \n";
        }
        break;
    }
  }
  if (bDebug) alert(debugOut);
}

// Used for debugging to list buttons
function debug_yoyo()
{
  if(isOtherBrowser) return;
  keyboard_init();
  alert("toggleKeyCodeIndex="+toggleKeyCodeIndex+"\n"+"toggleKeyCode="+toggleKeyCode);

  var out="HostButtonList Aray \n============================\n";
  for (member in HostButtonList )
  {
    out = out + member + " = " +HostButtonList[member] +"\n";
  }
  alert(out);
  out="ApplicationButtonList Aray \n============================\n";
  for (member in ApplicationButtonList )
  {
    out = out + member + " = " +ApplicationButtonList[member] +"\n";
  }
  alert(out);
}

// Closes Print Job window
function closePrintJobWindow()
{
  if (pjw != null)
  {
    pjw.close();
  }
}

// Opens Print Window
function openPrintWindow(url)
{
  var features,newX,newY;
  if (isIE)
  {
    newX = window.screenLeft + 20;
    newY = window.screenTop + 20;
  }
  if (isNS4 || isNS6)
  {
    newX = window.screenX + 20;
    newY = window.screenY + 20;
  }
  features = 'resizable,top=' + newY +
             ',left=' + newX +
             ',screenX=' + newX +
             ',screenY=' + newY + ',scrollbars,status,toolbar';
  pjw = window.open(url,"HATSv1PrintJobViewOpenedByEndUser",features );
}

// Toggles Search
function searchToggles()
{
  var cells;

  cells = document.getElementsByTagName("input");
  for (var i=0; i < cells.length; i++)
  {
    if (cells[i].getAttribute("name") == "toggle")
    {
      toggleButtons[toggleButtonArraySize++] = cells[i];
    }
  }
  //search links
  cells = document.getElementsByTagName("a");
  for (var i=0; i < cells.length; i++)
  {
    if (cells[i].getAttribute("name") == "toggle")
    {
      toggleLinks[toggleLinkArraySize++] = cells[i];
    }
  }
  toggleSearched = true;
}

// Changes toggle captions
function changeToggleCaptions()
{
  if (!toggleSearched)
  {
    searchToggles();
  }
  var captionText = keyHandlingEnabled ? keyboardOffString: keyboardOnString;

  for (var i=0; i < toggleButtons.length; i++)
  {
    toggleButtons[i].value = captionText;
  }
  if(!isIEMobile){ 
          for (var i=0; i < toggleLinks.length; i++)
          {
            toggleLinks[i].replaceChild(
            document.createTextNode(captionText ),
            toggleLinks[i].firstChild);
          }
  }
}
