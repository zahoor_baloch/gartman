// Licensed Materials - Property of IBM
//
// AIMCHSR00
// (C)Copyright IBM Corp. 2003 - 2005  All Rights Reserved

//.============================================================================
//.Function:  Sets base environment variables representing browser, OS, and etc.
//.           This file must be included before any other HSR JavaScript files.
//.============================================================================

var brwsapname = navigator.appName;
var brwsusragent = navigator.userAgent;
// var brwsapversion = navigator.appVersion;

var isLinux = (navigator.platform.indexOf("Linux") != -1) ? true : false;

var NN = false; //Netscape Navigator
var MAC = false; //Mac
var NNCOMPAT = false; //Netscape Navigator compatible
var OPERA = false; //Opera
var MOZILLA = false; //Mozilla
var KONQUEROR = false; //Konqueror
var SAFARI = false; //Safari
var isIE = false; //Microsoft Internet Explorer

var isNS4 = (document.layers) ? true : false;
var isIE4 = (document.all) ? true : false;

var isIE5 = false;
try{
	isIE5=(document.all && document.getElementById) ? true : false;
}catch(e){
	isIE5=false;
}

var isNS6 = false;
try{
	isNS6=((document.getElementById)&&(!isIE4)) ? true : false;
}catch(e){
	isNS6=false;
}

var isOtherBrowser = false;
var isIEMobile = false; 

if ((brwsusragent.toLowerCase().indexOf("mac")!=-1)) 
  MAC=true;

if ((brwsapname == 'Microsoft Internet Explorer'))
{
  //brwsapname = 'Microsoft Internet Explorer'; 
  isIE = true;
}

if(brwsusragent.indexOf('Windows CE')!=-1){ 
  brwsapname = 'Microsoft Internet Explorer Mobile';
  isIEMobile = true;
  isIE = true; 
  event=window.event;
}

if (brwsusragent.indexOf('Opera') != -1)
{
  brwsapname = 'Opera';
  isIE=false;
  NN=false;
  NNCONMPAT = true;
  OPERA = true;
  isIE4=false;
}

if (brwsapname.indexOf('Netscape') != -1)
{
  brwsapname = 'Netscape';
  NN = true;
  NNCOMPAT = true;
  isIE = false;
  isIE4=false;
}

if (brwsapname == 'Netscape')
{
  if ((brwsusragent.indexOf('Mozilla') != -1) || (brwsusragent.indexOf('Firebird') != -1))
  {
    brwsapname = 'Mozilla';
    NNCOMPAT = true;
    MOZILLA = true;
    isIE = false;
    isIE4=false;
    isNS4=false;
  }
}

if (brwsusragent.indexOf('Konqueror') != -1)
{
  brwsapname = 'Konqueror';
  KONQUEROR = true;
  NNCOMPAT = true;
  isIE = false;
  NN = false;
  isNS4 = false;
  isNS6 = false;
  isIE4=false;
}

if (brwsusragent.indexOf('Safari') != -1)
{
  brwsapname = 'Safari';
  SAFARI = true;
  NNCOMPAT = true;
  isIE = false;
  NN = false;
  isNS4 = false;
  //isNS6 = false;
  isIE4=false;
}

if (MAC && isIE)
{
  isIE = true;
  isIE4=false;
}

if (!isIE)
{
   isIE5 = false;
}

isOtherBrowser = (!isIE) && (!isNS6);

function getBrowserAppName()
{
    return brwsapname;
}

function getBrowserUserAgent()
{
    return brwsusragent;
}

// alert ("MAC = "+MAC+", isLinux = "+isLinux+", brwsapname = "+brwsapname+", isOtherBrowser = "+isOtherBrowser+"\nisIE = "+isIE+", isIE4 = "+isIE4+", isIE5 = "+isIE5+"\nNN = "+NN+", NNCOMPAT = "+NNCOMPAT+", isNS4 = "+isNS4+", isNS6 = "+isNS6+", MOZILLA = "+MOZILLA);
