<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.common.*" %>
<%@ page import="com.ibm.hats.transform.regions.BlockScreenRegion" %>
<%@ page import="com.ibm.hats.transform.components.Component" %>
<%@ taglib uri="/WEB-INF/classes/tld/hats.tld" prefix="HATS" %>
<% 
	//retrieve the dynamic region from the request
	BlockScreenRegion region = (BlockScreenRegion) request.getAttribute("com.ibm.hats.transform.regions.ScreenRegion"); 
	
	//retrieve the host screen from the request
	HostScreen hostScreen = (HostScreen) ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getHostScreen();
	
	//useful information about the dynamic region
	int fieldStart = Component.convertRowColToPos(region.startRow, region.startCol, hostScreen.getSizeCols());
	int fieldEnd = Component.convertRowColToPos(region.endRow, region.endCol, hostScreen.getSizeCols());
	int fieldLength = fieldEnd - fieldStart + 1;
%>
<style>
input.HWHITE {
		border:none;
	outline:none;
	margin:0px;
	padding:0px 4px 0 6px;
	width:450px;
	height:18px;
	font:normal 11px Tahoma, Geneva, sans-serif;
	background: transparent url(../common/images/field600.png) no-repeat top right;
    line-height:18px;
}
input.HWHITE:focus {
	background: transparent url(../common/images/field600.png) no-repeat bottom right;
}

</style>
<!-- place your content here -->
<HATS:Component type="com.ibm.hats.transform.components.InputComponent" widget="com.ibm.hats.transform.widgets.InputWidget" dynamicRegion="com.ibm.hats.transform.regions.ScreenRegion" componentSettings="clipInputFields:true|extractCaptions:false|stripReplaceWith::|trimCaptions:true|stripCaptions:true|clipCaptions:false|stripAfterDelimiters::|-|.|" widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:true|columnSeparatorStyle:|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:|cursorModeCEPAltValue:|stripUnderlinesOnInputs:true|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|" applyTextReplacement="true"/>