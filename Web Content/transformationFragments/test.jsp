<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.common.*" %>
<%@ page import="com.ibm.hats.transform.regions.BlockScreenRegion" %>
<%@ page import="com.ibm.hats.transform.components.Component" %>
<%@ taglib uri="/WEB-INF/classes/tld/hats.tld" prefix="HATS" %>
<% 
	//retrieve the dynamic region from the request
	BlockScreenRegion region = (BlockScreenRegion) request.getAttribute("com.ibm.hats.transform.regions.ScreenRegion"); 
	
	//retrieve the host screen from the request
	HostScreen hostScreen = (HostScreen) ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getHostScreen();
	
	//useful information about the dynamic region
	int fieldStart = Component.convertRowColToPos(region.startRow, region.startCol, hostScreen.getSizeCols());
	int fieldEnd = Component.convertRowColToPos(region.endRow, region.endCol, hostScreen.getSizeCols());
	int fieldLength = fieldEnd - fieldStart + 1;
	
	System.out.println("region.startRow:"+region.startRow+"......region.startCol:"+region.startCol+"......fieldStart:"+fieldStart+".....fieldEnd:"+fieldEnd+"......fieldLength:"+fieldLength);
	
	String name = "in_"+fieldStart+"_"+fieldLength;
	System.out.println("in test.jsp fragment...................name:"+name);
	
%>
<!-- place your content here -->

<a href='http://www.hanksspec.com'><input size="<%=fieldLength %>" fillrequired="false" onkeypress="allowAlphaNumericShift(event)" onblur="allowAlphaNumericShift(event)" onclick="setCursorPosition('<%=fieldStart %>', 'HATSForm');allowAlphaNumericShift(event)" autoenter="false" maxlength="<%=fieldLength %>" name="<%=name %>" ondrop="allowAlphaNumericShift(event)" onchange="checkInput(this);allowAlphaNumericShift(event)" class="field610" value="http://www.hanksspec.com " entryrequired="false" onpaste="allowAlphaNumericShift(event)" fieldexitrequired="false" class="HATSINPUT HWHITE" onfocus="setFocusFieldIntoGlobal(this, 'HATSForm')" type="text"></a>

<script>
//var inputAnch = document.getElementsByTagName('input');
//for(var i=0;i<inputAnch.length;i++){
//alert(i);
//}
</script>
