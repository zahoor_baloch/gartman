<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.common.*" %>
<%@ page import="com.ibm.hats.transform.regions.BlockScreenRegion" %>
<%@ page import="com.ibm.hats.transform.components.Component" %>
<%@ taglib uri="/WEB-INF/classes/tld/hats.tld" prefix="HATS" %>
<% 
	//retrieve the dynamic region from the request
	BlockScreenRegion region = (BlockScreenRegion) request.getAttribute("com.ibm.hats.transform.regions.ScreenRegion"); 
	
	//retrieve the host screen from the request
	HostScreen hostScreen = (HostScreen) ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getHostScreen();
	
	//useful information about the dynamic region
	int fieldStart = Component.convertRowColToPos(region.startRow, region.startCol, hostScreen.getSizeCols());
	int fieldEnd = Component.convertRowColToPos(region.endRow, region.endCol, hostScreen.getSizeCols());
	int fieldLength = fieldEnd - fieldStart + 1;
	
	String name = "in_"+fieldStart+"_"+fieldLength;
	int fieldInt = fieldLength+1;
	String clas = "field"+fieldInt+"0";
%>


<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script language="JavaScript">
function sendF4Prompt(cursorPos) {
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[pf4]', 'HATSForm');
}
</script>


<table cellpadding="0" cellspacing="0">
<tr>
<td>
	<input size="<%=fieldLength %>" onkeypress="allowAlphaNumericShift(event)" onblur="allowAlphaNumericShift(event)" onclick="setCursorPosition('<%=fieldStart %>', 'HATSForm');allowAlphaNumericShift(event)" maxlength="<%=fieldLength %>" name="<%=name %>" onchange="checkInput(this);allowAlphaNumericShift(event)" class="<%=clas%>" onfocus="setFocusFieldIntoGlobal(this, 'HATSForm')" type="text">
</td>
<td>
	<img style="cursor: pointer; "
						title="Please click to send F4 command" border="0"
						src="../common/images/find.png"
						align="absmiddle" onclick="sendF4Prompt('<%=fieldStart%>');">
</td>
</tr>
</table>