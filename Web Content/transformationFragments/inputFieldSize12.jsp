<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.common.*" %>
<%@ page import="com.ibm.hats.transform.regions.BlockScreenRegion" %>
<%@ page import="com.ibm.hats.transform.components.Component" %>
<%@ taglib uri="/WEB-INF/classes/tld/hats.tld" prefix="HATS" %>
<% 
	//retrieve the dynamic region from the request
	BlockScreenRegion region = (BlockScreenRegion) request.getAttribute("com.ibm.hats.transform.regions.ScreenRegion"); 
	
	//retrieve the host screen from the request
	HostScreen hostScreen = (HostScreen) ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getHostScreen();
	
	//useful information about the dynamic region
	int fieldStart = Component.convertRowColToPos(region.startRow, region.startCol, hostScreen.getSizeCols());
	int fieldEnd = Component.convertRowColToPos(region.endRow, region.endCol, hostScreen.getSizeCols());
	int fieldLength = fieldEnd - fieldStart + 1;
%>
<!-- place your content here -->
<HATS:Component type="com.ibm.hats.transform.components.InputComponent"
	widget="com.ibm.hats.transform.widgets.InputWidget"
	dynamicRegion="com.ibm.hats.transform.regions.ScreenRegion"
	componentSettings="clipInputFields:true|extractCaptions:false|stripReplaceWith::|trimCaptions:true|stripCaptions:true|clipCaptions:false|stripAfterDelimiters::|-|.|"
	widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:field130|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
	applyTextReplacement="true" textReplacement="" alternate=""
	alternateRenderingSet="" />