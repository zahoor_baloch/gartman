<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ page import="com.ibm.hats.common.*" %>
<%@ page import="com.ibm.hats.transform.regions.BlockScreenRegion" %>
<%@ page import="com.ibm.hats.transform.components.Component" %>
<%@ taglib uri="/WEB-INF/classes/tld/hats.tld" prefix="HATS" %>
<% 
	//retrieve the dynamic region from the request
	BlockScreenRegion region = (BlockScreenRegion) request.getAttribute("com.ibm.hats.transform.regions.ScreenRegion"); 
	
	//retrieve the host screen from the request
	HostScreen hostScreen = (HostScreen) ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getHostScreen();
	
	//useful information about the dynamic region
	int fieldStart = Component.convertRowColToPos(region.startRow, region.startCol, hostScreen.getSizeCols());
	int fieldEnd = Component.convertRowColToPos(region.endRow, region.endCol, hostScreen.getSizeCols());
	int fieldLength = fieldEnd - fieldStart + 1;
%>
<!-- place your content here -->
<HATS:Component type="com.ibm.hats.transform.components.InputComponent" widget="com.ibm.hats.transform.widgets.DropdownWidget" dynamicRegion="com.ibm.hats.transform.regions.ScreenRegion" componentSettings="clipInputFields:true|extractCaptions:false|stripReplaceWith::|trimCaptions:true|stripCaptions:true|clipCaptions:false|stripAfterDelimiters::|-|.|" widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:HATSTABLE|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:HATSCAPTION|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Copy=1|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:HATSDROPDOWN|optionStyleClass:HATSOPTION|showSubmitButton:false|caption:|captionVariable:|" applyTextReplacement="true"/>