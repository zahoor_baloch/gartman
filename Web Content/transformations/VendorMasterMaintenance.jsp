<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
<script>
function checkWidth() {
if (parent.document.getElementById("hatsFrame").width == 1050)
{document.getElementById("colcenterover").className = "contentbgmainright"; }
else if (parent.document.getElementById("hatsFrame").width == 1208) 
{document.getElementById("colcenterover").className = "contentbgmainrightfull";}
}

function setActionCode(actionCode,caption){
document.HATSForm.in_252_1.value = actionCode;
document.getElementById('textfield10').value = caption;
}
</script>
</head>

<body onload="">

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<!-- Start of the HATS form. -->
<HATS:Form>

<!-- Insert your HATS component tags here. -->

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<!--Menu Start-->
<div class="bartmaintop">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="comtxt">
	<tr>
	<td width="100"><HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="11"
				componentSettings="" row="2" /></td>
	<td align="center" class="comtxt2">&nbsp;</td>
	<td width="100"><HATS:Component col="72" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="2" /></td>
	</tr>
</table>
</div>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->
	
    
<div class="boxloanpage">    
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="left">

<div style="height:465px" class="TabmainContent">

<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Vendor:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="4" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:true|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent" ecol="7"
								componentSettings="" row="4" /></td>
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Action:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><input type="text" class="field300" name="textfield10" id="textfield10" /></input><HATS:Component
						col="12" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="12"
						componentSettings="" row="4" /></td>
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Pattern After:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="21" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="4" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="26" componentSettings="" row="4" /></td>
	</tr>
</table>
    
<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
	<tr>
	<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="commontab">
	<tr>
	<td colspan="2" align="left" class="tabtxthead barhead4new2">Action Codes</td>
	</tr>
	<tr class="oddbar13">
	<td width="50%"><a href="javascript:void(0)" onclick="setActionCode('A',this.innerHTML);">Add a new record</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('F',this.innerHTML);">Work with comments</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('C',this.innerHTML);">Change an existing record</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('L',this.innerHTML);">Work with locations</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('D',this.innerHTML);">Delete an exiting record</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('R',this.innerHTML);">Work with remit to</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('E',this.innerHTML);">Work with contacts</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('S',this.innerHTML);">Work with sources of supply</a></td>
	</tr>
    <tr class="oddbar13">
	<td>&nbsp;</td>
	<td><a href="javascript:void(0)" onclick="setActionCode(' ',this.innerHTML);">Inquiry</a></td>
	</tr>
</table>
	</td>
	</tr>
    <tr>
	<td>
    <div class="clear5px"></div>
	<div class="bartab2">If the vendor's number is not known, you may get a list by leaving the vendor number blank, and pressing enter.</div>
	</td>
	</tr>
		<tr>
			<td align="center">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td align="center" style="color: red; font: bold 11px Tahoma, Geneva, sans-serif">
				<HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="24" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="71"
						componentSettings="" row="24" />
			</td>
		</tr>
</table>


</div>
	</td>
	</tr>
	
</table>
	</div>
   
	<!--Menu End-->

<script>
var actions =new Array("","A","C","D","E","F","L","R","S");
var actionDesc =new Array("Inquiry","Add a new record","Change an existing record","Delete an existing record","Work with contacts","Work with comments","Work with locations","Work with remit to","Work with sources of supply");
var action = document.HATSForm.in_252_1.value;
var actionIndex = 0;
document.HATSForm.in_252_1.style.display = "none";
for(var i=0; i<actions.length; i++){
	if(actions[i] == action){
		actionIndex = i;
		break;
	}
}
document.getElementById('textfield10').value = actionDesc[actionIndex];
</script>

</HATS:Form>
</body>
</html>
