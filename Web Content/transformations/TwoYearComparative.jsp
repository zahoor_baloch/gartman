<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
<script src="../common/SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="../common/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<div class="screenshowtxt09">
<HATS:Component col="2" alternate=""
					widget="com.ibm.hats.transform.widgets.LabelWidget"
					alternateRenderingSet="" erow="3" textReplacement=""
					widgetSettings="trim:true|style:|labelStyleClass:|"
					type="com.ibm.hats.transform.components.TextComponent" ecol="36"
					componentSettings="" row="3" />
</div>

<div id="TabbedPanels1" class="TabbedPanels">
      <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0" style="margin-right:5px; outline:none;">Graph</li>
        <li class="TabbedPanelsTab" tabindex="0" style="margin-right:5px; outline:none;">Data</li>
      </ul>
      <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent2" align="left">
        
        
        	<HATS:Component col="6" alternate=""
					widget="com.ibm.hats.transform.widgets.VerticalBarGraphWidget"
					alternateRenderingSet="" erow="20"
					textReplacement="January 08=Jan 08,matchCase,matchLTR|February=Feb,matchLTR|March=Mar,matchLTR|April=Apr,matchLTR|June=Jun,matchLTR|July=Jul,matchLTR|August=Aug,matchLTR|September=Sep,matchLTR|October=Oct,matchLTR|November=Nov,matchLTR|December=Dec,matchLTR"
					widgetSettings="width:1030|backgroundColor:#ffffff|dataSource12:13|dataSource11:12|height:430|extractSource:col|dataSource10:11|dataSource9:10|dataSource8:9|extractDataSetLabels:false|dataSource7:8|dataSource6:7|dataSource5:6|dataSource4:5|headerRows:1|dataSetLabelIndex:1|dataSource3:4|dataSource2:3|dataSource1:2|dataSource2Color:#00ff00|dataSetNumber:6|yAxisTitle:|dataSource4Color:#ffff00|dataSource6Color:#ff80ff|dataSource8Color:#00c000|dataSource11Color:#c000c0|textAntialiasing:true|backgroundImage:graph.png|headerColumns:1|axisColor:#000000|dataSource1Color:#0000ff|dataSource3Color:#ff00ff|labelColor:#000000|defaultFont:SansSerif-(default)-12|dataSource5Color:#00ffff|dataSource10Color:#c0c000|dataSource7Color:#c00000|labelIndex:1|dataSource12Color:#00c0c0|dataSource9Color:#808080|alternateText:Tow Year Comparative Sales &amp; Profit Graph|bgroot:C:\Workspaces\Gartman_CVS\Gartman\Web Content\common\images\|extractLabels:true|dataSource2Legend:Sales Cur 12 Mo|dataSource7Legend:Jul|dataSource1Legend:Sales Prev 12 Mo|dataSource4Legend:Profit Prev 12 Mo|dataSource9Legend:Sep|dataSource6Legend:Profit Budget|dataSource3Legend:Sales Budget|dataSource8Legend:Aug|dataSource5Legend:Profit Cur 12 Mo|dataSource10Legend:Oct|dataSource12Legend:Dec|xAxisTitle:|dataSource11Legend:Nov|"
					type="com.ibm.hats.transform.components.TableComponent" ecol="119"
					componentSettings="startCol:6|endRow:20|columnBreaks:18,31,44,52,65,73,86,99,107|endCol:119|excludeCols:4,6,9|visualTableDefaultValues:false|startRow:9|numberOfTitleRows:0|excludeRows:|includeEmptyRows:true|"
					row="9" />
        
        </div>
        <div class="TabbedPanelsContent2">
        
        <div class="maintopmenu09">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
              <td class="bartmaintop58" align="left">Sales</td>
              <td class="bartmaintop58" align="left">Profit</td>
            </tr>
            <tr>
              <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
      <tr>
        <td align="left" class="tabtxthead barhead4new2"> Period</td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="9" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="10" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="11" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="12" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="13" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="14" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="15" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="16" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="17" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="18" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="19" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="20" /></td>
        </tr>
      <tr><td height="2" bgcolor="#0066CC"></td></tr>
      <tr class="oddbar09 totaltxt">
        <td><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="17"
						componentSettings="" row="21" />&nbsp;</td>
      </tr>
      </table>
              </td>
              <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
      <tr>
        <td align="left" class="tabtxthead barhead4new2"> Prev 12 Mo</td>
        <td align="left" class="tabtxthead barhead4new2">Cur 12 Mo</td>
        <td align="left" class="tabtxthead barhead4new2">Var %</td>
        <td align="left" class="tabtxthead barhead4new2">Budget</td>
        <td align="left" class="tabtxthead barhead4new2">Var %</td>
        </tr>
      <tr class="oddbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="9" /></td>
        </tr>
      <tr class="evenbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="10" /></td>
        </tr>
      <tr class="oddbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="11" /></td>
        </tr>
      <tr class="evenbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="12" /></td>
        </tr>
      <tr class="oddbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="13" /></td>
        </tr>
      <tr class="evenbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="14" /></td>
        </tr>
      <tr class="oddbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="15" /></td>
        </tr>
      <tr class="evenbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="16" /></td>
        </tr>
      <tr class="oddbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="17" /></td>
        </tr>
      <tr class="evenbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="18" /></td>
        </tr>
      <tr class="oddbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="19" /></td>
        </tr>
      <tr class="evenbar09">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="20" /></td>
        </tr>
      <tr><td colspan="5" height="2" bgcolor="#0066CC"></td></tr>
      <tr class="oddbar09 totaltxt">
        <td align="right"><HATS:Component col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="30"
						componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="72"
						componentSettings="" row="21" /></td>
      </tr>
      </table>
              </td>
              <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
      <tr>
        <td align="left" class="tabtxthead barhead4new2"> Prev 12 Mo</td>
        <td align="left" class="tabtxthead barhead4new2">Cur 12 Mo</td>
        <td align="left" class="tabtxthead barhead4new2">Var %</td>
        <td align="left" class="tabtxthead barhead4new2">Budget</td>
        <td align="left" class="tabtxthead barhead4new2">Var %</td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="9" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="10" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="11" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="12" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="13" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="14" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="15" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="16" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="17" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="18" /></td>
        </tr>
      <tr class="oddbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="19" /></td>
        </tr>
      <tr class="evenbar09">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="20" /></td>
        </tr>
      <tr><td colspan="5" height="2" bgcolor="#0066CC"></td></tr>
      <tr class="oddbar09 totaltxt">
        <td><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="85"
						componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="87" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="98"
						componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="100" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="106"
						componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="108" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="119"
						componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="121" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="127"
						componentSettings="" row="21" /></td>
      </tr>
      </table>
              </td>
            </tr>
          </table>
        </div>
        
        </div>
      </div>
    </div>


<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
//-->
</script>
</HATS:Form>

</body>
</html>