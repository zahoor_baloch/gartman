<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@ page import="com.ibm.hats.common.*"%>

<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>

</head>
<body> 

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>
<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<script language="JavaScript">
function sendF4Prompt(cursorPos) {
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[pf4]', 'HATSForm');
}

function sendF17Prompt(cursorPos) {
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[pf17]', 'HATSForm');
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="left">
	
<table width="auto" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td><div class="Tabmainselected">Header</div></td>
	<td><div class="Tabmain"><a href="javascript:void();" onclick="javascript:ms('[enter]', 'HATSForm');">Line Items</a></div></td>
	<td><!--<div class="Tabmain"><a href="javascript:ms('macrorun_GoTo_MiscCharges','hatsportletid');">Misc Charges</a></div>-->
		<div class="Tabmain"><a href="javascript:void();" onclick="javascript:ms('[pf10]', 'HATSForm');">Misc Charges</a></div>
	</td>
	</tr>
</table>
<div class="clear"></div>

<div class="TabmainContent">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="txt2" style="border-collapse:collapse;">
	<tr>
		
		<td colspan="8" bgcolor="#c9cfd6" style="padding:1px 15px 1px 15px;" class="fillrank">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
					<span style="float:left;" id="cr">
					Customer Rank:					
					</span>
					
					<span style="float:left; margin-left: 10px">
						<HATS:Component col="29" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="29"
								componentSettings="" row="7" />
					</span>			
					</td>					
				</tr>
			</table>
		
		</td>
		
	</tr>
	<tr>
		<td colspan="8" height="5"></td>
	</tr>
	<tr>
	<td align="left" bgcolor="#FFFFFF" class="fillbar"><strong style="white-space: nowrap;">Sold To:</strong></td>
	<td width="220" class="fillbarnew" style="padding:2px 0px 0px 3px;"><HATS:Component
						col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="3" /></td>
	<td width="10"></td>
	<td width="142" align="left" bgcolor="#FFFFFF" class="fillbar">
	<table width="auto" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td><strong style="white-space: nowrap;">Bill To:</strong></td>
	<td><HATS:Component
						col="29" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="38"
						componentSettings="" row="4" /></td>
	<td><img id="269_10"style="cursor: pointer;"
						title="Please click to send F4 command" border="0"
						src="../common/images/find.png"
						align="absmiddle" onclick="sendF4Prompt('269');"></td>
	</tr>
	</table>
	</td>
					<td width="158" class="fillbarnew" style="padding:3px;"><strong>O/R</strong>:<HATS:Component
						col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="42"
						componentSettings="" row="4" /></td>
	<td width="10"></td>
	<td align="left" bgcolor="#FFFFFF" class="fillbar">
	<table width="auto" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td><strong style="white-space: nowrap;">Ship To:</strong></td>
	<td><HATS:Component col="70"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="3" /></td>
	<td></td>
	</tr>
	</table>
	</td>
	<td width="220" class="fillbarnew" style="padding:3px;"><HATS:Component
						col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="4" /></td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="4" /></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="2" /></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="5" /></td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="5" /></td>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbar"><strong>Store</strong></td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="29" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="33"
						componentSettings="" row="6" /></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="6" /></td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="26"
						componentSettings="" row="6" /></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">&nbsp;</td>
	<td align="center" valign="bottom">&nbsp;</td>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">
	<div style="float:left"><HATS:Component col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="72"
						componentSettings="" row="7" /></div>
	<div style="float:right"><HATS:Component col="74" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.FieldComponent" ecol="75"
						componentSettings="" row="7" /></div>
	</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td align="right" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">
    <div style="text-align:left; float:left"><HATS:Component col="2"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="27"
						componentSettings="" row="7" /></div>
    </td>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbar"><strong>GLN</strong>&nbsp;</td>
	<td align="left" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="35" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="6" /></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field02|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="64"
						componentSettings="" row="8" />
	</td>
	<td>
	<HATS:Component col="66"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="75"
						componentSettings="" row="8" />
	</td>
	<td>
	<img style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('626');"/>
	</td>
	<td>
	<HATS:Component col="77"
						alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="79"
						componentSettings="" row="8" />
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td align="right" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">
	<div style="text-align:left; float:left"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="8" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="27"
						componentSettings="" row="8" /></div></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td align="left" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">
	
	<HATS:Component col="50" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="74"
						componentSettings="" row="9" /></td>
	</tr>
</table>


      
<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="35%" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="9"
								componentSettings="" row="10" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<HATS:Component col="11" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="25" componentSettings="" row="10" />
	<img id="731_15" style="cursor: pointer; " title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('731');"/></td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Contact:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"> <HATS:Component
								col="8" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="22" componentSettings="" row="11" />
    </td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="15"
								componentSettings="" row="12" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"><HATS:Component
								col="17" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="17" componentSettings="" row="12" /> <HATS:Component
								col="19" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="23"
								componentSettings="" row="12" /></td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="11"
								componentSettings="" row="13" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"> <HATS:Component
								col="13" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="15" componentSettings="" row="13" /><img id="973_3" style="cursor: pointer; " title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('973');"><img id="973_3_2" style="cursor: pointer; " title="Please click to send F17 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF17Prompt('973');">
    </td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="12"
								componentSettings="" row="14" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="14" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="23" componentSettings="" row="14" />
								<img id="1054_10" style="cursor: pointer; " title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1054');"/></td>
	</tr>
	<tr bgcolor="#c9cfd6" height="29">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="15" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="12"
								componentSettings="" row="15" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"> <HATS:Component
								col="14" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="15" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="14" componentSettings="" row="15" />
    </td>
	</tr>
	<tr bgcolor="#c9cfd6" height="30">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="9"
								componentSettings="" row="16" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="11" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="16" componentSettings="" row="16" />
								</td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="17" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="7"
								componentSettings="" row="17" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="9" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="17" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="16" componentSettings="" row="17" />
								<img id="1289_8" style="cursor: pointer; " title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1289');"/>
								<HATS:Component
								col="18" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="17" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="25" componentSettings="" row="17" />
	</td>
	</tr>
</table>
	</td>
    <td width="10">&nbsp;</td>
    <td width="35%" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6" height="26">
	<td width="180" align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="29" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="38"
								componentSettings="" row="10" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"> <HATS:Component
								col="40" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="44" componentSettings="" row="10" />
    </td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">	    <HATS:Component
								col="29" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="31"
								componentSettings="" row="11" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;">
	 
	<table width="auto" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td>
    <HATS:Component col="33" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="47" componentSettings="" row="11" />
	</td>
	<td><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="11" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:HATSINPUT|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:COMPONENT|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="49" componentSettings="" row="11" /></td>
	
	</tr>
	</table>
						
    </td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">	    <HATS:Component
								col="29" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="38"
								componentSettings="" row="12" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="40" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:inputfield3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="49" componentSettings="" row="12" />
								<img id="920_10" style="cursor: pointer; " title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('920');"/></td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"> <div id="lb_1005_4">Commission Rate:</div></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="45" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="48" componentSettings="" row="13" /></td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><div id="lb_1079_2">Price code:</div></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"><HATS:Component
								col="39" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="40" componentSettings="" row="14" />
    </td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td class="tabtxthead2 bar" align="right" style="padding: 0px;">
<table width="auto" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="29"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="32" componentSettings="" row="15" /></td>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="34"
										alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="34" componentSettings="" row="15" /></td>
	</tr>
</table>
	</td>
	<td class="tabtxthead2 bar" align="left">
<table width="auto" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="37"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="41" componentSettings="" row="15" /></td>
	<td align="left" bgcolor="#c9cfd6" style="color:#3c6a8c;"><HATS:Component col="43"
										alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="43" componentSettings="" row="15" /> <HATS:Component
										col="46" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="50" componentSettings="" row="15" /></td>
	</tr>
</table>
	</td>
	</tr>
	<tr bgcolor="#c9cfd6" height="31">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><div id="currencyCode">Currency Code:&nbsp;</div></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"><HATS:Component
								col="40" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="42"
								componentSettings="" row="18" />&nbsp;</td>								
	</tr>
	<tr bgcolor="#c9cfd6">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="18"
								componentSettings="" row="18" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="20" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="27" componentSettings="" row="18" />
								<img id="1380_8" style="cursor: pointer; " title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1380');"/></td>
	</tr>
</table>
    </td>
    <td width="10">&nbsp;</td>
    <td width="35%" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6" height="26">
	<td class="tabtxthead2 bar" style="padding: 0px;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td width="100" align="right" bgcolor="#c9cfd6"><HATS:Component
								col="54" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="58"
								componentSettings="" row="10" /></td>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component
								col="60" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="61" componentSettings="" row="10" />
	</td>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="63"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="67" componentSettings="" row="10" />
	</td>
	</tr>
</table>
	</td>
	<td class="tabtxthead2 bar" align="left" style="padding: 0px;">
<table width="auto" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="69"
										alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="70" componentSettings="" row="10" /></td>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="72"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="76" componentSettings="" row="10" />
	</td>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="78"
										alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="79" componentSettings="" row="10" />
	</td>
	</tr>
</table>
	</td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="54" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="57"
								componentSettings="" row="11" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="58" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="80" componentSettings="" row="11" /></td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="54" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="59"
								componentSettings="" row="12" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"><HATS:Component
								col="61" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="65" componentSettings="" row="12" /><img id="941_5" style="cursor: pointer; " title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('941');"><img id="941_5_2" style="cursor: pointer; " title="Please click to send F17 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF17Prompt('941');">
    </td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="54" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="64"
								componentSettings="" row="13" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="66" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="80" componentSettings="" row="13" /></td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="54" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="62"
								componentSettings="" row="14" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="65" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="74" componentSettings="" row="14" /></td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td class="tabtxthead2 bar" align="right" style="padding: 0px;">
<table width="auto" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="54"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="57" componentSettings="" row="15" /></td>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="59"
										alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="59" componentSettings="" row="15" /></td>
	</tr>
</table>
	</td>
	<td class="tabtxthead2 bar" align="left">
<table width="auto" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="62"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="15" /></td>
	<td align="left" bgcolor="#c9cfd6" style="color:#3c6a8c;"><HATS:Component col="68"
										alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="68" componentSettings="" row="15" /> <HATS:Component
										col="71" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="75" componentSettings="" row="15" /></td>
	</tr>
</table>
	</td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td class="tabtxthead2 bar" align="right" style="padding: 0px;">
<table width="auto" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="54"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="16" /></td>
	<td align="left" bgcolor="#c9cfd6"><HATS:Component col="61"
										alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="61" componentSettings="" row="16" /></td>
	</tr>
</table>
	</td>
	<td class="tabtxthead2 bar" align="left" style="padding: 0px;">
<table width="auto" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td colspan="2" align="right" bgcolor="#c9cfd6"><HATS:Component
								col="64" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="69"
								componentSettings="" row="16" /></td>
<td align="left" bgcolor="#c9cfd6">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component col="71" alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="80" componentSettings="" row="16" />
	</td>
	<td>
	<img id="1271_10" style="cursor: pointer; " title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1271');"/>
	</td>
	</tr>
	</table>
	</td>
	</tr>
</table>
	</td>
	</tr>
	<tr bgcolor="#c9cfd6" height="26">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="54" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="66"
								componentSettings="" row="18" />&nbsp;</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="color:#3c6a8c;"><HATS:Component
								col="68" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="79"
								componentSettings="" row="18" />&nbsp;</td>
	</tr>
</table>
    </td>
  </tr>  
</table>
<div class="clear5px"></div>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" >
		<table border="0" cellpadding="0" cellspacing="0">
		<tr height="20">
		<td width="420" class="fillbarnew" style="padding:2px 0px 0px 3px;" align="left"><HATS:Component
								col="12" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="70"
								componentSettings="" row="19" />							
							</td>
		</tr>
		<tr height="20">
		<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;" align="left"><HATS:Component
								col="12" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="20" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="70"
								componentSettings="" row="20" /></td>
		</tr>
		<tr height="20">
	<td align="left" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
								col="12" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="21" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="70"
								componentSettings="" row="21" /></td>
		</tr>
		</table>
		
		</td>
	</tr>
	</table>

			
	<div id="errorMsg" align="center" class="errorMsgNew">					
															
        <%String errorMsg = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("errorOnHeaderTab", true).getString(0).trim();%>

		<%if (errorMsg.length()>0 && !errorMsg.equalsIgnoreCase("")) {
			System.out.println("Error Message:............."+errorMsg);			
			out.print(errorMsg);%>						
		<%}%>		
		<input type="hidden" id="h1" value=<%="\""+errorMsg+"\""%>>
		<script type="text/javascript">
			var errorMsg = document.getElementById("h1").value;
			if(errorMsg == ""){				
				document.getElementById("errorMsg").style.visibility="hidden";
			}			
		</script>
	</div>

			</div>
	</td>
	</tr>
</table>
  
<script type="text/javascript">
try{
//this
if(typeof document.HATSForm.in_731_15 == 'undefined') {
	 	document.getElementById("731_15").style.display = "none";	 
}
if(typeof document.HATSForm.in_833_15 == 'undefined') {
	 //	document.getElementById("833_15").style.display = "none";	 
}
//this
if(typeof document.HATSForm.in_920_10 == 'undefined') {
	 	document.getElementById("920_10").style.display = "none";	 
}
//this
if(typeof document.HATSForm.in_941_5 == 'undefined') {
	 	document.getElementById("941_5").style.display = "none";
	 	document.getElementById("941_5_2").style.display = "none";
	 	
}
//this
if(typeof document.HATSForm.in_973_3 == 'undefined') {
	 	document.getElementById("973_3").style.display = "none";	
	 	document.getElementById("973_3_2").style.display = "none"; 
}
//this
if(typeof document.HATSForm.in_1054_10 == 'undefined') {
	 	document.getElementById("1054_10").style.display = "none";	 
}
if(typeof document.HATSForm.in_1079_2 == 'undefined') {
	 	document.getElementById("currencyCode").style.display = "none";	 
	 	document.getElementById("lb_1079_2").style.display = "none";
	 	//document.getElementById("currencyCode").style.display = "none";
}
if(typeof document.HATSForm.in_1134_1 == 'undefined') {
	 	//document.getElementById("1134_1").style.display = "none";	 
}

if(typeof document.HATSForm.in_1211_6 == 'undefined') {
	 	//document.getElementById("1211_6").style.display = "none";	 
}
//this	 
if(typeof document.HATSForm.in_1289_8 == 'undefined') {

	 	document.getElementById("1289_8").style.display = "none";
	 		 
}
//this
if(typeof document.HATSForm.in_1380_8 == 'undefined') {
	 	document.getElementById("1380_8").style.display = "none";	 
}
//this
if(typeof document.HATSForm.in_1271_10 == 'undefined') {
	 	document.getElementById("1271_10").style.display = "none";	 
}

if(typeof document.HATSForm.in_269_10 == 'undefined') {
	 	document.getElementById("269_10").style.display = "none";	 
}

if(typeof document.HATSForm.in_1005_4 == 'undefined') {
	 	document.getElementById("lb_1005_4").style.display = "none";	 
}
}catch(err){

}
	 


//if(typeof document.HATSForm.in_1154_1 == 'undefined') {
		 /*var cell = document.getElementById("cell");

		if ( cell.hasChildNodes() )
		{
			alert(cell.childNodes.length);
   			 while ( cell.childNodes.length >= 1 )
   			 {
        		cell.removeChild( cell.firstChild );       
    		} 
		}*/		  
//}


</script>

</HATS:Form>
</body>
</html>