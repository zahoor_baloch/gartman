
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<script>
function setValuesAndGoTo2(val1, val2) {
	document.forms["HATSForm"].hatsgv_promptGV1.value = val1;
	document.forms["HATSForm"].hatsgv_promptGV2.value = val2;
	document.forms["HATSForm"].hatsgv_macroName.value = "setValuesAndGoTo2("+val1+","+val2+")";
	ms('macrorun_MasterMenuLevel02Macro', 'hatsportletid');
}

function changeIsOrdered(){

	document.forms["HATSForm"].hatsgv_isOrdered.value = "";
	ms('[enter]', 'HATSForm');
	
}
</script>

<HATS:Form>
<% String macroName = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("macroName", true).getString(0).trim();
	
	System.out.println( "macroName" + macroName);
	
%>
<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isOrdered">
<input type="hidden" name="hatsgv_isPin" value=""></input>


<div style="display: block;">
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
<tr height="400">
<td valign="middle" align="center">
<div class="mainpopt">
<table>
<tr>
<td class="toptxt">
WARNING:You have chosen to kill this order, Are you sure You want to Kill?
</td>
</tr>
<tr>
<td valign="bottom" align="center">
<table width="auto" height="100%" cellspacing="0" cellpadding="0">
<tr>
<td>
<a class="funbtn" href="javascript:<%= macroName%>" target="_parent">Yes</a>
<a class="funbtn" href="javascript:changeIsOrdered()">No</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</div>






</HATS:Form>


</body>
</html>
