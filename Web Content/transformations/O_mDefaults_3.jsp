<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<div class="clear17px"></div>
<table cellpadding="0" cellspacing="0" width="100%" border="0" >
  <tr>
  	<td align="center" valign="top">
    	<table cellpadding="0" cellspacing="1" width="60%" border="0" bgcolor="#536F97">
              <tr>
                  <td align="left" valign="middle" width="80%" class="omText" >
                      Do you wish to have the system automatically refresh item master info (desc, etc.) when creating a credit memo from sales histroy on the duplicate portion of duplicate/reverse?
                  </td>
                  <td align="left" valign="middle" width="20%" class="omText">
                      <HATS:Component col="58" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="58"
						componentSettings="" row="6" />
                  </td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Do you wish to use the multi-line entry when keying line items?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <HATS:Component col="33" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="33"
						componentSettings="" row="9" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      For single line entry, do you wish to use the abreviated version?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <HATS:Component col="37" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="37"
						componentSettings="" row="11" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      For single line entry, do you wish to use the abreviated counter sales version?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <HATS:Component col="52" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="52"
						componentSettings="" row="13" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Display bill codes on each new order?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <HATS:Component col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="54"
						componentSettings="" row="14" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Display A/R information on each new order?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <HATS:Component col="59" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="59"
						componentSettings="" row="15" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
					Clear customer number after each new order?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="60"
						componentSettings="" row="16" /></td>
              </tr>
        </table>
    </td>
  </tr>
  
</table>


</HATS:Form>
</body>
</html>