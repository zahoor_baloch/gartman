<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%>

<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>


<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<div class="bartmaintop">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="comtxt">
	<tr>
	<td width="100"><HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="11"
				componentSettings="" row="2" /></td>
	<td align="center" class="comtxt2">&nbsp;</td>
	<td width="100"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="77"
				componentSettings="" row="2" /></td>
	</tr>
</table>
</div>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<br><br>

<div class="maintopmenudd">

<TABLE width="100%" cellspacing="0" cellpadding="0" border="0">
<TBODY>
<TR>
<TD align="center">
<div class="menubtn"><HATS:Component col="4" alternate=""
					widget="com.ibm.hats.transform.widgets.LinkWidget"
					alternateRenderingSet="" erow="15" textReplacement=""
					widgetSettings="style:|captionType:DESCRIPTION|separator:<br>|layout:TABLE|trimCaptions:true|target:|columnsPerRow:2|emenubarStyleClass:HATSEMENUBAR|linkStyleClass:menubtn|"
					type="com.ibm.hats.transform.components.SelectionListComponent"
					ecol="74" componentSettings="" row="3" /></div>
</TD>
</TR>
<tr>
		<td width="100%"> <div class="clear5px"></div></td>
</tr>

</TBODY>
</TABLE>

</div>
<br/>
<div class="belowbtn">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="middle">
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td></td>
          </tr>
        </table></td>
    </tr>
    <tr>
    	<td align="center" valign="middle" >
    	<table width="auto" border="0" cellspacing="0" cellpadding="0">
    	<tr>
    		<td class="errorMsg2">
    		<%String errorMsg = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("errorMessage", true).getString(0);%>
			<%if (errorMsg.length()>0) {
			
			out.print(errorMsg);
			}%>
    		</td>
    	</tr>
    	</table>
    	</td>
    </tr>
  </table>
</div>

<input type="text" name="in_1472_2" style="display:none;"></input>
	
</HATS:Form>

</body>
</html>