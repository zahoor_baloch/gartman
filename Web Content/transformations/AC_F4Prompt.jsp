<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>


<div class="bartmaintop">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="comtxt">
	<tr>
	<td width="100"><HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="11"
				componentSettings="" row="2" /></td>
	<td align="center" class="comtxt2">AC Prompt&nbsp;</td>
	<td width="100" align="right"><HATS:Component col="71" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="78"
				componentSettings="" row="2" />&nbsp;</td>
	</tr>
</table>
</div>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<br><br>

<div class="popupmain57">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center" valign="middle">
<table width="100%" height="150" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td background="../common/images/popup_top_left.png" width="16">&nbsp;</td>
	<td height="16" background="../common/images/popup_top_middle.png">&nbsp;</td>
	<td background="../common/images/popup_top_right.png" width="16">&nbsp;</td>
	</tr>
	<tr>
	<td background="../common/images/popup_left_middle.png">&nbsp;</td>
	<td bgcolor="#FFFFFF" align="center">
	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tbody>
	<tr bgcolor="#c9cfd6">
	<td width="150" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="24" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="42"
				componentSettings="" row="5" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="44" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="73"
				componentSettings="" row="5" /></td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td bgcolor="#ffffff" class="bar" colspan="2">&nbsp;</td></tr>
	<tr bgcolor="#c9cfd6">
	<td colspan="2" bgcolor="#ffffff" >

<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<td align="left" width="307" class="tabtxthead barhead4new2">Action Codes</td>
	<td align="center" width="80" class="tabtxthead barhead4new2">My List?</td>
	</tr>
	<tr class="oddbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="8" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="8" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="9" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="9" />&nbsp;</td>
	</tr>
	<tr class="oddbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="10" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="10" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="11" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="11" />&nbsp;</td>
	</tr>
	<tr class="oddbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="12" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="12" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="13" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="13" />&nbsp;</td>
	</tr>
	<tr class="oddbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="14" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="14" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="15" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="15" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="15" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="15" />&nbsp;</td>
	</tr>
	<tr class="oddbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="16" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="16" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="17" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="17" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="17" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="17" />&nbsp;</td>
	</tr>
	<tr class="oddbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="18" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="18" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="19" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="19" />&nbsp;</td>
	</tr>
	<tr class="oddbar10">
	<td><HATS:Component col="24" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="20" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="68"
								componentSettings="" row="20" />&nbsp;</td>
	<td align="center"><HATS:Component col="73" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="20" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:HATSCHECKBOX|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="73" componentSettings="" row="20" />&nbsp;</td>
	</tr>
	</tbody>
</table>
	</td>
	</tr>
	</tbody>
</table>
	</td>
	<td background="../common/images/popup_right_middle.png">&nbsp;</td>
	</tr>
	<tr>
	<td background="../common/images/popup_below_left.png" width="16">&nbsp;</td>
	<td height="16" background="../common/images/popup_below_middle.png">&nbsp;</td>
	<td background="../common/images/popup_below_right.png" width="16">&nbsp;</td>
	</tr>
</table>
	</td>
	</tr>
</table>
</div>







</HATS:Form>

</body>
</html>