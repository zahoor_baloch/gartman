<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>
<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_gvSid">
<input type="hidden" name="hatsgv_gvPosition">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<script language="JavaScript">
screenName = "CompanyAnalysis_Combo";
</script>

	<div class="clear5px"></div>

	<div class="bartmaintop">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="comtxt">
      <tr>
        <td width="100"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="2" /></td>
        <td align="center" class="comtxt2"><HATS:Component col="24"
				alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="57"
				componentSettings="" row="2" /></td>
        <td width="100"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="2" /></td>
      </tr>
    </table>
    </div>
	
    <!--line-->
    <table width="100%">
    <tr><td height="1"><div class="line"></div></td></tr>
    </table>
    <!--line-->

	<table width="100%" height="15" border="0" cellspacing="0" cellpadding="0" class="topbarrr2" style="border-collapse:collapse;">
		<tr>
		<td width="5%" align="right" class="topbarrtxt">As of: &nbsp;</td>
		<td width="11%" align="center" bgcolor="#FFFFFF"><HATS:Component
				col="9" alternate=""
				widget="com.ibm.hats.transform.widgets.CalendarWidget"
				alternateRenderingSet="" erow="3" textReplacement=""
				widgetSettings="tableCellStyleClass:|trimSpacesOnInputs:false|stripUnderlinesOnInputs:false|tableRowStyleClass:|tableStyleClass:|columnsPerRow:1|tableCaptionCellStyleClass:|labelStyleClass:asdf|restrictMinDate:false|useInlineCalendar:true|inputFieldStyleClass:fld2|blinkStyle:font-style: italic|restrictMaxDate:false|captionSource:VALUE|launcherImage:calendar.gif|mapExtendedAttributes:false|patternLocaleSelected:|defaultValue:|rangeEnd:|launcherCaption:Date...|underlineStyle:text-decoration: underline|preserveColors:false|readOnly:false|columnSeparatorStyle:border-width: 1px; border-style: solid|style:|pattern:MM-dd-yyyy|patternLocale:SERVER|caption:|eliminateMaxlengthInIdeographicFields:false|launcherButtonStyleClass:HATSBUTTON|launcherType:IMAGE|rangeStart:|launcherLinkStyleClass:HATSLINK|reverseVideoStyle:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="18"
				componentSettings="" row="3" /></td>
		<td width="25%" class="topbarrtxt">&nbsp;</td>
		<td class="topbarrtxt"><HATS:Component col="25" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="3" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="57"
				componentSettings="" row="3" /></td>
		<td width="8%" align="right" class="topbarrtxt">Filter : &nbsp;</td>
		<td width="5%" align="center" bgcolor="#FFFFFF"><HATS:Component
				col="69" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="3" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="71"
				componentSettings="" row="3" />
		</td>
		<td width="5%" align="center" bgcolor="#FFFFFF"><HATS:Component
				col="73" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="3" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="75"
				componentSettings="" row="3" />
		</td>
		<td width="5%" align="center" bgcolor="#FFFFFF"><HATS:Component
				col="77" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="3" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="79"
				componentSettings="" row="3" />
		</td>
		<td width="5%" align="center" bgcolor="#FFFFFF" style="border-bottom:1px solid #666;">&nbsp;</td>
		</tr>
		
		<tr>
		  <td align="right" class="topbarrtxt">&nbsp;</td>
		  <td align="center">&nbsp;</td>
		  <td class="topbarrtxt">&nbsp;</td>
		  <td>&nbsp;</td>
		  <td width="10%" align="right" class="topbarrtxt">Enable filter ? &nbsp;</td>
			<td align="right" style="border-left: 1px solid #000;"
				class="topbarrtxt">Loc:&nbsp;</td>
			<td align="center" bgcolor="#FFFFFF">
          <div style="padding:0 6px 0 0;">
          <HATS:Component col="74" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="74"
				componentSettings="" row="4" /></div>
          </td>
			<td align="right" class="topbarrtxt">Div:&nbsp;</td>
			<td align="center" bgcolor="#FFFFFF"><HATS:Component col="80"
				alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="80"
				componentSettings="" row="4" /></td>
		  </tr>
	</table>
	
    
	<!--line-->
    <table width="100%">
    <tr><td height="1"><div class="line"></div></td></tr>
    </table>
    <!--line-->
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
    	<tr>
		<td width="200" align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="19"
				componentSettings="" row="5" /></td>
		<td width="150" align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="21" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="33"
				componentSettings="" row="5" /></td>
		<td width="150" align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="35" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="5" /></td>
		<td width="150" align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="52"
				componentSettings="" row="5" /></td>
		<td width="150" align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="54" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="66"
				componentSettings="" row="5" /></td>
		<td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="68" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="80"
				componentSettings="" row="5" /></td>
		</tr>
    </table>
<div class="companyinnerboxmain">
	<HATS:Component type="com.ibm.hats.transform.components.TableComponent" widget="gartman.widgets.TableCACombo" row="6" col="2" erow="19" ecol="80" componentSettings="startCol:2|endRow:19|columnBreaks:20,34,48,53,67|endCol:80|excludeCols:|visualTableDefaultValues:false|startRow:6|numberOfTitleRows:0|excludeRows:|includeEmptyRows:true|" widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|collapseAreaClass:|readOnly:false|linkType:link|expandRowClass:|normalColumnLayout:*|trimSpacesOnInputs:false|tableEvenRowStyleClass:HATSTABLEEVENROW|headerRows:|collapseRepresentation:link|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|buttonStyle:HATSBUTTON|expandClass:|tableStyleClass:HATSTABLE|border:0|reverseVideoStyle:|trimHeaders:true|underlineStyle:text-decoration: underline|linkStyle:HATSLINK|collapseClass:|collapseAltValue:|tableOddRowStyleClass:HATSTABLEODDROW|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|tableHeaderRowStyleClass:HATSTABLEHEADER|expandHeaderValue:|expandAreaStyle:|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|keepExpansionOnServer:false|columnSeparatorStyle:border-width: 1px; border-style: solid|expandHeaderStyle:|filenamePrefix:spreadsheet|preserveColors:false|provideSpreadsheetFile:false|collapseAreaStyle:|headerColumns:|expandRowStyle:|expandRepresentation:link|blinkStyle:font-style: italic|cursorModeCEPAltValue:|expandAltValue:|stripUnderlinesOnInputs:false|expandValue:+|tableCellStyleClass:HATSTABLECELL|extendedColumnLayout:|expandStyle:|expandAreaClass:|style:|collapseValue:-|dataModeCEPAltValue:|expandHeaderClass:|linkCaption:Download|useCursorExactPositioningOption:false|collapseStyle:|fileExtname:csv|" componentIdentifier="CompanyAnalysis_cpmbo" applyTextReplacement="true"/>
</div>
	
</HATS:Form>

</body>
</html>