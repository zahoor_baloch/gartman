<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript">
function setCustomer(elem){
	//alert(document.getElementsByName("in_242_10")[0].value);
	 document.getElementsByName("in_242_10")[0].value = elem.id;
	 document.getElementsByName("in_242_10")[0].focus();
	//alert(elem.id);
	
}
function sendF4Prompt(cursorPos) {
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[pf4]', 'HATSForm');
}
</script>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<div class="bartmaintop">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="comtxt">
	<tr>
	<td width="100"><HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="11"
				componentSettings="" row="2" /></td>
	<td align="center" class="comtxt2">&nbsp;</td>
	<td width="100"><HATS:Component col="72" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="2" /></td>
	</tr>
</table>
</div>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<div class="maintopmenudd">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Customer#:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="11"
				componentSettings="" row="4" />
				<img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('242');"/>
	    </td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Search:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="14" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="43"
				componentSettings="" row="4" /></td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Phone#:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="46" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="57"
				componentSettings="" row="4" /></td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Order Type:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="64" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="65"
				componentSettings="" row="4" />
				<img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('304');"/></td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Location:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	    <HATS:Component col="73" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="75"
				componentSettings="" row="4" /><HATS:Component col="77" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="78"
				componentSettings="" row="4" />
				<img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('313');"/></td>
	  </tr>
	<tr bgcolor="#c9cfd6">
	  <td colspan="10" bgcolor="#ffffff" class="bar"></td>
	  </tr>
	<tr bgcolor="#c9cfd6">
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Quick Order:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="7" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="7" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="7"
				componentSettings="" row="7" /></td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Contact:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="14" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="6" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="28"
				componentSettings="" row="6" />
				<img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('414');"/></td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Taken by:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="31" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="6" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="45"
				componentSettings="" row="6" />
				<img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('431');"/></td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Credit memo?</td>
	  <td bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="53" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="6" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="53"
				componentSettings="" row="6" /></td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Source:</td>
	  <td bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="63" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="6" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="65"
				componentSettings="" row="6" />
				<img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('463');"/></td>
	  </tr>
	<tr bgcolor="#c9cfd6">
	  <td colspan="10" bgcolor="#ffffff" class="bar"></td>
	  </tr>
	<tr bgcolor="#c9cfd6">
	  <td colspan="3" align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">City/State/Zip/Postal (additional search):</td>
	  <td colspan="5" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="14" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="36"
				componentSettings="" row="8" /><HATS:Component col="38" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="39"
				componentSettings="" row="8" /><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="45"
				componentSettings="" row="8" /><HATS:Component col="47" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="61"
				componentSettings="" row="8" /></td>
	  <td bgcolor="#c9cfd6" class="tabtxthead2 bar">&nbsp;</td>
	  <td bgcolor="#c9cfd6" class="tabtxthead2 bar">&nbsp;</td>
	  </tr>
</table>

<div class="clear10px"></div>

<table width="auto" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	<td colspan="2" width="400" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="20" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="10" /></td>
	<td width="100" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">&nbsp;</td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td colspan="3" bgcolor="#ffffff" class="bar"></td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="17" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="36"
				componentSettings="" row="11" />&nbsp;</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="38" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="51"
				componentSettings="" row="11" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="54" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="63"
				componentSettings="" row="11" /></td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td colspan="3" bgcolor="#ffffff" class="bar"></td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td colspan="3" align="center" bgcolor="#c9cfd6" class="errorMsg bar"><HATS:Component
				col="6" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="75"
				componentSettings="" row="12" />&nbsp;</td>
	</tr>
</table>

<div class="clear10px"></div>

<table width="auto" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr>
	<td colspan="3" align="center" bgcolor="#1f599a" class="tabtxthead bar">Default Locations</td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td width="100" align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Sales:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<HATS:Component col="36" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="38"
				componentSettings="" row="15" /></td>
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="42" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="43"
				componentSettings="" row="15" /></td>
	</tr>
	<tr bgcolor="#c9cfd6">
	  <td colspan="3" bgcolor="#ffffff" class="bar"></td>
	  </tr>
	<tr bgcolor="#c9cfd6">
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Inventory:</td>
	  <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="36" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="38"
				componentSettings="" row="16" /></td>
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="42" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="43"
				componentSettings="" row="16" /></td>
	  </tr>
	<tr bgcolor="#c9cfd6">
	  <td colspan="3" bgcolor="#ffffff" class="bar"></td>
	  </tr>
	<tr bgcolor="#c9cfd6">
	  <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Delivery:</td>
      <td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="36" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="38"
				componentSettings="" row="17" /></td>
      <td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
				col="42" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="43"
				componentSettings="" row="17" /></td>
	  </tr>
</table>

</div>

<div class="clear50px"></div>

<div style="float: left;">
<table cellspacing="0" cellpadding="0" style="border-collapse:collapse;" align="left">
	<tr>
	<td align="left" valign="middle" bgcolor="#1f599a" class="tabtxthead bar" style="padding: 5px 10px;">
	<strong>Customers:&nbsp;</strong>
	</td>
	<%//String userNames = "L A R ONNE LOTT314SHE LARONNETST L O 567890 LARONNETST LOTT314SHE LARONNETST";
	String userNames = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("userNames", true).getString(0).trim();
			if(!(userNames.equals(""))){
				String[] userNamesArray = userNames.split(" ");
				String longName = "";
				for(int userNamesCounter=0; userNamesCounter<userNamesArray.length; userNamesCounter++){
				
					if(userNamesArray[userNamesCounter].length()==10) {
	%>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="padding: 5px 10px;">
	<a class="oEntry" href="#" id="<%=userNamesArray[userNamesCounter]%>" onclick="setCustomer(this);return false"><%=userNamesArray[userNamesCounter]%></a>
	</td>
	<%		
					}else{
						longName = longName + userNamesArray[userNamesCounter];
						if(longName.length()==10){
	%>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar" style="padding: 5px 10px;">
	<a class="oEntry" href="#" id="<%=longName%>" onclick="setCustomer(this);return false"><%=longName%></a>
	</td>
	<%
							longName="";		
						}else{
							longName = longName + " ";
						}
						//longName = longName = " ";
					}
				}
			}				
	%>		
	</tr>
</table>
</div>

<div class="clear5px"></div>

<div class="bartab2"><HATS:Component col="1" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="20" textReplacement=""
		widgetSettings="trim:true|style:|labelStyleClass:|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="80"
		componentSettings="" row="20" /></div>
<div class="bartab1"><HATS:Component col="2" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="24" textReplacement=""
		widgetSettings="trim:true|style:|labelStyleClass:|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="79"
		componentSettings="" row="24" /></div>

<div class="clear5px"></div>
<%
	String orderNotesMessage = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("orderNotesMessage", true).getString(0).trim();
	if(!(orderNotesMessage.equals("")) && orderNotesMessage.equalsIgnoreCase("Order Notes  F6 to View") ){		
%>
		<div class="txtmsg"> Order Notes  F6 to View </div>
<%
	}
%>
	

<div>
</div>
</HATS:Form>

</body>
</html>