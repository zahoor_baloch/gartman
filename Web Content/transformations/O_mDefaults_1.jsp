<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" bgcolor="#cccccc">
  <tr>
      <td align="center" width="25%" valign="middle" class="lineTitle">Customer</td>
      <td align="center" width="25%" valign="middle" class="lineTitle">Header</td>
      <td align="center" width="25%" valign="middle" class="lineTitle">Line Item</td>
      <td align="center" width="25%" valign="middle" class="lineTitle">Misc. Charge</td>
  </tr>
  <tr class="rowOdd">
      <td id="td1" align="right" width="25%" valign="middle">
          Search name&nbsp;:<!-- <input type="text" name="text" size="16" class="login_field2" /> -->
          <HATS:Component col="24" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:true|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:1|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:1|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:true|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="24"
				componentSettings="" row="4" />
      </td>
      <td align="right" width="25%" valign="middle">
          Contact name&nbsp;:<HATS:Component col="40" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="40"
				componentSettings="" row="4" />
      </td>
      <td align="right" width="25%" valign="middle">
          Action&nbsp;:<HATS:Component col="56" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="56"
				componentSettings="" row="4" />
      </td>
      <td align="right" width="25%" valign="middle">
          Print price&nbsp;:<HATS:Component col="72" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="4" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="72"
				componentSettings="" row="4" />
      </td>
  </tr>
  <tr class="rowEven">
      <td align="right" width="25%" valign="middle">
          Phone number&nbsp;:<HATS:Component col="24" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="24"
				componentSettings="" row="5" />
      </td>
      <td align="right" width="25%" valign="middle">
          Tape/Taken by&nbsp;:<HATS:Component col="40" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="40"
				componentSettings="" row="5" />
      </td>
      <td align="right" width="25%" valign="middle">
          Sequence number&nbsp;:<HATS:Component col="56" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="56"
				componentSettings="" row="5" />
      </td>
      <td align="right" width="25%" valign="middle">
          Print&nbsp;:<HATS:Component col="72" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="5" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="72"
				componentSettings="" row="5" />
      </td>
  </tr>
  
  <tr class="rowOdd">
      <td align="right" width="25%" valign="middle">&nbsp;
          
      </td>
      <td align="right" width="25%" valign="middle">
          Via&nbsp;:<HATS:Component col="40" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="6" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="40"
				componentSettings="" row="6" />
      </td>
      <td align="right" width="25%" valign="middle">
          Item number&nbsp;:<HATS:Component col="56" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="6" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="56"
				componentSettings="" row="6" />
      </td>
      <td align="right" width="25%" valign="middle">
          Via&nbsp;:<HATS:Component col="72" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="6" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="72"
				componentSettings="" row="6" />
      </td>
  </tr>
  <tr class="rowEven">
  <!-- login_field2 -->
  		<td align="right" width="25%" valign="middle">&nbsp;
          
      </td>
      <td align="right" width="25%" valign="middle">
          P/O number&nbsp;:<HATS:Component col="40" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="7" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="40"
				componentSettings="" row="7" />
      </td>
      <td align="right" width="25%" valign="middle">&nbsp;
          
      </td>
      <td align="right" width="25%" valign="middle">
          P/O number&nbsp;:<HATS:Component col="72" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="7" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="72"
				componentSettings="" row="7" />
      </td>
  </tr>
  
  <tr class="rowOdd">
  	<!-- login_field2 -->
	<td align="right" width="25%" valign="middle">&nbsp;
          
      </td>
      <td align="right" width="25%" valign="middle">
          Ship to&nbsp;:<HATS:Component col="40" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="40"
				componentSettings="" row="8" />
      </td>
      <td align="right" width="25%" valign="middle">&nbsp;
          
      </td>
      <td align="right" width="25%" valign="middle">
          Side mark&nbsp;:<HATS:Component col="72" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field6|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:login_field2|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="72"
				componentSettings="" row="8" />
      </td>
  </tr>
</table>

<div class="clear17px"></div>

<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
	<td align="center" valign="top">
<table cellpadding="0" cellspacing="1" width="60%" border="0" bgcolor="#536F97">
	<tr>
	<td align="center" colspan="2" valign="middle" class="tabtxthead barhead4new2">
	<strong>Place a Y for the desired cursor position above:</strong>
	</td>
	</tr>
	<tr>
 	<td align="left" valign="middle" class="omText" width="70%">
	Limit fields shown in order entry?
	</td>
	<td align="left" valign="middle" class="omText" width="30%">
	    
	<HATS:Component col="59" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="59"
						componentSettings="" row="10" /></td>
	</tr>
              
	<tr>
	<td align="left" valign="middle" class="omText">
	Have P/O required message show on header screen?
	</td>
	<td align="left" valign="middle" class="omText">
	
	<HATS:Component col="65" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="65"
						componentSettings="" row="11" /></td>
	</tr>
              
	<tr>
                  <td align="left" valign="middle" class="omText">
                      Show best buy for verification?
                  </td>
                  <td align="left" valign="middle" class="omText">
                     
                  <HATS:Component col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="56"
						componentSettings="" row="12" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Display Complementary items at:
                  </td>
                  <td align="left" valign="middle" class="omText">
		                  <table cellpadding="0" cellspacing="0">
		                  <tr>
		                  <td>
			                  <HATS:Component col="49" alternate=""
									widget="com.ibm.hats.transform.widgets.DropdownWidget"
									alternateRenderingSet="" erow="13" textReplacement=""
									widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:L=L;M=M;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
									type="com.ibm.hats.transform.components.InputComponent" ecol="49"
									componentSettings="" row="13" />
		               </td>
		               <td>
		              		<span class="coptions">(L)ine/(M)ischg</span>
		               </td>
		               </tr>
		               </table>
               </td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Auto select complementary items?
                  </td>
                  <td align="left" valign="middle" class="omText">
                      
                  <HATS:Component col="58" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="58"
						componentSettings="" row="14" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Price line items at?
                  </td>
                  <td align="left" valign="middle" class="omText">
	                  <table cellpadding="0" cellspacing="0">
	                  <tr>
	                  <td>
	                      <HATS:Component col="37" alternate=""
							widget="com.ibm.hats.transform.widgets.DropdownWidget"
							alternateRenderingSet="" erow="15" textReplacement=""
							widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:L=L;M=M;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
							type="com.ibm.hats.transform.components.InputComponent" ecol="37"
							componentSettings="" row="15" />
	                  </td>
	                  <td>
	              		 <span class="coptions">(L)ine/(M)ischg</span>
	              	 </td>
	              	</tr>
	              	</table>
              </td>
              </tr>
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Show cost in place of customer holds info?
                  </td>
                  <td align="left" valign="middle" class="omText">
                      
                  <HATS:Component col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="61"
						componentSettings="" row="16" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Show full (F) screen automatically?
                  </td>
                  <td align="left" valign="middle" class="omText">
                      
                  <HATS:Component col="58" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="58"
						componentSettings="" row="17" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Should system back order material if not available?
                  </td>
                  <td align="left" valign="middle" class="omText">
                      
                  <HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="64"
						componentSettings="" row="18" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Show routing screen on all new orders?
                  </td>
                  <td align="left" valign="middle" class="omText">
                      
                  <HATS:Component col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="61"
						componentSettings="" row="19" /></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Use customer location for complementary items?
                  </td>
                  <td align="left" valign="middle" class="omText">
                      
                  <HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="64"
						componentSettings="" row="20" /></td>
              </tr>
              
	<tr>
	<td align="left" valign="middle" class="omText">
	Use default location codes from previous order?
	</td>
	<td align="left" valign="middle" class="omText">
	<HATS:Component col="62" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="62"
						componentSettings="" row="21" /> 
	<span class="coptions"></span></td>
	</tr>
</table>
    </td>
	</tr>
  
</table>
<div class="clear20px"></div>
 <div class="bartab1"><HATS:Component col="2" alternate=""
		widget="com.ibm.hats.transform.widgets.LabelWidget"
		alternateRenderingSet="" erow="24" textReplacement=""
		widgetSettings="trim:true|style:|labelStyleClass:|"
		type="com.ibm.hats.transform.components.TextComponent" ecol="41"
		componentSettings="" row="24" /></div>
<div class="clear5px"></div>


</HATS:Form>

</body>
</html>
