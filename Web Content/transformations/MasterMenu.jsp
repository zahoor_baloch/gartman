<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>

<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>



<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<div style="padding-left: 0px;" class="contetntimg"><img src="../common/images/gartman_default/content-main-img.jpg" width="690" height="421" /></div>
<br/>
<div class="belowbtn">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="middle">
        <table width="auto" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td></td>
          </tr>
        </table></td>
    </tr>
    <tr>
    	<td align="center" valign="middle" >
    	<table width="auto" border="0" cellspacing="0" cellpadding="0">
    	<tr>
    		<td class="errorMsg">
    		<%String errorMsg = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("errorMessage", true).getString(0);%>
			<%if (errorMsg.length()>0) {
			
			out.print(errorMsg);
			}%>
    		</td>
    	</tr>
    	</table>
    	</td>
    </tr>
  </table>
</div>

<% 
	String initialMenu = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("InitialMenu", true).getString(0);
	//initialMenu = "MASTER"; 
	if(initialMenu.equalsIgnoreCase("MASTER") && session.getAttribute("abc") == null) {
	
	session.setAttribute("html", "<iframe id='menuFrame' name='menuFrame' src='Master.jsp' width='185' scrolling='no' frameborder='0' height='521'></iframe>");
%>
<script language="JavaScript">
top.parent.document.getElementById('menuFrameDiv').innerHTML = "<iframe id='menuFrame' name='menuFrame' src='Master.jsp' width='185' scrolling='no' frameborder='0' height='521'></iframe>";
top.parent.document.getElementById('menuFrameDiv').style.display = "block";
</script>
<%}
session.setAttribute("abc", "xyz");
%>

</HATS:Form>
</body>
</html>
