<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>


<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

    <div class="clear5px"></div>
    
    <div class="bartmaintop">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="comtxt">
      <tr>
        <td width="100"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="2" /></td>
        <td align="center" class="comtxt2"><HATS:Component col="37"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="43"
						componentSettings="" row="2" /></td>
        <td width="100"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="77"
						componentSettings="" row="2" /></td>
      </tr>
    </table>
    </div>
	
    <!--line-->
    <table width="100%">
    <tr><td height="1"><div class="line"></div></td></tr>
    </table>
    <!--line-->
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
      <tr>
        <td width="150" align="left" class="tabtxthead barhead4new2"> Opt</td>
        <td width="75" align="center" class="tabtxthead barhead4new2">Company</td>
        <td align="left" class="tabtxthead barhead4new2">Name</td>
      </tr>
      <tr class="oddbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|showSubmitButton:false|optionStyleClass:|dropdownStyleClass:comfild|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="9" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="9" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="9" />&nbsp;</td>
      </tr>
      <tr class="evenbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:comfild|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="10" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="10" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="10" />&nbsp;</td>
      </tr>
      <tr class="oddbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|showSubmitButton:false|optionStyleClass:|dropdownStyleClass:comfild|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="11" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="11" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="11" />&nbsp;</td>
      </tr>
      <tr class="evenbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:comfild|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="12" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="12" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="12" />&nbsp;</td>
      </tr>
      <tr class="oddbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|showSubmitButton:false|optionStyleClass:|dropdownStyleClass:comfild|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="13" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="13" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="13" />&nbsp;</td>
      </tr>
      <tr class="evenbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:comfild|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="14" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="14" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="14" />&nbsp;</td>
      </tr>
      <tr class="oddbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|showSubmitButton:false|optionStyleClass:|dropdownStyleClass:comfild|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="15" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="15" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="15" />&nbsp;</td>
      </tr>
      <tr class="evenbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:comfild|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="16" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="16" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="16" />&nbsp;</td>
      </tr>
      <tr class="oddbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|showSubmitButton:false|optionStyleClass:|dropdownStyleClass:comfild|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="17" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="17" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="17" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="17" />&nbsp;</td>
      </tr>
      <tr class="evenbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:comfild|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="18" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="18" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="18" />&nbsp;</td>
      </tr>
      <tr class="oddbarana">
        <td><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:true|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Company Analysis=1;Locations=2;Divisions=3;Location Analysis=4;Five Year Comparative=5;Daily Deposits=6;Sales Manager=7;Sales Reps=8;Budgets=9;Cash Batch=C;Invoices Batches=I;Check Batches=P|captionSource:VALUE|sharedGVs:false|style:|useHints:true|showSubmitButton:false|optionStyleClass:|dropdownStyleClass:comfild|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="2"
						componentSettings="" row="19" /></td>
        <td align="center"><HATS:Component col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="19" /></td>
        <td><HATS:Component col="17" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="41"
						componentSettings="" row="19" />&nbsp;</td>
      </tr>
      </table>
    
   
	

</HATS:Form>

</body>
</html>