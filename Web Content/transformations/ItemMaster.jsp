
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
<script>
function checkWidth() {
if (parent.document.getElementById("hatsFrame").width == 975)
{document.getElementById("colcenterover").className = "contentbgmainright"; }
else if (parent.document.getElementById("hatsFrame").width == 1208) 
{document.getElementById("colcenterover").className = "contentbgmainrightfull";}
}

function setActionCode(actionCode,caption){
document.HATSForm.in_184_1.value = actionCode;
document.getElementById('textfield10').value = caption;
}
</script>
</head>

<body onload="">

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<!-- Start of the HATS form. -->
<HATS:Form>

<!-- Insert your HATS component tags here. -->

<div class="container58">

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<!--Menu Start-->
    
	<div class="boxloanpage">
    
    
    <div class="clear"></div>
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="left">

<div class="TabmainContent">

<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Item:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="3" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:true|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="19" componentSettings="" row="3" /></td>
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Action:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><input type="text" class="field300" name="textfield10" id="textfield10" /><HATS:Component
						col="24" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings=""
						type="com.ibm.hats.transform.components.InputComponent" ecol="24"
						componentSettings="" row="3" /></td>
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Pattern After:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
								col="30" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="3" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:true|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="47" componentSettings="" row="3" /></td>
	</tr>
</table>
    
<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
	<tr>
	<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="commontab">
	<tr>
	<td colspan="2" align="left" class="tabtxthead barhead4new2">Action Codes</td>
	</tr>
	<tr class="oddbar13">
	<td width="49%"><a href="javascript:void(0)" onclick="setActionCode('A',this.innerHTML);">Add a new item	</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('P',this.innerHTML);">Work with Location prices</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('B',this.innerHTML);">Work with location info</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('R',this.innerHTML);">work with prod structure</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('C',this.innerHTML);">Change an existing item</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('S',this.innerHTML);">Work with substitutes</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('D',this.innerHTML);">Delete an existing item</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('T',this.innerHTML);">Work with additional desc</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('E',this.innerHTML);">Work with complementaries</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('U',this.innerHTML);">Sales tax</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('F',this.innerHTML);">Work with specifications</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('V',this.innerHTML);">Secondary vendors</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('H',this.innerHTML);">Work with complementary Where Usd</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('W',this.innerHTML);">Work with buying controls</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('G',this.innerHTML);">Work with  go with items</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('X',this.innerHTML);">Drop item</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('K',this.innerHTML);">Work with Kits</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('Y',this.innerHTML);">Lot control</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('L',this.innerHTML);">Create laminate details</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('Z',this.innerHTML);">User definded fields</a></td>
	</tr>
</table>
	</td>
	</tr>
    <tr>
	<td valign="bottom">
    <div class="clear5px"></div>
<table id="checkbox" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="commontab">
	<tr>
	<td colspan="4" align="center" class="tabtxthead barhead4new">Check the items to be displayed each time you work with an item:</td>
	</tr>
	<tr class="oddbar14">
	<td width="10" valign="middle"><HATS:Component col="38" alternate=""
								widget="com.ibm.hats.transform.widgets.CheckboxWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="38" componentSettings="" row="18" /></td>
	<td>Work with location info</td>
	<td width="10"><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="75" componentSettings="" row="17" /></td>
	<td>Work with product structures</td>
	</tr>
	<tr class="evenbar14">
	<td><HATS:Component col="38" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="38" componentSettings="" row="18" /></td>
	<td>Work with complementaries</td>
	<td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="75" componentSettings="" row="18" /></td>
	<td>Work with substitutes</td>
	</tr>
    <tr class="oddbar14">
	<td><HATS:Component col="38" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="38" componentSettings="" row="19" /></td>
	<td>Work with go with items</td>
	<td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="75" componentSettings="" row="19" /></td>
	<td>Work with addintional desc</td>
	</tr>
	<tr class="evenbar14">
	<td><HATS:Component col="38" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="38" componentSettings="" row="20" /></td>
	<td>Work with kits</td>
	<td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="75" componentSettings="" row="20" /></td>
	<td>Work with sales tax</td>
	</tr>
    <tr class="oddbar14">
	<td><HATS:Component col="38" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="21" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="38" componentSettings="" row="21" /></td>
	<td>Work with location prices</td>
	<td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="21" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="75" componentSettings="" row="21" /></td>
	<td>Work with user defined fields</td>
	</tr>
</table>
</td>
</tr>
	
	<tr>
		<td align="right">
				<table width="auto" border="0" cellspacing="5" cellpadding="0">
				  <tr>
				    <td>
				<div class="prevbtn">
				      <a name="[pageup]" accesskey="HATSForm" href="javascript:ms('[pageup]','HATSForm')">Previous</a>
				</div>
				    </td>
				    <td>
				<div class="nextbtn">
				      <a name="[pagedn]" accesskey="HATSForm" href="javascript:ms('[pagedn]','HATSForm')">Next</a>
				</div>
				    </td>
				  </tr>
				</table>
		</td>
		</tr>
					
				<tr>
					<td align="center" style="color: red; font: bold 11px Tahoma, Geneva, sans-serif">
						<HATS:Component col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="24" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="71"
								componentSettings="" row="24" />
					</td>
				</tr>
	</table>




</div>
	</td>
	</tr>
</table>
	</div>
    
    
    
    </div>


<script>
var actions =new Array("","A", "B", "C", "D", "E", "F", "H", "G", "K", "L", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
var actionDesc =new Array("","Add a new item","Work with location info","Change an existing item","Delete an existing item","Work with complementaries","Work with specifications","Work with complementary Where Usd","Work with go with items","Work with kits","Create laminate details","Work with location prices","Work with prod structure","Work with substitutes","Work with additional desc","Sales tax","Secondary vendors","Work with buying controls","Drop item","Lot control","User defined fields");
var action = document.HATSForm.in_184_1.value;
var actionIndex = 0;
document.HATSForm.in_184_1.style.display = "none";
for(var i=0; i<actions.length; i++){
	if(actions[i] == action){
		actionIndex = i;
		break;
	}
}
document.getElementById('textfield10').value = actionDesc[actionIndex];


var table = document.getElementById("checkbox");

for (var i = 0, row; row = table.rows[i]; i++) {

   //iterate through rows
   //rows would be accessed using the "row" variable assigned in the for loop
   for (var j = 0, col; col = row.cells[j]; j+=2) {
   	var tdtrim = col.innerHTML.replace(/(&nbsp;|\s)$/,"");
   	col.innerHTML = tdtrim;   	
     //iterate through columns
     //columns would be accessed using the "col" variable assigned in the for loop
   }  
}
</script>

</HATS:Form>
<!-- End of the HATS form. -->


</body>
</html>
