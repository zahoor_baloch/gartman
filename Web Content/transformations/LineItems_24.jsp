<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>

</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>



<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<script language="JavaScript">
function sendF4Prompt(cursorPos) {
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[pf4]', 'HATSForm');
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="left">
	
<table width="auto" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td><div class="Tabmain"><a href="javascript:void();" onclick="javascript:ms('[pf12]', 'HATSForm');">Header</a></div></td>
	<td><div class="Tabmainselected">Line Items</div></td>
	<td><div class="Tabmain"><a href="javascript:void();" onclick="javascript:ms('[pf10]', 'HATSForm');">Misc Charges</a></div></td>
	</tr>
</table>


<div class="clear"></div>

<div class="TabmainContent">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="txt2">
	<tr>
		
		<td colspan="8" bgcolor="#c9cfd6" style="padding:1px 15px 1px 15px;" class="fillrank">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="50%">
						<span style="float:left;">
							Customer Rank:					
						</span>
						
						<span style="float:left; margin-left: 10px">
							<HATS:Component col="49" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="4" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="49"
								componentSettings="" row="4" />
						</span>
						
						<span style="float:left; margin-left: 10px">
							<HATS:Component col="39" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="2" textReplacement=""
								widgetSettings=""
								type="com.ibm.hats.transform.components.TextComponent" ecol="41"
								componentSettings="" row="2" />
						</span>	
					</td>
				</tr>
			</table>
		
		</td>
		
	</tr>
	<tr>
		<td colspan="8" height="5"></td>
	</tr>
	
	<tr>
	<td align="left" bgcolor="#FFFFFF" class="fillbar"><strong>Sold To:</strong></td>
	<td width="220" class="fillbarnew" style="padding:2px 0px 0px 3px;"><HATS:Component
						col="19" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="28"
						componentSettings="" row="3" /></td>
	<td width="10"></td>
	<td align="left" bgcolor="#FFFFFF" class="fillbar"><strong>Natl:</strong></td>
	<td width="220" class="fillbarnew" style="padding:2px 0px 0px 3px;"><strong>Phone</strong></td>
	<td width="10"></td>
	<td align="left" bgcolor="#FFFFFF" class="fillbar"><strong>Ship To:</strong></td>
	<td width="220" class="fillbarnew" style="padding:2px 0px 0px 3px;"></td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="28"
						componentSettings="" row="4" /></td>
	<td>&nbsp;</td>
	<td></td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="35" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="46"
						componentSettings="" row="4" /></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="52" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="76"
						componentSettings="" row="4" /></td>
	</tr>
	<tr>
	  <td>&nbsp;</td>
	  <td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="28"
						componentSettings="" row="5" /></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><strong>Contact</strong></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;"><HATS:Component
						col="52" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="5" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="76"
						componentSettings="" row="5" /></td>
	  </tr>
	<tr>
	<td>&nbsp;</td>
	<td align="right" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">
	<div style="text-align:left; float:left"><HATS:Component col="4"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="28"
						componentSettings="" row="6" /></div> 
	<div style="text-align:right; float:right"></div>
	</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
					<td align="left" bgcolor="#FFFFFF" class="fillbor58"
						style="padding: 3px;"><HATS:Component col="33" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="6" />&nbsp;</td>
					<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td align="right" bgcolor="#FFFFFF" class="fillbor58" style="padding:3px;">
	<div style="text-align:left; float:left"><HATS:Component col="52"
						alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="6" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="76"
						componentSettings="" row="6" /></div><div style="text-align:right; float:right"></div>
	</td>
	</tr>
	</table>

<div class="clear5px"></div>

<%
String isBottom = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isBottom", true).getString(0).trim();
String seqNumber = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("seqNumber", true).getString(0).trim();
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" id="notfold" class="tabtxt">
	<tr>
	<td colspan="9" align="center" bgcolor="#1f599a" class="tabtxthead bar">CART II</td>
	</tr>
    <tr>
      <td width="5%" align="left" class="tabtxthead barhead4new2">Seq</td>
	<td width="28%" align="left" class="tabtxthead barhead4new2">Qty Shipped</td>
	<td width="28%" align="left" class="tabtxthead barhead4new2">Item #</td>
	<td width="10%" align="left" class="tabtxthead barhead4new2">Inv Loc</td>
	<td width="10%" align="left" class="tabtxthead barhead4new2">Color</td>
	<td width="4%" align="left" class="tabtxthead barhead4new2">Inv</td>
	<td width="2%" align="left" class="tabtxthead barhead4new2">C</td>
	<td width="3%" align="left" class="tabtxthead barhead4new2">N/C</td>
	<td align="left" class="tabtxthead barhead4new2">Net Price</td>
	</tr>
	<tr class="oddbar10">
	  <td align="left">
	  <HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="9" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	  <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="10" />&nbsp;</td>
	</tr>
    <tr class="oddbar10">
      <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="11" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	  <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="12" />&nbsp;</td>
	</tr>
    <tr class="oddbar10">
      <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="13" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	  <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="14" />&nbsp;</td>
	</tr>
	<tr class="oddbar10">
      <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="15" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
      <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="16" />&nbsp;</td>
	</tr>
	<tr class="oddbar10">
		<%	
		if(isBottom.equalsIgnoreCase("Bottom")) {			
		%>
			<td align="left" colspan="11"><a href="javascript:ms('[pageup]','HATSForm')">Previous</a></td>
		<%				
		}else{
			if(isBottom.equalsIgnoreCase("More...")) {
				if(seqNumber.length()>1) {		
		%>
				<td align="left" colspan="5"><a href="javascript:ms('[pageup]','HATSForm')">Previous</a></td>
				<td align="right" colspan="6"><a href="javascript:ms('[pagedn]','HATSForm')">Next...</a></td>			
		<%
				}else{
		%>
				<td align="right" colspan="11"><a href="javascript:ms('[pagedn]','HATSForm')">Next...</a></td>
		<%	
				}
			}else{
		%>
			<td align="right" colspan="11">&nbsp;</td>
		<%	
			}	
		} 
		%>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; display: none;" id="fold" class="tabtxt">
	<tr>
	<td colspan="9" align="center" bgcolor="#1f599a" class="tabtxthead bar">CART II</td>
	</tr>
    <tr>
      <td width="5%" align="left" class="tabtxthead barhead4new2">Seq</td>
	<td width="28%" align="left" class="tabtxthead barhead4new2">Qty Shipped</td>
	<td width="28%" align="left" class="tabtxthead barhead4new2">Item #</td>
	<td width="10%" align="left" class="tabtxthead barhead4new2">Inv Loc</td>
	<td width="10%" align="left" class="tabtxthead barhead4new2">Color</td>
	<td width="4%" align="left" class="tabtxthead barhead4new2">Inv</td>
	<td width="2%" align="left" class="tabtxthead barhead4new2">C</td>
	<td width="3%" align="left" class="tabtxthead barhead4new2">N/C</td>
	<td align="left" class="tabtxthead barhead4new2">Net Price</td>
	</tr>
	<tr class="oddbar10">
	  <td align="left">
	  <HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="9" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="9" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="9" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	  <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="right"><HATS:Component col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="23"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="left"><HATS:Component col="25" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="34"
						componentSettings="" row="10" />&nbsp;</td>
	<td align="right"><HATS:Component col="36" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="41"
						componentSettings="" row="10" /></td>
	<td align="center"><HATS:Component col="43" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="52"
						componentSettings="" row="10" /></td>
	<td align="left"><HATS:Component col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="57"
						componentSettings="" row="10" /></td>
					<td align="left" colspan="3"><HATS:Component col="59" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="68"
						componentSettings="" row="10" /></td>
				</tr>
    <tr class="oddbar10">
      <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="11" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="11" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="11" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	  <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="right"><HATS:Component col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="23"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="left"><HATS:Component col="25" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="34"
						componentSettings="" row="12" />&nbsp;</td>
	<td align="right"><HATS:Component col="36" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="41"
						componentSettings="" row="12" /></td>
	<td align="center"><HATS:Component col="43" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="52"
						componentSettings="" row="12" /></td>
	<td align="left"><HATS:Component col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="57"
						componentSettings="" row="12" /></td>
					<td align="left" colspan="3"><HATS:Component col="59" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="68"
						componentSettings="" row="12" /></td>
				</tr>
    <tr class="oddbar10">
      <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="13" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="13" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
	  <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="right"><HATS:Component col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="23"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="left"><HATS:Component col="25" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="34"
						componentSettings="" row="14" />&nbsp;</td>
	<td align="right"><HATS:Component col="36" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="41"
						componentSettings="" row="14" /></td>
	<td align="center"><HATS:Component col="43" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="52"
						componentSettings="" row="14" /></td>
	<td align="left"><HATS:Component col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="57"
						componentSettings="" row="14" /></td>
					<td align="left" colspan="3"><HATS:Component col="59" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="68"
						componentSettings="" row="14" /></td>
				</tr>
	<tr class="oddbar10">
      <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="19"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="21" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="38"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="40" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="47"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="58"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="60" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="62"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="64" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="64"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="66" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="68"
						componentSettings="" row="15" />&nbsp;</td>
	<td align="left"><HATS:Component col="70" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="15" />&nbsp;</td>
	</tr>
	<tr class="evenbar10">
      <td align="left"><HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="4"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="right"><HATS:Component col="15" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="23"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="left"><HATS:Component col="25" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="34"
						componentSettings="" row="16" />&nbsp;</td>
	<td align="right"><HATS:Component col="36" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="41"
						componentSettings="" row="16" /></td>
	<td align="center"><HATS:Component col="43" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="52"
						componentSettings="" row="16" /></td>
	<td align="left"><HATS:Component col="54" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="57"
						componentSettings="" row="16" /></td>
					<td align="left" colspan="3"><HATS:Component col="59" alternate=""
						widget="com.ibm.hats.transform.widgets.FieldWidget"
						alternateRenderingSet="" erow="16" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|stylesForTD:|dataModeCEPValue:*|alignment:NORMAL|readOnly:false|addSpanTagToText:false|extendedStyleForTD:true|trimSpacesOnInputs:true|fieldStyleClass:HATSFIELD|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableNumericLock:false|tableStyleClass:HATSFIELDTABLE|reverseVideoStyle:|usingMonospaceFont:true|trimLinks:true|underlineStyle:text-decoration: underline|protectedLinkStyleClass:HATSPROTLINK|classesForTD:|cursorModeCEPValue:*|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|showLinksForProtectedFields:false|preserveColors:true|omitExtraneousTables:false|reverseColorForTD:true|layout:SEPARATED|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|autoKeyForProtectedFieldLinks:|extendedStyleInsideTD:true|renderProtectedTextAsLinks:false|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|linkStyleClass:HATSPROTLINK|optimizedOutput:true|"
						type="com.ibm.hats.transform.components.FieldComponent" ecol="68"
						componentSettings="" row="16" /></td>
				</tr>
	<tr class="oddbar10">
		<%	
		if(isBottom.equalsIgnoreCase("Bottom")) {			
		%>
			<td align="left" colspan="11"><a href="javascript:ms('[pageup]','HATSForm')">Previous</a></td>
		<%				
		}else{
			if(isBottom.equalsIgnoreCase("More...")) {
				int seqNumberInt = 0;
				if(!(seqNumber.equalsIgnoreCase(""))){
					seqNumberInt = Integer.parseInt(seqNumber); 
				}
				if(seqNumberInt>4) {		
		%>
				<td align="left" colspan="5"><a href="javascript:ms('[pageup]','HATSForm')">Previous</a></td>
				<td align="right" colspan="6"><a href="javascript:ms('[pagedn]','HATSForm')">Next...</a></td>			
		<%
				}else{
		%>
				<td align="right" colspan="11"><a href="javascript:ms('[pagedn]','HATSForm')">Next...</a></td>
		<%	
				}
			}else{
		%>
			<td align="right" colspan="11">&nbsp;</td>
		<%	
			}	
		} 
		%>
	</tr>
</table>

<div class="clear5px"></div>

<div id="notAbbreviated" style="display: block;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Action Code</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="7"
						componentSettings="" row="18" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="24"
						componentSettings="" row="18" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Company</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Location</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Quantity Order</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">UOM</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Cut</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="61"
						componentSettings="" row="18" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">N/C</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Inv</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="67" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="69"
						componentSettings="" row="18" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Dir</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Width</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Length</td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent" ecol="3"
								componentSettings="" row="19" />
	</td>
	<td>
	<img style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1442');"/>
	</td>
	</tr>
	</table>
	</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="7"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:login_field1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:width: 190px|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="26"
						componentSettings="" row="19" /><img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1449');"/></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="30"
						componentSettings="" row="19" />
	</td>
	<td>
	<img id="1468_3" style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1468');">
	</td>
	</tr>
	</table>					
	</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="32" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="33"
						componentSettings="" row="19" />
						<img id="1472_2" style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1472');"/></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component
						col="35" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="43"
						componentSettings="" row="19" />
	</td>
	<td>
	<img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1475');"/>
	</td>
	</tr>
	</table>
	</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="45"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="47" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="47"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="59"
						componentSettings="" row="19" /> 
	</td>
	<td>
	<img style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1489');"/>
	</td>
	<td>
	<HATS:Component
						col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="61"
						componentSettings="" row="19" />
    </td>
    </tr>
    </table>
    </td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="63" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="63"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component
						col="65" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="65"
						componentSettings="" row="19" />
	</td>
	<td>
	<img id="1505_1" style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1505');"/>
	</td>
	</tr>
	</table>
	</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="68" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="68"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="71" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="71"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="73" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="75"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="77" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="79"
						componentSettings="" row="19" /></td>
	</tr>
	<!--<tr><td colspan="15" bgcolor="#ffffff" class="bar">d</td></tr>-->
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Type</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="9"
						componentSettings="" row="20" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="11" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="20" /></td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="51"
						componentSettings="" row="20" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Quantity Ship</td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="63" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="20" /></td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="79"
						componentSettings="" row="20" /></td>
	<td colspan="5" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">&nbsp;</td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="5"
						componentSettings="" row="21" />
						<img style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1604');"/></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="8"
						componentSettings="" row="21" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="11" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="40"
						componentSettings="" row="21" /></td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="42" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="51"
						componentSettings="" row="21" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="61"
						componentSettings="" row="21" />
						<img id="1653_9" style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1653');"/></td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component
						col="63" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field2|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="73"
						componentSettings="" row="21" />
	</td>
	<td>
	<img id="1663_11" style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1663');"/>
	
	</td>
	</tr>					
	</table>
	</td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="79"
						componentSettings="" row="21" />
						<img id="1675_5" style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1675');"/></td>
	<td colspan="5" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">&nbsp;</td>
	</tr>
	</table>
 </div> 
 
<div id="limitFields" style="display: none;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Action Code</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="7"
						componentSettings="" row="18" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="24"
						componentSettings="" row="18" /></td>
	
	
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Quantity Order</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">UOM</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Cut</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="61"
						componentSettings="" row="18" /></td>
	
	</tr>
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent" ecol="3"
								componentSettings="" row="19" />
	</td>
	<td>
	<img id="1442_2" style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1442');"/>
	</td>
	</tr>
	</table>
	</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="7"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:login_field1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:width: 190px|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="26"
						componentSettings="" row="19" /><img id="1449_18" style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png"  align="absmiddle" onclick="sendF4Prompt('1449');"/></td>
							
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component
						col="35" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="43"
						componentSettings="" row="19" />
	</td>
	<td>
	<img id="1475_9" style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png"  align="absmiddle" onclick="sendF4Prompt('1475');"/>
	</td>
	</tr>
	</table>
	</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="45" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="45"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="47" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="47"
						componentSettings="" row="19" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	<table cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="59"
						componentSettings="" row="19" /> 
	</td>
	<td>
	<img id="1489_11" style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1489');"/>
	</td>
	<td>
	<HATS:Component
						col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="61"
						componentSettings="" row="19" />
    </td>
    </tr>
    </table>
    </td>
	
	</tr>
	<!--<tr><td colspan="15" bgcolor="#ffffff" class="bar">d</td></tr>-->
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Type</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="7" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="9"
						componentSettings="" row="20" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="11" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="40"
						componentSettings="" row="20" /></td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"></td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"></td>
	
	</tr>
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="5"
						componentSettings="" row="21" />
						<img id="1604_2" style="cursor: pointer;" title="Please click to send F4 command" border="0"
						src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1604');"/></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="8" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="8"
						componentSettings="" row="21" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="11" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="40"
						componentSettings="" row="21" /></td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"></td>
	<td colspan="2" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
	
	</td>	
	</tr>
	</table>
 </div> 
 
 <div id="abbreviated" style="display: none;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Action Code</td>
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="7"
						componentSettings="" row="18" /></td>
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="24"
						componentSettings="" row="18" /></td>
	
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="28" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="35"
						componentSettings="" row="18" /></td>
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">Quantity Order</td>
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">UOM</td>
	
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="49" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="61"
						componentSettings="" row="18" /></td>	
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="63" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="73"
						componentSettings="" row="18" /></td>
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="18" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="80"
						componentSettings="" row="18" /></td>
	
	</tr>
	<tr bgcolor="#c9cfd6">
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
					<HATS:Component col="2" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:HATSCAPTION|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent" ecol="3"
								componentSettings="" row="19" />
					</td>
					<td><img id="1442_2" style="cursor: pointer;"
								title="Please click to send F4 command" border="0"
								src="../common/images/find.png" 
								align="absmiddle" onclick="sendF4Prompt('1442');">
					</td>
				</tr>
			</table>
		</td>
		
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="5" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="7"
						componentSettings="" row="19" />
		</td>
		
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:true|inputFieldStyleClass:login_field1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:width: 190px|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="26"
						componentSettings="" row="19" /><img id="1449_18"
						style="cursor: pointer;" title="Please click to send F4 command"
						border="0" src="../common/images/find.png" 
						 align="absmiddle" onclick="sendF4Prompt('1449');"></td>
				
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
			<table cellpadding="0" cellspacing="0">
			<tr>
			<td>
			<HATS:Component col="28" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="30" componentSettings="" row="19" /> 
			</td>
			<td>
			</td>
			<td>
			<HATS:Component col="32" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="33" componentSettings="" row="19" />&nbsp;
			
		    </td>
		    <td><img id="a_1468_3" style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1468');"/></td>
		    </tr>
		    </table>
		</td>
		
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<HATS:Component col="37" alternate=""
								widget="com.ibm.hats.transform.widgets.InputWidget"
								alternateRenderingSet="" erow="19" textReplacement=""
								widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
								type="com.ibm.hats.transform.components.InputComponent"
								ecol="45" componentSettings="" row="19" />
					</td>
					<td>
						<img id="a_1477_9" style="cursor: pointer;" title="Please click to send F4 command" border="0"
										src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1477');"/>
					</td>
				</tr>
			</table>
		</td>
		
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="47" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="47"
						componentSettings="" row="19" />
		</td>		
		
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<HATS:Component
										col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent" ecol="59"
										componentSettings="" row="19" /> 
					</td>
					<td>
						<img id="a_1489_11" style="cursor: pointer;" title="Please click to send F4 command" border="0" src="../common/images/find.png" align="absmiddle" onclick="sendF4Prompt('1489');"/>
					</td>
					<td>
						<HATS:Component
										col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.InputWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
										type="com.ibm.hats.transform.components.InputComponent" ecol="61"
										componentSettings="" row="19" />
				    </td>
			    </tr>
		    </table>
	    </td>
		
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="63" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="73"
						componentSettings="" row="19" />
		</td>			
	
		<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="75" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="19" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4new|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="79"
						componentSettings="" row="19" />
		</td>
				
	</tr>
	<!--<tr><td colspan="15" bgcolor="#ffffff" class="bar">d</td></tr>-->
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="9" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="11"
						componentSettings="" row="20" /></td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="13" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="20" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="42"
						componentSettings="" row="20" /></td>
	<td colspan="6" bgcolor="#c9cfd6" class="tabtxthead2 bar"></td>	
	</tr>
	
	<tr bgcolor="#c9cfd6">
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="4" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="5"
						componentSettings="" row="21" /></td>
	
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="10" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="10"
						componentSettings="" row="21" /></td>
	
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="13" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="21" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="42"
						componentSettings="" row="21" /></td>
	<td colspan="6" align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar">&nbsp;</td>
	</tr>
	</table>
 </div>
 
<div class="clear2px"></div>

<div class="bartab1"><HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="22" /></div>
<div class="clear2px"></div>
<div class="bartab2"><HATS:Component col="2" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="23" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="79"
				componentSettings="" row="23" /></div>
</div>
</td>
</tr>
</table>
<%
	String temp = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("visited", true).getString(0).trim();
	if(temp.equalsIgnoreCase("View1")) {
%>

<script>
document.getElementById("table1").style.display="block";
document.getElementById("table2").style.display="none";
document.getElementById("view1").style.display="block";
document.getElementById("view2").style.display="none";
</script>

<%	} else if(temp.equalsIgnoreCase("View2")) { %>

<script>
document.getElementById("table1").style.display="none";
document.getElementById("table2").style.display="block";
document.getElementById("view1").style.display="none";
document.getElementById("view2").style.display="block";
</script>

<%	} 


	String isAbbreviated = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isAbbreviated", true).getString(0).trim();	
	if(isAbbreviated.equalsIgnoreCase("Location")) {	
%>

		<script>
		document.getElementById("notAbbreviated").style.display="none";
		document.getElementById("abbreviated").style.display="block";
		
		if(typeof document.HATSForm.in_1468_3 == 'undefined') {
	 	document.getElementById("a_1468_3").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1477_9 == 'undefined') {
		 	document.getElementById("a_1477_9").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1489_11 == 'undefined') {
		 	document.getElementById("a_1489_11").style.display = "none";	 
		}
		</script>

<% 
	}else{
%>

		<script>
		document.getElementById("abbreviated").style.display="none";
		document.getElementById("notAbbreviated").style.display="block";
		
		if(typeof document.HATSForm.in_1468_3 == 'undefined') {
			document.getElementById("notAbbreviated").style.display="none";
			document.getElementById("limitFields").style.display="block";
		}
		
		if(typeof document.HATSForm.in_1442_2 == 'undefined') {
	 	document.getElementById("1442_2").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1449_18 == 'undefined') {
		 	document.getElementById("1449_18").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1472_2 == 'undefined') {
		 	document.getElementById("1472_2").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1475_9 == 'undefined') {
		 	document.getElementById("1475_9").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1489_11 == 'undefined') {
		 	document.getElementById("1468_11").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1468_3 == 'undefined') {
		 	document.getElementById("1468_3").style.display = "none";	 
		}
		
		
		if(typeof document.HATSForm.in_1505_1 == 'undefined') {
		 	document.getElementById("1505_1").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1604_2 == 'undefined') {
		 	document.getElementById("1604_2").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1653_9 == 'undefined') {
		 	document.getElementById("1653_9").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1663_11 == 'undefined') {
		 	document.getElementById("1663_11").style.display = "none";	 
		}
		
		if(typeof document.HATSForm.in_1675_5 == 'undefined') {
		 	document.getElementById("1675_5").style.display = "none";	 
		}
		</script>
<%	
	}
	
	
	String isFold = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("isFold", true).getString(0).trim();	
	if(isFold.equalsIgnoreCase("Serial #")) {	
%>

		<script>
		document.getElementById("notfold").style.display="none";
		document.getElementById("fold").style.display="block";
		</script>
<%	
	}
%>

</HATS:Form>

<script>			
			var checkValue='<%=request.getParameter("actionCode")%>';
			if(checkValue=='null' || checkValue == null ){
			document.getElementsByName('in_1442_2')[0].value='';
			}else {
			document.getElementsByName('in_1442_2')[0].value=checkValue;
			}
   		
    var allBrTags = document.getElementsByTagName("br"),
    length = allBrTags.length;
   
	while(length--) {
    	allBrTags[length].parentNode.removeChild(allBrTags[length]);
	}

	
</script>

</body>
</html>