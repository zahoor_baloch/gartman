
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
<script>
function checkWidth() {
if (parent.document.getElementById("hatsFrame").width == 1050)
{document.getElementById("colcenterover").className = "contentbgmainright"; }
else if (parent.document.getElementById("hatsFrame").width == 1208) 
{document.getElementById("colcenterover").className = "contentbgmainrightfull";}
}

function setActionCode(actionCode,caption){
document.HATSForm.in_176_2.value = actionCode;
document.getElementById('textfield10').value = caption;
}
</script>
</head>
<body onload="">

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<!-- Start of the HATS form. -->
<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>
<!-- Insert your HATS component tags here. -->

<div class="container58">

<div class="bartmaintop">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="comtxt">
	<tr>
	<td width="100"></td>
	<td align="center" class="comtxt2">&nbsp;</td>
	<td width="100"><HATS:Component col="71" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="2" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="78"
				componentSettings="" row="2" /></td>
	</tr>
</table>
</div>

<!--line-->
<table width="100%">
<tr><td height="1"><div class="line"></div></td></tr>
</table>
<!--line-->

<!--Menu Start-->

    
	<div class="boxloanpage">
   
    
    
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="left">

<div style="height:465px" class="TabmainContent">

<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Customer #:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:true|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="11"
						componentSettings="" row="3" /></td>
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Action:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><input type="text" class="field300" name="textfield10" id="textfield10" /><HATS:Component
						col="16" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="17"
						componentSettings="" row="3" /></td>
	<td align="right" bgcolor="#c9cfd6" class="tabtxthead2 bar">Pattern After:</td>
	<td align="left" bgcolor="#c9cfd6" class="tabtxthead2 bar"><HATS:Component
						col="23" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:HATSTABLE|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:true|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="32"
						componentSettings="" row="3" /></td>
	</tr>
</table>
    
<div class="clear5px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
	<tr>
	<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="commontab">
	<tr>
	<td colspan="3" align="left" class="tabtxthead barhead4new2">Action Codes</td>
	</tr>
	<tr class="oddbar13">
	<td width="36%"><a href="javascript:void(0)" onclick="setActionCode('A',this.innerHTML);">Add A New Customer</a></td>
	<td width="30%"><a href="javascript:void(0)" onclick="setActionCode('L',this.innerHTML);">Locations To Search Inv</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('V',this.innerHTML);">W/W Vendor/Customer</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('B',this.innerHTML);">W/W Billing/Marketing</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('M',this.innerHTML);">Work With Mail Codes</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('VI',this.innerHTML);">View Image</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('C',this.innerHTML);">Change A Customer</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('N',this.innerHTML);">Work With Comments</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('W',this.innerHTML);">Work With Buying Group</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('D',this.innerHTML);">Delete A Customer</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('O',this.innerHTML);">Work With Invoice U/M</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('X',this.innerHTML);">Item/Sales Tax</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('E',this.innerHTML);">Extract Mailing List</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('P',this.innerHTML);">Work With Payment Hist</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('Y',this.innerHTML);">Taxable</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('F',this.innerHTML);">W/W Buying Authority</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('Q',this.innerHTML);">Work With Profile</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('Z',this.innerHTML);">Work Wtih Credit Info</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('G',this.innerHTML);">Work Wtih Bill To</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('R',this.innerHTML);">Work With Routes</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('#',this.innerHTML);">W/W Trading Partner/Xrf</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('H',this.innerHTML);">Hold/Release Activity</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('S',this.innerHTML);">Work With Ship to</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('@',this.innerHTML);">W/W Bank Accounts</a></td>
	</tr>
    <tr class="oddbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('J',this.innerHTML);">W/W Price Exceptions</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('T',this.innerHTML);">Work With High Credit</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('$',this.innerHTML);">W/W Credit Card Info</a></td>
	</tr>
	<tr class="evenbar13">
	<td><a href="javascript:void(0)" onclick="setActionCode('K',this.innerHTML);">Work With Contacts</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('U',this.innerHTML);">Work With Shipping Info</a></td>
	<td><a href="javascript:void(0)" onclick="setActionCode('!',this.innerHTML);">Personal Guarantor</a></td>
	</tr>
</table>
	</td>
	</tr>
    <tr>
	<td>
    <div class="clear5px"></div>
		<table id="checkbox" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="commontab">
			<tr>
			<td colspan="6" align="center" class="tabtxthead barhead4new">Check the items to be displayed each time you work with a customer</td>
			</tr>
			<tr class="oddbar14">
			<td width="10"><HATS:Component col="27" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="27" componentSettings="" row="17" /></td>
			<td>Buying Authority</td>
			<td width="10"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="50" componentSettings="" row="17" /></td>
			<td>Bill To</td>
			<td width="10"><HATS:Component col="73" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="73" componentSettings="" row="17" /></td>
			<td>Billing/Marketing</td>
			</tr>
			<tr class="evenbar14">
			<td><HATS:Component col="27" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="27" componentSettings="" row="18" /></td>
			<td>Price Exception</td>
			<td><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="50" componentSettings="" row="18" /></td>
			<td>Contacts</td>
			<td><HATS:Component col="73" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="73" componentSettings="" row="18" /></td>
			<td>Locations</td>
			</tr>
		    <tr class="oddbar14">
			<td><HATS:Component col="27" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="27" componentSettings="" row="19" /></td>
			<td>Payment History </td>
			<td><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="50" componentSettings="" row="19" /></td>
			<td>Ship To Info</td>
			<td><HATS:Component col="73" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="73" componentSettings="" row="19" /></td>
			<td>Comments</td>
			</tr>
			<tr class="evenbar14">
			<td><HATS:Component col="27" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="27" componentSettings="" row="20" /></td>
			<td>High Credit</td>
			<td><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="50" componentSettings="" row="20" /></td>
			<td>Mail Codes</td>
			<td><HATS:Component col="73" alternate=""
										widget="com.ibm.hats.transform.widgets.CheckboxWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="style:|tableRowStyleClass:|tableStyleClass:|labelStyleClass:|tableCaptionCellStyleClass:|captionSource:VALUE|checkboxStyleClass:|trimCaptions:true|deselectValue:|columnsPerRow:1|tableCellStyleClass:|caption:|inputFieldStyleClass:HATSINPUT|selectValue:Y|"
										type="com.ibm.hats.transform.components.InputComponent"
										ecol="73" componentSettings="" row="20" /></td>
			<td>Buying Group</td>
			</tr>
		</table>
		</td>
		</tr>
	
					<tr>
						<td align="center">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td align="center" style="color: red; font: bold 11px Tahoma, Geneva, sans-serif">
							<HATS:Component col="1" alternate=""
						widget="com.ibm.hats.transform.widgets.LabelWidget"
						alternateRenderingSet="" erow="24" textReplacement=""
						widgetSettings="trim:true|style:|labelStyleClass:|"
						type="com.ibm.hats.transform.components.TextComponent" ecol="80"
						componentSettings="" row="24" />
						</td>
					</tr>
</table>


</div>
	</td>
	</tr>
</table>
	</div>
        
    </div>

	<!--Menu End-->

<script>
var actions =new Array("","A","B","C","D","E","F","G","H","J","K","L","M","N","O","P","Q","R","S","T","U","V","VI","W","X","Y","Z","#","@","$","!");
var actionDesc =new Array("","Add A New Customer","W/W Billing/Marketing","Change A Customer","View Image","Extract Mailing List","W/W Buying Authority","Work With Bill To","Hold/Release Activity","W/W Price Exceptions","Work With Contacts","Locations To Search Inv","Work With Mail Codes","Work With Comments","Work With Invoice U/M","Work With Payment Hist","Work With Profile","Work With Routes","Work With Ship To","Work With High Credit","Work With Shipping Info","W/W Vendor/Customer","View Image","Work With Buying Group","Item/Sales Tax","Taxable","Work With Credit Info","W/W Trading Partner/Xrf","W/W Bank Accounts","W/W Credit Card Info","Personal Guarantor");
var action = document.HATSForm.in_176_2.value;
var actionIndex = 0;
document.HATSForm.in_176_2.style.display = "none";
for(var i=0; i<actions.length; i++){
	if(actions[i] == action){
		actionIndex = i;
		break;
	}
}
document.getElementById('textfield10').value = actionDesc[actionIndex];

var table = document.getElementById("checkbox");

for (var i = 0, row; row = table.rows[i]; i++) {
   //iterate through rows
   //rows would be accessed using the "row" variable assigned in the for loop
   for (var j = 0, col; col = row.cells[j]; j+=2) {
   	var tdtrim = col.innerHTML.replace(/(&nbsp;|\s)$/,"");
   	col.innerHTML = tdtrim;   	
     //iterate through columns
     //columns would be accessed using the "col" variable assigned in the for loop
   }  
}
</script>

</HATS:Form>
<!-- End of the HATS form. -->
</body>
</html>
