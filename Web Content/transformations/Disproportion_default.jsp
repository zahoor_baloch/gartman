<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>

<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>

<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
</script>

<script type="text/javascript">
    portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
</script>

<script type="text/javascript">
    PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<script type="text/javascript">


function sendF4Prompt(cursorPos) {
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[pf4]', 'HATSForm');
}

</script>


<div align="center">

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>
<table height="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td valign="top">
	<HATS:DefaultRendering renderingSet="disproportionCustomRS" erow="-1"
		col="1" settings="" ecol="-1" row="2" applyTextReplacement="true"
		applyGlobalRules="true" />
		</td>
		</tr>
		</table>

	<%
	String curTab = "";
	
	if(request.getParameter("curTab") != null) {
		curTab = request.getParameter("curTab").trim();
%>
<input type="hidden" name="currentTab" value="<%=curTab %>">
<%
	}
	
%>
</HATS:Form>


</div>

</body>
</html>
