<% out.println("<!--default_transformation.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>

<body>
<% out.println("-->"); %>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
</script>

<script type="text/javascript">
    portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
</script>

<script type="text/javascript">
    PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>
<div align="center">
<HATS:Form>

<table>
  <tr>
    <td>
      <HATS:Component type="com.ibm.hats.transform.components.FieldComponent" widget="com.ibm.hats.transform.widgets.FieldWidget" row="1" col="1" erow="-1" ecol="-1" componentSettings="" widgetSettings="mapBackgroundColors:true|allowPositionOnProtected:false|magentaBackgroundColor:#ff00ff|blinkStyle:font-style: italic|tableCellClass:|greenBackgroundColor:#00ff00|redBackgroundColor:#ff0000|characterRendering:false|linkClass:HATSLINK|mapExtendedAttributes:false|tableClass:HATSFIELDTABLE|cyanBackgroundColor:#00ffff|yellowBackgroundColor:#ffff33|underlineStyle:text-decoration: underline|preserveColors:true|readOnly:false|style:|columnSeparatorStyle:border-width: 1px; border-style: solid|whiteBackgroundColor:#ffffff|reverseVideoStyle:background-color:#666666|blueBackgroundColor:#0000ff|" textReplacement="" applyTextReplacement="false" applyGlobalRules="false"/>
    </td>
  </tr>
  <tr>
    <td align="center">
      <HATS:HostKeypad />
    </td>
  </tr>
  <tr>
    <td align="center">
      <HATS:OIA/>
    </td>
  </tr>
</table>

</HATS:Form>
</div>


<% out.println("<!--default_transformation.jsp"); %>
</body>
</html>
<% out.println("-->"); %>
