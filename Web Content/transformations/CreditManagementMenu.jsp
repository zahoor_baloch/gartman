<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<script language="JavaScript">
function sendF4Prompt(cursorPos) {
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[pf4]', 'HATSForm');
}
</script>


<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>


    <!--line-->
    <table width="100%">
    <tr><td height="1"><div class="line"></div></td></tr>
    </table>
    <!--line-->
    
    <div class="maintopmenudd">
          <TABLE width="100%" cellspacing="0" cellpadding="0" border="0">
<TBODY>
<TR>
<TD align="center">
<div class="menubtn">
<HATS:Component col="32" alternate=""
					widget="com.ibm.hats.transform.widgets.LinkWidget"
					alternateRenderingSet="" erow="14" textReplacement=""
					widgetSettings="style:|captionType:DESCRIPTION|separator:<br>|layout:TABLE|trimCaptions:true|target:|columnsPerRow:2|emenubarStyleClass:HATSEMENUBAR|linkStyleClass:menubtn|"
					type="com.ibm.hats.transform.components.SelectionListComponent"
					ecol="102" componentSettings="" row="3" /></div>
</TD>
</TR>
</TBODY>
</TABLE>
        </div>
    
	<div class="clear5px"></div>
    
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
	<tr bgcolor="#c9cfd6">
	
	<td colspan="1" align="right" class="tabtxthead2 bar08">Customer</td>
	<td colspan="1" align="left" class="tabtxthead2 bar08"><HATS:Component col="69"
				alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field3|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="78"
				componentSettings="" row="16" /><img id="" style="cursor: pointer; "
						title="Please click to send F4 command" border="0"
						src="../common/images/find.png"
						align="absmiddle" onclick="sendF4Prompt('2049');"></td>
	<td colspan="1" align="right" class="tabtxthead2 bar08">Collector</td>
	<td align="left" class="tabtxthead2 bar08"><HATS:Component col="74"
				alternate="" widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="75"
				componentSettings="" row="18" />
			<img id="" style="cursor: pointer; "
						title="Please click to send F4 command" border="0"
						src="../common/images/find.png"
						align="absmiddle" onclick="sendF4Prompt('2318');">
				</td>
	<td align="right" class="tabtxthead2 bar08">Location</td>
	<td align="left" class="tabtxthead2 bar08">
    <HATS:Component col="100" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field4|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="102"
				componentSettings="" row="18" />
				<HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="105"
				componentSettings="" row="18" /></td>
	
	</tr>
	
	<tr>
	<td colspan="6" bgcolor="#ffffff" class="bar08"></td>
	</tr>
	<tr bgcolor="#c9cfd6">
	<td colspan="2" align="right" class="tabtxthead2 bar08">Sort credit hold list by :</td>
	<td colspan="4" align="left" class="tabtxthead2 bar08">
		<table cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<HATS:Component
					col="64" alternate=""
					widget="com.ibm.hats.transform.widgets.InputWidget"
					alternateRenderingSet="" erow="20" textReplacement=""
					widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:login_field5|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:true|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
					type="com.ibm.hats.transform.components.InputComponent" ecol="64"
					componentSettings="" row="20" />
		</td>
		<td>&nbsp;
		(D=Ship Date, blank=Customer)
		</td>
		</tr>
		</table>
	</td>	
	</tr>
    <tr>
	<td colspan="6" bgcolor="#ffffff" class="bar08"></td>
	</tr>
    <tr bgcolor="#c9cfd6">
	<td colspan="6" align="center" class="tabtxthead2 bar08"><span style="color:#224b85;">*Note:</span> Customer filter must be entered before using “Work with Credit Information” or displaying “Customer Credit Parameters”.</td>
	</tr>
</table>
    
    
</HATS:Form>

</body>
</html>