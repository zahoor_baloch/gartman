<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@page import="com.ibm.hats.common.*"%><html>
<head>

<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>

<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
</script>

<script type="text/javascript">
    portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
</script>

<script type="text/javascript">
    PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<script type="text/javascript">

function setActionCode(val){

document.getElementsByName('actionCode')[0].value=document.getElementById(val.id).innerHTML.substring(0,2);

//ms('[enter]', 'HATSForm');

}

function sendF4Prompt(cursorPos) {
	setCursorPosition(cursorPos, 'HATSForm');
	ms('[pf4]', 'HATSForm');
}

function inc(){
var inc = document.getElementsByName('hatsgv_inc')[0].value;
var incInt = parseInt(inc);
document.getElementsByName('hatsgv_inc')[0].value=++incInt;
}

function dec(){
var inc = document.getElementsByName('hatsgv_inc')[0].value;
var incInt = parseInt(inc);
document.getElementsByName('hatsgv_inc')[0].value=--incInt;
}
</script>


<div align="center">

<HATS:Form>
	
<%
String inc = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("inc", true).getString(0).trim();
String gsName = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("gsName", true).getString(0).trim();
String csName = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("csName", true).getString(0).trim();

System.out.println("gsName........."+gsName);
System.out.println("csName........."+csName);
String gsName2 = "gsName";
GlobalVariable gsGV = null;

if(!gsName.equals(csName)){
	gsGV = new GlobalVariable(gsName2,csName);
	IBaseInfo ibaseInfo = (IBaseInfo)((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO));
	ibaseInfo.getGlobalVariables().put(gsName2,gsGV);
	
	String zero = "0";
	inc = zero;
}

if(inc.equalsIgnoreCase("")){
	String zero = "0";
	inc = zero;
}
System.out.println("inc............"+inc);
%>
<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_inc" value=<%=inc%>>
<input type="hidden" name="hatsgv_isPin" value=""></input>

<table height="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td valign="top">
<%		String checkDots = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("checkDots", true).getString(0).trim();
		if(checkDots.equalsIgnoreCase("......")){
			System.out.println("first if is running..........");			
%>
			<HATS:DefaultRendering renderingSet="customRS" erow="-1" col="1"
				settings="" ecol="-1" row="1" applyTextReplacement="true"
				applyGlobalRules="true" />		
<%		
		}else{
%>
			
				<HATS:DefaultRendering renderingSet="customRS" erow="-1" col="1"
				settings="" ecol="-1" row="2" applyTextReplacement="true"
				applyGlobalRules="true" />

<%	
	
		}	
		
%>
    </td>
  </tr>

  
  
  
</table>

<%
	String curTab = "";
	
	if(request.getParameter("curTab") != null) {
		curTab = request.getParameter("curTab").trim();
%>
<input type="hidden" name="currentTab" value="<%=curTab %>">
<%
	}
	
%>

<input type="hidden" name="actionCode" value="">


<%
String checkMenu_1 = "";
HostScreen hostScreen_1 = ((TransformInfo) request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getHostScreen();

if(hostScreen_1.getFieldAtPos(01,003) != null){
	checkMenu_1 = hostScreen_1.getFieldAtPos(01,003).getFieldString().trim();
}

System.out.println("checkMenu_1"+checkMenu_1);

if(checkMenu_1.equalsIgnoreCase("Item Menu")){
%>
	<script>
	var selectEle = document.getElementsByTagName('select')[0];
	selectEle.size = 7;
	selectEle.style.height = "135px";
	selectEle.style.width = "150px";
	</script>
<%
}
%>
<script>
//document.getElementById('p_407').setAttribute("colspan","30");
//alert(document.getElementById('p_407'));
</script>

<%

int incInt =Integer.parseInt(inc);
if(incInt<=0){
%>
<script>
if(document.getElementById('prevbtn')!=null){
document.getElementById('prevbtn').style.display = "none";
}
</script>
<%
}
%>
</HATS:Form>

<script type="text/javascript">
if(document.getElementById('p_322')!=null && document.getElementById('p_882')!=null && document.getElementById('p_1602')!=null){

document.getElementById('p_322').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_402').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_482').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_562').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_642').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_722').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_802').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_882').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_962').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_1042').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_1122').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_1202').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_1282').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_1362').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_1442').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_1522').setAttribute("onclick","setActionCode(this)");
document.getElementById('p_1602').setAttribute("onclick","setActionCode(this)");

}

if(document.getElementById('p_562') != null && document.getElementById('p_562').innerHTML=="Stocking&nbsp;Item&nbsp;.&nbsp;.") {
				 	//document.getElementById("1475_9").style.display = "none";
				 	//document.getElementById('p_581').innerHTML= "<img style='cursor: pointer;' title='Please click to send F4 command' border='0' src='../common/images/find.png' align='absmiddle' onclick='sendF4Prompt(580);'>";	 
				}


</script>

</div>

</body>
</html>
