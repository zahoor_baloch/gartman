<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
<script src="../common/SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="../common/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<div class="screenshowtxt09">
<HATS:Component col="2" alternate=""
					widget="com.ibm.hats.transform.widgets.LabelWidget"
					alternateRenderingSet="" erow="3" textReplacement=""
					widgetSettings="trim:true|style:|labelStyleClass:|"
					type="com.ibm.hats.transform.components.TextComponent" ecol="36"
					componentSettings="" row="3" />
</div>

<div id="TabbedPanels1" class="TabbedPanels">
      <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0" style="margin-right:5px; outline:none;">Graph</li>
        <li class="TabbedPanelsTab" tabindex="0" style="margin-right:5px; outline:none;">Data</li>
      </ul>
      <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent2" align="left">
        	<HATS:Component col="2" alternate=""
					widget="com.ibm.hats.transform.widgets.LineGraphWidget"
					alternateRenderingSet="" erow="20" textReplacement=""
					widgetSettings="width:1030|backgroundColor:#ffffff|dataSource12:13|height:430|dataSource11:12|extractSource:col|dataSource10:11|dataSource9:10|dataSource8:9|extractDataSetLabels:true|dataSource7:8|dataSource6:7|dataSource5:6|dataSource4:5|headerRows:1|dataSetLabelIndex:1|dataSource3:4|dataSource2:3|dataSource1:2|dataSource2Color:#00ff00|dataSetNumber:5|yAxisTitle:|dataSource4Color:#ffff00|dataSource6Color:#00ffff|dataSource8Color:#00c000|dataSource11Color:#c000c0|textAntialiasing:true|backgroundImage:graph.png|headerColumns:1|axisColor:#000000|dataSource1Color:#ff0000|dataSource3Color:#0000ff|labelColor:#000000|defaultFont:SansSerif-(default)-12|dataSource5Color:#00ffff|dataSource10Color:#c0c000|dataSource7Color:#c00000|labelIndex:1|dataSource12Color:#00c0c0|dataSource9Color:#808080|alternateText:Five Year Comparative Line Graph|bgroot:C:\Workspaces\Gartman_CVS\Gartman\Web Content\common\images\|extractLabels:true|dataSource2Legend:|dataSource7Legend:Jul|dataSource1Legend:|dataSource4Legend:|dataSource9Legend:Sep|dataSource6Legend:2010|dataSource3Legend:|dataSource8Legend:Aug|dataSource5Legend:|dataSource10Legend:Oct|dataSource12Legend:Dec|xAxisTitle:|dataSource11Legend:Nov|"
					type="com.ibm.hats.transform.components.TableComponent" ecol="102"
					componentSettings="startCol:2|endRow:20|columnBreaks:14,27,40,48,61,69,82,90|endCol:102|excludeCols:4,6,8|visualTableDefaultValues:false|startRow:8|numberOfTitleRows:0|excludeRows:|includeEmptyRows:true|"
					row="8" />       
        </div>
        <div class="TabbedPanelsContent2">
        
        <div class="maintopmenu09">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
      <tr>
        <td align="left" class="tabtxthead barhead4new2">Period </td>
        <td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="25"
				componentSettings="" row="8" /></td>
        <td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="38"
				componentSettings="" row="8" /></td>
        <td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="8" /></td>
        <td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="59"
				componentSettings="" row="8" /></td>
        <td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="8" /></td>
        <td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="80"
				componentSettings="" row="8" /></td>
        <td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="8" /></td>
        <td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="101"
				componentSettings="" row="8" /></td>
			<td align="left" class="tabtxthead barhead4new2"><HATS:Component
				col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="8" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="8" /></td>
		</tr>
      <tr class="oddbar09">
        <td>January</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="9" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="9" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="9" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="9" /></td>
		</tr>
      <tr class="evenbar09">
        <td>February</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="10" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="10" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="10" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="10" /></td>
		</tr>
      <tr class="oddbar09">
        <td>March</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="11" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="11" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="11" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="11" /></td>
		</tr>
      <tr class="evenbar09">
        <td>April</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="12" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="12" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="12" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="12" /></td>
		</tr>
      <tr class="oddbar09">
        <td>May</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="13" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="13" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="13" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="13" /></td>
		</tr>
      <tr class="evenbar09">
        <td>June</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="14" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="14" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="14" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="14" /></td>
		</tr>
      <tr class="oddbar09">
        <td>July</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="15" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="15" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="15" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="15" /></td>
		</tr>
      <tr class="evenbar09">
        <td>August</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="16" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="16" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="16" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="16" /></td>
		</tr>
      <tr class="oddbar09">
        <td>September</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="17" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="17" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="17" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="17" /></td>
		</tr>
      <tr class="evenbar09">
        <td>October</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="18" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="18" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="18" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="18" /></td>
		</tr>
      <tr class="oddbar09">
        <td>November</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="19" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="19" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="19" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="19" /></td>
		</tr>
      <tr class="evenbar09">
        <td>December</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="20" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="20" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="20" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="20" /></td>
		</tr>
      <tr><td colspan="9" height="2" bgcolor="#0066CC"></td>
			<td height="2" bgcolor="#0066CC"></td>
		</tr>
      <tr class="oddbar09 totaltxt">
        <td>Total</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="21" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="21" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="21" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="21" /></td>
		</tr>
      <tr class="evenbar09">
        <td>Averag</td>
        <td align="right"><HATS:Component col="15" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="26"
				componentSettings="" row="22" /></td>
        <td align="right"><HATS:Component col="28" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="39"
				componentSettings="" row="22" /></td>
        <td align="right"><HATS:Component col="41" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="47"
				componentSettings="" row="22" /></td>
        <td align="right"><HATS:Component col="49" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="60"
				componentSettings="" row="22" /></td>
        <td align="right"><HATS:Component col="62" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="68"
				componentSettings="" row="22" /></td>
        <td align="right"><HATS:Component col="70" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="81"
				componentSettings="" row="22" /></td>
        <td align="right"><HATS:Component col="83" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="89"
				componentSettings="" row="22" /></td>
        <td align="right"><HATS:Component col="91" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="102"
				componentSettings="" row="22" /></td>
			<td align="right"><HATS:Component col="104" alternate=""
				widget="com.ibm.hats.transform.widgets.LabelWidget"
				alternateRenderingSet="" erow="22" textReplacement=""
				widgetSettings="trim:true|style:|labelStyleClass:|"
				type="com.ibm.hats.transform.components.TextComponent" ecol="110"
				componentSettings="" row="22" /></td>
		</tr>
      </table>
      </div>
        
        </div>
      </div>
    </div>



<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
//-->
</script>
</HATS:Form>

</body>
</html>