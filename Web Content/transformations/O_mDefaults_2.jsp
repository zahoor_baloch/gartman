<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>
<body>
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>


<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<div class="clear17px"></div>
<table cellpadding="0" cellspacing="0" width="100%" border="0">
  <tr>
  	<td align="center" valign="top">
    	<table cellpadding="0" cellspacing="1" width="60%" border="0" bgcolor="#536F97">
              <tr>
                  <td align="left" valign="middle" class="omText" width="80%">
                      Protect order number override field?
                  </td>
                  <td align="left" valign="middle" class="omText" width="20%">
                      <span class="coptions"><HATS:Component col="56"
						alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="3" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="56"
						componentSettings="" row="3" /> (/N)</span>
                  </td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Default all orders to ship complete?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <span class="coptions"><HATS:Component col="58"
						alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="4" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="58"
						componentSettings="" row="4" /></span></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Do you wish to have the system automatically show the header screen on each order?
                  </td>
                  <td align="left" valign="middle" class="omText"><span
						class="coptions"><HATS:Component col="53" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="7" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="53"
						componentSettings="" row="7" /></span>&nbsp;<span class="coptions">(/N)</span>
                  </td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Do you wish to have the system automatically show the misc charges screen on each order?
                  </td>
                  <td align="left" valign="middle" class="omText"><span
						class="coptions"><HATS:Component col="56" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="10" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="56"
						componentSettings="" row="10" /></span>&nbsp;<span class="coptions">(/N)</span>
                  </td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Do you wish text to be upper case ONLY?
                  </td>
                  <td align="left" valign="middle" class="omText"><span
						class="coptions"><HATS:Component col="57" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="12" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="57"
						componentSettings="" row="12" /></span>&nbsp;<span class="coptions">(/N)</span>
                  </td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Show sales tax inquiry on ship to override?
                  </td>
                  <td align="left" valign="middle" class="omText"><span
						class="coptions"><HATS:Component col="61" alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="13" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="61"
						componentSettings="" row="13" /></span></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Show sales inquiry on each line item?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <span class="coptions"><HATS:Component col="59"
						alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="14" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="59"
						componentSettings="" row="14" /></span></td>
              </tr>
              
              <tr>
                  <td align="left" valign="middle" class="omText">
                      Use pull down instead of pop up menu?
                  </td>
                  <td align="left" valign="middle" class="omText">
                  <span class="coptions"><HATS:Component col="59"
						alternate=""
						widget="com.ibm.hats.transform.widgets.DropdownWidget"
						alternateRenderingSet="" erow="15" textReplacement=""
						widgetSettings="tableCellStyleClass:|autoSubmitKey:[enter]|useString:true|tableRowStyleClass:|tableStyleClass:|autoSubmitOnSelect:false|captionType:DESCRIPTION|columnsPerRow:1|submitButtonCaption:Submit|labelStyleClass:|tableCaptionCellStyleClass:|useGlobalVariable:false|valueVariable:|size:1|stringListItems:Y=Y;N=N;|captionSource:VALUE|sharedGVs:false|style:|useHints:true|dropdownStyleClass:field1|optionStyleClass:|showSubmitButton:false|caption:|captionVariable:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="59"
						componentSettings="" row="15" /></span></td>
              </tr>
              
              
        </table>
    </td>
  </tr>
  
</table>


</HATS:Form>
</body>
</html>
