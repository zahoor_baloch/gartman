<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
<script src="../common/SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="../common/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<script language="javascript">

function checkWidth() {
if (parent.document.getElementById("hatsFrame").width == 1050)
{document.getElementById("colcenterover").className = "contentbgmainright"; }
else if (parent.document.getElementById("hatsFrame").width == 1208) 
{document.getElementById("colcenterover").className = "contentbgmainrightfull";}

}

</script>
</head>
<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>
<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<script language="JavaScript">
function setCursorAndGo(val) {
	setCursorPosition(val, 'HATSForm');
	ms('[enter]', 'HATSForm');
}
</script>


<!--Menu Start-->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center">
	<div>
    
    <div class="topbarrmain">
	<table width="100%" height="15" border="0" cellspacing="0" cellpadding="0" class="topbarr" style="border-collapse:collapse;">
		<tr>
		<td width="4%" align="center" bgcolor="#FFFFFF">
		
		<HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="4"
						componentSettings="" row="2" /></td>
		<td width="4%" align="center" bgcolor="#FFFFFF" class="leftline2"><HATS:Component
						col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="7"
						componentSettings="" row="2" />
		</td>
		<td width="11%" class="topbarrtxt">*All Locations</td>
		<td>&nbsp;</td>
		<td width="6%" align="right" class="topbarrtxt">As of :</td>
		<td width="11%" align="center" bgcolor="#FFFFFF">
		<HATS:Component col="122" alternate=""
						widget="com.ibm.hats.transform.widgets.CalendarWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|useInlineCalendar:true|overrideSize:false|trimSpacesOnInputs:false|pattern:MM-dd-yyyy|inputFieldStyleClass:fld2|launcherImage:calendar.gif|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|rangeStart:|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|rangeEnd:|launcherButtonStyleClass:HATSBUTTON|underlineStyle:text-decoration: underline|defaultValue:|dataModeCEPRepresentation:link|maxlen:5|cursorModeCEPValue:*|cursorModeCEPRepresentation:link|restrictMinDate:false|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|launcherType:IMAGE|patternLocaleSelected:|launcherCaption:Date...|restrictMaxDate:false|launcherLinkStyleClass:HATSLINK|preserveColors:false|enableInputRestrictions:true|labelStyleClass:asdf|cursorModeCEPAltValue:|blinkStyle:font-style: italic|patternLocale:SERVER|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="131"
						componentSettings="" row="2" /></td>
		</tr>
	</table>
		<div class="clear5px"></div>
	</div>
    
	<div class="boxloanpage">

	<div id="TabbedPanels1" class="TabbedPanels">
		<ul class="TabbedPanelsTabGroup">
		<li class="TabbedPanelsTab" tabindex="0" style="margin-right:5px; outline:none;">Tab 01</li>
		<li class="TabbedPanelsTab" tabindex="0" style="margin-right:5px; outline:none;">Tab 02</li>
		</ul>
	<div class="TabbedPanelsContentGroup">
	<div class="TabbedPanelsContent">
	<div class="maintopmenu58">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td valign="top" width="230">
	<table width="auto" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr>
		<td class="tabtxthead barhead4new3">Menu</td>
		</tr>
		<tr>
		<td>
        <div class="leftbtnbg">
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('398');">Company Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('530');">Location Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('662');">Order Type Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('794');">Sales Manager</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('926');">Sales Rep System</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1058');">Vendor Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1190');">Bill to Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1322');">Customer Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1454');">Product Sales Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1586');">Family Sales Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1718');">Buying Group Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1850');">Segment Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1982');">Route Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('2114');">Open Order Summary Inquiry</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('2246');">A/R Balances by Location Inq</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('2378');">Inventory Value Inquiry</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('2510');">Division Sales Analysis</a></div>
        </div>
        </td>
		</tr>
	</table>
        </td>
		<td width="5"></td>
		<td valign="top">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td colspan="3" valign="top">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="commaintab">
		          <tr>
		            <td align="left" class="tabtxthead barhead4new">YTD Through....</td>
		            <td align="left" class="tabtxthead barhead4new"><HATS:Component
										col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="3" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="3" /></td>
		            <td align="left" class="tabtxthead barhead4new"><HATS:Component
										col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="3" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="3" /></td>
		            <td align="left" class="tabtxthead barhead4new">Change</td>
		            </tr>
		          <tr class="oddbar58">
		            <td>Sales..........</td>
		            <td><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="4" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="4" /></td>
		            <td><HATS:Component col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="4" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="4" /></td>
		            <td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="4" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="79" componentSettings="" row="4" /></td>
		            </tr>
		          <tr class="evenbar58">
		            <td>Gross Profit...</td>
		            <td><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="5" /></td>
		            <td><HATS:Component col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="5" /></td>
		            <td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="79" componentSettings="" row="5" /></td>
		            </tr>
		          <tr class="oddbar58">
		            <td>Gross Profit %.</td>
		            <td><HATS:Component col="54" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="6" /></td>
		            <td><HATS:Component col="67" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="6" /></td>
		            <td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="6" /></td>
		            </tr>
		          <tr class="evenbar58">
		            <td>Sales Budget...</td>
		            <td><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="7" /></td>
		            <td><HATS:Component col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="7" /></td>
		            <td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="79" componentSettings="" row="7" /></td>
		            </tr>
		          <tr class="oddbar58">
		            <td>Profit Budget..</td>
		            <td><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="8" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="8" /></td>
		            <td><HATS:Component col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="8" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="8" /></td>
		            <td><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="8" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="79" componentSettings="" row="8" /></td>
		            </tr>
		          </table>
		  </td>
		</tr>
		<tr><td colspan="3" height="0"></td></tr>
		<tr>
		<td colspan="3" valign="top">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="commaintab">
		    <tr>
		      <td width="58%" align="left" class="tabtxthead barhead4new">Top five sales reps</td>
		      <td width="25%" align="left" class="tabtxthead barhead4new"><HATS:Component
										col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="9" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="9" /></td>
		      <td align="left" class="tabtxthead barhead4new">YTD Sales</td>
		      </tr>
		    <tr class="oddbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="10" />&nbsp;</td>
		      <td><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="10" /></td>
		      <td><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="10" /></td>
		      </tr>
		    <tr class="evenbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="11" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="11" />&nbsp;</td>
		      <td><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="11" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="11" /></td>
		      <td><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="11" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="11" /></td>
		      </tr>
		    <tr class="oddbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="12" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="12" />&nbsp;</td>
		      <td><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="12" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="12" /></td>
		      <td><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="12" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="12" /></td>
		      </tr>
		    <tr class="evenbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="13" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="13" />&nbsp;</td>
		      <td><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="13" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="13" /></td>
		      <td><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="13" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="13" /></td>
		      </tr>
		    <tr class="oddbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="14" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="14" />&nbsp;</td>
		      <td><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="14" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="14" /></td>
		      <td><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="14" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="14" /></td>
		      </tr>
		    </table>
		  </td>
		</tr>
		<tr><td colspan="3" height="0"></td></tr>
		<tr>
		<td width="58%">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr>
		<td align="left" class="tabtxthead barhead4new2">Top five customers</td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2013');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="16" />&nbsp;</td>
		</tr>
		<tr class="evenbar">
		<td onclick="setCursorAndGo('2145');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="17" />&nbsp;</td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2277');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="18" />&nbsp;</td>
		</tr>
		<tr class="evenbar">
		<td onclick="setCursorAndGo('2409');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="19" />&nbsp;</td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2541');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="20" />&nbsp;</td>
		</tr>
		</table>
		</td>
		<td width="25%">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr>
		<td align="left" class="tabtxthead barhead4new2"><HATS:Component
										col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="15" /></td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2038');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="16" /></td>
		</tr>
		<tr class="evenbar">
		<td onclick="setCursorAndGo('2170');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="17" /></td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2302');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="18" /></td>
		</tr>
		<tr class="evenbar">
		<td onclick="setCursorAndGo('2434');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="19" /></td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2566');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="20" /></td>
		</tr>
	</table>
		</td>
		<td align="right">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr>
		<td align="left" class="tabtxthead barhead4new2">YTD Sales</td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2050');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="16" /></td>
		</tr>
		<tr class="evenbar">
		<td onclick="setCursorAndGo('2182');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="17" /></td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2314');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="18" /></td>
		</tr>
		<tr class="evenbar">
		<td onclick="setCursorAndGo('2446');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="19" /></td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2578');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="20" /></td>
		</tr>
	</table>
		</td>
		</tr>
	</table>
    	</td>
		</tr>
	</table>
	</div>
	</div>
	<div class="TabbedPanelsContent">
	<div class="maintopmenu58">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td valign="top">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		    <tr>
		      <td align="right">&nbsp;</td>
		      <td align="right" class="tabtxthead barhead4new">Today</td>
		      <td class="tabtxthead barhead4new">&nbsp;</td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Invoices</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('610');"><HATS:Component col="82"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="85"
								componentSettings="" row="5" /></td>
		      <td align="right" valign="top" class="leftline" onclick="setCursorAndGo('610');"><HATS:Component
								col="87" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="5" /></td>
		      </tr>
		    <tr class="evenbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Sales Orders </td>
		      <td align="right" valign="top" onclick="setCursorAndGo('874');"><HATS:Component col="82"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="85"
								componentSettings="" row="7" /></td>
		      <td align="right" valign="top" class="leftline" onclick="setCursorAndGo('874');"><HATS:Component
								col="87" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="7" /></td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Cash</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1138');"></td>
		      <td align="right" valign="top" class="leftline" onclick="setCursorAndGo('1138');"><HATS:Component
								col="82" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="93"
								componentSettings="" row="9" /></td>
		      </tr>
		    <tr class="evenbar" height="33">
		      <td valign="top" class="tabtxthead barheadrpt">Invoice</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1402');">&nbsp;</td>
		      <td align="right" valign="top" class="leftline" onclick="setCursorAndGo('1402');"><HATS:Component
								col="82" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="93"
								componentSettings="" row="11" /></td>
		      </tr>
		    <tr class="evenbar" height="33">
		      <td valign="top" class="tabtxthead barheadrpt">Checks</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1534');">&nbsp;</td>
		      <td align="right" valign="top" class="leftline" onclick="setCursorAndGo('1534');"><HATS:Component
								col="82" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="93"
								componentSettings="" row="12" />-</td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Cuts</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1798');"><HATS:Component col="82"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="87"
								componentSettings="" row="14" /></td>
		      <td align="right" valign="top" class="leftline" onclick="setCursorAndGo('1798');">&nbsp;</td>
		      </tr>
		    <tr class="evenbar58" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Days Sales in Inv</td>
		      <td colspan="2" align="right" valign="top"><HATS:Component
								col="89" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="16" /></td>
		      </tr>
		    <tr class="oddbar58" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Turns</td>
		      <td colspan="2" align="right" valign="top"><HATS:Component
								col="89" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="18" /></td>
		      </tr>
		    <tr class="evenbar58" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Index</td>
		      <td colspan="2" align="right" valign="top"><HATS:Component
								col="89" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="20" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="20" /></td>
		      </tr>
		    <tr class="oddbar59" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>
		      <td colspan="2" align="right" valign="top">&nbsp;</td>
		      </tr>
		    </table>
		  </td>
		<td valign="top">
		  <table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		    <tr>
		      <td width="80" align="right" class="tabtxthead barhead4new">Elapsed</td>
		      <td width="50" align="right" class="tabtxthead barhead4new ">0:00:00</td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td align="right" valign="top">&nbsp;<HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="103" componentSettings="" row="5" /></td>
		      <td align="right" valign="top" class="leftline"><HATS:Component
								col="105" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="5" /></td>
		      </tr>
		    <tr class="evenbar" height="37">
		      <td align="right" valign="top">&nbsp;<HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="103" componentSettings="" row="7" /></td>
		      <td align="right" valign="top" class="leftline"><HATS:Component
								col="105" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="7" /></td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td align="right" valign="top">&nbsp;</td>
		      <td align="right" valign="top" class="leftline"><HATS:Component
								col="100" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="111" componentSettings="" row="9" /></td>
		      </tr>
		    <tr class="evenbar" height="33">
		      <td align="right" valign="top"><HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="111" componentSettings="" row="11" /></td>
		      <td align="right" valign="top" class="leftline">&nbsp;</td>
		      </tr>
		    <tr class="evenbar" height="33">
		      <td align="right" valign="top">&nbsp;<HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="111" componentSettings="" row="12" /></td>
		      <td align="right" valign="top" class="leftline">&nbsp;</td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td align="right" valign="top">&nbsp;<HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="105" componentSettings="" row="14" /></td>
		      <td align="right" valign="top" class="leftline">&nbsp;</td>
		      </tr>
		    <tr class="evenbar58" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Days Sales in A/R</td>
		      <td align="right" valign="top"><HATS:Component col="107"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="16" /></td>
		      </tr>
		    <tr class="oddbar58" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Profits %</td>
		      <td align="right" valign="top"><HATS:Component col="107"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="18" /></td>
		      </tr>
		    <tr class="evenbar58" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">G.M.R.O.I</td>
		      <td align="right" valign="top"><HATS:Component col="107"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="20" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="20" /></td>
		      </tr>
		    <tr class="oddbar59" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>
		      <td align="right" valign="top">&nbsp;</td>
		      </tr>
		    </table>
		  </td>
		<td valign="top">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		    <tr>
		      <td>&nbsp;</td>
		      <td class="tabtxthead barhead4new">Last  Night</td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Open Orders</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('656');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="5" /><br />
							<HATS:Component col="123" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="6" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="6" /></td>
		      </tr>
		    <tr class="evenbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">A/R</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1052');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="8" /></td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Inventory</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1316');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="10" /></td>
		      </tr>
		    <tr class="evenbar" height="33">
		      <td valign="top" class="tabtxthead barheadrpt">P</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1580');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="12" /></td>
		      </tr>
		    <tr class="evenbar" height="33">
		      <td valign="top" class="tabtxthead barheadrpt">O</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1712');"><HATS:Component col="123"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="13" /></td>
		      </tr>
		    <tr class="oddbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">Open Recvrs</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1976');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="15" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="15" /></td>
		      </tr>
		    <tr class="evenbar" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">A/P</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('2240');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="17" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="17" /></td>
		      </tr>
		    <tr class="oddbar59" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>
		      <td align="right" valign="top">&nbsp;</td>
		      </tr>
		    <tr class="evenbar59" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>
		      <td align="right" valign="top">&nbsp;</td>
		      </tr>
		    <tr class="oddbar59" height="37">
		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>
		      <td align="right" valign="top">&nbsp;</td>
		      </tr>
		    </table>
		  </td>
		<td valign="top">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="right" valign="top">
	<table width="99%" border="0" cellpadding="0" cellspacing="0" class="tabtxt" style="border-collapse:collapse;">
		<tr>
		<td width="35%" align="right" class="tabtxthead barhead4new2">CSR</td>
		<td width="25%" align="right" class="tabtxthead barhead4new2">Orders</td>
		<td align="right" class="tabtxthead barhead4new2">$ Amount</td>
 		</tr>
	</table>
    <div class="clear"></div>
	<div class="companyinnerboxmain2">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr class="oddbarana" onclick="setCursorAndGo('2782');">
		<td width="33%" align="right" valign="top"><HATS:Component col="10"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="19" componentSettings="" row="22" /></td>
		<td width="26%" align="right" valign="top"><HATS:Component col="21"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="26" componentSettings="" row="22" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="28" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="43" componentSettings="" row="22" /></td>
		</tr>
		<tr class="evenbarana" onclick="setCursorAndGo('2914');">
		<td align="right" valign="top"><HATS:Component col="10" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="19" componentSettings="" row="23" /></td>
		<td align="right" valign="top"><HATS:Component col="21" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="26" componentSettings="" row="23" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="28" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="43" componentSettings="" row="23" /></td>
		</tr>
		<tr class="oddbarana" onclick="setCursorAndGo('3046');">
		<td align="right" valign="top"><HATS:Component col="10" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="19" componentSettings="" row="24" /></td>
		<td align="right" valign="top"><HATS:Component col="21" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="26" componentSettings="" row="24" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="28" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="43" componentSettings="" row="24" /></td>
		</tr>
		<tr class="evenbarana" onclick="setCursorAndGo('3178');">
		<td align="right" valign="top"><HATS:Component col="10" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="19" componentSettings="" row="25" /></td>
		<td align="right" valign="top"><HATS:Component col="21" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="26" componentSettings="" row="25" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="28" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="43" componentSettings="" row="25" /></td>
		</tr>
		<tr class="oddbarana" onclick="setCursorAndGo('2822');">
		<td align="right" valign="top"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="22" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="22" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="68" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="83" componentSettings="" row="22" /></td>
		</tr>
		<tr class="evenbarana" onclick="setCursorAndGo('2954');">
		<td align="right" valign="top"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="23" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="23" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="68" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="83" componentSettings="" row="23" /></td>
		</tr>
		<tr class="oddbarana" onclick="setCursorAndGo('3086');">
		<td align="right" valign="top"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="24" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="24" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="68" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="83" componentSettings="" row="24" /></td>
		</tr>
		<tr class="evenbarana" onclick="setCursorAndGo('3218');">
		<td align="right" valign="top"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="25" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="25" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="68" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="83" componentSettings="" row="25" /></td>
		</tr>
		<tr class="oddbarana" onclick="setCursorAndGo('2862');">
		<td align="right" valign="top"><HATS:Component col="90" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="99" componentSettings="" row="22" /></td>
		<td align="right" valign="top"><HATS:Component col="101" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="106" componentSettings="" row="22" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="108" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="123" componentSettings="" row="22" /></td>
		</tr>
        <tr class="evenbarana" onclick="setCursorAndGo('2994');">
		<td align="right" valign="top"><HATS:Component col="90" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="99" componentSettings="" row="23" /></td>
		<td align="right" valign="top"><HATS:Component col="101" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="106" componentSettings="" row="23" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="108" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="123" componentSettings="" row="23" /></td>
		</tr>
		<tr class="oddbarana" onclick="setCursorAndGo('3126');">
		<td align="right" valign="top"><HATS:Component col="90" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="99" componentSettings="" row="24" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="24" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="108" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="123" componentSettings="" row="24" /></td>
		</tr>
        <tr class="evenbarana" onclick="setCursorAndGo('3258');">
		<td align="right" valign="top"><HATS:Component col="90" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="99" componentSettings="" row="25" /></td>
		<td align="right" valign="top"><HATS:Component col="101" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="106" componentSettings="" row="25" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="108" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="123" componentSettings="" row="25" /></td>
		</tr>
		<tr class="oddbarana">
		<td align="right" valign="top"></td>
		<td align="right" valign="top"></td>
		<td align="right" valign="top">&nbsp;</td>
		</tr>

        </table>
	</div>
		</td>
		</tr>
	</table>
    	</td>
		</tr>
	</table>
	</div>
	</div>
              

	</div>
	</div>
	</div>
    
    

	</div>
	</td>
	</tr>
	</table>
	<!--Menu End-->


<script type="text/javascript">
screenName = "ExecutiveMenu";
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
//-->
</script>

</HATS:Form>

</body>
</html>