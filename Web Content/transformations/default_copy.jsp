<% out.println("<!--default.jsp"); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<html>
<head>
<link href="../common/css/menu.css" rel="stylesheet" type="text/css" />

<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
</head>

<body>
<% out.println("-->"); %>   
<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
</script>

<script type="text/javascript">
    portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
</script>

<script type="text/javascript">
    PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<div align="center">

<HATS:Form>

<table>
  <tr>
    <td>
      <HATS:DefaultRendering/>
    </td>
  </tr>
<!--  <tr>-->
<!--    <td align="center">-->
<!--      <HATS:HostKeypad />-->
<!--    </td>-->
<!--  </tr>-->
<!--  <tr>-->
<!--   <td align="center">-->
<!--      <HATS:OIA/>-->
<!--   </td>-->
<!--  </tr>-->
</table>

</HATS:Form>

</div>
</body>
</html>
