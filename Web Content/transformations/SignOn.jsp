<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@ page import="com.ibm.hats.common.*"%>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../common/css/style.css" rel="stylesheet" type="text/css" />

<HATS:VCTStylesLink/>
</head>
<body>

<script id="1" type="text/javascript" src="../common/env.js">
</script>

<script id="2" type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script id="3" type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script id="4" type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<div class="clear80px"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td align="left">
<div class="login_bg">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td width="300">
              <div class="clear35px"></div>
              <div class="logintxt">User Id :</div>
              </td>
            <td>
            <div class="clear35px"></div>

<HATS:Component col="53" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="6" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:logginfield|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="62"
				componentSettings="" row="6" />
            </td>
          </tr>
          <tr>
            <td>
              <div class="clear25px"></div>
              <div class="logintxt">Password :</div>
              </td>
            <td>
              <div class="clear25px"></div>
<HATS:Component
				col="53" alternate=""
				widget="com.ibm.hats.transform.widgets.InputWidget"
				alternateRenderingSet="" erow="7" textReplacement=""
				widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:logginfield|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
				type="com.ibm.hats.transform.components.InputComponent" ecol="62"
				componentSettings="" row="7" />
              </td>
          </tr>
          <tr>
              <td align="right" colspan="2">
              <div class="clear39px"></div>
              <div class="clickbtnmain">
              <div class="clickherebtn">
<input type="button" name="[enter]" class="HostButton" accesskey="HATSForm" value="Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" onclick="ms('[enter]','HATSForm')">
              </div>
              </div>
              </td>
            </tr>
        </table>
</div>
</td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
        <td align="center" colspan="2" height="40" valign="middle" class="errorMsg">
        <%String errorMsg = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("errorMsgOnSignOn", true).getString(0);%>

<%if (errorMsg.length()>0 && errorMsg.indexOf("(C) COPYRIGHT IBM CORP. 1980, 2007.")==-1) {
	System.out.println("Error Message:............." + errorMsg); 
	out.print(errorMsg);%>
	
	<% 	}%>
        </td>
        </tr>
        </table>
</td>
</tr>
</table>

<script language="JavaScript">
top.parent.document.getElementById("isMouseMoveAllowed").value ="false";
//alert(top.parent.document.getElementById("isMouseMoveAllowed").value);
top.parent.document.getElementById("indexbody").removeAttribute("onmousemove");
document.getElementById("gdb").removeAttribute("onmousemove");

top.parent.document.getElementById('menuFrameDiv').style.display = "none";

top.parent.document.onmousemove="";
parent.document.getElementById("checkMaster").value="check";

</script>

<%
	if(session.getAttribute("abc") != null) {
		session.removeAttribute("abc");
	}
%>
<%
	session.setAttribute("html",null);
%>
</HATS:Form>

</body>
</html>
