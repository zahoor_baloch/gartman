<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" %>
<%@ taglib uri="hats.tld" prefix="HATS" %>
<%@ page import="com.ibm.hats.common.*"%>
<html>
<head>
<meta name="GENERATOR" content="IBM WebSphere Studio">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<HATS:VCTStylesLink/>
<script src="../common/SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="../common/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<script language="javascript">

function checkWidth() {
if (parent.document.getElementById("hatsFrame").width == 1050)
{document.getElementById("colcenterover").className = "contentbgmainright"; }
else if (parent.document.getElementById("hatsFrame").width == 1208) 
{document.getElementById("colcenterover").className = "contentbgmainrightfull";}

}

</script>
</head>

<body>

<script type="text/javascript" src="../common/env.js">
</script>

<script type="text/javascript" src="../common/lxgwfunctions.js">
  	portletID="hatsportletid";activeID="default";formID="HATSForm";
</script>

<script type="text/javascript" src="../common/KBS.js">
  	PortletKBInited['hatsportletid']=false;
</script>

<script type="text/javascript" src="../common/HatsJS.js">
</script>

<HATS:Form>

<input type="hidden" name="hatsgv_promptGV1">
<input type="hidden" name="hatsgv_promptGV2">
<input type="hidden" name="hatsgv_promptGV3">
<input type="hidden" name="hatsgv_promptGV4">
<input type="hidden" name="hatsgv_promptGV5">
<input type="hidden" name="curTab" id="curTab">
<input type="hidden" name="hatsgv_macroName">
<input type="hidden" name="hatsgv_isPin" value=""></input>

<script language="JavaScript">
function setCursorAndGo(val) {
	setCursorPosition(val, 'HATSForm');
	ms('[enter]', 'HATSForm');
}

function setCurrentTabName(currTab) {
	document.getElementById('curTab').value = currTab;
}

</script>


<!--Menu Start-->
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center">
<!--	<div class="contentbgmainright" id="colcenterover">-->
    
    <div class="topbarrmain">
	<table width="100%" height="15" border="0" cellspacing="0" cellpadding="0" class="topbarr" style="border-collapse:collapse;">
		<tr>
		<td width="4%" align="center" bgcolor="#FFFFFF">
		<HATS:Component col="2" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="4"
						componentSettings="" row="2" />
		</td>
		<td width="4%" align="center" bgcolor="#FFFFFF" class="leftline2">
		<HATS:Component
						col="6" alternate=""
						widget="com.ibm.hats.transform.widgets.InputWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|trimSpacesOnInputs:false|inputFieldStyleClass:fld1|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|tableStyleClass:|reverseVideoStyle:|captionSource:VALUE|underlineStyle:text-decoration: underline|cursorModeCEPValue:*|maxlen:5|dataModeCEPRepresentation:link|cursorModeCEPRepresentation:link|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|preserveColors:false|enableInputRestrictions:true|labelStyleClass:|blinkStyle:font-style: italic|cursorModeCEPAltValue:|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableRowStyleClass:|tableCaptionCellStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="7"
						componentSettings="" row="2" />
		</td>
		<td width="12%" class="topbarrtxt">*All Locations</td>
		<td>&nbsp;</td>
		<td width="6%" align="right" class="topbarrtxt">As of :</td>
		<td width="12%" align="center" bgcolor="#FFFFFF">
		<HATS:Component col="122" alternate=""
						widget="com.ibm.hats.transform.widgets.CalendarWidget"
						alternateRenderingSet="" erow="2" textReplacement=""
						widgetSettings="eliminateMaxlengthInIdeographicFields:false|dataModeCEPValue:*|readOnly:false|overrideSize:false|useInlineCalendar:true|trimSpacesOnInputs:false|pattern:MM-dd-yyyy|inputFieldStyleClass:fld2|launcherImage:calendar.gif|size:5|cursorCEPRepresentationStyle:background-color: yellow;border: 1px dashed #999999;|enableCheckAttributes:true|enableNumericLock:false|rangeStart:|tableStyleClass:test|reverseVideoStyle:|captionSource:VALUE|rangeEnd:|launcherButtonStyleClass:HATSBUTTON|underlineStyle:text-decoration: underline|defaultValue:|dataModeCEPRepresentation:link|maxlen:5|cursorModeCEPValue:*|cursorModeCEPRepresentation:link|restrictMinDate:false|cursorModeCEPStyle:border: 1px solid #999999;height: 1.75em;|mapExtendedAttributes:false|columnSeparatorStyle:border-width: 1px; border-style: solid|checkboxStyleClass:HATSINPUT|columnsPerRow:1|launcherType:IMAGE|patternLocaleSelected:|launcherCaption:Date...|restrictMaxDate:false|launcherLinkStyleClass:HATSLINK|preserveColors:false|enableInputRestrictions:true|labelStyleClass:asdf|cursorModeCEPAltValue:|patternLocale:SERVER|blinkStyle:font-style: italic|stripUnderlinesOnInputs:false|caption:|tableCellStyleClass:|style:|dataModeCEPAltValue:|useCursorExactPositioningOption:false|overrideMaxLen:false|tableCaptionCellStyleClass:|tableRowStyleClass:|"
						type="com.ibm.hats.transform.components.InputComponent" ecol="131"
						componentSettings="" row="2" />
		</td>
		<td>&nbsp;</td>
		</tr>
	</table>
		<div class="clear5px"></div>
	</div>
    
	<div class="boxloanpage">

	<div id="TabbedPanels1" class="TabbedPanels">
		<ul class="TabbedPanelsTabGroup">
<!--		 TabbedPanelsTabSelected-->
		<li class="TabbedPanelsTab" tabindex="0" style="margin-right:5px; outline:none;" id="YearlyTabLi" >Yearly</li>
		<li class="TabbedPanelsTab" tabindex="0" style="margin-right:5px; outline:none;" id="dailyTabLi" >Daily</li>
		</ul>
	<div class="TabbedPanelsContentGroup">
	<div class="TabbedPanelsContent TabbedPanelsContentVisible" style="display: block; " id="YearlyTab">
	<div class="maintopmenu58">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td valign="top" width="230">
	<table width="auto" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr>
		<td class="tabtxthead barhead4new3">Menu</td>
		</tr>
		<tr>
		<td>
		<div class="leftbtnbg">
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('398');">Company Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('530');">Location Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('662');">Order Type Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('794');">Sales Manager</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('926');">Sales Rep System</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1058');">Vendor Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1190');">Bill to Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1322');">Customer Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1454');">Product Sales Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1586');">Family Sales Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1718');">Buying Group Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1850');">Segment Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('1982');">Route Analysis</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('2114');">Open Order Summary Inquiry</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('2246');">A/R Balances by Location Inq</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('2378');">Inventory Value Inquiry</a></div>
        <div class="leftbtn58"><a href="javascript:void();" onclick="setCursorAndGo('2510');">Division Sales Analysis</a></div>
        </div>
        </td>
		</tr>
	</table>
        </td>
		<td width="5"></td>
		<td valign="top">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td colspan="3" valign="top">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="commaintab">
		    <tr>
		      <td align="left" class="tabtxthead barhead4new" width="150">YTD Through....</td>
		      <td align="right" class="tabtxthead barhead4new" width="120"><HATS:Component
										col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="3" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="3" /></td>
		      <td align="right" class="tabtxthead barhead4new" width="150"><HATS:Component
										col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="3" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="3" /></td>
		      <td align="right" class="tabtxthead barhead4new">Change</td>
		      </tr>
		    <tr class="oddbar58">
		      <td>Sales..........</td>
		      <td align="right"><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="4" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="4" /></td>
		      <td align="right"><HATS:Component col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="4" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="4" /></td>
		      <td align="right"><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="4" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="79" componentSettings="" row="4" /></td>
		      </tr>
		    <tr class="evenbar58">
		      <td>Gross Profit...</td>
		      <td align="right"><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="5" /></td>
		      <td align="right"><HATS:Component col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="5" /></td>
		      <td align="right"><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="5" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="79" componentSettings="" row="5" /></td>
		      </tr>
		    <tr class="oddbar58">
		      <td>Gross Profit %.</td>
		      <td align="right"><HATS:Component col="54" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="6" /></td>
		      <td align="right"><HATS:Component col="67" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="6" /></td>
		      <td align="right"><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="6" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="6" /></td>
		      </tr>
		    <tr class="evenbar58">
		      <td>Sales Budget...</td>
		      <td align="right"><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="7" /></td>
		      <td align="right"><HATS:Component col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="7" /></td>
		      <td align="right"><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="7" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="79" componentSettings="" row="7" /></td>
		      </tr>
		    <tr class="oddbar58">
		      <td>Profit Budget..</td>
		      <td align="right"><HATS:Component col="49" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="8" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="60" componentSettings="" row="8" /></td>
		      <td align="right"><HATS:Component col="62" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="8" textReplacement=""
										widgetSettings="trim:false|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="73" componentSettings="" row="8" /></td>
		      <td align="right"><HATS:Component col="75" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="8" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="79" componentSettings="" row="8" /></td>
		      </tr>
		    </table>
		  </td>
          </tr>
		<tr><td colspan="3" height="0"></td></tr>
		<tr>
		<td colspan="3" valign="top">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="commaintab">
		    <tr>
		      <td width="281" align="left" class="tabtxthead barhead4new">Top five sales reps</td>
		      <td width="150" align="right" class="tabtxthead barhead4new"><HATS:Component
										col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="9" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="9" /></td>
		      <td align="right" class="tabtxthead barhead4new">YTD Sales</td>
		      </tr>
		    <tr class="oddbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="10" />&nbsp;</td>
		      <td align="right"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="10" /></td>
		      <td align="right"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="10" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="10" /></td>
		      </tr>
		    <tr class="evenbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="11" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="11" />&nbsp;</td>
		      <td align="right"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="11" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="11" /></td>
		      <td align="right"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="11" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="11" /></td>
		      </tr>
		    <tr class="oddbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="12" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="12" />&nbsp;</td>
		      <td align="right"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="12" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="12" /></td>
		      <td align="right"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="12" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="12" /></td>
		      </tr>
		    <tr class="evenbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="13" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="13" />&nbsp;</td>
		      <td align="right"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="13" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="13" /></td>
		      <td align="right"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="13" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="13" /></td>
		      </tr>
		    <tr class="oddbar58">
		      <td><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="14" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="14" />&nbsp;</td>
		      <td align="right"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="14" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="14" /></td>
		      <td align="right"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="14" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="14" /></td>
		      </tr>
		    </table>
		  </td>
          </tr>
		<tr><td colspan="3" height="0"></td></tr>
		<tr>
		<td width="293">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr>
		<td align="left" class="tabtxthead barhead4new2">Top five customers</td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2013');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="16" />&nbsp;</td>
		</tr>
		<tr class="evenbar">
		<td onclick="setCursorAndGo('2145');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="17" />&nbsp;</td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2277');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="18" />&nbsp;</td>
		</tr>
		<tr class="evenbar">
		<td onclick="setCursorAndGo('2409');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="19" />&nbsp;</td>
		</tr>
		<tr class="oddbar">
		<td onclick="setCursorAndGo('2541');"><HATS:Component col="33" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="56" componentSettings="" row="20" />&nbsp;</td>
		</tr>
		</table>
		</td>
		<td width="161">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr>
		<td align="right" class="tabtxthead barhead4new2"><HATS:Component
										col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="15" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="67" componentSettings="" row="15" /></td>
		</tr>
		<tr class="oddbar">
		<td align="right" onclick="setCursorAndGo('2038');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="16" /></td>
		</tr>
		<tr class="evenbar">
		<td align="right" onclick="setCursorAndGo('2170');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="17" /></td>
		</tr>
		<tr class="oddbar">
		<td align="right" onclick="setCursorAndGo('2302');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="18" /></td>
		</tr>
		<tr class="evenbar">
		<td align="right" onclick="setCursorAndGo('2434');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="19" /></td>
		</tr>
		<tr class="oddbar">
		<td align="right" onclick="setCursorAndGo('2566');"><HATS:Component col="58" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="68" componentSettings="" row="20" /></td>
		</tr>
	</table>
		</td>
		<td align="right">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		    <tr>
		      <td align="right" class="tabtxthead barhead4new2">YTD Sales</td>
		      </tr>
		    <tr class="oddbar">
		      <td align="right" onclick="setCursorAndGo('2050');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="16" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="16" /></td>
		      </tr>
		    <tr class="evenbar">
		      <td align="right" onclick="setCursorAndGo('2182');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="17" /></td>
		      </tr>
		    <tr class="oddbar">
		      <td align="right" onclick="setCursorAndGo('2314');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="18" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="18" /></td>
		      </tr>
		    <tr class="evenbar">
		      <td align="right" onclick="setCursorAndGo('2446');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="19" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="19" /></td>
		      </tr>
		    <tr class="oddbar">
		      <td align="right" onclick="setCursorAndGo('2578');"><HATS:Component col="70" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="20" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="80" componentSettings="" row="20" /></td>
		      </tr>
		    </table>
		  </td>
        
        </tr>
	</table>
    	</td>
        
        <td width="5"></td>
        
        <td valign="top">
        
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		    <tr>
		      <td width="130" class="tabtxthead barhead4new" style="border-right:none;">Today</td>
		      <td class="tabtxthead barhead4new" style="border-left:none;">&nbsp;</td>
		      </tr>
		    <tr class="oddbar58">
		      <td valign="top" class="tabtxthead barheadrpt">Days Sales in Inv</td>
		      <td align="right" valign="top"><HATS:Component
								col="89" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="16" /></td>
		      </tr>
		    <tr class="oddbar58">
		      <td valign="top" class="tabtxthead barheadrpt">Turns</td>
		      <td align="right" valign="top"><HATS:Component
								col="89" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="18" /></td>
		      </tr>
		    <tr class="evenbar58">
		      <td valign="top" class="tabtxthead barheadrpt">Index</td>
		      <td align="right" valign="top"><HATS:Component
								col="89" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="20" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="20" /></td>
		      </tr>
	</table>
        </td>
		</tr>
		<tr>
		<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
<!--		<tr>-->
<!--		<td width="130" class="tabtxthead barhead4new" style="border-right:none;">Elapsed</td>-->
<!--		<td class="tabtxthead barhead4new" style="border-left:none;">&nbsp;</td>-->
<!--		</tr>-->
		<tr class="oddbar58">
		<td width="128" valign="top" class="tabtxthead barheadrpt"><div style="white-space: nowrap;">Days Sales in A/R</div></td>
		<td align="right" valign="top"><HATS:Component col="107"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="16" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="16" /></td>
		</tr>
		<tr class="oddbar58">
		<td valign="top" class="tabtxthead barheadrpt">Profits %</td>
		<td align="right" valign="top"><HATS:Component col="107"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="18" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="18" /></td>
		</tr>
		<tr class="evenbar58">
		<td valign="top" class="tabtxthead barheadrpt">G.M.R.O.I</td>
		<td align="right" valign="top"><HATS:Component col="107"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="20" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="20" /></td>
		</tr>
	</table>
        </td>
		</tr>
		<tr>
		<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		    <tr>
		      <td width="130" class="tabtxthead barhead4new2" style="border-right:none;">Last  Night</td>
		      <td class="tabtxthead barhead4new2" style="border-left:none;">&nbsp;</td>
		      </tr>
		    <tr class="oddbar">
		      <td valign="top" class="tabtxthead barheadrpt">Open Orders</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('656');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="5" /></td>
		      </tr>
		    <tr class="oddbar">
		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('656');"><HATS:Component col="123" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="6" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="6" /></td>
		      </tr>
		    <tr class="evenbar">
		      <td valign="top" class="tabtxthead barheadrpt">A/R Invoice</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1052');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="8" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="8" /></td>
		      </tr>
		    <tr class="oddbar">
		      <td valign="top" class="tabtxthead barheadrpt">Inventory</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1316');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="10" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="10" /></td>
		      </tr>
		    <tr class="evenbar">
		      <td valign="top" class="tabtxthead barheadrpt">P/O Amount </td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1580');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="12" /></td>
		      </tr>
		    <tr class="evenbar">
		      <td valign="top" class="tabtxthead barheadrpt">Number of P/O</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1712');"><HATS:Component col="123"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="13" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="13" /></td>
		      </tr>
		    <tr class="oddbar">
		      <td valign="top" class="tabtxthead barheadrpt">Open Recvrs</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('1976');"><HATS:Component col="118"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="15" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="129" componentSettings="" row="15" /></td>
		      </tr>
		    <tr class="evenbar">
		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;A/P Invoice</td>
		      <td align="right" valign="top" onclick="setCursorAndGo('2231');"><HATS:Component col="118"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="17" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="129" componentSettings="" row="17" /></td>
		      </tr>
		      <tr class="evenbar">
		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>
		      <td align="right" valign="top">&nbsp;</td>
		      </tr>
		     
		      <tr class="oddbar" height="19">
		      <td valign="top" class="tabtxthead barheadrpt"></td>
		      <td align="right" valign="top"></td>
		      </tr>
	</table>
        </td>
		</tr>
	</table>
		</td>
        
		</tr>
	</table>
	</div>
	</div>
	<div class="TabbedPanelsContent" style="display: none; " id="dailyTab" >
	<div class="maintopmenu58">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td valign="top">
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		    <tr>
		      <td align="right">&nbsp;</td>
		      <td align="right" class="tabtxthead barhead4new">Today</td>
		      <td class="tabtxthead barhead4new">&nbsp;</td>
		      </tr>
		    <tr class="oddbar" height="46" >
		      <td valign="top" class="tabtxthead barheadrpt">A/R Invoices<br><br></td>
		      <td align="right" valign="top" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('610');"><HATS:Component col="82"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="85"
								componentSettings="" row="5" /><br><br></td>
		      <td align="right" valign="top" class="leftline" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('610');"><HATS:Component
								col="87" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="5" /></td>
		      </tr>
		    <tr class="evenbar" height="46">
		      <td valign="top" class="tabtxthead barheadrpt">Sales Orders <br><br></td>
		      <td align="right" valign="top" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('874');"><HATS:Component col="82"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="85"
								componentSettings="" row="7" /><br><br></td>
		      <td align="right" valign="top" class="leftline" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('874');"><HATS:Component
								col="87" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="98"
								componentSettings="" row="7" /></td>
		      </tr>
		    <tr class="oddbar" height="46">
		      <td valign="top" class="tabtxthead barheadrpt">Cash<br><br></td>
		      <td align="right" valign="top" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('1138');"><br><br></td>
		      <td align="right" valign="top" class="leftline" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('1138');"><HATS:Component
								col="82" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="93"
								componentSettings="" row="9" /></td>
		      </tr>
		    <tr class="evenbar" height="45">
		      <td valign="top" class="tabtxthead barheadrpt">A/P Invoice</td>
		      <td align="right" valign="top" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('1402');">&nbsp;</td>
		      <td align="right" valign="top" class="leftline" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('1402');"><HATS:Component
								col="82" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="93"
								componentSettings="" row="11" /></td>
		      </tr>
		    <tr class="evenbar" height="45">
		      <td valign="top" class="tabtxthead barheadrpt">Checks</td>
		      <td align="right" valign="top" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('1534');">&nbsp;</td>
		      <td align="right" valign="top" class="leftline" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('1534');"><HATS:Component
								col="82" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="93"
								componentSettings="" row="12" /></td>
		      </tr>
		    <tr class="oddbar" height="45">
		      <td valign="top" class="tabtxthead barheadrpt">Cuts<br><br></td>
		      <td align="right" valign="top" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('1798');"><HATS:Component col="82"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent" ecol="87"
								componentSettings="" row="14" /><br><br></td>
		      <td align="right" valign="top" class="leftline" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('1798');">&nbsp;</td>
		      </tr>
<!--		    <tr class="oddbar" height="33">-->
<!--		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
<!--		    <tr class="oddbar" height="33">-->
<!--		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
<!--		    <tr class="oddbar" height="33">-->
<!--		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
<!--		    <tr class="oddbar" height="33">-->
<!--		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
<!--		    <tr class="oddbar">-->
<!--		      <td valign="top" class="tabtxthead barheadrpt">&nbsp;</td>-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
		    </table>
		  </td>
		<td valign="top">
		  <table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		    <tr>
		      <td width="80" align="right" class="tabtxthead barhead4new">Elapsed</td>
		      <td width="50" align="right" class="tabtxthead barhead4new "><HATS:Component
								col="108" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="3" textReplacement=""
								widgetSettings="trim:false|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="115" componentSettings="" row="3" /></td>
		      </tr>
		    <tr class="oddbar58" height="46">
		      <td align="right" valign="top">&nbsp;<HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="103" componentSettings="" row="5" /><br><br></td>
		      <td align="right" valign="top" class="leftline"><HATS:Component
								col="105" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="5" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="5" /></td>
		      </tr>
		    <tr class="evenbar58" height="46">
		      <td align="right" valign="top">&nbsp;<HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="103" componentSettings="" row="7" /><br><br></td>
		      <td align="right" valign="top" class="leftline"><HATS:Component
								col="105" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="7" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="116" componentSettings="" row="7" /></td>
		      </tr>
		    <tr class="oddbar58" height="46">
		      <td align="right" valign="top">&nbsp;<br><br></td>
		      <td align="right" valign="top" class="leftline"><HATS:Component
								col="100" alternate=""
								widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="9" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="111" componentSettings="" row="9" /></td>
		      </tr>
		    <tr class="evenbar58" height="45">
		      <td align="right" valign="top"><HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="11" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="111" componentSettings="" row="11" /></td>
		      <td align="right" valign="top" class="leftline">&nbsp;</td>
		      </tr>
		    <tr class="evenbar58" height="45">
		      <td align="right" valign="top">&nbsp;<HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="12" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="111" componentSettings="" row="12" /></td>
		      <td align="right" valign="top" class="leftline">&nbsp;</td>
		      </tr>
		    <tr class="oddbar58" height="45">
		      <td align="right" valign="top">&nbsp;<HATS:Component col="100"
								alternate="" widget="com.ibm.hats.transform.widgets.LabelWidget"
								alternateRenderingSet="" erow="14" textReplacement=""
								widgetSettings="trim:true|style:|labelStyleClass:|"
								type="com.ibm.hats.transform.components.TextComponent"
								ecol="105" componentSettings="" row="14" /><br><br></td>
		      <td align="right" valign="top" class="leftline">&nbsp;</td>
		      </tr>
<!--		    <tr class="oddbar58" height="33">-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
<!--		    <tr class="oddbar58" height="33">-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
<!--		    <tr class="oddbar58" height="33">-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
<!--		    <tr class="oddbar58" height="33">-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
<!--		    <tr class="oddbar58">-->
<!--		      <td align="right" valign="top">&nbsp;</td>-->
<!--		      <td align="right" valign="top" class="leftline">&nbsp;</td>-->
<!--		      </tr>-->
		    </table>
		  </td>
		<td valign="top">&nbsp;</td>
		<td valign="top">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="right" valign="top">
	<div class="companyinnerboxmain2">
	<table width="99%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;" class="tabtxt">
		<tr>
		<td width="35%" align="right" class="tabtxthead barhead4new2">CSR</td>
		<td width="25%" align="right" class="tabtxthead barhead4new2">Orders</td>
		<td align="right" class="tabtxthead barhead4new2">$ Amount</td>
 		</tr>
		<tr class="oddbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('2782');">
		<td align="right" valign="top"><HATS:Component col="10"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="19" componentSettings="" row="22" /></td>
		<td align="right" valign="top"><HATS:Component col="21"
										alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="26" componentSettings="" row="22" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="28" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="43" componentSettings="" row="22" /></td>
		</tr>
		<tr class="evenbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('2914');">
		<td align="right" valign="top"><HATS:Component col="10" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="19" componentSettings="" row="23" /></td>
		<td align="right" valign="top"><HATS:Component col="21" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="26" componentSettings="" row="23" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="28" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="43" componentSettings="" row="23" /></td>
		</tr>
		<tr class="oddbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('3046');">
		<td align="right" valign="top"><HATS:Component col="10" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="19" componentSettings="" row="24" /></td>
		<td align="right" valign="top"><HATS:Component col="21" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="26" componentSettings="" row="24" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="28" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="43" componentSettings="" row="24" /></td>
		</tr>
		<tr class="evenbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('3178');">
		<td align="right" valign="top"><HATS:Component col="10" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="19" componentSettings="" row="25" /></td>
		<td align="right" valign="top"><HATS:Component col="21" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="26" componentSettings="" row="25" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="28" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="43" componentSettings="" row="25" /></td>
		</tr>
		<tr class="oddbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('2822');">
		<td align="right" valign="top"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="22" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="22" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="68" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="83" componentSettings="" row="22" /></td>
		</tr>
		<tr class="evenbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('2954');">
		<td align="right" valign="top"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="23" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="23" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="68" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="83" componentSettings="" row="23" /></td>
		</tr>
		<tr class="oddbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('3086');">
		<td align="right" valign="top"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="24" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="24" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="68" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="83" componentSettings="" row="24" /></td>
		</tr>
		<tr class="evenbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('3218');">
		<td align="right" valign="top"><HATS:Component col="50" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="59" componentSettings="" row="25" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="25" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="68" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="83" componentSettings="" row="25" /></td>
		</tr>
		<tr class="oddbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('2862');">
		<td align="right" valign="top"><HATS:Component col="90" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="99" componentSettings="" row="22" /></td>
		<td align="right" valign="top"><HATS:Component col="101" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="106" componentSettings="" row="22" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="108" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="22" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="123" componentSettings="" row="22" /></td>
		</tr>
        <tr class="evenbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('2994');">
		<td align="right" valign="top"><HATS:Component col="90" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="99" componentSettings="" row="23" /></td>
		<td align="right" valign="top"><HATS:Component col="101" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="106" componentSettings="" row="23" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="108" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="23" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="123" componentSettings="" row="23" /></td>
		</tr>
		<tr class="oddbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('3126');">
		<td align="right" valign="top"><HATS:Component col="90" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="99" componentSettings="" row="24" /></td>
		<td align="right" valign="top"><HATS:Component col="61" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="66" componentSettings="" row="24" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="108" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="24" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="123" componentSettings="" row="24" /></td>
		</tr>
        <tr class="evenbarana" onclick="javascript:setCurrentTabName('DailyTab');setCursorAndGo('3258');">
		<td align="right" valign="top"><HATS:Component col="90" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="99" componentSettings="" row="25" /></td>
		<td align="right" valign="top"><HATS:Component col="101" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="106" componentSettings="" row="25" /></td>
		<td align="right" valign="top">&nbsp;<HATS:Component col="108" alternate=""
										widget="com.ibm.hats.transform.widgets.LabelWidget"
										alternateRenderingSet="" erow="25" textReplacement=""
										widgetSettings="trim:true|style:|labelStyleClass:|"
										type="com.ibm.hats.transform.components.TextComponent"
										ecol="123" componentSettings="" row="25" /></td>
		</tr>
		<tr class="oddbarana">
		<td align="right" valign="top"></td>
		<td align="right" valign="top"></td>
		<td align="right" valign="top">&nbsp;</td>
		</tr>
		
		<tr>
		<td align="right" colspan="3" valign="middle">
		<%//String pgUpIndication = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("pageUpIndication", true).getString(0);%>
		<%//if (pgUpIndication.length()>0 || !pgUpIndication.equalsIgnoreCase("")) {
		//System.out.println("Page UP indication:............. " + pgUpIndication);
		//String btn1 = "<div class=\"functionbtn\"><a href=\"javascript:ms('[pageup]', 'HATSForm')\" title=\"PAGEUP\">Page UP</a></div>";
		//String btn2 = "<div class=\"functionbtn\"><a href=\"javascript:ms('[pagedn]', 'HATSForm')\" title=\"PAGEDN\">Page Down</a></div>";
		//out.print(btn1);
		//out.print(btn2);%>
		<% //	}%>
		</td>
		</tr>
		
        </table>
	</div>
		</td>
		</tr>
	</table>
    	</td>
		</tr>
	</table>
	</div>
	</div>
              

	</div>
	</div>
	</div>
    
    <div class="clear5px"></div>
    
	<div class="belowbtnnew2">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		
		<td align="left" valign="middle" class="errorMsg">
		<%String errorMsg = ((TransformInfo)request.getAttribute(CommonConstants.REQ_TRANSFORMINFO)).getGlobalVariable("errorMsgOnExecutiveMenu", true).getString(0);%>
		<%if (errorMsg.length()>0 || !errorMsg.equalsIgnoreCase("")) {
		System.out.println("Error Message:............." + errorMsg); 
		out.print(errorMsg);%>
		<% 	}%>
        </td>
		</tr>
	</table>
	</div>

<!--	</div>-->
	</td>
	</tr>
	</table>
	<!--Menu End-->


<script type="text/javascript">
screenName = "ExecutiveMenu";
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1", {defaultTab:0});
</script>

<%
	if(request.getParameter("currentTab") != null) {
		if(request.getParameter("currentTab").trim().equalsIgnoreCase("DailyTab")) {
%>
<script type="text/javascript">
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1", {defaultTab:1});
</script>
<%
	} }
%>

<!--<div id="directions_extension" style="position: absolute !important; background-image: initial !important; background-attachment: initial !important; background-origin: initial !important; background-clip: initial !important; background-color: rgb(238, 238, 255) !important; border-top-width: 1px !important; border-right-width: 1px !important; border-bottom-width: 1px !important; border-left-width: 1px !important; border-top-style: solid !important; border-right-style: solid !important; border-bottom-style: solid !important; border-left-style: solid !important; border-top-color: rgb(170, 170, 255) !important; border-right-color: rgb(170, 170, 255) !important; border-bottom-color: rgb(170, 170, 255) !important; border-left-color: rgb(170, 170, 255) !important; padding-top: 3px !important; padding-right: 3px !important; padding-bottom: 3px !important; padding-left: 3px !important; border-top-left-radius: 5px 5px !important; border-top-right-radius: 5px 5px !important; border-bottom-right-radius: 5px 5px !important; border-bottom-left-radius: 5px 5px !important; display: none; background-position: initial initial !important; background-repeat: initial initial !important; "></div>-->

</HATS:Form>

</body>
</html>