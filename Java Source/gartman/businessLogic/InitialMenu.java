package gartman.businessLogic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.ibm.hats.common.IBusinessLogicInformation;
import com.ibm.hats.common.GlobalVariable;
import com.ibm.hats.common.IGlobalVariable;

public class InitialMenu
{
	static Connection connection;
	
    public static void getUserInitialMenu(IBusinessLogicInformation blInfo) throws Exception {
		
    	Statement statement = null;
    	String userName = blInfo.getGlobalVariable("UserName").getString().trim();
    	
		if(blInfo.getGlobalVariable("InitialMenu") == null) {
		//if(blInfo.getRequest().getSession().getAttribute("InitialMenu") == null) {	
    		try {
    			
    			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
    			connection = DriverManager.getConnection("jdbc:as400://as400.gartman.com/QTEMP", "SYED", "SYED");
    			System.out.println("Connection created. " + connection);
    			statement = connection.createStatement();
    			String query = "SELECT UPINMN FROM USRMENU.TEMPUSR WHERE UPUPRF='" + userName + "'";
                ResultSet rs = statement.executeQuery(query);

                while(rs.next()) {
                	setGlobalVariable(blInfo, "InitialMenu", rs.getString(1).trim());
                	System.out.println(rs.getString(1).trim());
                	//blInfo.getRequest().getSession().setAttribute("InitialMenu", rs.getString(1).trim());
                }
    			
     		} catch (Exception e) {
    			e.printStackTrace();
    		} finally {
    			statement.close();
    			connection.close();
    			connection = null;
    		}
     		
		}
    }

	public static void setGlobalVariable(IBusinessLogicInformation blInfo, String name, Object value) {
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		if ( gv == null )
		{
			gv = new GlobalVariable(name,value);
		}
		else
		{
			gv.set(value);		
		}
		blInfo.getGlobalVariables().put(name,gv);
	}

	public static void setSharedGlobalVariable(IBusinessLogicInformation blInfo, String name, Object value) {
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		if ( gv == null )
		{
			gv = new GlobalVariable(name,value);
		}
		else
		{
			gv.set(value);		
		}
		blInfo.getSharedGlobalVariables().put(name,gv);
	}
	
	public static void removeGlobalVariable(IBusinessLogicInformation blInfo, String name) {
		IGlobalVariable gv = blInfo.getGlobalVariable(name);
		if ( gv != null )
		{
			blInfo.getGlobalVariables().remove(name);
			gv.clear();
			gv = null;
		}
	}
	
	public static void removeSharedGlobalVariable(IBusinessLogicInformation blInfo, String name) {
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		if ( gv != null )
		{
			blInfo.getSharedGlobalVariables().remove(name);
			gv.clear();
			gv = null;
		}
	}

    public static IGlobalVariable getGlobalVariable(IBusinessLogicInformation blInfo, String name) {
        IGlobalVariable gv = blInfo.getGlobalVariable(name);
        return gv;
    }

	public static IGlobalVariable getSharedGlobalVariable(IBusinessLogicInformation blInfo, String name) {
		IGlobalVariable gv = blInfo.getSharedGlobalVariable(name);
		return gv;
	}

}
