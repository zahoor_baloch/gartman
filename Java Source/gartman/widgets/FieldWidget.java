package gartman.widgets;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import com.ibm.hats.transform.RenderingRulesEngine;
import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.FieldComponentElement;
import com.ibm.hats.transform.elements.FieldRowComponentElement;
import com.ibm.hats.transform.elements.InputComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;
import com.ibm.hsr.screen.HsrAttributes;

public class FieldWidget extends Widget implements HTMLRenderer {

	public FieldWidget(ComponentElement[] componentElements, Properties settings) {
		super(componentElements, settings);
		// TODO Auto-generated constructor stub
	}

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);		
		int inputTdExtraColspan =0;
		for(int i=0;i<componentElements.length;i++){
			FieldRowComponentElement frce=(FieldRowComponentElement)componentElements[i];
			
			int extraColspan = 0;			
			int frceCount = frce.getElementCount();
			for(int j=0; j<frce.getElementCount(); j++){
				 FieldComponentElement fieldComponentElement = (FieldComponentElement)frce.getElement(j);
								
				 int length = fieldComponentElement.getLength();				
				 String previewText = fieldComponentElement.getText();
				 String previewText2 = "";
				 //String previewTextTrim = previewText.trim();
				 String clas = "";
				 int startPos = fieldComponentElement.getStartPos();
				 int foregroundColor = fieldComponentElement.getForegroundColor();
				 boolean rv = fieldComponentElement.isReverseVideo();
				 System.out.println("rv....:"+rv);
				 switch(foregroundColor){
				 	case 1:
				 		clas="HBLUE HF";
				 		if(rv)clas+=" ";
				 		break;
				 	case 2:
				 		clas="HGREEN HF";
				 		if(rv)clas+="";
				 		break;
				 	case 3:
				 		clas="HCYAN HF";
				 		if(rv)clas+="";
				 		break;
				 	case 4:
				 		clas="HRED HF";
				 		if(rv)clas+="";
				 		break;
				 	case 5:
				 		clas="HMAGENTA HF";
				 		if(rv)clas+=" RHMAGENTA";
				 		break;
				 	case 6:
				 		clas="HBROWN HF";
				 		if(rv)clas+="";
				 		break;
				 	case 7:
				 		clas="HWHITE HF";
				 		if(rv)clas+="";
				 		break;
				 }
				// System.out.println("X"+fieldComponentElement.getPreviewText()+"X..........X"+startPos+"X.........."+fieldComponentElement.getBackgroundColor());
				 
				 if(fieldComponentElement.isHolderOnly() && !fieldComponentElement.isSplit()){
					 if(length>1){
						 buffer.append("<td class=\"HGREEN\" colspan=\""+length+"\">&nbsp;</td>");
					 }else{
						 buffer.append("<td class=\"HGREEN\">&nbsp;</td>");
					 }
				 }else if(fieldComponentElement.isHolderOnly() && fieldComponentElement.isSplit()){
					 if(length>1){
						 buffer.append("<td class=\"HGREEN\" colspan=\""+length+"\">&nbsp;</td>");
					 }else{
						 buffer.append("<td class=\"HGREEN\">&nbsp;</td>");
					 }
				 }else if( fieldComponentElement.isProtected()){
					System.out.println("PreviewText:X"+fieldComponentElement.getPreviewText()+"X.....StartPos:X"+fieldComponentElement.getStartPos()+"X......ForgroundColor:X"+foregroundColor+"X............BackGroundColor:X"+fieldComponentElement.getBackgroundColor()+"X......."+fieldComponentElement.isReverseVideo()+"X");
					 
					 if(fieldComponentElement.getPreviewText().trim().equals("")){
						 buffer.append("<td class=\""+clas+"\" colspan=\""+length+"\">&nbsp;</td>");
					 }else{
						 char[] prevTextChar = fieldComponentElement.getPreviewText().toCharArray();						 
						 for(int k=0; k<prevTextChar.length; k++){							 
							 if(prevTextChar[k] == ' '){								
								//// previewText2 +="&nbsp;";
							 }else{								
								 ///previewText2 +=prevTextChar[k];
								 previewText2 = fieldComponentElement.getPreviewText();
							 }
						 }
						 if(extraColspan>0){
								buffer.append("<td class=\"HGREEN\" colspan=\""+extraColspan+"\">&nbsp;</td>");
								extraColspan = 0;
						}
						 String tdAlign = "";
						 
							 switch(length){
							 	case 2:
							 		if((previewText.trim().equals("on") && startPos == 573)){//UFM
							 			tdAlign = "center";
							 			}
							 	case 4:
							 		if((previewText.trim().equals("Sign") && startPos == 568) || (previewText.trim().equals("View") && startPos == 563)){//UFM
							 			tdAlign = "center";
							 		}
							 	case 5:
							 		if(previewText.equals("Name:")){//(Screen-UserFileMaintenance)							 			
							 			tdAlign = "right";
							 		}
							 		break;
							 	case 7:
							 		if(previewText.equals("Include") && startPos==1364){//(Screen-UserFileMaintenance)							 			
							 			tdAlign = "right";
							 		}
							 		break;
							 	case 8:							 			
							 		break;
							 	case 9://
							 		if((previewText.equals("Side Mark") && startPos==1372)
							 				|| (previewText.equals("category:"))){//{(S-LM)
							 			tdAlign = "right";							 			
							 		}else if((previewText.equals("Currency:") && startPos == 1122)){
							 			length -= 3;	inputTdExtraColspan=3;
							 		}
							 		break;
							 	case 10:
							 		if((previewText.trim().equals("on report?") && startPos == 1382)){//UFM
							 			length -= 3;	inputTdExtraColspan=3;
							 		}
							 	case 11:
							 		if((previewText.trim().equals("Information") && startPos == 576)){//UFM
							 			tdAlign = "center";
							 			}
							 	case 12:
							 		if(previewText.equals("back orders?") && startPos==680){//(S-SHS)							 		
							 			tdAlign = "right";
							 		}
							 		break;
							 	case 13:
							 		if(previewText.equals("Start  (HHMM)") || (previewText.equals("Include cost?") && startPos==890)){//(S-UFM)							 		
							 			tdAlign = "right";
							 		}else if(previewText.equals("End (HHMM)   ")){//(S-UFM)							 		
							 			tdAlign = "center";
							 		}
							 		break;
							 	case 14:							 		
							 		if((previewText.equals("non=S/A items?") && startPos==747)){//(S-CMFM)							 		
							 			tdAlign = "right";						 			
							 		}else if((previewText.equals("UPS Shipper #:"))){
							 			length -= 4;	inputTdExtraColspan=4;
							 		}
							 		break;	
							 	case 15:
							 		if((previewText.equals("Name/Address: /"))){//{(S-LM)							 			
							 			tdAlign = "right";							 				
							 		}else if((previewText.equals("Federal tax id:"))){
							 			length -= 5;	inputTdExtraColspan=5;
							 		}							 		
							 		break;
							 	case 16:
							 		if((previewText.equals("Parent location:"))){					 			
							 			tdAlign = "right";
							 		}else if((previewText.equals("Valid Delv. Loc?"))){
							 			length -= 6;	inputTdExtraColspan=6;
							 		}else if((previewText.equals("Customer number:"))){
							 			length -= 6;	inputTdExtraColspan=6;
							 		}
							 		break;
							 	case 17:
							 		if((previewText.equals("Default location:"))							 				
							 				|| (previewText.equals("Include; directs?") && startPos==727)
							 				|| (previewText.equals("Side Mark Search:") && startPos==1442)){							 			
							 			tdAlign = "right";							 		
							 		}else if((previewText.equals("Number of copies:") && startPos == 430)){
							 			length -= 8;	inputTdExtraColspan=8;
							 		}else if((previewText.equals("Sales Rep number:") && startPos == 379)){
							 			length -= 5;	inputTdExtraColspan=5;
							 		}
							 		
							 		break;
							 	case 19:
							 		if((previewText.equals("Print Qty on Recap?") && startPos==1252)){//(S-LM)							 		
							 			tdAlign = "right";
							 			////length -= 6;	extraColspan+=6;
							 		}
							 		break;
							 	case 20:
							 		if(((previewText.equals("Do you wish to print") && startPos==1281))){//(S-LM)							 		
							 			tdAlign = "right";
							 			////length -= 6;	extraColspan+=6;
							 		}else if(previewText.equals("Y-T-D earnings G/L#:")){
							 			length -= 5;	inputTdExtraColspan=5;
							 		}
							 		break;
							 	case 21:
							 		if((previewText.equals("Include credit memos?") && startPos==802) ){//(S-SHS)							 		
							 			tdAlign = "right";							 			
							 		}else if((previewText.equals("Include orig invoice?") && startPos==858)){
							 			length -= 12;	inputTdExtraColspan=12;
							 		}
							 		break;
							 	case 22:
							 		if(previewText.equals("Limit user to company:")){//(S-UFM)							 			
							 			//tdAlign = "right";
							 			length -= 6;	inputTdExtraColspan=6;
							 		}else if((previewText.equals("Print customer labels?") && startPos==1326)){
							 			tdAlign = "right";
							 		}
							 		break;
							 	case 23:
							 		if(	(previewText.trim().equals("Show on Sales Analysis?"))){//(S-LM)							 			
							 			//tdAlign = "right";
							 			length -= 9;	inputTdExtraColspan=9;   
							 		}else if((previewText.equals("Retained earnings G/L#:"))){
							 			length -= 10;	inputTdExtraColspan=10;
							 		}else if((previewText.trim().equals("Company number for G/L:"))){
							 			length -= 6;	inputTdExtraColspan=6;
							 		}						 		
							 		break;
							 	case 24:
							 		if(previewText.trim().equals("........................") && startPos==844){//(S-LM)							 			
							 			length -= 11;	inputTdExtraColspan=11;
							 		}
							 		break;
							 	case 26:
							 		if(previewText.trim().equals("Include; trip information?") && startPos==1045){//(S-LM)							 			
							 			tdAlign = "right";
							 			////length -= 9;	extraColspan+=9;
							 		}
							 		break;
							 	case 25:
							 		if(previewText.trim().equals("Credit manager user/WSID:")){//(S-LM)							 			
							 			//tdAlign = "right";
							 			length -= 8;	inputTdExtraColspan=8;
							 		}
							 		break;
							 	case 27:
							 		if((previewText.trim().equals("If so, print profit $ or %:") && startPos==913)){//(Screen-Location Master(LM))
							 			tdAlign = "right";
							 			////length -= 12;	extraColspan+=12;
							 		}else if((previewText.trim().equals("Pool transfers for release?"))){
							 			length -= 13;	inputTdExtraColspan=13;
							 		}
							 		break;
							 	case 28:
							 		if((previewText.equals("Interactive Transfer Update?"))){//(S-IRO)
							 			//tdAlign = "right";
							 			length -= 12;	inputTdExtraColspan=12;
							 		}else if((previewText.equals("Display one item per screen.") && startPos==952)
							 				|| (previewText.equals("Purchase order is for loc. .") && startPos==1084)
							 				|| (previewText.equals("Pull transfers from loc. . .") && startPos==1216)
							 				|| (previewText.equals("Calculate @ location level .") && startPos==1348)
							 				|| (previewText.equals("Use location table . . . . .") && startPos==1480)
							 				|| (previewText.equals("Sort by item description . .") && startPos==1612)
							 				|| (previewText.equals("Show altername item in list.") && startPos==1744)){
							 			tdAlign = "right";
							 		}
							 		//
							 		break;
							 	case 29:
							 		if(previewText.trim().equals("Enter a Y for Job Accounting:")){//(S-CMFM)
							 				//tdAlign = "right";
							 			length -= 7;	extraColspan+=7;
							 		}
							 		break;
							 	case 33:
							 		if(previewText.trim().equals("Include; open (unshipped) orders?") && startPos == 644){//(Screen-SHS)
							 			length -= 13;	extraColspan+=13;
							 		}
							 		break;
							 	case 36:
							 		if((previewText.trim().equals("......................................."))
							 				|| (previewText.trim().equals("trip upcharge (overbill) in sales $?") && startPos==1074)){//(Screen-userFileMaintenance)
							 			tdAlign = "right";
							 		}
							 		break;
							 	case 39:
							 		if(previewText.trim().equals("Default inventory selection - location:")){//(Screen-userFileMaintenance)
							 			//length -= 5;	extraColspan+=5;
							 			tdAlign = "right";
							 		}else if((previewText.trim().equals(".......................................") && startPos == 588)){//UFM
							 			length -= 19;	inputTdExtraColspan=19;
							 		}else if((previewText.trim().equals("Process invoices with balance due ONLY?") && startPos == 1204)){//SHS
							 			tdAlign = "right";
						 			}else if((previewText.trim().equals("Do you wish to sort report by salesman?") && startPos == 498)){
						 				length -= 19;	inputTdExtraColspan=19;
						 			}
							 		break;
							 	case 41:
							 		if(previewText.trim().equals("Do you wish to consolidate all locations?") && startPos==577){//(Screen-SHS)							 			
							 			length -= 21;	inputTdExtraColspan=21;
							 		}
							 		break;
							 	case 49:
							 		if((previewText.trim().equals("Include special charges (rebate, discount, etc.)?") && startPos==972)
							 				|| ((previewText.trim().equals("Do you wish to show quantities in billable units?") && startPos==1136))){//(Screen-SHS)							 			
							 			tdAlign = "right";
							 		}
							 		break;
							 	case 63:							 							 			
							 		if(previewText.trim().equals("Enter a Y if 13 account periods will be used in general ledger:")){//(S-CMFM)
							 			//tdAlign = "right";
							 			length -= 23;	inputTdExtraColspan=23;
							 		}
							 		break;							 	
							 	case 64:
							 		if(previewText.equals("Enter vendor number to be used when billing inventory transfers:")){//{(S-LMM)
							 			length -= 18;	inputTdExtraColspan=18;
							 		}
							 		break;
							 	case 68:
							 		if(previewText.equals("Enter the number of the customer to be used for inventory transfers:")){//{(S-LMM)
							 			length -= 18;	inputTdExtraColspan=18;
							 		}
							 		break;
							 	case 70:
							 		if((previewText.equals("Use (O)rder point, order quantity or (S)afety stock/lead time. . . . .") && startPos==291)//{(S-IRO2)
							 				|| (previewText.equals("Enter a Y if you wish to see only those items at or below minimum. . .") && startPos==423)
							 				|| (previewText.equals("Seasonal adjustment factor . . . . . . . . . . . . . . . . . . . . . .") && startPos==1347)
							 				|| (previewText.equals("Desired customer service level . . . . . . . . . . . . . . . . . . . .") && startPos==1479)
							 				|| (previewText.equals("ONLY process items with back order not on P/O (Y-yes, A-all B/O) . . .") && startPos==1611)
							 				|| (previewText.equals("Automatically attach back orders?  . . . . . . . . . . . . . . . . . .") && startPos==1743)
							 				|| (previewText.equals("Include non-stock items (Y-yes, O-non-stock only)  . . . . . . . . . .") && startPos==1875)
							 				|| (previewText.equals("Include order-on-demand items. . . . . . . . . . . . . . . . . . . . .") && startPos==2007)
							 				|| (previewText.equals("Display (P)rimary, (S)econdary lot or blank for none . . . . . . . . .") && startPos==2139)
							 				|| (previewText.equals("Use order points/quantities from location. . . . . . . . . . . . . . .") && startPos==2271)
							 				|| (previewText.equals("When calculating on order and on back order, include directs?  . . . .") && startPos==2799)
							 				|| (previewText.equals("Enter an O to show only customer guarantee items . . . . . . . . . . .") && startPos==2931)
							 				|| (previewText.equals("Enter a Y if you wish to clear all suggested amounts to order. . . . .") && startPos==3063)
							 				|| (previewText.equals("Include future back orders?. . . . . . . . . . . . . . . . . . . . . .") && startPos==3195)){
							 			length -= 20;	inputTdExtraColspan=20;
							 		}
							 		break;
							 	case 73:
							 		if(previewText.equals("Display the line item over-ride screen as each item is entered  . . . . .")
							 				|| previewText.equals("Price each line item as it is entered . . . . . . . . . . . . . . . . . .")
							 				|| previewText.equals("Prompt freight for common carrier order?. . . . . . . . . . . . . . . . .")
							 				|| previewText.equals("Prompt for price o/r authorization? . . . . . . . . . . . . . . . . . . .")
							 				|| previewText.equals("Does this user have the authority to authorize price overrides? . . . . .")
							 				|| previewText.equals("Do you wish to confirm the line items?. . . . . . . . . . . . . . . . . .")
							 				|| previewText.equals("Display order text automatically? . . . . . . . . . . . . . . . . . . . .")
							 				|| previewText.equals("Prompt for ship-to information? . . . . . . . . . . . . . . . . . . . . .")
							 				|| previewText.equals("Should the delivery date be required? . . . . . . . . . . . . . . . . . .")
							 				|| previewText.equals("Should the contact name be a required field?. . . . . . . . . . . . . . .")
							 				|| previewText.equals("Prompt for ship complete? . . . . . . . . . . . . . . . . . . . . . . . .")){//{(S-UFM)
							 			//tdAlign = "center";
							 			length -= 23;	inputTdExtraColspan=23;	
							 		}else{
							 			//tdAlign = "center";
							 			//length -= 10;	extraColspan+=10;
							 		}
							 		break;
							 }	
							 for(int k=0; k<extraColspan; k++){
								 previewText2 = "&nbsp;"+ previewText2;
							 }
							 extraColspan = 0;
						 buffer.append("<td align=\""+tdAlign+"\"class=\""+clas+"\" colspan=\""+length+"\">"+previewText2+"</td>");
					 }
				 }else{
					 int colspan = length;
					 String ruleReplacement = 
							RenderingRulesEngine.processMatchingElement(fieldComponentElement, contextAttributes);
					 	
					if (ruleReplacement != null) {
							
							
							if(inputTdExtraColspan>0){
								colspan = inputTdExtraColspan+length;
								inputTdExtraColspan = 0;
							 }
							buffer.append( "<td class=\""+clas+"\" colspan=\""+colspan+"\">"+ruleReplacement+"</td>");
					} else {						
						 if(extraColspan>0){
							colspan = extraColspan+length;
							extraColspan = 0;
						 }
						 String em = "";
						 if(length<4){
							 if(length==1 || length==2){
								 em = "width:2em";
							 }else{
								 em = "width:3em";
							 }						 
						 }
						 if(fieldComponentElement.isHidden()){
							 buffer.append( "<td class=\""+clas+"\" colspan=\""+colspan+"\"><input size=\""+length+"\" fillrequired=\"false\" onkeypress=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onblur=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onclick=\"setCursorPosition("+startPos+", 'HATSForm');convertToUpperCase(event);allowAlphaNumericShift(event)\" autoenter=\"false\" maxlength=\""+length+"\" name=\"in_"+startPos+"_"+length+"\" ondrop=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onchange=\"checkInput(this);convertToUpperCase(event);allowAlphaNumericShift(event)\" style=\"ime-mode:inactive;"+em+"\" entryrequired=\"false\" onpaste=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" fieldexitrequired=\"false\" class=\""+clas+"\" onfocus=\"setFocusFieldIntoGlobal(this, 'HATSForm')\" type=\"password\" value=\""+fieldComponentElement.getPreviewText().trim()+"\"></td>");
						 }else{
							 if(fieldComponentElement.isAlphaNumeric()){
								 if(fieldComponentElement.getPreviewText().length()>1){
									 buffer.append( "<td class=\""+clas+"\" colspan=\""+colspan+"\"><input size=\""+length+"\" fillrequired=\"false\" onkeypress=\"allowAlphaNumericShift(event)\" onblur=\"allowAlphaNumericShift(event)\" onclick=\"setCursorPosition("+startPos+", 'HATSForm');allowAlphaNumericShift(event)\" autoenter=\"false\" maxlength=\""+length+"\" name=\"in_"+startPos+"_"+length+"\" ondrop=\"allowAlphaNumericShift(event)\" onchange=\"checkInput(this);allowAlphaNumericShift(event)\" style=\"ime-mode:inactive;"+em+"\" entryrequired=\"false\" onpaste=\"allowAlphaNumericShift(event)\" fieldexitrequired=\"false\" class=\""+clas+"\" onfocus=\"setFocusFieldIntoGlobal(this, 'HATSForm')\" type=\"text\" value=\""+fieldComponentElement.getPreviewText().trim()+"\"></td>");
								 }else{
									 buffer.append( "<td class=\""+clas+"\" colspan=\""+colspan+"\"><input size=\""+length+"\" fillrequired=\"false\" onkeypress=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onblur=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onclick=\"setCursorPosition("+startPos+", 'HATSForm');convertToUpperCase(event);allowAlphaNumericShift(event)\" autoenter=\"false\" maxlength=\""+length+"\" name=\"in_"+startPos+"_"+length+"\" ondrop=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onchange=\"checkInput(this);convertToUpperCase(event);allowAlphaNumericShift(event)\" style=\"ime-mode:inactive;"+em+"\" entryrequired=\"false\" onpaste=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" fieldexitrequired=\"false\" class=\""+clas+"\" onfocus=\"setFocusFieldIntoGlobal(this, 'HATSForm')\" type=\"text\" value=\""+fieldComponentElement.getPreviewText().trim()+"\"></td>");
								 }
							 }else if(fieldComponentElement.isNumericOnly()){
								 buffer.append("<td class=\""+clas+"\" colspan=\""+colspan+"\"><input size=\""+length+"\" fillrequired=\"false\" onkeypress=\"allowNumericOnly(event)\" onblur=\"allowNumericOnly(event)\" onclick=\"setCursorPosition("+startPos+",'HATSForm');allowNumericOnly(event)\" autoenter=\"false\" maxlength=\""+length+"\" name=\"in_"+startPos+"_"+length+"\" ondrop=\"allowNumericOnly(event)\" onchange=\"checkInput(this);allowNumericOnly(event)\" style=\"ime-mode:inactive;"+em+"\" entryrequired=\"false\" onpaste=\"allowNumericOnly(event)\" fieldexitrequired=\"false\" class=\""+clas+"\" onfocus=\"setFocusFieldIntoGlobal(this, 'HATSForm')\" type=\"text\" value=\""+fieldComponentElement.getPreviewText().trim()+"\"></td>");
							 }
							 else{
								 buffer.append( "<td class=\""+clas+"\" colspan=\""+colspan+"\"><input size=\""+length+"\" fillrequired=\"false\" onkeypress=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onblur=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onclick=\"setCursorPosition("+startPos+", 'HATSForm');convertToUpperCase(event);allowAlphaNumericShift(event)\" autoenter=\"false\" maxlength=\""+length+"\" name=\"in_"+startPos+"_"+length+"\" ondrop=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" onchange=\"checkInput(this);convertToUpperCase(event);allowAlphaNumericShift(event)\" style=\"ime-mode:inactive;"+em+"\" entryrequired=\"false\" onpaste=\"convertToUpperCase(event);allowAlphaNumericShift(event)\" fieldexitrequired=\"false\" class=\""+clas+"\" onfocus=\"setFocusFieldIntoGlobal(this, 'HATSForm')\" type=\"text\" value=\""+fieldComponentElement.getPreviewText().trim()+"\"></td>");
							 }
							 
						 }
					 }
					    
				}
					 			
			}		
		
			if(extraColspan>0){
				buffer.append("<td class=\"HGREEN\" colspan=\""+extraColspan+"\">&nbsp;</td>");
				extraColspan = 0;
			 }
		}
	return (buffer);

	}

}
