package gartman.widgets;

/**
 * @author talhAiqbaL
 */
import java.util.Properties;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.FunctionKeyComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;

public class VistaButtonWidget extends Widget implements HTMLRenderer {

	public VistaButtonWidget(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);
		// TODO Auto-generated constructor stub
	}

	public StringBuffer drawHTML() {
		// TODO Auto-generated method stub
		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);
		ComponentElement[] elements = this.getComponentElements();

		buffer.append("<div class=\"belowbtn\">");
		buffer.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		buffer.append("<tr>");
		buffer.append("<td align=\"center\" valign=\"middle\">");
		buffer.append("<table width=\"auto\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		buffer.append("<tr>");
		buffer.append("<td>");
		buffer.append("<div id=\"vista_toolbar\">");
		buffer.append("<div align=\"center\">");
		buffer.append("<ul>");
		
		//for(int i=0;i<elements.length;i++){
			FunctionKeyComponentElement funcKeyCompElement=(FunctionKeyComponentElement)elements[0];
			String caption = funcKeyCompElement.getCaption().trim();
			buffer.append("<li><a href=\"#\" title=\"\"><span>");
			buffer.append(caption);
			buffer.append("</span></a></li>");
		//}
		
		buffer.append("</ul>");

		buffer.append("</div>");

		buffer.append("</div>");
		buffer.append("</td>");
		buffer.append("</tr>");
		buffer.append("</table></td>");
		buffer.append("</tr>");
		buffer.append("</table>");
		buffer.append("</div>");
		return (buffer);
	}
}
