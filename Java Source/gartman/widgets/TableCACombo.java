package gartman.widgets;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.elements.TableCellComponentElement;
import com.ibm.hats.transform.elements.TableComponentElement;
import com.ibm.hats.transform.html.HTMLElementFactory;
import com.ibm.hats.transform.renderers.HTMLRenderer;
import com.ibm.hats.transform.widgets.Widget;

public class TableCACombo extends Widget implements HTMLRenderer {

	public TableCACombo(ComponentElement[] componentElements,
			Properties settings) {
		super(componentElements, settings);

	}

	public StringBuffer drawHTML() {

		StringBuffer buffer = new StringBuffer(256);
		HTMLElementFactory factory = HTMLElementFactory.newInstance(
				contextAttributes, settings);
		
//		buffer.append("<div class=\"companyinnerboxmain\">");
		buffer.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse;\" class=\"tabtxt\">");
		
		ComponentElement[] elements = this.getComponentElements();
		
		int noOfComponents = elements.length;
		for (int compNo=0; compNo<noOfComponents; compNo++) {
			
			TableComponentElement tce = (TableComponentElement)elements[compNo];
			TableCellComponentElement tcce[][] = tce.getCells();
			String oddEven = "evenbarana";

			int val = 1;
			try {
				for (int rc = 0; rc < tcce.length; rc++) {
					if (oddEven.equalsIgnoreCase("oddbarana")) {
						oddEven = "evenbarana";
					} else {
						oddEven = "oddbarana";	
					}
					
//					ScreenRegion regionThisText = tcce[rc][0].getConsumedRegion();
//					System.out.println("startRow: " + regionThisText.startRow);
//					System.out.println("startCol: " + regionThisText.startCol);
//					int sid = tce.getScreenId();
//					int absPos =((regionThisText.startRow -1 ) * 80) + regionThisText.startCol ;
//					System.out.println("Screen ID: " + sid);
//					System.out.println("absolute position: " + absPos);
					int sid = tcce[rc][0].getScreenId();
					int absPosRow = tcce[rc][0].getStartPos();
					if(rc%14 == 0) {
						val = 1;
					}
					
					//System.out.println("position of this row:" + absPosRow);
					//buffer.append("<input type='hidden' name='in_"+absPosRow+"_1_"+sid+"' id='in_"+absPosRow+"_1_"+sid+"'>");
					//gotoDetails(" + val + ", " + sid + ")
					//buffer.append("<tr onclick=\"javascript:setCursorAndGo("+absPosRow+");\" class=\""+ oddEven + "\">");					
					buffer.append("<tr onclick=\"javascript:setPromptsCursorAndGo('"+val+"', '"+ sid +"');\" class=\""+ oddEven + "\">");
					for(int cc = 0; cc < tcce[rc].length; cc++) {
						String temp = tcce[rc][0].getPreviewText().trim() + tcce[rc][1].getPreviewText().trim() + tcce[rc][2].getPreviewText().trim() + tcce[rc][3].getPreviewText().trim() + tcce[rc][4].getPreviewText().trim() + tcce[rc][5].getPreviewText().trim();
						if(!temp.trim().equalsIgnoreCase("")) {
							if(cc == 0) {
								buffer.append("<td width=\"200\">&nbsp;");
								buffer.append(tcce[rc][cc].getPreviewText());
								buffer.append("</td>");
							} else if(cc == 1) {
								buffer.append("<td width=\"150\">");
								buffer.append(tcce[rc][cc].getPreviewText());
								buffer.append("</td>");
							} else if(cc == 2) {
								buffer.append("<td width=\"150\">");
								buffer.append(tcce[rc][cc].getPreviewText());
								buffer.append("</td>");
							} else if(cc == 3) {
								buffer.append("<td width=\"150\">");
								buffer.append(tcce[rc][cc].getPreviewText());
								buffer.append("</td>");
							} else if(cc == 4) {
								buffer.append("<td align=\"right\" width=\"150\">");
								buffer.append(tcce[rc][cc].getPreviewText());
								buffer.append("</td>");
							} else if(cc == 5) {
								buffer.append("<td align=\"right\">");
								buffer.append(tcce[rc][cc].getPreviewText());
								buffer.append("</td>");
							}
						} else {
							break;
						}
						
					}
					
					buffer.append("</tr>");
					val++;
				}
				buffer.append("</table>");
//				buffer.append("</div>");
				System.out.println("before" + buffer);
				
			} catch (Exception e) {
				System.out.println("Company Analysis Combination Exception = " + e.getMessage());
			}
		}
						
		return (buffer);
	}

	public int getPropertyPageCount() {

		return (1);
	}

	public Vector getCustomProperties(int iPageNumber, Properties properties,
			ResourceBundle bundle) {

		return (null);
	}

	public Properties getDefaultValues(int iPageNumber) {

		return (super.getDefaultValues(iPageNumber));
	}

}
