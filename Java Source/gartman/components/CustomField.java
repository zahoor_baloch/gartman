package gartman.components;

import java.util.Properties;
import java.util.Vector;
//import navPort.businessLogic.ExceptionLabelLoader;

import com.ibm.hats.transform.components.Component;
import com.ibm.hats.transform.elements.ComponentElement;
import com.ibm.hats.transform.regions.BlockScreenRegion;
import com.ibm.hats.transform.regions.LinearScreenRegion;
import com.ibm.hsr.screen.HsrScreen;
import com.ibm.hats.transform.components.FieldComponent;
import com.ibm.hats.transform.components.InputComponent;
import com.ibm.hats.transform.elements.InputComponentElement;
import com.ibm.hats.common.TextReplacementPair;
import com.ibm.hats.transform.components.TextComponent;
import com.ibm.hats.transform.elements.TextComponentElement;

public class CustomField extends FieldComponent {
	
	//static Vector exceptionLabels = ExceptionLabelLoader.getExceptionLabels();
	public CustomField(HsrScreen arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	
	public ComponentElement[] recognize(BlockScreenRegion bsr, Properties props) {
		// System.out.println("Size of vector: " + exceptionLabels.size());
		
		ComponentElement[] elements = super.recognize(bsr, props);
		Vector twoCharacterTextReplacements = new Vector();
		Vector threeCharacterTextReplacements = new Vector();
		Vector moreThanThreeCharacterTextReplacements = new Vector();
		
		if(elements != null)
		{
			for(int i = 0; i < elements.length; i++) {
				
				String originalText = elements[i].getPreviewText();
				
				// System.out.println("Before replacement: " + elements[i].getPreviewText());
				String text = elements[i].getPreviewText().toLowerCase();
				
				String text2 = "";
				
				
				String[] textWords = text.split("[\\s-/=\\.]");
				
				for(int j = 0; j < textWords.length; j++)
				{
					
					boolean isExceptionLabel = false;
					String word = textWords[j].trim();
					if(elements[i].getConsumedRegion().startRow == 1 && elements[i].getConsumedRegion().startCol == 1) {
						// System.out.println("Inside if");
						// System.out.println(textWords[i]);
						if(j==1) {
							
							continue;
						}
					}
					//for(int k=0; k<exceptionLabels.size(); k++) {
						//if(word.equalsIgnoreCase((String)exceptionLabels.get(k)))
						//{
						//	isExceptionLabel = true;
						//}
					//}
					//if(isExceptionLabel) {
						//continue;
					//}
					if(word.length() > 1)
					{	
						
						if(word.trim().charAt(0) >= 97 && word.trim().charAt(0) <= 122)
						{
							
							if(word.length() == 2) {
								
								if(word.equalsIgnoreCase("of")) {
									// System.out.println("Found an if...");
									String firstCharacter = word.substring(0, 1).toUpperCase();
									String remainingCharacters = word.substring(1, textWords[j].length());
									TextReplacementPair textReplacementPair = new TextReplacementPair(word + " ", firstCharacter + remainingCharacters + " ", null, false);
									twoCharacterTextReplacements.add(textReplacementPair);
								}
								else {
									String firstCharacter = word.substring(0, 1).toUpperCase();
									String remainingCharacters = word.substring(1, textWords[j].length());
									TextReplacementPair textReplacementPair = new TextReplacementPair(word, firstCharacter + remainingCharacters, null, false);
									twoCharacterTextReplacements.add(textReplacementPair);
								}
								
							} else if(word.length() == 3) {
								
								String firstCharacter = word.substring(0, 1).toUpperCase();
								String remainingCharacters = word.substring(1, textWords[j].length());
								TextReplacementPair textReplacementPair = new TextReplacementPair(word, firstCharacter + remainingCharacters, null, false);
								threeCharacterTextReplacements.add(textReplacementPair);
							} else if(word.length() > 3) {
								
								String firstCharacter = word.substring(0, 1).toUpperCase();
								String remainingCharacters = word.substring(1, textWords[j].length());
								TextReplacementPair textReplacementPair = new TextReplacementPair(word, firstCharacter + remainingCharacters, null, false);
								moreThanThreeCharacterTextReplacements.add(textReplacementPair);
							}
							
						}
					}
				}
				// Replace 2 characters words first
				for(int idx1=0; idx1<twoCharacterTextReplacements.size(); idx1++) {
						elements[i].doTextReplacement((TextReplacementPair)twoCharacterTextReplacements.get(idx1), this.contextAttributes);
				}
				// Then 3 characters
				for(int idx1=0; idx1<threeCharacterTextReplacements.size(); idx1++) {
					elements[i].doTextReplacement((TextReplacementPair)threeCharacterTextReplacements.get(idx1), this.contextAttributes);
				}
				// And then more than three
				for(int idx1=0; idx1<moreThanThreeCharacterTextReplacements.size(); idx1++) {
					elements[i].doTextReplacement((TextReplacementPair)moreThanThreeCharacterTextReplacements.get(idx1), this.contextAttributes);
				}				
			}
			// System.out.println();
		}
		// System.out.println("End time: " + new java.util.Date());
		return elements;
	}

	public ComponentElement[] recognize(LinearScreenRegion arg0, Properties arg1) {
		// TODO Auto-generated method stub
		return null;
	}

}
